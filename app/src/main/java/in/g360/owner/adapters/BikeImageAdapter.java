package in.g360.owner.adapters;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.recyclerview.widget.RecyclerView;

import java.io.File;
import java.util.ArrayList;

import in.g360.owner.activities.ImageActivity;
import in.g360.owner.holders.BikeImageHolder;
import in.g360.owner.models.BikeImagesModel;

public class BikeImageAdapter extends RecyclerView.Adapter<BikeImageHolder> {

    private ArrayList<BikeImagesModel> bikeImagesModels;
    Activity context;
    LayoutInflater li;
    int resource;
    private int lastPosition = -1;

    public BikeImageAdapter(ArrayList<BikeImagesModel> bikeImagesModels, ImageActivity context, int resource) {
        this.bikeImagesModels = bikeImagesModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public BikeImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutSeeds = li.inflate(resource, parent,false);
        BikeImageHolder bikeImageHolder = new BikeImageHolder(layoutSeeds);
        return bikeImageHolder;
    }

    @Override
    public void onBindViewHolder(BikeImageHolder holder, final int position) {

        setAnimation(holder.itemView, position);

        holder.bike_img.setImageURI(Uri.fromFile(new File(bikeImagesModels.get(position).getBikeImage())));

        holder.setItemClickListener((v, pos) -> {

        });

    }

    @Override
    public int getItemCount() {

        return this.bikeImagesModels.size();

    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
