package in.g360.owner.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import in.g360.owner.R;
import in.g360.owner.interfaces.JobCardItemClickListener;
import in.g360.owner.models.JobCardModel;

public class JobCardAdapter extends RecyclerView.Adapter<JobCardAdapter.JobCardHolder> {

    public List<JobCardModel> jobCardModelList;
    Activity context;
    private LayoutInflater li;
    private int resource;
    private int lastPosition = -1;
    Typeface regular, bold;

    public JobCardAdapter(List<JobCardModel> jobCardModelList, Activity context, int resource) {
        this.jobCardModelList = jobCardModelList;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public JobCardHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_jobcard, viewGroup, false);
        return new JobCardHolder(view);
    }

    @Override
    public void onBindViewHolder(JobCardHolder jobCardHolder, final int position) {

        setAnimation(jobCardHolder.itemView, position);
        jobCardHolder.bind(jobCardModelList.get(position));

    }

    @Override
    public int getItemCount() {
        return this.jobCardModelList.size();
    }


    public class JobCardHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView service_img;
        public TextView service_name_txt, state_txt;
        JobCardItemClickListener jobCardItemClickListener;

        JobCardHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            service_img = itemView.findViewById(R.id.service_img);
            service_name_txt = itemView.findViewById(R.id.service_name_txt);
            state_txt = itemView.findViewById(R.id.state_txt);

        }

        void bind(final JobCardModel jobCardModel) {

            regular = Typeface.createFromAsset(context.getAssets(), "proxima_nova_regular.otf");
            bold = Typeface.createFromAsset(context.getAssets(), "proxima_nova_bold.otf");

            String service = jobCardModel.getServiceItem();
            String status = jobCardModel.getStatus();

            service_name_txt.setTypeface(regular);
            state_txt.setTypeface(regular);

            service_name_txt.setText(service);

            if (status.equalsIgnoreCase("0")) {
                state_txt.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                state_txt.setText("Pending");
            }

            if (status.equalsIgnoreCase("1")) {
                state_txt.setTextColor(ContextCompat.getColor(context, R.color.green));
                state_txt.setText("Done");
            }

            if (status.equalsIgnoreCase("2")) {
                state_txt.setTextColor(ContextCompat.getColor(context, R.color.red));
                state_txt.setText("Rejected");
            }

            itemView.setOnClickListener(view -> {

            });
        }

        @Override
        public void onClick(View view) {

            this.jobCardItemClickListener.onItemClick(view, getLayoutPosition());
        }
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}