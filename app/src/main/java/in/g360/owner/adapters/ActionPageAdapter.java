package in.g360.owner.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import in.g360.owner.R;
import in.g360.owner.activities.ActionPageActivity;
import in.g360.owner.interfaces.ActionPageItemClickListener;
import in.g360.owner.models.BookingAddonServicesModel;

public class ActionPageAdapter extends RecyclerView.Adapter<ActionPageAdapter.ActionPageHolder> {

    public ArrayList<BookingAddonServicesModel> bookingAddonServicesModels;
    ActionPageActivity context;
    private LayoutInflater li;
    private int resource;
    private int lastPosition = -1;
    Typeface regular, bold;

    public ActionPageAdapter(ArrayList<BookingAddonServicesModel> bookingAddonServicesModels, ActionPageActivity context, int resource) {
        this.bookingAddonServicesModels = bookingAddonServicesModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ActionPageHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_action_page, viewGroup, false);
        return new ActionPageHolder(view);
    }

    @Override
    public void onBindViewHolder(ActionPageHolder historyHolder, final int position) {

        setAnimation(historyHolder.itemView, position);
        historyHolder.bind(bookingAddonServicesModels.get(position));

    }

    @Override
    public int getItemCount() {
        return this.bookingAddonServicesModels.size();
    }

    public class ActionPageHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView service_img;
        TextView service_name_txt, price_txt;
        ImageView accept_img,reject_img;
        ActionPageItemClickListener actionPageItemClickListener;

        ActionPageHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            service_img = itemView.findViewById(R.id.service_img);
            service_name_txt = itemView.findViewById(R.id.service_name_txt);
            price_txt = itemView.findViewById(R.id.price_txt);
            accept_img = itemView.findViewById(R.id.accept_img);
            reject_img = itemView.findViewById(R.id.reject_img);

        }

        void bind(final BookingAddonServicesModel bookingAddonServicesModel) {

            regular = Typeface.createFromAsset(context.getAssets(), "proxima_nova_regular.otf");
            bold = Typeface.createFromAsset(context.getAssets(), "proxima_nova_bold.otf");

            service_name_txt.setTypeface(regular);
            price_txt.setTypeface(bold);

            service_name_txt.setText(bookingAddonServicesModel.getServiceName());
            price_txt.setText("\u20B9 " + bookingAddonServicesModel.getCost());

            accept_img.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void onClick(View view) {

                    reject_img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_cancel));
                    accept_img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_select));

                    bookingAddonServicesModel.setStatus("1");
                    context.getTotalPrice();
                }
            });

            reject_img.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void onClick(View view) {

                    accept_img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_deselect));
                    reject_img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_reject));

                    bookingAddonServicesModel.setStatus("2");
                    context.getTotalPrice();

                }
            });

            itemView.setOnClickListener(view -> {

            });
        }

        @Override
        public void onClick(View view) {

            this.actionPageItemClickListener.onItemClick(view, getLayoutPosition());
        }
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
