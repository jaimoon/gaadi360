package in.g360.owner.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;

import in.g360.owner.R;
import in.g360.owner.activities.WalletActivity;
import in.g360.owner.filters.WalletHistorySearch;
import in.g360.owner.interfaces.WalletHistoryItemClickListener;
import in.g360.owner.models.WalletHistoryModel;

public class WalletHistoryAdapter extends RecyclerView.Adapter<WalletHistoryAdapter.WalletHistoryHolder> implements Filterable {

    private FirebaseAnalytics mFirebaseAnalytics;
    public ArrayList<WalletHistoryModel> walletHistoryModels, filterList;
    WalletActivity context;
    WalletHistorySearch filter;
    private LayoutInflater li;
    private int resource;
    private int lastPosition = -1;
    Typeface regular, bold;

    public WalletHistoryAdapter(ArrayList<WalletHistoryModel> walletHistoryModels, WalletActivity context, int resource) {
        this.walletHistoryModels = walletHistoryModels;
        this.filterList = walletHistoryModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public WalletHistoryHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_wallet_history, viewGroup, false);
        return new WalletHistoryHolder(view);
    }

    @Override
    public void onBindViewHolder(WalletHistoryHolder historyHolder, final int position) {

        setAnimation(historyHolder.itemView, position);
        historyHolder.bind(walletHistoryModels.get(position));

    }

    @Override
    public int getItemCount() {
        return this.walletHistoryModels.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new WalletHistorySearch(filterList, this);
        }

        return filter;
    }

    public class WalletHistoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView reference_txt, refered_txt, amount_txt;
        String reference, refered, amount;
        WalletHistoryItemClickListener walletHistoryItemClickListener;

        WalletHistoryHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);

            reference_txt = itemView.findViewById(R.id.reference_txt);
            refered_txt = itemView.findViewById(R.id.refered_txt);
            amount_txt = itemView.findViewById(R.id.amount_txt);

        }

        void bind(final WalletHistoryModel walletHistoryModel) {

            regular = Typeface.createFromAsset(context.getAssets(), "proxima_nova_regular.otf");
            bold = Typeface.createFromAsset(context.getAssets(), "proxima_nova_bold.otf");

            reference = walletHistoryModel.getReference();
            refered = walletHistoryModel.getReferred();
            amount = walletHistoryModel.getAmount();

            reference_txt.setTypeface(bold);
            refered_txt.setTypeface(bold);
            amount_txt.setTypeface(bold);

            if (reference.equalsIgnoreCase("null")){

                reference_txt.setText("--");

            }else {
                reference_txt.setText(reference);
            }
            if (refered.equalsIgnoreCase("null")){

                refered_txt.setText("--");

            }else {
                refered_txt.setText(refered);
            }

            if (walletHistoryModel.getType().equalsIgnoreCase("1")){

                amount_txt.setTextColor(ContextCompat.getColor(context, R.color.green));

            }else if (walletHistoryModel.getType().equalsIgnoreCase("2")){

                amount_txt.setTextColor(ContextCompat.getColor(context, R.color.red));

            }

            amount_txt.setText("\u20B9 " + amount);

            itemView.setOnClickListener(view -> {

            });

        }

        @Override
        public void onClick(View view) {

            this.walletHistoryItemClickListener.onItemClick(view, getLayoutPosition());
        }
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
