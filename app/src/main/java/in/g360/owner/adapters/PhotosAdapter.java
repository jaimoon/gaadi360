package in.g360.owner.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import in.g360.owner.R;
import in.g360.owner.activities.HistoryDetailActivity;
import in.g360.owner.interfaces.PhotosItemClickListener;

public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.PhotosHolder> {

    public List<String> mylist;
    Activity context;
    private LayoutInflater li;
    private int resource;
    private int lastPosition = -1;
    Typeface regular, bold;

    AlertDialog dialog;

    private static final String TAG = "Touch";
    private static final float MIN_ZOOM = 1f, MAX_ZOOM = 1f;
    Matrix matrix = new Matrix();
    Matrix savedMatrix = new Matrix();
    static final int NONE = 0;
    static final int DRAG = 1;
    static final int ZOOM = 2;
    int mode = NONE;
    PointF start = new PointF();
    PointF mid = new PointF();
    float oldDist = 1f;

    public PhotosAdapter(List<String> mylist, Activity context, int resource) {
        this.mylist = mylist;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public PhotosHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_photos, viewGroup, false);
        return new PhotosHolder(view);
    }

    @Override
    public void onBindViewHolder(PhotosHolder historyHolder, final int position) {

        setAnimation(historyHolder.itemView, position);
        historyHolder.bind(mylist.get(position));

    }

    @Override
    public int getItemCount() {
        return this.mylist.size();
    }


    public class PhotosHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView bike_photo_img;
        PhotosItemClickListener photosItemClickListener;

        PhotosHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            bike_photo_img = itemView.findViewById(R.id.bike_photo_img);


        }

        void bind(final String mylist) {

            regular = Typeface.createFromAsset(context.getAssets(), "proxima_nova_regular.otf");
            bold = Typeface.createFromAsset(context.getAssets(), "proxima_nova_bold.otf");

            Picasso.with(context)
                    .load(mylist)
                    .placeholder(R.drawable.app_icon)
                    .fit()
                    .into(bike_photo_img);

            bike_photo_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });

            bike_photo_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    zoomDialog(mylist);

                }
            });

            itemView.setOnClickListener(view -> {

            });
        }

        @Override
        public void onClick(View view) {

            this.photosItemClickListener.onItemClick(view, getLayoutPosition());
        }
    }

    private void zoomDialog(String mylist) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        LayoutInflater inflater = context.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.zoom_image_dialog, null);

        ImageView close_img = dialog_layout.findViewById(R.id.close_img);
        close_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        ImageView bike_img = dialog_layout.findViewById(R.id.bike_img);

        Picasso.with(context)
                .load(mylist)
                .placeholder(R.drawable.app_icon)
                .fit()
                .into(bike_img);

        bike_img.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                ImageView view = (ImageView) v;
                view.setScaleType(ImageView.ScaleType.MATRIX);
                float scale;

                dumpEvent(event);

                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        savedMatrix.set(matrix);
                        start.set(event.getX(), event.getY());
                        mode = DRAG;
                        break;

                    case MotionEvent.ACTION_UP:

                    case MotionEvent.ACTION_POINTER_UP:

                        mode = NONE;
                        Log.d(TAG, "mode=NONE");
                        break;

                    case MotionEvent.ACTION_POINTER_DOWN:

                        oldDist = spacing(event);
                        Log.d(TAG, "oldDist=" + oldDist);
                        if (oldDist > 5f) {
                            savedMatrix.set(matrix);
                            midPoint(mid, event);
                            mode = ZOOM;
                            Log.d(TAG, "mode=ZOOM");
                        }
                        break;

                    case MotionEvent.ACTION_MOVE:

                        if (mode == DRAG) {
                            matrix.set(savedMatrix);
                            matrix.postTranslate(event.getX() - start.x, event.getY() - start.y);
                        } else if (mode == ZOOM) {
                            float newDist = spacing(event);
                            Log.d(TAG, "newDist=" + newDist);
                            if (newDist > 5f) {
                                matrix.set(savedMatrix);
                                scale = newDist / oldDist;
                                matrix.postScale(scale, scale, mid.x, mid.y);
                            }
                        }
                        break;
                }

                view.setImageMatrix(matrix);
                return true;

            }
        });

        builder.setView(dialog_layout);
        dialog = builder.create();
        dialog.setCancelable(true);
        dialog.show();
    }

    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float) Math.sqrt(x * x + y * y);
    }

    private void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }

    private void dumpEvent(MotionEvent event) {
        String names[] = {"DOWN", "UP", "MOVE", "CANCEL", "OUTSIDE", "POINTER_DOWN", "POINTER_UP", "7?", "8?", "9?"};
        StringBuilder sb = new StringBuilder();
        int action = event.getAction();
        int actionCode = action & MotionEvent.ACTION_MASK;
        sb.append("event ACTION_").append(names[actionCode]);

        if (actionCode == MotionEvent.ACTION_POINTER_DOWN || actionCode == MotionEvent.ACTION_POINTER_UP) {
            sb.append("(pid ").append(action >> MotionEvent.ACTION_POINTER_ID_SHIFT);
            sb.append(")");
        }

        sb.append("[");
        for (int i = 0; i < event.getPointerCount(); i++) {
            sb.append("#").append(i);
            sb.append("(pid ").append(event.getPointerId(i));
            sb.append(")=").append((int) event.getX(i));
            sb.append(",").append((int) event.getY(i));
            if (i + 1 < event.getPointerCount())
                sb.append(";");
        }

        sb.append("]");
        Log.d("Touch Events ---------", sb.toString());
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
