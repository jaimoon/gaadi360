package in.g360.owner.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import in.g360.owner.MainActivity;
import in.g360.owner.R;
import in.g360.owner.filters.BikeModelSearch;
import in.g360.owner.interfaces.BikeModelItemClickListener;
import in.g360.owner.models.BikeModelModel;

public class BikeModelAdapter extends RecyclerView.Adapter<BikeModelAdapter.BikeModelHolder> implements Filterable {

    public ArrayList<BikeModelModel> bikeModelModels, filterList;
    public MainActivity context;
    BikeModelSearch filter;
    LayoutInflater li;
    int resource;
    Typeface regular, bold;

    public BikeModelAdapter(ArrayList<BikeModelModel> bikeModelModels, MainActivity context, int resource) {
        this.bikeModelModels = bikeModelModels;
        this.filterList = bikeModelModels;
        this.context = context;
        this.resource = resource;

        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public BikeModelHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, null);
        BikeModelHolder slh = new BikeModelHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final BikeModelHolder holder, final int position) {

        regular = Typeface.createFromAsset(context.getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(context.getAssets(), "proxima_nova_bold.otf");

        holder.bike_model_name_txt.setTypeface(regular);
        holder.bike_model_name_txt.setText(bikeModelModels.get(position).getModelName());

        holder.setItemClickListener((v, pos) ->
                context.setBikeModelName(bikeModelModels.get(pos).getModelName(), bikeModelModels.get(pos).getModelId()));
    }

    @Override
    public int getItemCount() {
        return this.bikeModelModels.size();

    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new BikeModelSearch(filterList, this);
        }

        return filter;
    }

    public class BikeModelHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView bike_model_name_txt;

        BikeModelItemClickListener bikeModelItemClickListener;

        public BikeModelHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            bike_model_name_txt = itemView.findViewById(R.id.bike_model_name_txt);

        }

        @Override
        public void onClick(View view) {

            this.bikeModelItemClickListener.onItemClick(view, getLayoutPosition());
        }

        public void setItemClickListener(BikeModelItemClickListener ic) {
            this.bikeModelItemClickListener = ic;
        }
    }

}
