package in.g360.owner.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import in.g360.owner.MainActivity;
import in.g360.owner.R;
import in.g360.owner.filters.BikeKilometerSearch;
import in.g360.owner.interfaces.BikeKilometerItemClickListener;
import in.g360.owner.models.BikeKilometersModel;


public class BikeKilometerAdapter extends RecyclerView.Adapter<BikeKilometerAdapter.BikeKilometerHolder> implements Filterable {

    public ArrayList<BikeKilometersModel> bikeKilometersModels, filterList;
    public MainActivity context;
    BikeKilometerSearch filter;
    LayoutInflater li;
    int resource;
    Typeface regular, bold;

    public BikeKilometerAdapter(ArrayList<BikeKilometersModel> bikeKilometersModels, MainActivity context, int resource) {
        this.bikeKilometersModels = bikeKilometersModels;
        this.filterList = bikeKilometersModels;
        this.context = context;
        this.resource = resource;

        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public BikeKilometerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, null);
        BikeKilometerHolder slh = new BikeKilometerHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final BikeKilometerHolder holder, final int position) {

        regular = Typeface.createFromAsset(context.getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(context.getAssets(), "proxima_nova_bold.otf");

        holder.bike_kilometer_name_txt.setTypeface(regular);
        holder.bike_kilometer_name_txt.setText(bikeKilometersModels.get(position).getStartRange() + " - " + bikeKilometersModels.get(position).getEndRange());

        holder.setItemClickListener((v, pos) ->
                context.setBikeKilometerName(bikeKilometersModels.get(position).getKilometerRangeId(),bikeKilometersModels.get(position).getStartRange(), bikeKilometersModels.get(position).getEndRange()));
    }

    @Override
    public int getItemCount() {
        return this.bikeKilometersModels.size();

    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new BikeKilometerSearch(filterList, this);
        }

        return filter;
    }

    public class BikeKilometerHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView bike_kilometer_name_txt;

        BikeKilometerItemClickListener bikeKilometerItemClickListener;

        public BikeKilometerHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            bike_kilometer_name_txt = itemView.findViewById(R.id.bike_kilometer_name_txt);

        }

        @Override
        public void onClick(View view) {

            this.bikeKilometerItemClickListener.onItemClick(view, getLayoutPosition());
        }

        public void setItemClickListener(BikeKilometerItemClickListener ic) {
            this.bikeKilometerItemClickListener = ic;
        }
    }

}
