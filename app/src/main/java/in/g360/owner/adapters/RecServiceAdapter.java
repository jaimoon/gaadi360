package in.g360.owner.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.g360.owner.R;
import in.g360.owner.activities.ServicesListActivity;
import in.g360.owner.interfaces.RecServiceItemClickListener;
import in.g360.owner.models.RecServiceModel;

public class RecServiceAdapter extends RecyclerView.Adapter<RecServiceAdapter.RecServiceHolder> {

    private ArrayList<RecServiceModel> recServiceModels;
    ServicesListActivity context;
    private LayoutInflater li;
    private int resource;
    private int lastPosition = -1;
    Typeface regular, bold;
    SharedPreferences servicePreferences;
    String prefServiceId, serviceName;

    public RecServiceAdapter(ArrayList<RecServiceModel> recServiceModels, ServicesListActivity context, int resource) {
        this.recServiceModels = recServiceModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public RecServiceHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_rec_service, viewGroup, false);
        return new RecServiceHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onBindViewHolder(RecServiceHolder recServiceHolder, final int position) {

        setAnimation(recServiceHolder.itemView, position);
        recServiceHolder.bind(recServiceModels.get(position));

    }

    @Override
    public int getItemCount() {
        return this.recServiceModels.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class RecServiceHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView rec_service_img, sel_img, info_img;
        public TextView rec_service_name;

        RecServiceItemClickListener recServiceItemClickListener;

        public RecServiceHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            rec_service_img = itemView.findViewById(R.id.rec_service_img);
            sel_img = itemView.findViewById(R.id.sel_img);
            info_img = itemView.findViewById(R.id.info_img);
            rec_service_name = itemView.findViewById(R.id.rec_service_name);

        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        void bind(final RecServiceModel recServiceModel) {

            regular = Typeface.createFromAsset(context.getAssets(), "proxima_nova_regular.otf");
            bold = Typeface.createFromAsset(context.getAssets(), "proxima_nova_bold.otf");

            Picasso.with(context)
                    .load(recServiceModel.getFileId())
                    .placeholder(R.drawable.ic_bolts)
                    .fit()
                    .into(rec_service_img);

            rec_service_name.setTypeface(regular);
            rec_service_name.setText(recServiceModel.getServiceName());

            info_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    context.infoDialog(recServiceModel.getServiceId());

                }
            });

            /*servicePreferences = PreferenceManager.getDefaultSharedPreferences(context);
            prefServiceId = servicePreferences.getString("serviceId", "");
            serviceName = servicePreferences.getString("serviceName", "");

            Log.d("serviceId", prefServiceId);
            Log.d("getServiceId", recServiceModel.getServiceId());

            prefServiceId = "," + prefServiceId + ",";
            String getId = "," + recServiceModel.getServiceId() + ",";

            if (prefServiceId.contains(getId)) {

                Log.d("CheckId", "TRUE--->" + prefServiceId + "\n" + recServiceModel.getServiceId());

                recServiceModel.setChecked(true);
                sel_img.setVisibility(View.VISIBLE);

                String serviceId = recServiceModel.getServiceId();
                String serviceName = recServiceModel.getServiceName();

                String checked = "1";
                context.getServiceIds(checked, serviceId, serviceName);

            } else {
                Log.d("CheckId", "FALSE--->" + recServiceModel + "\n" + recServiceModel.getServiceId());
            }*/

            itemView.setOnClickListener(view -> {

                recServiceModel.setChecked(!recServiceModel.isChecked());
                sel_img.setVisibility(recServiceModel.isChecked() ? View.VISIBLE : View.GONE);

                String serviceId = recServiceModel.getServiceId();
                String serviceName = recServiceModel.getServiceName();

                if (recServiceModel.isChecked()) {
                    String checked = "1";
                    context.getServiceIds(checked, serviceId, serviceName);
                } else {
                    String checked = "0";
                    context.getServiceIds(checked, serviceId, serviceName);
                }

            });
        }

        @Override
        public void onClick(View view) {

            this.recServiceItemClickListener.onItemClick(view, getLayoutPosition());
        }
    }

    public ArrayList<RecServiceModel> getSelected() {
        ArrayList<RecServiceModel> selected = new ArrayList<>();
        for (int i = 0; i < recServiceModels.size(); i++) {
            if (recServiceModels.get(i).isChecked()) {
                selected.add(recServiceModels.get(i));
                Log.d("ProAddDeal", String.valueOf(recServiceModels.get(i).toString().length()));
            }
        }
        return selected;
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
