package in.g360.owner.adapters;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.recyclerview.widget.RecyclerView;

import java.io.File;
import java.util.ArrayList;

import in.g360.owner.activities.ImageActivity;
import in.g360.owner.holders.BikeVideoHolder;
import in.g360.owner.models.BikeVideosModel;

public class BikeVideoAdapter extends RecyclerView.Adapter<BikeVideoHolder> {

    private ArrayList<BikeVideosModel> bikeVideosModels;
    Activity context;
    LayoutInflater li;
    int resource;
    private int lastPosition = -1;

    public BikeVideoAdapter(ArrayList<BikeVideosModel> bikeVideosModels, ImageActivity context, int resource) {
        this.bikeVideosModels = bikeVideosModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public BikeVideoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutSeeds = li.inflate(resource, parent,false);
        BikeVideoHolder bikeVideoHolder = new BikeVideoHolder(layoutSeeds);
        return bikeVideoHolder;
    }

    @Override
    public void onBindViewHolder(BikeVideoHolder holder, final int position) {

        setAnimation(holder.itemView, position);

        Log.d("VIDEOURI", "onBindViewHolder: "+Uri.fromFile(new File(bikeVideosModels.get(position).getBikeVideo())));

        holder.bike_video.setVideoURI(Uri.fromFile(new File(bikeVideosModels.get(position).getBikeVideo())));
        holder.bike_video.start();

        holder.setItemClickListener((v, pos) -> {

        });

    }

    @Override
    public int getItemCount() {

        return this.bikeVideosModels.size();

    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
