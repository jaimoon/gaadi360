package in.g360.owner.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import in.g360.owner.MainActivity;
import in.g360.owner.filters.BikeBrandSearch;
import in.g360.owner.holders.BikeBrandHolder;
import in.g360.owner.models.BikeBrandModel;

public class BikeBrandAdapter extends RecyclerView.Adapter<BikeBrandHolder> implements Filterable {

    public ArrayList<BikeBrandModel> bikeBrandModels, filterList;
    public MainActivity context;
    BikeBrandSearch filter;
    LayoutInflater li;
    int resource;
    Typeface regular, bold;

    public BikeBrandAdapter(ArrayList<BikeBrandModel> bikeBrandModels, MainActivity context, int resource) {
        this.bikeBrandModels = bikeBrandModels;
        this.filterList = bikeBrandModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public BikeBrandHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, null);
        BikeBrandHolder bbh = new BikeBrandHolder(layout);
        return bbh;
    }

    @Override
    public void onBindViewHolder(final BikeBrandHolder holder, final int position) {

        regular = Typeface.createFromAsset(context.getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(context.getAssets(), "proxima_nova_bold.otf");

        holder.bike_brand_name_txt.setTypeface(regular);
        holder.bike_brand_name_txt.setText(bikeBrandModels.get(position).getBike_brand_name());

        holder.setItemClickListener((v, pos) ->
                context.setBikeBrandName(bikeBrandModels.get(pos).getBike_brand_name(), bikeBrandModels.get(pos).getId()));
    }

    @Override
    public int getItemCount() {
        return this.bikeBrandModels.size();

    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new BikeBrandSearch(filterList, this);
        }

        return filter;
    }
}
