package in.g360.owner.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import in.g360.owner.R;
import in.g360.owner.activities.ActionPageActivity;
import in.g360.owner.activities.ConfirmationDetailActivity;
import in.g360.owner.interfaces.ActionPageItemClickListener;
import in.g360.owner.models.BookingAddonServicesModel;
import in.g360.owner.models.SourceModel;

public class SourceAdapter extends RecyclerView.Adapter<SourceAdapter.SourceHolder> {

    public ArrayList<SourceModel> sourceModels;
    ConfirmationDetailActivity context;
    private LayoutInflater li;
    private int resource;
    private int lastPosition = -1;
    Typeface regular, bold;

    public SourceAdapter(ArrayList<SourceModel> sourceModels, ConfirmationDetailActivity context, int resource) {
        this.sourceModels = sourceModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public SourceHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_source_type, viewGroup, false);
        return new SourceHolder(view);
    }

    @Override
    public void onBindViewHolder(SourceHolder historyHolder, final int position) {

        setAnimation(historyHolder.itemView, position);
        historyHolder.bind(sourceModels.get(position));

    }

    @Override
    public int getItemCount() {
        return this.sourceModels.size();
    }

    public class SourceHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView source_name_txt;
        ActionPageItemClickListener actionPageItemClickListener;

        SourceHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            source_name_txt = itemView.findViewById(R.id.source_name_txt);

        }

        void bind(final SourceModel sourceModel) {

            regular = Typeface.createFromAsset(context.getAssets(), "proxima_nova_regular.otf");
            bold = Typeface.createFromAsset(context.getAssets(), "proxima_nova_bold.otf");

            source_name_txt.setTypeface(regular);
            source_name_txt.setText(sourceModel.getName());

            itemView.setOnClickListener(view -> {
                context.setSourceType(sourceModel.id, sourceModel.getName());
            });
        }

        @Override
        public void onClick(View view) {

            this.actionPageItemClickListener.onItemClick(view, getLayoutPosition());
        }
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
