package in.g360.owner.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.g360.owner.R;
import in.g360.owner.activities.ServicesListActivity;
import in.g360.owner.interfaces.InspectionItemClickListener;
import in.g360.owner.models.InspectionModel;

public class InspectionAdapter extends RecyclerView.Adapter<InspectionAdapter.InspectionHolder> {

    private ArrayList<InspectionModel> inspectionModels;
    ServicesListActivity context;
    private LayoutInflater li;
    private int resource;
    private int lastPosition = -1;
    Typeface regular, bold;
    SharedPreferences inspectionPreferences;
    String inspections, prefInspectionId, inspectionNames;

    public InspectionAdapter(ArrayList<InspectionModel> inspectionModels, ServicesListActivity context, int resource) {
        this.inspectionModels = inspectionModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public InspectionHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_inspection, viewGroup, false);
        return new InspectionHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onBindViewHolder(InspectionHolder inspectionHolder, final int position) {

        setAnimation(inspectionHolder.itemView, position);
        inspectionHolder.bind(inspectionModels.get(position));

    }

    @Override
    public int getItemCount() {
        return this.inspectionModels.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class InspectionHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView inspection_img, sel_img;
        public TextView inspection_name;

        InspectionItemClickListener inspectionItemClickListener;


        public InspectionHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            inspection_img = itemView.findViewById(R.id.inspection_img);
            sel_img = itemView.findViewById(R.id.sel_img);
            inspection_name = itemView.findViewById(R.id.inspection_name);
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        void bind(final InspectionModel inspectionModel) {

            regular = Typeface.createFromAsset(context.getAssets(), "proxima_nova_regular.otf");
            bold = Typeface.createFromAsset(context.getAssets(), "proxima_nova_bold.otf");

            Picasso.with(context)
                    .load(inspectionModel.getFileId())
                    .placeholder(R.drawable.ic_bolts)
                    .fit()
                    .into(inspection_img);

            inspection_name.setTypeface(regular);
            inspection_name.setText(inspectionModel.getInspectionName());

            /*inspectionPreferences = PreferenceManager.getDefaultSharedPreferences(context);

            inspections = inspectionPreferences.getString("inspections", "");
            prefInspectionId = inspectionPreferences.getString("inspectionId", "");
            inspectionNames = inspectionPreferences.getString("inspectionNames", "");

            Log.d("inspectionId", prefInspectionId);
            Log.d("getInspectionId", inspectionModel.getInspectionId());

            prefInspectionId = "," + prefInspectionId + ",";
            String getId = "," + inspectionModel.getInspectionId() + ",";

            if (prefInspectionId.contains(getId)) {

                Log.d("CheckId", "TRUE--->" + prefInspectionId + "\n" + inspectionModel.getInspectionId());

                inspectionModel.setChecked(true);
                sel_img.setVisibility(View.VISIBLE);

                String inspectionId = inspectionModel.getInspectionId();
                String inspectionName = inspectionModel.getInspectionName();

                String checked = "1";
                context.getInspectionIds(checked, inspectionId, inspectionName);

            } else {
                inspectionModel.setChecked(false);
                sel_img.setVisibility(View.GONE);
            }*/

            itemView.setOnClickListener(view -> {
                inspectionModel.setChecked(!inspectionModel.isChecked());
                sel_img.setVisibility(inspectionModel.isChecked() ? View.VISIBLE : View.GONE);

                String inspectionId = inspectionModel.getInspectionId();
                String inspectionName = inspectionModel.getInspectionName();

                if (inspectionModel.isChecked()) {
                    String checked = "1";
                    context.getInspectionIds(checked, inspectionId, inspectionName);
                } else {
                    String checked = "0";
                    context.getInspectionIds(checked, inspectionId, inspectionName);
                }

            });
        }

        @Override
        public void onClick(View view) {

            this.inspectionItemClickListener.onItemClick(view, getLayoutPosition());
        }
    }

    public ArrayList<InspectionModel> getSelected() {
        ArrayList<InspectionModel> selected = new ArrayList<>();
        for (int i = 0; i < inspectionModels.size(); i++) {
            if (inspectionModels.get(i).isChecked()) {
                selected.add(inspectionModels.get(i));
                Log.d("ProAddDeal", "" + inspectionModels.get(i).toString().length());
            }
        }
        return selected;
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
