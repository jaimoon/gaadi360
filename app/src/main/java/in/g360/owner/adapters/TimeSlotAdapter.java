package in.g360.owner.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import in.g360.owner.MainActivity;
import in.g360.owner.R;
import in.g360.owner.interfaces.TimeSlotItemClickListener;
import in.g360.owner.models.TimeSlotsModel;

public class TimeSlotAdapter extends RecyclerView.Adapter<TimeSlotAdapter.TimeSlotHolder> {

    public ArrayList<TimeSlotsModel> timeSlotsModels;
    public MainActivity context;
    LayoutInflater li;
    int resource;
    int selectedPosition;
    String startTime, endTime,displayTime;
    Typeface regular, bold;

    public TimeSlotAdapter(ArrayList<TimeSlotsModel> timeSlotsModels, MainActivity context, int resource) {
        this.timeSlotsModels = timeSlotsModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public TimeSlotHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, null);
        TimeSlotHolder slh = new TimeSlotHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final TimeSlotHolder holder, final int position) {

        startTime = timeSlotsModels.get(position).getPickupStartTime();
        endTime = timeSlotsModels.get(position).getPickupEndTime();
        displayTime = timeSlotsModels.get(position).getDisplayName();

        regular = Typeface.createFromAsset(context.getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(context.getAssets(), "proxima_nova_bold.otf");

        holder.timeSlot_txt.setTypeface(regular);
        holder.timeSlot_txt.setText(displayTime);

        if (selectedPosition == position) {
            holder.timeSlot_txt.setBackgroundResource(R.drawable.rounded_orange_border);
            holder.timeSlot_txt.setTextColor(Color.WHITE);

        } else {
            holder.timeSlot_txt.setBackgroundResource(R.drawable.rounded_layout);
            holder.timeSlot_txt.setTextColor(Color.BLACK);

        }

        holder.setItemClickListener(new TimeSlotItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {

                selectedPosition = position;
                context.setTimeSlot(timeSlotsModels.get(selectedPosition).getPickupStartTime(),
                        timeSlotsModels.get(selectedPosition).getPickupEndTime(),timeSlotsModels.get(selectedPosition).getDisplayName());
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.timeSlotsModels.size();

    }

    public class TimeSlotHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView timeSlot_txt;

        TimeSlotItemClickListener timeSlotItemClickListener;

        public TimeSlotHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            timeSlot_txt = itemView.findViewById(R.id.timeSlot_txt);

        }

        @Override
        public void onClick(View view) {

            this.timeSlotItemClickListener.onItemClick(view, getLayoutPosition());
        }

        public void setItemClickListener(TimeSlotItemClickListener ic) {
            this.timeSlotItemClickListener = ic;
        }
    }

}
