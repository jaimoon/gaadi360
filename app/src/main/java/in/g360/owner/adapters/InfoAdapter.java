package in.g360.owner.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import in.g360.owner.activities.ServicesListActivity;
import in.g360.owner.filters.InfoSearch;
import in.g360.owner.holders.InfoHolder;
import in.g360.owner.interfaces.InfoItemClickListener;
import in.g360.owner.models.ServiceDetailModel;

public class InfoAdapter extends RecyclerView.Adapter<InfoHolder> implements Filterable {

    String serviceId;
    public ArrayList<ServiceDetailModel> serviceDetailModels, filterList;
    ServicesListActivity context;
    InfoSearch filter;
    LayoutInflater li;
    int resource;
    Typeface regular, bold;

    public InfoAdapter(String serviceId, ArrayList<ServiceDetailModel> serviceDetailModels, ServicesListActivity context, int resource) {
        this.serviceId = serviceId;
        this.serviceDetailModels = serviceDetailModels;
        this.filterList = serviceDetailModels;
        this.context = context;
        this.resource = resource;

        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public InfoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, null);
        InfoHolder bbh = new InfoHolder(layout);
        return bbh;
    }

    @Override
    public void onBindViewHolder(final InfoHolder holder, final int position) {

        regular = Typeface.createFromAsset(context.getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(context.getAssets(), "proxima_nova_bold.otf");

        Log.d("MainServiceId",serviceId);

        String serviceItem = null;

        if (serviceDetailModels.get(position).getServiceId().equalsIgnoreCase(serviceId)){

            Log.d("Required",serviceDetailModels.get(position).getServiceItem());

            serviceItem = serviceDetailModels.get(position).getServiceItem();

        }

        holder.info_txt.setTypeface(regular);
        holder.info_txt.setText(serviceItem);

        if (holder.info_txt.getText().toString().equalsIgnoreCase("")){
            holder.info_txt.setVisibility(View.GONE);
        }


        holder.setItemClickListener(new InfoItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return this.serviceDetailModels.size();

    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new InfoSearch(filterList, this);
        }

        return filter;
    }
}
