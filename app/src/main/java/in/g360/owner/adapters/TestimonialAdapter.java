package in.g360.owner.adapters;

import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import in.g360.owner.MainActivity;
import in.g360.owner.R;
import in.g360.owner.models.FeaturedModel;

public class TestimonialAdapter extends RecyclerView.Adapter<TestimonialAdapter.FeaturedHolder> {

    List<FeaturedModel> data;
    MainActivity context;
    Typeface regular, bold;

    public TestimonialAdapter(MainActivity context, List<FeaturedModel> data) {
        this.context = context;
        this.data = data;
    }

    public static class FeaturedHolder extends RecyclerView.ViewHolder {

        public ImageView imageView;
        public TextView testimonial_title,testimonial_txt;

        public FeaturedHolder(View v) {
            super(v);

            imageView = v.findViewById(R.id.feature_img);
            testimonial_title = v.findViewById(R.id.testimonial_title);
            testimonial_txt = v.findViewById(R.id.testimonial_txt);

        }
    }

    @Override
    public TestimonialAdapter.FeaturedHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_testimonial, parent, false);
        FeaturedHolder vh = new FeaturedHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final FeaturedHolder holder, int position) {

        regular = Typeface.createFromAsset(context.getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(context.getAssets(), "proxima_nova_bold.otf");

        FeaturedModel all = data.get(position);
        holder.imageView.setImageResource(all.id);
        holder.testimonial_title.setTypeface(bold);
        holder.testimonial_title.setText(all.name);
        holder.testimonial_txt.setTypeface(regular);
        holder.testimonial_txt.setText(all.msg);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

}
