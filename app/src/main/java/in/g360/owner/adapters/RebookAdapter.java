package in.g360.owner.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import in.g360.owner.MainActivity;
import in.g360.owner.R;
import in.g360.owner.interfaces.RebookItemClickListener;
import in.g360.owner.models.RebookModel;

public class RebookAdapter extends RecyclerView.Adapter<RebookAdapter.RebookHolder> {

    private ArrayList<RebookModel> rebookModels;
    MainActivity context;
    private LayoutInflater li;
    private int resource;
    private int lastPosition = -1;
    Date prevDate,curdate = new Date();

    public RebookAdapter(ArrayList<RebookModel> rebookModels, MainActivity context, int resource) {
        this.rebookModels = rebookModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public RebookHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_rebook, viewGroup, false);
        return new RebookHolder(view);
    }

    @Override
    public void onBindViewHolder(RebookHolder rebookHolder, final int position) {

        setAnimation(rebookHolder.itemView, position);
        rebookHolder.bind(rebookModels.get(position));

    }

    @Override
    public int getItemCount() {
        return this.rebookModels.size();
    }

    public class RebookHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView bike_img;
        public TextView brand_txt, services_txt, price_txt, last_service_date_txt,rebook_btn;
        RebookItemClickListener rebookItemClickListener;
        String brandId, modelId, brandName, modelName;
        Typeface regular, bold;


        public RebookHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            regular = Typeface.createFromAsset(context.getAssets(), "proxima_nova_regular.otf");
            bold = Typeface.createFromAsset(context.getAssets(), "proxima_nova_bold.otf");

            bike_img = itemView.findViewById(R.id.bike_img);
            rebook_btn = itemView.findViewById(R.id.rebook_btn);
            brand_txt = itemView.findViewById(R.id.brand_txt);
            services_txt = itemView.findViewById(R.id.services_txt);
            price_txt = itemView.findViewById(R.id.price_txt);
            last_service_date_txt = itemView.findViewById(R.id.last_service_date_txt);

        }

        void bind(final RebookModel rebookModel) {

            brandId = rebookModel.getBrandId();
            modelId = rebookModel.getModelId();
            brandName = rebookModel.getBrandName();
            modelName = rebookModel.getModelName();

            brand_txt.setTypeface(regular);
            brand_txt.setText(brandName + "\n" + modelName);
            String services = rebookModel.getServices();
            String finalString = services.replace(",", "\n");
            services_txt.setTypeface(regular);
            services_txt.setText(finalString);
            price_txt.setTypeface(bold);
            price_txt.setText("\u20B9 " + rebookModel.getFinalPrice());

            //last_service_date_txt.setText(rebookModel.getBookingDate() + " " + rebookModel.getBookingTime());

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            try {
                prevDate = format.parse(rebookModel.getBookingDate());
                System.out.println(prevDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            long diff = curdate.getTime() - prevDate.getTime();
            String days = String.valueOf(TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            Log.d("DIFFFF", days);

            if (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) > 90){

                last_service_date_txt.setTypeface(regular);

                Animation anim = new AlphaAnimation(0.2f, 1.0f);
                anim.setDuration(700);
                anim.setStartOffset(30);
                anim.setRepeatMode(Animation.REVERSE);
                anim.setRepeatCount(Animation.INFINITE);
                last_service_date_txt.startAnimation(anim);

                last_service_date_txt.setTextColor(Color.RED);
                last_service_date_txt.setText("Last Service "+days+" Days Ago");
            }else {
                last_service_date_txt.setTypeface(regular);
                last_service_date_txt.setText("Last Service "+days+" Days Ago");
            }

            rebook_btn.setTypeface(bold);
            rebook_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    context.getRebookData(brandId, brandName, modelId, modelName);

                }
            });

            itemView.setOnClickListener(view -> {

            });
        }

        @Override
        public void onClick(View view) {

            this.rebookItemClickListener.onItemClick(view, getLayoutPosition());
        }
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
