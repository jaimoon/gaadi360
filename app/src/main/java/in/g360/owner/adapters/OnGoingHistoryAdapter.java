package in.g360.owner.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.g360.owner.MainActivity;
import in.g360.owner.R;
import in.g360.owner.activities.ActionPageActivity;
import in.g360.owner.activities.FinalPaymentActivity;
import in.g360.owner.activities.HistoryDetailActivity;
import in.g360.owner.activities.IFCActivity;
import in.g360.owner.filters.HistorySearch;
import in.g360.owner.interfaces.HistoryItemClickListener;
import in.g360.owner.models.HistoryModel;
import in.g360.owner.utils.AppUrls;

public class OnGoingHistoryAdapter extends RecyclerView.Adapter<OnGoingHistoryAdapter.HistoryHolder> {

    private FirebaseAnalytics mFirebaseAnalytics;
    public ArrayList<HistoryModel> historyModels, filterList;
    MainActivity context;
    private LayoutInflater li;
    private int resource;
    private int lastPosition = -1;
    Typeface regular, bold;

    public OnGoingHistoryAdapter(ArrayList<HistoryModel> historyModels, MainActivity context, int resource) {
        this.historyModels = historyModels;
        this.filterList = historyModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public HistoryHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_ongoing_history, viewGroup, false);
        return new HistoryHolder(view);
    }

    @Override
    public void onBindViewHolder(HistoryHolder historyHolder, final int position) {

        setAnimation(historyHolder.itemView, position);
        historyHolder.bind(historyModels.get(position));

    }

    @Override
    public int getItemCount() {
        return this.historyModels.size();
    }


    public class HistoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView bike_img, status_img;
        TextView status_txt, sst_name_txt, brand_txt, services_txt, price_txt;
        HistoryItemClickListener historyItemClickListener;
        String bookingId, bookingTime, bookingDate, status, brandId, modelId, brandName, modelName, sstName, sstAddress;


        HistoryHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);

            bike_img = itemView.findViewById(R.id.bike_img);
            status_img = itemView.findViewById(R.id.status_img);
            status_txt = itemView.findViewById(R.id.status_txt);
            sst_name_txt = itemView.findViewById(R.id.sst_name_txt);
            brand_txt = itemView.findViewById(R.id.brand_txt);
            services_txt = itemView.findViewById(R.id.services_txt);
            price_txt = itemView.findViewById(R.id.price_txt);

        }

        void bind(final HistoryModel historyModel) {

            regular = Typeface.createFromAsset(context.getAssets(), "proxima_nova_regular.otf");
            bold = Typeface.createFromAsset(context.getAssets(), "proxima_nova_bold.otf");

            brandId = historyModel.getBrandId();
            modelId = historyModel.getBrandId();
            brandName = historyModel.getBrandName();
            modelName = historyModel.getModelName();
            sstName = historyModel.getServiceCenterName();
            sstAddress = historyModel.getPickupAddress();

            bookingId = historyModel.getBookingId();
            bookingTime = historyModel.getBookingTime();
            bookingDate = historyModel.getBookingDate();
            status = historyModel.getStatus();

            SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
            Date d = null;
            try {
                d = df.parse(bookingTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar cal = Calendar.getInstance();
            cal.setTime(d);
            cal.add(Calendar.HOUR, 5);
            cal.add(Calendar.MINUTE, 30);
            String newTime = df.format(cal.getTime());

            try {
                SimpleDateFormat dateFormatter = new SimpleDateFormat("HH:mm:ss");
                Date date = dateFormatter.parse(newTime);
                SimpleDateFormat timeFormatter = new SimpleDateFormat("h:mm a");
                //SimpleDateFormat timeFormatter = new SimpleDateFormat("h a");
                newTime = timeFormatter.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            sst_name_txt.setTypeface(bold);
            sst_name_txt.setText(sstName);
            brand_txt.setTypeface(regular);
            brand_txt.setText(brandName + "\n" + modelName);
            String services = historyModel.getServices();
            String finalString = services.replace(",", "\n");
            services_txt.setTypeface(regular);
            services_txt.setText(finalString);
            price_txt.setTypeface(bold);
            price_txt.setText("\u20B9 " + historyModel.getPaidAmount());
            status_txt.setTypeface(regular);
            if (status.equalsIgnoreCase("1")) {
                status_txt.setText("On Going");
            } else if (status.equalsIgnoreCase("2")) {
                status_txt.setText("PickUp Assigned");
            } else if (status.equalsIgnoreCase("3")) {
                status_txt.setText("PickUp Completed");
            } else if (status.equalsIgnoreCase("4")) {
                status_txt.setText("Mechanic Assigned");
            } else if (status.equalsIgnoreCase("5")) {
                status_txt.setText("Service Completed");
            } else if (status.equalsIgnoreCase("6")) {
                status_txt.setText("Payment Completed");
            } else if (status.equalsIgnoreCase("7")) {
                status_txt.setText("Delivered");
            } else if (status.equalsIgnoreCase("8")) {
                status_txt.setText("Drop Off");
            } else if (status.equalsIgnoreCase("9")) {
                status_txt.setText("Waiting For Approval");
            } else if (status.equalsIgnoreCase("12")) {
                status_txt.setText("Cancelled");
            } else {
                status_txt.setText("N.A");
            }

            status_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (historyModel.getStatus().equalsIgnoreCase("5")) {
                        if (historyModel.getPaymentStatus().equalsIgnoreCase("1")) {
                            checkIFC(bookingId);
                            /*Intent intent = new Intent(context, FinalPaymentActivity.class);
                            intent.putExtra("bookingId", bookingId);
                            context.startActivity(intent);*/
                        } else {
                            Intent intent = new Intent(context, HistoryDetailActivity.class);
                            intent.putExtra("bookingId", bookingId);
                            context.startActivity(intent);
                        }
                    } else if (status.equalsIgnoreCase("8")) {

                        if (historyModel.getPaymentStatus().equalsIgnoreCase("1")) {
                            checkIFC(bookingId);
                            /*Intent intent = new Intent(context, FinalPaymentActivity.class);
                            intent.putExtra("bookingId", bookingId);
                            context.startActivity(intent);*/
                        } else {
                            Intent intent = new Intent(context, HistoryDetailActivity.class);
                            intent.putExtra("bookingId", bookingId);
                            context.startActivity(intent);
                        }

                    } else if (status.equalsIgnoreCase("9")) {
                        Intent intent = new Intent(context, ActionPageActivity.class);
                        intent.putExtra("bookingId", bookingId);
                        context.startActivity(intent);
                    } else {
                        Intent intent = new Intent(context, HistoryDetailActivity.class);
                        intent.putExtra("bookingId", bookingId);
                        context.startActivity(intent);
                    }

                }
            });

            itemView.setOnClickListener(view -> {

                if (historyModel.getStatus().equalsIgnoreCase("5")) {
                    if (historyModel.getPaymentStatus().equalsIgnoreCase("1")) {
                        checkIFC(bookingId);
                        /*Intent intent = new Intent(context, FinalPaymentActivity.class);
                        intent.putExtra("bookingId", bookingId);
                        context.startActivity(intent);*/
                    } else {
                        Intent intent = new Intent(context, HistoryDetailActivity.class);
                        intent.putExtra("bookingId", bookingId);
                        context.startActivity(intent);
                    }
                } else if (status.equalsIgnoreCase("8")) {

                    if (historyModel.getPaymentStatus().equalsIgnoreCase("1")) {
                        checkIFC(bookingId);
                        /*Intent intent = new Intent(context, FinalPaymentActivity.class);
                        intent.putExtra("bookingId", bookingId);
                        context.startActivity(intent);*/
                    } else {
                        Intent intent = new Intent(context, HistoryDetailActivity.class);
                        intent.putExtra("bookingId", bookingId);
                        context.startActivity(intent);
                    }

                } else if (status.equalsIgnoreCase("9")) {
                    Intent intent = new Intent(context, ActionPageActivity.class);
                    intent.putExtra("bookingId", bookingId);
                    context.startActivity(intent);
                } else {
                    Intent intent = new Intent(context, HistoryDetailActivity.class);
                    intent.putExtra("bookingId", historyModel.getBookingId());
                    context.startActivity(intent);
                }

            });
        }

        @Override
        public void onClick(View view) {

            this.historyItemClickListener.onItemClick(view, getLayoutPosition());
        }
    }

    private void checkIFC(String bookingId) {

        String url = AppUrls.BASE_URL + AppUrls.CHECK_IFC + bookingId + "/isfeedback";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.optString("status");
                    String code = jsonObject.optString("code");
                    String message = jsonObject.optString("message");

                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                    String isfeedback = jsonObject1.optString("isfeedback");

                    if (isfeedback.equalsIgnoreCase("true")) {

                        Intent intent = new Intent(context, FinalPaymentActivity.class);
                        intent.putExtra("bookingId", bookingId);
                        context.startActivity(intent);

                    } else if (isfeedback.equalsIgnoreCase("false")) {

                        Intent intent = new Intent(context, IFCActivity.class);
                        intent.putExtra("bookingId", bookingId);
                        context.startActivity(intent);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) /*{
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        }*/;
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    public String convertStringDateToAnotherStringDate(String stringdate, String stringdateformat, String returndateformat) {

        try {
            Date date = new SimpleDateFormat(stringdateformat).parse(stringdate);
            String returndate = new SimpleDateFormat(returndateformat).format(date);
            return returndate;
        } catch (ParseException e) {
            e.printStackTrace();
            return stringdate;
        }

    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
