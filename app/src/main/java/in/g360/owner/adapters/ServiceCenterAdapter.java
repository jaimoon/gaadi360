package in.g360.owner.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import in.g360.owner.R;
import in.g360.owner.activities.ConfirmationDetailActivity;
import in.g360.owner.activities.ServiceCenterActivity;
import in.g360.owner.interfaces.ServiceCenterItemClickListener;
import in.g360.owner.models.ServiceCenterModel;

public class ServiceCenterAdapter extends RecyclerView.Adapter<ServiceCenterAdapter.ServiceCenterHolder> {

    public ArrayList<ServiceCenterModel> serviceCenterModels;
    public ServiceCenterActivity context;
    LayoutInflater li;
    int resource;
    SharedPreferences sstPreferences;
    SharedPreferences.Editor sstEditor;
    String condStr,sstId;

    public ServiceCenterAdapter(String condStr,ArrayList<ServiceCenterModel> serviceCenterModels, ServiceCenterActivity context, int resource) {
        this.serviceCenterModels = serviceCenterModels;
        this.context = context;
        this.resource = resource;
        this.condStr = condStr;

        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ServiceCenterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, null);
        ServiceCenterHolder slh = new ServiceCenterHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final ServiceCenterHolder holder, final int position) {

        sstPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sstEditor = sstPreferences.edit();

        sstId = sstPreferences.getString("sstId", "");

        holder.sst_name.setText(serviceCenterModels.get(position).getServiceCenterName());
        holder.sst_person.setText(serviceCenterModels.get(position).getContactName());
        holder.sst_price.setText("Rs." + serviceCenterModels.get(position).getEstimatedCost() + "/-");
        holder.sst_address.setText(serviceCenterModels.get(position).getAddress());
        holder.sst_distance.setText(serviceCenterModels.get(position).getDistance() + " kms");
        holder.sst_timings.setText(serviceCenterModels.get(position).getDisplayTime());
        holder.sst_rating.setRating(Float.parseFloat(serviceCenterModels.get(position).getAverageRating()));

        if (sstId.equalsIgnoreCase(serviceCenterModels.get(position).getServiceCenterId())) {
            holder.sst_rl.setBackgroundResource(R.color.bg_gray);
        } else {
            holder.sst_rl.setBackgroundResource(R.color.white);
        }

        holder.sst_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sstEditor.putString("sstId", serviceCenterModels.get(position).getServiceCenterId());
                sstEditor.putString("sstName", serviceCenterModels.get(position).getServiceCenterName());
                sstEditor.putString("sstAddress", serviceCenterModels.get(position).getAddress());
                sstEditor.putString("sstDistance", serviceCenterModels.get(position).getDistance());
                sstEditor.putString("sstInitPrice", serviceCenterModels.get(position).getInitialPaidAmount());
                sstEditor.putString("sstPrice", serviceCenterModels.get(position).getEstimatedCost());
                sstEditor.putString("sstRating", serviceCenterModels.get(position).getAverageRating());
                sstEditor.putString("sstOpenTime", serviceCenterModels.get(position).getOpenTime());
                sstEditor.putString("sstCloseTime", serviceCenterModels.get(position).getCloseTime());
                sstEditor.putString("sstPerson", serviceCenterModels.get(position).getContactName());
                sstEditor.putString("sstDisplayTime", serviceCenterModels.get(position).getDisplayTime());
                sstEditor.putString("sstNumber", serviceCenterModels.get(position).getContactMobileNumber());
                sstEditor.apply();

                Intent intent = new Intent(context, ConfirmationDetailActivity.class);
                intent.putExtra("activity", "ServiceCenterAdapter");
                intent.putExtra("condStr", condStr);
                context.startActivity(intent);
                context.finish();
            }
        });

        holder.setItemClickListener(new ServiceCenterItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

                sstEditor.putString("sstId", serviceCenterModels.get(position).getServiceCenterId());
                sstEditor.putString("sstName", serviceCenterModels.get(position).getServiceCenterName());
                sstEditor.putString("sstAddress", serviceCenterModels.get(position).getAddress());
                sstEditor.putString("sstDistance", serviceCenterModels.get(position).getDistance());
                sstEditor.putString("sstInitPrice", serviceCenterModels.get(position).getInitialPaidAmount());
                sstEditor.putString("sstPrice", serviceCenterModels.get(position).getEstimatedCost());
                sstEditor.putString("sstRating", serviceCenterModels.get(position).getAverageRating());
                sstEditor.putString("sstOpenTime", serviceCenterModels.get(position).getOpenTime());
                sstEditor.putString("sstCloseTime", serviceCenterModels.get(position).getCloseTime());
                sstEditor.putString("sstPerson", serviceCenterModels.get(position).getContactName());
                sstEditor.putString("sstDisplayTime", serviceCenterModels.get(position).getDisplayTime());
                sstEditor.apply();

                Intent intent = new Intent(context, ConfirmationDetailActivity.class);
                intent.putExtra("activity", "ServiceCenterAdapter");
                intent.putExtra("condStr", condStr);
                context.startActivity(intent);
                context.finish();
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.serviceCenterModels.size();

    }

    public class ServiceCenterHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        RelativeLayout sst_rl;
        ImageView sst_img, sst_select;
        TextView sst_name, sst_person, sst_timings, sst_price, sst_address, sst_distance;
        RatingBar sst_rating;

        ServiceCenterItemClickListener serviceCenterItemClickListener;

        ServiceCenterHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            sst_rl = itemView.findViewById(R.id.sst_rl);
            sst_img = itemView.findViewById(R.id.sst_img);
            sst_name = itemView.findViewById(R.id.sst_name);
            sst_person = itemView.findViewById(R.id.sst_person);
            sst_timings = itemView.findViewById(R.id.sst_timings);
            sst_price = itemView.findViewById(R.id.sst_price);
            sst_address = itemView.findViewById(R.id.sst_address);
            sst_distance = itemView.findViewById(R.id.sst_distance);
            sst_rating = itemView.findViewById(R.id.sst_rating);
            sst_select = itemView.findViewById(R.id.sst_select);

        }

        @Override
        public void onClick(View view) {

            this.serviceCenterItemClickListener.onItemClick(view, getLayoutPosition());
        }

        public void setItemClickListener(ServiceCenterItemClickListener ic) {
            this.serviceCenterItemClickListener = ic;
        }
    }

}
