package in.g360.owner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import in.g360.owner.R;
import in.g360.owner.activities.LoginActivity;
import in.g360.owner.interfaces.ServiceTypeItemClickListener;
import in.g360.owner.models.ServiceTypeModel;

public class ServiceTypeAdapter extends RecyclerView.Adapter<ServiceTypeAdapter.ServiceTypeHolder> {

    private ArrayList<ServiceTypeModel> serviceTypeModels;
    LoginActivity context;
    private LayoutInflater li;
    private int resource;
    private int lastPosition = -1;

    public ServiceTypeAdapter(ArrayList<ServiceTypeModel> serviceTypeModels, LoginActivity context, int resource) {
        this.serviceTypeModels = serviceTypeModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ServiceTypeHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_service_type, viewGroup, false);
        return new ServiceTypeHolder(view);
    }

    @Override
    public void onBindViewHolder(ServiceTypeHolder generalServiceHolder, final int position) {

        setAnimation(generalServiceHolder.itemView, position);
        generalServiceHolder.bind(serviceTypeModels.get(position));

    }

    @Override
    public int getItemCount() {
        return this.serviceTypeModels.size();
    }

    public class ServiceTypeHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public CheckBox service_type_chk;

        ServiceTypeItemClickListener serviceTypeItemClickListener;


        public ServiceTypeHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            service_type_chk = itemView.findViewById(R.id.service_type_chk);

        }

        void bind(final ServiceTypeModel serviceTypeModel) {

            service_type_chk.setText(serviceTypeModel.getName());

            service_type_chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    /*Intent intent = new Intent(context, MainActivity.class);
                    context.startActivity(intent);*/
                }
            });

            itemView.setOnClickListener(view -> {

            });
        }

        @Override
        public void onClick(View view) {

            this.serviceTypeItemClickListener.onItemClick(view, getLayoutPosition());
        }
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
