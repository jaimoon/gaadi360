package in.g360.owner.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import in.g360.owner.MainActivity;
import in.g360.owner.R;
import in.g360.owner.models.FeaturedModel;

public class FeaturedAdapter extends RecyclerView.Adapter<FeaturedAdapter.FeaturedHolder> {

    List<FeaturedModel> data;
    MainActivity context;

    public FeaturedAdapter(MainActivity context, List<FeaturedModel> data) {
        this.context = context;
        this.data = data;
    }

    public static class FeaturedHolder extends RecyclerView.ViewHolder {

        public ImageView imageView;

        public FeaturedHolder(View v) {
            super(v);

            imageView = v.findViewById(R.id.feature_img);

        }
    }

    @Override
    public FeaturedAdapter.FeaturedHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_featured, parent, false);
        FeaturedHolder vh = new FeaturedHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final FeaturedHolder holder, int position) {

        FeaturedModel all = data.get(position);

        holder.imageView.setImageResource(all.id);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

}
