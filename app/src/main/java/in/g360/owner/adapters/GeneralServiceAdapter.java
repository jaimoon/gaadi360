package in.g360.owner.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import in.g360.owner.R;
import in.g360.owner.activities.ServicesListActivity;
import in.g360.owner.interfaces.GeneralServiceItemClickListener;
import in.g360.owner.models.GeneralServiceInfoModel;

public class GeneralServiceAdapter extends RecyclerView.Adapter<GeneralServiceAdapter.GeneralServiceHolder> {

    private ArrayList<GeneralServiceInfoModel> generalServiceInfoModels;
    ServicesListActivity context;
    private LayoutInflater li;
    private int resource;
    private int lastPosition = -1;
    Typeface regular, bold;

    public GeneralServiceAdapter(ArrayList<GeneralServiceInfoModel> generalServiceInfoModels, ServicesListActivity context, int resource) {
        this.generalServiceInfoModels = generalServiceInfoModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public GeneralServiceHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_general_service, viewGroup, false);
        return new GeneralServiceHolder(view);
    }

    @Override
    public void onBindViewHolder(GeneralServiceHolder generalServiceHolder, final int position) {

        setAnimation(generalServiceHolder.itemView, position);
        generalServiceHolder.bind(generalServiceInfoModels.get(position));

    }

    @Override
    public int getItemCount() {
        return this.generalServiceInfoModels.size();
    }

    public class GeneralServiceHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView info_name;
        GeneralServiceItemClickListener generalServiceItemClickListener;


        public GeneralServiceHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            info_name = itemView.findViewById(R.id.info_name);

        }

        void bind(final GeneralServiceInfoModel generalServiceInfoModel) {

            regular = Typeface.createFromAsset(context.getAssets(), "proxima_nova_regular.otf");
            bold = Typeface.createFromAsset(context.getAssets(), "proxima_nova_bold.otf");

            info_name.setTypeface(regular);
            info_name.setText(generalServiceInfoModel.getServiceItem());

            itemView.setOnClickListener(view -> {

            });
        }

        @Override
        public void onClick(View view) {

            this.generalServiceItemClickListener.onItemClick(view, getLayoutPosition());
        }
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
