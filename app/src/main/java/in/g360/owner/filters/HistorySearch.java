package in.g360.owner.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.g360.owner.adapters.HistoryAdapter;
import in.g360.owner.models.HistoryModel;

public class HistorySearch extends Filter {

    HistoryAdapter adapter;
    ArrayList<HistoryModel> filterList;

    public HistorySearch(ArrayList<HistoryModel> filterList, HistoryAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        if (constraint != null && constraint.length() > 0) {
            constraint = constraint.toString().toUpperCase();

            ArrayList<HistoryModel> filteredPlayers = new ArrayList<HistoryModel>();

            for (int i = 0; i < filterList.size(); i++) {
                if (filterList.get(i).getServiceCenterName().toUpperCase().contains(constraint) ||
                        filterList.get(i).getBrandName().toUpperCase().contains(constraint) ||
                        filterList.get(i).getModelName().toUpperCase().contains(constraint) ||
                        filterList.get(i).getServices().toUpperCase().contains(constraint) ||
                        filterList.get(i).getFinalPrice().toUpperCase().contains(constraint) ||
                        filterList.get(i).getBookingDate().toUpperCase().contains(constraint) ||
                        filterList.get(i).getBookingTime().toUpperCase().contains(constraint)) {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count = filteredPlayers.size();
            results.values = filteredPlayers;
        } else {
            results.count = filterList.size();
            results.values = filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.historyModels = (ArrayList<HistoryModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
