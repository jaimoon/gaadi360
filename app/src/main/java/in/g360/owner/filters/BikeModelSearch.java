package in.g360.owner.filters;

import android.widget.Filter;
import java.util.ArrayList;
import in.g360.owner.adapters.BikeModelAdapter;
import in.g360.owner.models.BikeModelModel;

public class BikeModelSearch extends Filter {

    BikeModelAdapter adapter;
    ArrayList<BikeModelModel> filterList;

    public BikeModelSearch(ArrayList<BikeModelModel> filterList, BikeModelAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<BikeModelModel> filteredPlayers=new ArrayList<BikeModelModel>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getModelName().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results)
    {
        adapter.bikeModelModels = (ArrayList<BikeModelModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
