package in.g360.owner.filters;

import android.widget.Filter;
import java.util.ArrayList;

import in.g360.owner.adapters.WalletHistoryAdapter;
import in.g360.owner.models.WalletHistoryModel;

public class WalletHistorySearch extends Filter {

    WalletHistoryAdapter adapter;
    ArrayList<WalletHistoryModel> filterList;

    public WalletHistorySearch(ArrayList<WalletHistoryModel> filterList, WalletHistoryAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        if (constraint != null && constraint.length() > 0) {
            constraint = constraint.toString().toUpperCase();

            ArrayList<WalletHistoryModel> filteredPlayers = new ArrayList<WalletHistoryModel>();

            for (int i = 0; i < filterList.size(); i++) {
                if (filterList.get(i).getUserId().toUpperCase().contains(constraint) ||
                        filterList.get(i).getId().toUpperCase().contains(constraint) ||
                        filterList.get(i).getReference().toUpperCase().contains(constraint) ||
                        filterList.get(i).getReferenceType().toUpperCase().contains(constraint) ||
                        filterList.get(i).getReferenceId().toUpperCase().contains(constraint) ||
                        filterList.get(i).getAmount().toUpperCase().contains(constraint) ||
                        filterList.get(i).getType().toUpperCase().contains(constraint) ||
                        filterList.get(i).getStatus().toUpperCase().contains(constraint) ||
                        filterList.get(i).getReferred().toUpperCase().contains(constraint) ||
                        filterList.get(i).getCreatedTime().toUpperCase().contains(constraint) ||
                        filterList.get(i).getModifiedTime().toUpperCase().contains(constraint)) {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count = filteredPlayers.size();
            results.values = filteredPlayers;
        } else {
            results.count = filterList.size();
            results.values = filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.walletHistoryModels = (ArrayList<WalletHistoryModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
