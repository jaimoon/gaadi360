package in.g360.owner.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.g360.owner.adapters.InfoAdapter;
import in.g360.owner.models.ServiceDetailModel;

public class InfoSearch extends Filter {

    InfoAdapter adapter;
    ArrayList<ServiceDetailModel> filterList;

    public InfoSearch(ArrayList<ServiceDetailModel> filterList, InfoAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        if (constraint != null && constraint.length() > 0) {
            constraint = constraint.toString().toUpperCase();

            ArrayList<ServiceDetailModel> filteredPlayers = new ArrayList<ServiceDetailModel>();

            for (int i = 0; i < filterList.size(); i++) {
                if (filterList.get(i).getServiceItem().toUpperCase().contains(constraint)) {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count = filteredPlayers.size();
            results.values = filteredPlayers;
        } else {
            results.count = filterList.size();
            results.values = filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.serviceDetailModels = (ArrayList<ServiceDetailModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
