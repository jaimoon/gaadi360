package in.g360.owner.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.g360.owner.R;
import in.g360.owner.adapters.WalletHistoryAdapter;
import in.g360.owner.models.WalletHistoryModel;
import in.g360.owner.utils.AppUrls;
import in.g360.owner.utils.NetworkChecking;
import in.g360.owner.utils.UserSessionManager;

public class WalletActivity extends AppCompatActivity implements View.OnClickListener{

    ImageView close,no_history_img;
    TextView toolbar_title,no_history_txt,balance_txt;
    private boolean checkInternet;
    RecyclerView wallet_recyclerview;

    UserSessionManager userSessionManager;
    String accessToken, userMobile,userId, firstName, lastName, username, authority, email, mobile, emailVerified, mobileVerified,
            wallet,id, initialPaymentAmount, adImageId, radius, sstShowCount, ratingInterval, cancellationHours,
            createdTime, modifiedTime, preBookingDays, rescheduleCount, registrationAmount, referedAmount,
            walletDeductionPercentage;
    SearchView wallet_search;

    /*History*/
    WalletHistoryAdapter walletHistoryAdapter;
    ArrayList<WalletHistoryModel> walletHistoryModels = new ArrayList<WalletHistoryModel>();
    Typeface regular, bold;

    AlertDialog.Builder dialogBuilder;
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        checkInternet = NetworkChecking.isConnected(this);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);
        userMobile = userDetails.get(UserSessionManager.USER_MOBILE);

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);
        balance_txt = findViewById(R.id.balance_txt);
        balance_txt.setTypeface(bold);
        balance_txt.setOnClickListener(this);

        no_history_img = findViewById(R.id.no_history_img);
        no_history_txt = findViewById(R.id.no_history_txt);
        close = findViewById(R.id.close);
        close.setOnClickListener(this);

        wallet_recyclerview = findViewById(R.id.wallet_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        wallet_recyclerview.setLayoutManager(layoutManager);
        walletHistoryAdapter = new WalletHistoryAdapter(walletHistoryModels, WalletActivity.this, R.layout.row_wallet_history);

        getHistory();

        wallet_search = findViewById(R.id.wallet_search);
        EditText searchEditText = wallet_search.findViewById(androidx.appcompat.R.id.search_src_text);
        wallet_search.setOnClickListener(v -> wallet_search.setIconified(false));
        wallet_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                walletHistoryAdapter.getFilter().filter(query);
                return false;
            }
        });

        getProfile();

        getReferalAmount();
    }

    private void getReferalAmount() {

        String url = AppUrls.BASE_URL + AppUrls.REFERAL_AMOUNT_DETAIL;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    id = jsonObject.optString("id");
                    initialPaymentAmount = jsonObject.optString("initialPaymentAmount");
                    adImageId = jsonObject.optString("adImageId");
                    radius = jsonObject.optString("radius");
                    sstShowCount = jsonObject.optString("sstShowCount");
                    ratingInterval = jsonObject.optString("ratingInterval");
                    cancellationHours = jsonObject.optString("cancellationHours");
                    createdTime = jsonObject.optString("createdTime");
                    modifiedTime = jsonObject.optString("modifiedTime");
                    preBookingDays = jsonObject.optString("preBookingDays");
                    rescheduleCount = jsonObject.optString("rescheduleCount");
                    registrationAmount = jsonObject.optString("registrationAmount");
                    referedAmount = jsonObject.optString("referedAmount");
                    walletDeductionPercentage = jsonObject.optString("walletDeductionPercentage");


                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(WalletActivity.this);
        requestQueue.add(stringRequest);
    }

    private void getProfile() {
        String url = AppUrls.BASE_URL + AppUrls.PROFILE;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    userId = jsonObject.optString("userId");
                    firstName = jsonObject.optString("firstName");
                    lastName = jsonObject.optString("lastName");
                    username = jsonObject.optString("username");
                    authority = jsonObject.optString("authority");
                    email = jsonObject.optString("email");
                    mobile = jsonObject.optString("mobile");
                    emailVerified = jsonObject.optString("emailVerified");
                    mobileVerified = jsonObject.optString("mobileVerified");
                    wallet = jsonObject.optString("wallet");

                    balance_txt.setText("Balance : \u20B9 " + wallet);

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(WalletActivity.this);
        requestQueue.add(stringRequest);
    }

    private void getHistory() {

        String url = AppUrls.BASE_URL + AppUrls.WALLET_HISTORY;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                walletHistoryModels.clear();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    if (status.equalsIgnoreCase("2000")) {

                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        JSONArray jsonArray = jsonObject1.getJSONArray("walletHistory");

                        if (jsonArray.length()!=0) {

                            wallet_search.setVisibility(View.VISIBLE);
                            no_history_img.setVisibility(View.GONE);
                            no_history_txt.setVisibility(View.GONE);

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                WalletHistoryModel rm = new WalletHistoryModel();
                                rm.setUserId(jsonObject2.optString("userId"));
                                rm.setId(jsonObject2.optString("id"));
                                rm.setReference(jsonObject2.optString("reference"));
                                rm.setReferenceType(jsonObject2.optString("referenceType"));
                                rm.setReferenceId(jsonObject2.optString("referenceId"));
                                rm.setAmount(jsonObject2.optString("amount"));
                                rm.setType(jsonObject2.optString("type"));
                                rm.setStatus(jsonObject2.optString("status"));
                                rm.setReferred(jsonObject2.optString("referred"));
                                rm.setCreatedTime(jsonObject2.optString("createdTime"));
                                rm.setModifiedTime(jsonObject2.optString("modifiedTime"));

                                walletHistoryModels.add(rm);
                            }

                            wallet_recyclerview.setAdapter(walletHistoryAdapter);
                            walletHistoryAdapter.notifyDataSetChanged();
                        }else {
                            wallet_search.setVisibility(View.GONE);
                            no_history_txt.setVisibility(View.VISIBLE);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(WalletActivity.this);
        requestQueue.add(stringRequest);

    }

    @Override
    public void onClick(View v) {
        if (v == close){
            if (checkInternet) {
                Intent intent = new Intent(WalletActivity.this, SettingActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            }else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == balance_txt) {
            if (checkInternet) {

                showReferalDialog(R.layout.wallet_dialog);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }

    private void showReferalDialog(int layout) {
        dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(WalletActivity.this);
        View layoutView = getLayoutInflater().inflate(layout, null);
        TextView wallet_title = layoutView.findViewById(R.id.wallet_title);
        wallet_title.setTypeface(bold);
        TextView wallet_txt = layoutView.findViewById(R.id.wallet_txt);
        wallet_txt.setTypeface(regular);

        if (wallet.equalsIgnoreCase("0")){

            wallet_txt.setText("Refer a friend and save money for both.");


        }else {

            wallet_txt.setText("Thank you for referring Gaadi360 app. \n You earned \u20B9 " + wallet + " so far.");

        }

        Button cancel_btn = layoutView.findViewById(R.id.cancel_btn);
        cancel_btn.setTypeface(bold);
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();

            }
        });

        Button share_btn = layoutView.findViewById(R.id.share_btn);
        share_btn.setTypeface(bold);
        share_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkInternet) {
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Gaadi360");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Hi, save \u20B9 " + referedAmount + " by getting your next bike service using Gaadi360 app at http://app.Gaadi360.com . Please enter my phone number " + userMobile + " as referral code when you register so I can save on my next bike service. Happy and safe riding!");
                    startActivity(Intent.createChooser(sharingIntent, "Share via"));
                } else {
                    Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }

                alertDialog.dismiss();

            }
        });

        dialogBuilder.setView(layoutView);
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        alertDialog.setCancelable(true);
    }

    @Override
    public void onBackPressed() {

        if (checkInternet) {
            Intent intent = new Intent(WalletActivity.this, SettingActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();

        }else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }
}
