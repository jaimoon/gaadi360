package in.g360.owner.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;

import in.g360.owner.MainActivity;
import in.g360.owner.R;
import in.g360.owner.utils.GlobalCalls;
import in.g360.owner.utils.GpsUtils;
import in.g360.owner.utils.UserSessionManager;

public class TestActivity extends FragmentActivity implements View.OnClickListener, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private static String TAG = TestActivity.class.getSimpleName();
    private static final int AUTOCOMPLETE_REQUEST_CODE = 22;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    SupportMapFragment mapFragment;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private GoogleMap mMap;
    private GoogleMap.OnCameraIdleListener onCameraIdleListener;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    private Circle mCircle;
    double radiusInMeters = 100.0, latitute, longitiu;
    int strokeColor = 0xffff0000;
    int shadeColor = 0x44ff0000;
    String deviceId,  token, user_name, mobile, accessToken;
    List<Address> addresses;
    Geocoder geocoder;
    String latitude1, longitude1, activity;
    PlacesClient placesClient;
    LatLng latLng;
    String sendLat, sendLng;
    TextView toolbar_title, search_loc, area_txt, address_txt, save_button_txt;
    Typeface regular, bold;
    SharedPreferences locationPref;
    SharedPreferences.Editor locationEditor;
    public LocationManager lm;
    SharedPreferences preferences;
    String brandId, modelId, kilometerRangeId, bookingDate, bookingStartTime, bookingEndTime, bookingDisplayTime, brandName, modelName, kilometer;
    private boolean isGPS = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        activity = getIntent().getStringExtra("activity");

        locationPref = getSharedPreferences("Location", Context.MODE_PRIVATE);
        //locationPref = PreferenceManager.getDefaultSharedPreferences(this);
        locationEditor = locationPref.edit();

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        // places sdk
        String apiKey = getString(R.string.googleapikey);

        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), apiKey);
        }

        placesClient = Places.createClient(this);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        mobile = userDetails.get(UserSessionManager.USER_MOBILE);
        user_name = userDetails.get(UserSessionManager.USER_NAME);

        Log.d("MMMMMM", deviceId + "\n" + token + "\n" + user_name + "\n" + mobile);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        brandId = preferences.getString("brandId", "");
        brandName = preferences.getString("brandName", "");
        modelId = preferences.getString("modelId", "");
        modelName = preferences.getString("modelName", "");
        kilometer = preferences.getString("kilometer", "");
        kilometerRangeId = preferences.getString("kilometerRangeId", "");
        bookingDate = preferences.getString("bookingDate", "");
        bookingStartTime = preferences.getString("bookingStartTime", "");
        bookingEndTime = preferences.getString("bookingEndTime", "");
        bookingDisplayTime = preferences.getString("bookingDisplayTime", "");

        Log.d("Preferences", brandId + "\n" + brandName + "\n" + modelId + "\n" + modelName + "\n" + kilometer + "\n" +
                kilometerRangeId + "\n" + bookingDate + "\n" + bookingStartTime + "\n" + bookingEndTime + "\n" + bookingDisplayTime);

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);
        search_loc = findViewById(R.id.search_loc);
        search_loc.setTypeface(regular);
        search_loc.setOnClickListener(this);

        address_txt = findViewById(R.id.address_txt);
        address_txt.setTypeface(regular);
        area_txt = findViewById(R.id.area_txt);
        area_txt.setTypeface(bold);
        save_button_txt = findViewById(R.id.save_button_txt);
        save_button_txt.setTypeface(bold);
        save_button_txt.setOnClickListener(this);

        geocoder = new Geocoder(this, Locale.getDefault());

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);


            new GpsUtils(this).turnGPSOn(new GpsUtils.onGpsListener() {
                @Override
                public void gpsStatus(boolean isGPSEnable) {
                    // turn on GPS
                    isGPS = isGPSEnable;
                }
            });

        configureCameraIdle();

    }

    private void configureCameraIdle() {

        onCameraIdleListener = new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {

                latLng = mMap.getCameraPosition().target;

                Log.d("LATLNG", latLng.latitude + "\n" + latLng.longitude);

                try {
                    Geocoder geocoder = new Geocoder(TestActivity.this);
                    List<Address> addressList = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                    if (addressList != null && addressList.size() > 0) {
                        Address address = addressList.get(0);
                        String locality = addressList.get(0).getAddressLine(0);
                        String addr[] = locality.split(",", 2);
                        //Log.d("LOClity", "" + address);
                        //Log.d("ARYA", addr[0] + "////" + addr[1]);

                        sendLat = String.valueOf(latLng.latitude);
                        sendLng = String.valueOf(latLng.longitude);

                        String country = addressList.get(0).getCountryName();
                        String state = addressList.get(0).getAdminArea();
                        String city = addressList.get(0).getLocality();
                        String area = addressList.get(0).getSubLocality();
                        String pincode = addressList.get(0).getPostalCode();
                        latitute = addressList.get(0).getLatitude();
                        longitiu = addressList.get(0).getLongitude();
                        //if (!locality.isEmpty() && !country.isEmpty())

                            if (area != null) {
                                area_txt.setText(area);
                            } else {
                                area_txt.setText("Select Location");
                            }

                        address_txt.setText(locality);
                        //address_txt.setText(addr[1]);
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        };
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
        mMap.setOnCameraIdleListener(onCameraIdleListener);

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);

        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(19));

        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    /*@Override
    public void onPause() {
        super.onPause();

        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }*/

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onClick(View v) {

        if (v == search_loc) {

            onSearchCalled();
        }

        if (v == save_button_txt) {

            if (activity.equalsIgnoreCase("MainActivity")) {

                locationEditor.putString("latitude", sendLat);
                locationEditor.putString("longitude", sendLng);
                locationEditor.putString("locationName", address_txt.getText().toString() + "," + area_txt.getText().toString());
                locationEditor.apply();

                Intent intent = new Intent(TestActivity.this, MainActivity.class);

                intent.putExtra("activity", "TestActivity");
                intent.putExtra("latitude", sendLat);
                intent.putExtra("longitude", sendLng);
                intent.putExtra("location", address_txt.getText().toString() + "," + area_txt.getText().toString());

                intent.putExtra("brandId", brandId);
                intent.putExtra("modelId", modelId);
                intent.putExtra("kilometerRangeId", kilometerRangeId);
                intent.putExtra("brandName", brandName);
                intent.putExtra("modelName", modelName);
                intent.putExtra("kilometer", kilometer);
                intent.putExtra("bookingDate", bookingDate);
                intent.putExtra("bookingStartTime", bookingStartTime);
                intent.putExtra("bookingEndTime", bookingEndTime);
                intent.putExtra("bookingDisplayTime", bookingDisplayTime);

                startActivity(intent);

            }

            if (activity.equalsIgnoreCase("MainActivity2")) {

                locationEditor.putString("latitude", sendLat);
                locationEditor.putString("longitude", sendLng);
                locationEditor.putString("locationName", address_txt.getText().toString() + "," + area_txt.getText().toString());
                locationEditor.apply();

                Intent intent = new Intent(TestActivity.this, ServicesListActivity.class);
                startActivity(intent);
            }

            if (activity.equalsIgnoreCase("ConfirmationDetailActivity")) {
                Intent intent = new Intent(TestActivity.this, ConfirmationDetailActivity.class);
                intent.putExtra("activity", "TestActivity");
                intent.putExtra("latitude", sendLat);
                intent.putExtra("longitude", sendLng);
                intent.putExtra("location", address_txt.getText().toString() + "," + area_txt.getText().toString());
                startActivity(intent);
            }
        }
    }

    public void onSearchCalled() {
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).setCountry("IN").build(TestActivity.this);
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                String stringlat = place.getLatLng().toString();
                CharSequence name = place.getName();
                final CharSequence address = place.getAddress();

                if (name.toString().contains("°")) {
                    name = "";
                }
                stringlat = stringlat.substring(stringlat.indexOf("(") + 1);
                stringlat = stringlat.substring(0, stringlat.indexOf(")"));
                String latValue = stringlat.split(",")[0];
                latitude1 = latValue;
                String lngValue = stringlat.split(",")[1];
                longitude1 = lngValue;

                sendLat = latitude1;
                sendLng = longitude1;

                Double lat = Double.valueOf(latitude1);
                Double lng = Double.valueOf(longitude1);
                String fulladd = address.toString();
                String addr[] = fulladd.split(",", 2);

                try {
                    addresses = geocoder.getFromLocation(lat, lng, 1);
                    //    save_btn.setEnabled(true);
                    Log.d("LOXXX", addresses.get(0).getAddressLine(0));
                    String addLine = addresses.get(0).getAddressLine(0);
                    String city = addresses.get(0).getLocality();
                    String area = addresses.get(0).getSubLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    String addLineTwo = addresses.get(0).getThoroughfare();
                    String addLineOne = addresses.get(0).getFeatureName();

                    if (area != null) {
                        if (addr[1] != null && area != null && addr[1].contains(area)) {
                            addr[1] = addr[1].replace(area, " ");
                        }
                    }

                    if (city != null) {
                        if (addr[1].contains(city)) {
                            addr[1] = addr[1].replace(city, " ");
                        }
                    }

                    if (state != null) {
                        if (addr[1].contains(state)) {
                            addr[1] = addr[1].replace(state, " ");
                        }
                    }

                    if (postalCode != null) {
                        if (addr[1].contains(postalCode)) {
                            addr[1] = addr[1].replace(postalCode, " ");
                        }
                    }

                    if (country != null) {
                        if (addr[1].contains(country)) {
                            addr[1] = addr[1].replace(country, " ");
                        }
                    }

                    area_txt.setText(area);
                    address_txt.setText(addr[1]);


                    addr[1] = new LinkedHashSet<String>(Arrays.asList(addr[1].split("\\s"))).toString().replaceAll("[\\[\\],]", "");

                    if (!mobile.equalsIgnoreCase("null"))
                        Log.v("FFFFFFF", "//" + address.toString());

                    if (mCurrLocationMarker != null) {
                        mCurrLocationMarker.remove();
                    }
                    LatLng latLng = new LatLng(Double.parseDouble(latitude1), Double.parseDouble(longitude1));
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(latLng);
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(19));
                    if (mGoogleApiClient != null) {
                        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                Status status = Autocomplete.getStatusFromIntent(data);
                GlobalCalls.showToast(status.getStatusMessage(), TestActivity.this);

                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }
}
