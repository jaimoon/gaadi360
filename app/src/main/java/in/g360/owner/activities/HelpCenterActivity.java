package in.g360.owner.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.taishi.flipprogressdialog.FlipProgressDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.g360.owner.R;
import in.g360.owner.utils.AppUrls;
import in.g360.owner.utils.NetworkChecking;
import in.g360.owner.utils.UserSessionManager;

public class HelpCenterActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close;
    TextView toolbar_title, name_txt, email_txt, mobile_txt, company_name_txt, address_txt;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    String accessToken, name, email, mobile, companyName, address;

    /*ProgressDialog*/
    List<Integer> imageList = new ArrayList<Integer>();
    FlipProgressDialog flipProgressDialog;
    Typeface regular, bold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_center);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        checkInternet = NetworkChecking.isConnected(this);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);

        /*imageList.add(R.mipmap.ic_launcher);
        flipProgressDialog = new FlipProgressDialog();
        flipProgressDialog.setImageList(imageList);
        flipProgressDialog.setCanceledOnTouchOutside(true);
        flipProgressDialog.setDimAmount(0.8f); //0.0f
        flipProgressDialog.setBackgroundColor(Color.parseColor("#00FFFFFF"));
        flipProgressDialog.setBackgroundAlpha(0.2f);
        flipProgressDialog.setBorderStroke(0);
        flipProgressDialog.setBorderColor(-1);
        flipProgressDialog.setCornerRadius(16);
        flipProgressDialog.setImageSize(200);
        flipProgressDialog.setImageMargin(10);
        flipProgressDialog.setOrientation("rotationY");
        flipProgressDialog.setDuration(600);
        flipProgressDialog.setStartAngle(0.0f);
        flipProgressDialog.setEndAngle(180.0f);
        flipProgressDialog.setMinAlpha(0.0f);
        flipProgressDialog.setMaxAlpha(1.0f);
        flipProgressDialog.show(getFragmentManager(), "");
        flipProgressDialog.dismiss();*/

        close = findViewById(R.id.close);
        close.setOnClickListener(this);

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);
        name_txt = findViewById(R.id.name_txt);
        name_txt.setTypeface(regular);
        email_txt = findViewById(R.id.email_txt);
        email_txt.setTypeface(regular);
        mobile_txt = findViewById(R.id.mobile_txt);
        mobile_txt.setTypeface(regular);
        mobile_txt.setPaintFlags(mobile_txt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mobile_txt.setOnClickListener(this);
        company_name_txt = findViewById(R.id.company_name_txt);
        company_name_txt.setTypeface(regular);
        address_txt = findViewById(R.id.address_txt);
        address_txt.setTypeface(regular);

        getHelpCenterDetails();
    }

    private void getHelpCenterDetails() {
        String url = AppUrls.BASE_URL + AppUrls.HELP_CENTER;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String helpCenterId = jsonObject.getString("helpCenterId");
                    name = jsonObject.getString("name");
                    email = jsonObject.getString("email");
                    mobile = jsonObject.getString("mobile");
                    companyName = jsonObject.getString("companyName");
                    address = jsonObject.getString("address");

                    Log.d("HELPCENTER", name + "\n" + email + "\n" + mobile + "\n" + companyName + "\n" + address);

                    name_txt.setText(name);
                    email_txt.setText(email);
                    company_name_txt.setText(companyName);
                    mobile_txt.setText(mobile);
                    address_txt.setText(address);

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(HelpCenterActivity.this);
        requestQueue.add(stringRequest);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {
        if (v == close) {
            if (checkInternet) {
                Intent intent = new Intent(HelpCenterActivity.this, SettingActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == mobile_txt) {
            if (checkInternet) {

                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + Uri.encode(mobile)));
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(callIntent);


            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (checkInternet) {
            Intent intent = new Intent(HelpCenterActivity.this, SettingActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();

        }else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }
}
