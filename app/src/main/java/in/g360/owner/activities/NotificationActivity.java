package in.g360.owner.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import in.g360.owner.MainActivity;
import in.g360.owner.R;
import in.g360.owner.adapters.NotificationAdapter;
import in.g360.owner.models.NotificationModel;
import in.g360.owner.utils.AppUrls;
import in.g360.owner.utils.NetworkChecking;
import in.g360.owner.utils.UserSessionManager;

public class NotificationActivity extends AppCompatActivity implements View.OnClickListener {

    private boolean checkInternet;
    ImageView close, no_data_img;
    TextView toolbar_title, clear_txt;
    RecyclerView notification_recyclerview;
    Typeface regular, bold;
    SweetAlertDialog sweetAlertDialog;

    UserSessionManager userSessionManager;
    String accessToken;

    NotificationAdapter notificationAdapter;
    ArrayList<NotificationModel> notificationModels = new ArrayList<NotificationModel>();

    AlertDialog.Builder dialogBuilder;
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);
        clear_txt = findViewById(R.id.clear_txt);
        clear_txt.setTypeface(bold);
        clear_txt.setOnClickListener(this);

        checkInternet = NetworkChecking.isConnected(this);

        close = findViewById(R.id.close);
        close.setOnClickListener(this);

        no_data_img = findViewById(R.id.no_data_img);

        notification_recyclerview = findViewById(R.id.notification_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        notification_recyclerview.setLayoutManager(layoutManager);
        notificationAdapter = new NotificationAdapter(notificationModels, NotificationActivity.this, R.layout.row_notification);

        getNotification();
    }

    public void getNotification() {
        String url = AppUrls.BASE_URL + AppUrls.NOTIFICATIONS;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                notificationModels.clear();

                try {

                    JSONArray jsonArray = new JSONArray(response);

                    if (jsonArray.length() != 0) {

                        clear_txt.setVisibility(View.VISIBLE);
                        no_data_img.setVisibility(View.GONE);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            NotificationModel nm = new NotificationModel();
                            nm.setNotificationId(jsonObject.optString("notificationId"));
                            nm.setUserId(jsonObject.optString("userId"));
                            nm.setBookingId(jsonObject.optString("bookingId"));
                            nm.setDescription(jsonObject.optString("description"));
                            nm.setDeleted(jsonObject.optString("deleted"));
                            nm.setReadFlag(jsonObject.optString("readFlag"));
                            nm.setCreatedTime(jsonObject.optString("createdTime"));
                            nm.setModifiedTime(jsonObject.optString("modifiedTime"));
                            notificationModels.add(nm);
                        }

                        notification_recyclerview.setAdapter(notificationAdapter);
                        notificationAdapter.notifyDataSetChanged();
                    } else {
                        clear_txt.setVisibility(View.GONE);
                        no_data_img.setVisibility(View.VISIBLE);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(NotificationActivity.this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {

        if (v == close) {
            if (checkInternet) {
                Intent intent = new Intent(NotificationActivity.this, MainActivity.class);
                intent.putExtra("activity", "NotificationActivity");
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == clear_txt) {

            if (checkInternet) {

                showClearDialog(R.layout.warning_dialog);

                /*sweetAlertDialog = new SweetAlertDialog(NotificationActivity.this, SweetAlertDialog.WARNING_TYPE);
                sweetAlertDialog.setTitleText("Are you sure?");
                sweetAlertDialog.setContentText("You want to Clear All!");
                sweetAlertDialog.setCancelText("No, Keep It");
                sweetAlertDialog.setConfirmText("Yes, Clear");
                sweetAlertDialog.showCancelButton(true);

                sweetAlertDialog.setConfirmClickListener(sweetAlertDialog -> {

                    sweetAlertDialog.cancel();

                    clearNotifications();

                });

                sweetAlertDialog.setCancelClickListener(sDialog -> sDialog.cancel());
                sweetAlertDialog.show();*/


            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

    }

    private void showClearDialog(int layout) {
        dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(NotificationActivity.this);
        View layoutView = getLayoutInflater().inflate(layout, null);
        TextView title_txt = layoutView.findViewById(R.id.title_txt);
        title_txt.setTypeface(bold);
        title_txt.setText("Are you sure?");
        TextView msg_txt = layoutView.findViewById(R.id.msg_txt);
        msg_txt.setTypeface(regular);
        msg_txt.setText("You want to Clear All !");
        Button ok_btn = layoutView.findViewById(R.id.ok_btn);
        ok_btn.setTypeface(bold);
        ok_btn.setText("Yes, Clear");
        Button cancel_btn = layoutView.findViewById(R.id.cancel_btn);
        cancel_btn.setTypeface(bold);
        cancel_btn.setText("No, Keep It");
        dialogBuilder.setView(layoutView);
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();

        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialog.dismiss();

                clearNotifications();


            }
        });

        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialog.dismiss();

            }
        });
        alertDialog.setCancelable(false);
    }

    private void clearNotifications() {
        String url = AppUrls.BASE_URL + AppUrls.CLEAR_NOTIFICATIONS;

        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.optString("status");
                    String code = jsonObject.optString("code");
                    String message = jsonObject.optString("message");

                    getNotification();

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(NotificationActivity.this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(NotificationActivity.this, MainActivity.class);
        intent.putExtra("activity", "NotificationActivity");
        startActivity(intent);
    }
}
