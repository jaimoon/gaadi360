package in.g360.owner.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import cn.pedant.SweetAlert.SweetAlertDialog;
import in.g360.owner.MainActivity;
import in.g360.owner.R;
import in.g360.owner.adapters.AddonAdapter;
import in.g360.owner.adapters.InspAdapter;
import in.g360.owner.adapters.JobCardAdapter;
import in.g360.owner.adapters.PhotosAdapter;
import in.g360.owner.models.BookingAddonServicesModel;
import in.g360.owner.models.HistoryInspectionModel;
import in.g360.owner.models.JobCardModel;
import in.g360.owner.utils.AppUrls;
import in.g360.owner.utils.GlobalCalls;
import in.g360.owner.utils.NetworkChecking;
import in.g360.owner.utils.UserSessionManager;

public class HistoryDetailActivity extends AppCompatActivity implements View.OnClickListener {

    private FirebaseAnalytics mFirebaseAnalytics;
    ImageView close;
    private boolean checkInternet;
    String bookingId, accessToken, brandId, brandName, modelId, modelName, status, startRange, endRange,
            kilometerRange,walletAmount;
    UserSessionManager userSessionManager;
    TextView toolbar_title, booking_id_txt, otp_txt, brand_txt, model_txt, date_txt, time_txt, sst_name_txt, sst_person, sst_timings, sst_price,
            distance_txt, sst_address, service_detail_txt,
            pickup_txt, final_price_txt, discount_txt, bike_txt, sst_txt, service_txt, address_txt, est_price, est, est_price_txt, addon_price, addon,
            addon_price_txt, promo_price, promo, promo_price_txt, final_price, final_txt, photos_txt, addon_service_txt, insp_txt, moving_txt,
            kilometers_txt, reg_txt,wallet_txt, wallet_price_txt;
    RatingBar sst_rating;
    Typeface regular, bold;
    LinearLayout reschedule_ll;
    RelativeLayout photos_rl, addon_rl, insp_rl;
    Button concern_btn, rebook_btn, cancel_btn, reschedule_btn, jobcard_btn;
    SweetAlertDialog sweetAlertDialog;
    TableRow est_tr, addon_tr, promo_tr, final_tr,wallet_tr;
    AlertDialog.Builder dialogBuilder;
    AlertDialog dialog, alertDialog;
    EditText concern_edt;

    RecyclerView photos_recyclerview, addon_recyclerview, insp_recyclerview;
    PhotosAdapter photosAdapter;
    List<String> mylist = new ArrayList<String>();

    /*BikeBrand*/
    JobCardAdapter jobCardAdapter;
    List<JobCardModel> jobCardModels = new ArrayList<JobCardModel>();

    /*Addon's*/
    AddonAdapter addonAdapter;
    List<BookingAddonServicesModel> bookingAddonServicesModels = new ArrayList<BookingAddonServicesModel>();
    /*Inspection's*/
    InspAdapter inspAdapter;
    ArrayList<HistoryInspectionModel> historyInspectionModels = new ArrayList<HistoryInspectionModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_detail);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        checkInternet = NetworkChecking.isConnected(this);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);

        bookingId = getIntent().getStringExtra("bookingId");

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);

        close = findViewById(R.id.close);
        close.setOnClickListener(this);

        concern_btn = findViewById(R.id.concern_btn);
        concern_btn.setTypeface(bold);
        concern_btn.setOnClickListener(this);
        booking_id_txt = findViewById(R.id.booking_id_txt);
        booking_id_txt.setTypeface(bold);
        booking_id_txt.setText("Booking ID : " + bookingId);
        otp_txt = findViewById(R.id.otp_txt);
        otp_txt.setTypeface(bold);
        bike_txt = findViewById(R.id.bike_txt);
        bike_txt.setTypeface(bold);
        address_txt = findViewById(R.id.address_txt);
        address_txt.setTypeface(bold);
        sst_txt = findViewById(R.id.sst_txt);
        sst_txt.setTypeface(bold);
        service_txt = findViewById(R.id.service_txt);
        service_txt.setTypeface(bold);
        brand_txt = findViewById(R.id.brand_txt);
        brand_txt.setTypeface(regular);
        model_txt = findViewById(R.id.model_txt);
        model_txt.setTypeface(regular);
        kilometers_txt = findViewById(R.id.kilometers_txt);
        kilometers_txt.setTypeface(regular);
        reg_txt = findViewById(R.id.reg_txt);
        reg_txt.setTypeface(regular);
        date_txt = findViewById(R.id.date_txt);
        date_txt.setTypeface(regular);
        time_txt = findViewById(R.id.time_txt);
        time_txt.setTypeface(regular);
        sst_name_txt = findViewById(R.id.sst_name_txt);
        sst_name_txt.setTypeface(bold);
        sst_person = findViewById(R.id.sst_person);
        sst_person.setTypeface(regular);
        sst_timings = findViewById(R.id.sst_timings);
        sst_timings.setTypeface(regular);
        sst_price = findViewById(R.id.sst_price);
        sst_price.setTypeface(bold);
        sst_rating = findViewById(R.id.sst_rating);
        distance_txt = findViewById(R.id.distance_txt);
        distance_txt.setTypeface(regular);
        sst_address = findViewById(R.id.sst_address);
        sst_address.setTypeface(regular);
        service_detail_txt = findViewById(R.id.service_detail_txt);
        service_detail_txt.setTypeface(regular);
        pickup_txt = findViewById(R.id.pickup_txt);
        pickup_txt.setTypeface(regular);
        discount_txt = findViewById(R.id.discount_txt);
        discount_txt.setTypeface(bold);
        est_tr = findViewById(R.id.est_tr);
        wallet_tr = findViewById(R.id.wallet_tr);
        addon_tr = findViewById(R.id.addon_tr);
        promo_tr = findViewById(R.id.promo_tr);
        final_tr = findViewById(R.id.final_tr);
        est_price = findViewById(R.id.est_price);
        est_price.setTypeface(regular);
        est = findViewById(R.id.est);
        est.setTypeface(regular);
        wallet_txt = findViewById(R.id.wallet_txt);
        wallet_txt.setTypeface(regular);
        wallet_price_txt = findViewById(R.id.wallet_price_txt);
        wallet_price_txt.setTypeface(bold);
        est_price_txt = findViewById(R.id.est_price_txt);
        est_price_txt.setTypeface(bold);
        addon_price = findViewById(R.id.addon_price);
        addon_price.setTypeface(regular);
        addon = findViewById(R.id.addon);
        addon.setTypeface(regular);
        addon_price_txt = findViewById(R.id.addon_price_txt);
        addon_price_txt.setTypeface(bold);
        promo_price = findViewById(R.id.promo_price);
        promo_price.setTypeface(regular);
        promo = findViewById(R.id.promo);
        promo.setTypeface(regular);
        promo_price_txt = findViewById(R.id.promo_price_txt);
        promo_price_txt.setTypeface(bold);
        final_price = findViewById(R.id.final_price);
        final_price.setTypeface(regular);
        final_txt = findViewById(R.id.final_txt);
        final_txt.setTypeface(regular);
        final_price_txt = findViewById(R.id.final_price_txt);
        final_price_txt.setTypeface(bold);
        moving_txt = findViewById(R.id.moving_txt);
        moving_txt.setTypeface(bold);
        photos_rl = findViewById(R.id.photos_rl);
        photos_txt = findViewById(R.id.photos_txt);
        reschedule_ll = findViewById(R.id.reschedule_ll);
        reschedule_btn = findViewById(R.id.reschedule_btn);
        reschedule_btn.setTypeface(bold);
        reschedule_btn.setOnClickListener(this);
        cancel_btn = findViewById(R.id.cancel_btn);
        cancel_btn.setTypeface(bold);
        cancel_btn.setOnClickListener(this);
        jobcard_btn = findViewById(R.id.jobcard_btn);
        jobcard_btn.setTypeface(bold);
        jobcard_btn.setOnClickListener(this);
        rebook_btn = findViewById(R.id.rebook_btn);
        rebook_btn.setTypeface(bold);
        rebook_btn.setOnClickListener(this);

        insp_rl = findViewById(R.id.insp_rl);
        insp_txt = findViewById(R.id.insp_txt);
        insp_txt.setTypeface(bold);
        addon_rl = findViewById(R.id.addon_rl);
        addon_service_txt = findViewById(R.id.addon_service_txt);
        addon_service_txt.setTypeface(bold);

        insp_recyclerview = findViewById(R.id.insp_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        insp_recyclerview.setLayoutManager(layoutManager);
        inspAdapter = new InspAdapter(historyInspectionModels, HistoryDetailActivity.this, R.layout.row_insp);

        addon_recyclerview = findViewById(R.id.addon_recyclerview);
        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(getApplicationContext());
        addon_recyclerview.setLayoutManager(layoutManager1);
        addonAdapter = new AddonAdapter(bookingAddonServicesModels, HistoryDetailActivity.this, R.layout.row_addon);

        photos_recyclerview = findViewById(R.id.photos_recyclerview);
        photosAdapter = new PhotosAdapter(mylist, HistoryDetailActivity.this, R.layout.row_photos);
        photos_recyclerview.setLayoutManager(new GridLayoutManager(this, 1, GridLayoutManager.HORIZONTAL, false));

        getHistoryDetail();

    }

    private void getHistoryDetail() {
        String url = AppUrls.BASE_URL + AppUrls.HISTORY_DETAIL + bookingId + "/detail";

        Log.d("URL", url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(String response) {

                Log.d("RESP", response);

                try {
                    JSONObject jsonObj = new JSONObject(response);
                    String responceCode = jsonObj.optString("status");
                    String message = jsonObj.optString("message");

                    if (responceCode.equalsIgnoreCase("2000")) {

                        JSONObject jsonObject = jsonObj.getJSONObject("data");

                        String bookingId = jsonObject.optString("bookingId");
                        String userId = jsonObject.optString("userId");
                        String orderId = jsonObject.optString("orderId");
                        String serviceCenterId = jsonObject.optString("serviceCenterId");
                        String bookingType = jsonObject.optString("bookingType");
                        brandId = jsonObject.optString("brandId");
                        brandName = jsonObject.optString("brandName");
                        modelId = jsonObject.optString("modelId");
                        modelName = jsonObject.optString("modelName");
                        String kilometerRangeId = jsonObject.optString("kilometerRangeId");
                        String bookingDate = jsonObject.optString("bookingDate");
                        String bookingTime = jsonObject.optString("bookingTime");
                        String pickupAddress = jsonObject.optString("pickupAddress");
                        String pickupAddressLatitude = jsonObject.optString("pickupAddressLatitude");
                        String pickupAddressLongitude = jsonObject.optString("pickupAddressLongitude");
                        String addressType = jsonObject.optString("addressType");
                        String estimatedCost = jsonObject.optString("estimatedCost");
                        String finalPrice = jsonObject.optString("finalPrice");
                        String promocodeAmount = jsonObject.optString("promocodeAmount");
                        String initialPaidAmount = jsonObject.optString("initialPaidAmount");
                        String paymentStatus = jsonObject.optString("paymentStatus");
                        status = jsonObject.optString("status");
                        String deleted = jsonObject.optString("deleted");
                        String createdTime = jsonObject.optString("createdTime");
                        String modifiedTime = jsonObject.optString("modifiedTime");
                        String firstName = jsonObject.optString("firstName");
                        String lastName = jsonObject.optString("lastName");
                        String contactName = jsonObject.optString("contactName");
                        String serviceCenterName = jsonObject.optString("serviceCenterName");
                        String contactMobileNumber = jsonObject.optString("contactMobileNumber");
                        String averageRating = jsonObject.optString("averageRating");
                        String mobile = jsonObject.optString("mobile");
                        String serviceCenterLocation = jsonObject.optString("serviceCenterLocation");
                        String rescheduleCount = jsonObject.optString("rescheduleCount");
                        String rescheduleStatus = jsonObject.optString("rescheduleStatus");
                        String cancelStatus = jsonObject.optString("cancelStatus");
                        String paidAmount = jsonObject.optString("paidAmount");
                        String addonsAmount = jsonObject.optString("addonsAmount");
                        String pickupOtp = jsonObject.optString("pickupOtp");
                        String dropoffOtp = jsonObject.optString("dropoffOtp");
                        String registrationNumber = jsonObject.optString("registrationNumber");
                        Boolean movingCondition = jsonObject.optBoolean("movingCondition");
                        walletAmount = jsonObject.optString("walletAmount");

                        reg_txt.setText(registrationNumber);

                        if (movingCondition) {
                            moving_txt.setTextColor(ContextCompat.getColor(HistoryDetailActivity.this, R.color.green));
                            moving_txt.setText("Vehicle Is In Moving Condition");
                        } else {
                            moving_txt.setTextColor(ContextCompat.getColor(HistoryDetailActivity.this, R.color.red));
                            moving_txt.setText("Vehicle Is Not In Moving Condition");
                        }

                        if (jsonObject.has("kilometerRange")) {
                            JSONObject jsonObject1 = jsonObject.getJSONObject("kilometerRange");
                            startRange = jsonObject1.optString("startRange");
                            endRange = jsonObject1.optString("endRange");

                            kilometers_txt.setText(startRange + " - " + endRange + " km");
                        }

                        String services = "", inspections = "", addOnServices = "";
                        if (jsonObject.has("services")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("services");

                            services = IntStream.range(0, jsonArray.length()).mapToObj(i -> {
                                try {
                                    return jsonArray.getJSONObject(i).optString("serviceName");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    return "";
                                }
                            }).collect(Collectors.joining("\n"));

                            service_detail_txt.setText(services);
                        }

                        if (jsonObject.has("inspections")) {

                            JSONArray jsonArray = jsonObject.getJSONArray("inspections");

                            inspections = IntStream.range(0, jsonArray.length()).mapToObj(i -> {
                                try {
                                    return jsonArray.getJSONObject(i).optString("inspectionName");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    return "";
                                }
                            }).collect(Collectors.joining("\n"));

                            if (jsonArray.length() > 0) {

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    HistoryInspectionModel him = new HistoryInspectionModel();
                                    him.setBookingId(jsonObject1.optString("bookingId"));
                                    him.setInspectionId(jsonObject1.optString("inspectionId"));
                                    him.setState(jsonObject1.optString("state"));
                                    him.setInspectionName(jsonObject1.optString("inspectionName"));

                                    historyInspectionModels.add(him);
                                }

                                insp_recyclerview.setAdapter(inspAdapter);
                                inspAdapter.notifyDataSetChanged();
                            } else {
                                insp_rl.setVisibility(View.GONE);
                            }
                        }

                        if (jsonObject.has("fileIds")) {

                            photos_txt.setVisibility(View.VISIBLE);
                            photos_rl.setVisibility(View.VISIBLE);
                            photos_recyclerview.setVisibility(View.VISIBLE);

                            JSONArray jsonArray = jsonObject.getJSONArray("fileIds");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                String data = jsonArray.get(i).toString();
                                mylist.add(AppUrls.IMAGE_URL + data);
                            }

                            Log.d("PHOTOS", String.valueOf(mylist));

                            photos_recyclerview.setAdapter(photosAdapter);
                            photosAdapter.notifyDataSetChanged();

                        } else {
                            photos_txt.setVisibility(View.GONE);
                            photos_rl.setVisibility(View.GONE);
                            photos_recyclerview.setVisibility(View.GONE);
                        }


                        if (jsonObject.has("bookingAddonServices")) {

                            JSONArray jsonArray = jsonObject.getJSONArray("bookingAddonServices");

                            addOnServices = IntStream.range(0, jsonArray.length()).mapToObj(i -> {
                                try {
                                    return jsonArray.getJSONObject(i).optString("serviceName");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    return "";
                                }
                            }).collect(Collectors.joining("\n"));

                            if (jsonArray.length() > 0) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    BookingAddonServicesModel basm = new BookingAddonServicesModel();
                                    basm.setBookingId(jsonObject1.optString("bookingId"));
                                    basm.setServiceId(jsonObject1.optString("serviceId"));
                                    basm.setServiceName(jsonObject1.optString("serviceName"));
                                    basm.setCost(jsonObject1.optString("cost"));
                                    basm.setStatus(jsonObject1.optString("status"));

                                    bookingAddonServicesModels.add(basm);
                                }
                                addon_recyclerview.setAdapter(addonAdapter);
                                addonAdapter.notifyDataSetChanged();
                            } else {
                                addon_rl.setVisibility(View.GONE);
                            }

                            //service_detail_txt.setText(services + "\n" + inspections + "\n" + addOnServices);
                        }

                        if (jsonObject.has("kilometerRange")) {
                            JSONObject jsonObject1 = jsonObject.getJSONObject("kilometerRange");
                            startRange = jsonObject1.optString("startRange");
                            endRange = jsonObject1.optString("endRange");
                        }

                        String resultDate = convertStringDateToAnotherStringDate(bookingDate, "yyyy-MM-dd", "dd-MM-yyyy");
                        Log.d("ResultDate", resultDate);

                        String time1 = bookingTime;
                        String time2 = "05:30:00";

                        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
                        timeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

                        Date date1 = null, date2 = null;
                        long sum;
                        try {
                            date1 = timeFormat.parse(time1);
                            date2 = timeFormat.parse(time2);
                            sum = date1.getTime() + date2.getTime();
                            bookingTime = timeFormat.format(new Date(sum));

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        Log.d("Result", bookingTime);

                        brand_txt.setText(brandName);
                        model_txt.setText(modelName);
                        date_txt.setText(resultDate);

                        try {
                            SimpleDateFormat dateFormatter = new SimpleDateFormat("HH:mm:ss");
                            Date date = dateFormatter.parse(bookingTime);
                            SimpleDateFormat timeFormatter = new SimpleDateFormat("h:mm a");
                            //SimpleDateFormat timeFormatter = new SimpleDateFormat("h a");
                            bookingTime = timeFormatter.format(date);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        time_txt.setText(bookingTime);
                        sst_name_txt.setText(serviceCenterName);
                        sst_person.setText(contactName);
                        sst_price.setText("\u20B9 " + finalPrice);
                        sst_address.setText(serviceCenterLocation);
                        sst_rating.setRating(Float.parseFloat(averageRating));
                        pickup_txt.setText(pickupAddress);
                        est_price_txt.setText("\u20B9 " + estimatedCost);
                        if (addonsAmount.equalsIgnoreCase("0")) {
                            addon_tr.setVisibility(View.GONE);
                        } else {
                            addon_tr.setVisibility(View.VISIBLE);
                            addon_price_txt.setText("\u20B9 " + addonsAmount);
                        }
                        if (promocodeAmount.equalsIgnoreCase("0")) {
                            promo_tr.setVisibility(View.GONE);
                            discount_txt.setVisibility(View.GONE);
                        } else {
                            //discount_txt.setVisibility(View.VISIBLE);
                            //discount_txt.setText("You Availed Offer of \u20B9 " + promocodeAmount);
                            promo_tr.setVisibility(View.VISIBLE);
                            promo_price_txt.setText("\u20B9 " + promocodeAmount);
                        }

                        if (walletAmount.equalsIgnoreCase("0")) {
                            wallet_tr.setVisibility(View.GONE);
                        } else {
                            wallet_tr.setVisibility(View.VISIBLE);
                            wallet_price_txt.setText("\u20B9 " + walletAmount);
                        }


                        final_price_txt.setText("\u20B9 " + paidAmount);

                        int otpStatus = Integer.parseInt(status);
                        if (otpStatus < 3) {
                            otp_txt.setVisibility(View.VISIBLE);
                            otp_txt.setText("Pick Up Otp : " + pickupOtp);
                        } else if (otpStatus != 7 && otpStatus != 12) {
                            otp_txt.setVisibility(View.VISIBLE);
                            otp_txt.setText("Drop Off Otp : " + dropoffOtp);
                        }

                        if (status.equalsIgnoreCase("1")) {

                            if (rescheduleStatus.equalsIgnoreCase("1")) {
                                reschedule_btn.setVisibility(View.VISIBLE);
                            } else {
                                reschedule_btn.setVisibility(View.GONE);
                            }
                            if (cancelStatus.equalsIgnoreCase("1")) {
                                cancel_btn.setVisibility(View.VISIBLE);
                            } else {
                                cancel_btn.setVisibility(View.GONE);
                            }

                            rebook_btn.setVisibility(View.GONE);

                        } else if (status.equalsIgnoreCase("7") || status.equalsIgnoreCase("12")) {
                            concern_btn.setVisibility(View.VISIBLE);
                            jobcard_btn.setVisibility(View.VISIBLE);
                            reschedule_ll.setVisibility(View.GONE);
                            rebook_btn.setVisibility(View.VISIBLE);
                        } else {
                            reschedule_ll.setVisibility(View.GONE);
                            rebook_btn.setVisibility(View.GONE);

                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(HistoryDetailActivity.this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {
        if (v == close) {
            if (checkInternet) {
                Intent intent = new Intent(HistoryDetailActivity.this, HistoryActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == concern_btn) {
            showConcernAlertDialog(R.layout.dialog_concern_layout);
        }

        if (v == jobcard_btn) {

            showJobCard();
        }

        if (v == rebook_btn) {

            Bundle bundle = new Bundle();
            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ID-RebookActivity");
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "RebookActivity");
            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
            //mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

            //Logs an app event.
            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

            //Sets whether analytics collection is enabled for this app on this device.
            mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);

            //Sets the minimum engagement time required before starting a session. The default value is 10000 (10 seconds). Let's make it 20 seconds just for the fun
            mFirebaseAnalytics.setMinimumSessionDuration(20000);

            //Sets the duration of inactivity that terminates the current session. The default value is 1800000 (30 minutes).
            mFirebaseAnalytics.setSessionTimeoutDuration(500);

            //Sets the user ID property.
            mFirebaseAnalytics.setUserId("Gaadi360");

            //Sets a user property to a given value.
            mFirebaseAnalytics.setUserProperty("RebookActivity", "Gaadi360");

            if (checkInternet) {
                Intent intent = new Intent(HistoryDetailActivity.this, MainActivity.class);
                intent.putExtra("activity", "HistoryDetailActivity");
                intent.putExtra("brandId", brandId);
                intent.putExtra("modelId", modelId);
                intent.putExtra("brandName", brandName);
                intent.putExtra("modelName", modelName);
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == reschedule_btn) {
            if (checkInternet) {

                kilometerRange = startRange + " - " + endRange;

                Intent intent = new Intent(HistoryDetailActivity.this, RescheduleActivity.class);
                intent.putExtra("brandName", brandName);
                intent.putExtra("modelName", modelName);
                intent.putExtra("kilometerRange", kilometerRange);
                intent.putExtra("bookingId", bookingId);
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == cancel_btn) {
            if (checkInternet) {

                showClearDialog(R.layout.warning_dialog);

                /*sweetAlertDialog = new SweetAlertDialog(HistoryDetailActivity.this, SweetAlertDialog.WARNING_TYPE);
                sweetAlertDialog.setTitleText("Are you sure?");
                sweetAlertDialog.setCancelText("No, Stay");
                sweetAlertDialog.setConfirmText("Yes, Cancel");
                sweetAlertDialog.showCancelButton(true);

                sweetAlertDialog.setConfirmClickListener(sweetAlertDialog -> {

                    sweetAlertDialog.cancel();
                    cancelBooking();

                });

                sweetAlertDialog.setCancelClickListener(sDialog -> sDialog.cancel());
                sweetAlertDialog.show();*/


            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }

    private void showClearDialog(int layout) {
        dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(HistoryDetailActivity.this);
        View layoutView = getLayoutInflater().inflate(layout, null);
        TextView title_txt = layoutView.findViewById(R.id.title_txt);
        title_txt.setTypeface(bold);
        title_txt.setText("Are you sure?");
        TextView msg_txt = layoutView.findViewById(R.id.msg_txt);
        msg_txt.setTypeface(regular);
        msg_txt.setVisibility(View.GONE);
        Button ok_btn = layoutView.findViewById(R.id.ok_btn);
        ok_btn.setTypeface(bold);
        ok_btn.setText("Yes, Cancel");
        Button cancel_btn = layoutView.findViewById(R.id.cancel_btn);
        cancel_btn.setTypeface(bold);
        cancel_btn.setText("No, Stay");
        dialogBuilder.setView(layoutView);
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();

        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialog.dismiss();

                cancelBooking();


            }
        });

        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialog.dismiss();

            }
        });
        alertDialog.setCancelable(false);
    }

    private void showJobCard() {

        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            jobCardModels.clear();

            String url = AppUrls.BASE_URL + "booking/" + bookingId + "/jobcard";
            Log.d("URL",url);

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    response -> {

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            Log.d("RESPONSE",response);

                            String status = jsonObject.optString("status");
                            String code = jsonObject.optString("code");
                            if (status.equals("2000")) {
                                //flipProgressDialog.dismiss();



                                JSONObject jsonObject1 = jsonObject.optJSONObject("data");
                                JSONArray jsonArray = jsonObject1.optJSONArray("jobCard");

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                                    JobCardModel jobCardModel = new JobCardModel();
                                    jobCardModel.setBookingId(jsonObject2.optString("bookingId"));
                                    jobCardModel.setServiceId(jsonObject2.optString("serviceId"));
                                    jobCardModel.setPerform(jsonObject2.optString("perform"));
                                    jobCardModel.setServiceItem(jsonObject2.optString("serviceItem"));
                                    jobCardModel.setServiceName(jsonObject2.optString("serviceName"));
                                    jobCardModel.setStatus(jsonObject2.optString("status"));

                                    jobCardModels.add(jobCardModel);
                                }

                                jobCardDialog();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    },
                    error -> {

                    }) {
            };
            RequestQueue requestQueue = Volley.newRequestQueue(HistoryDetailActivity.this);
            requestQueue.add(stringRequest);

        } else {

            GlobalCalls.showToast("Please Check Internet Connection..!", HistoryDetailActivity.this);

        }


    }

    private void jobCardDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(HistoryDetailActivity.this);
        LayoutInflater inflater = HistoryDetailActivity.this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.jobcard_dialog, null);

        TextView toolbar_title = dialog_layout.findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);

        RecyclerView jobcard_recyclerview = dialog_layout.findViewById(R.id.jobcard_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        jobcard_recyclerview.setLayoutManager(layoutManager);

        jobCardAdapter = new JobCardAdapter(jobCardModels, HistoryDetailActivity.this, R.layout.row_jobcard);
        jobcard_recyclerview.setAdapter(jobCardAdapter);

        builder.setView(dialog_layout).setNegativeButton("CANCEL", (dialogInterface, i) -> {

            dialogInterface.dismiss();
        });

        dialog = builder.create();
        dialog.show();
    }

    private void showConcernAlertDialog(int layout) {
        dialogBuilder = new AlertDialog.Builder(HistoryDetailActivity.this);
        View layoutView = getLayoutInflater().inflate(layout, null);
        concern_edt = layoutView.findViewById(R.id.concern_edt);
        Button cancel_btn = layoutView.findViewById(R.id.cancel_btn);
        Button submit_btn = layoutView.findViewById(R.id.submit_btn);
        dialogBuilder.setView(layoutView);
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!concern_edt.getText().toString().equalsIgnoreCase("")) {
                    sendConcern();
                } else {
                    concern_edt.setError("Please Enter Concern");
                }

            }
        });
        alertDialog.setCancelable(false);
    }

    private void sendConcern() {
        String url = AppUrls.BASE_URL + AppUrls.CONCERN;

        JSONObject jsonObj = new JSONObject();

        try {
            jsonObj.put("bookingId", bookingId);
            jsonObj.put("concernMessage", concern_edt.getText().toString());

        } catch (Exception e) {

        }

        Log.d("JsonObj", jsonObj.toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("ConcernStatus", response.toString());

                try {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    String status = jsonObject.optString("status");
                    String message = jsonObject.optString("message");

                    Log.d("Status", status + "\n" + message);

                    if (status.equalsIgnoreCase("2000")) {
                        alertDialog.dismiss();
                        GlobalCalls.showToast("Your Concern Submitted Successfully..!", HistoryDetailActivity.this);

                    } else {
                        GlobalCalls.showToast("Something went wrong..!", HistoryDetailActivity.this);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                    Log.d("ResError", e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

                Log.d("EROORRR", error.getMessage());

            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(HistoryDetailActivity.this);
        requestQueue.add(request);
    }

    private void cancelBooking() {
        String url = AppUrls.BASE_URL + AppUrls.CANCEL_BOOKING + bookingId + "/cancel";

        StringRequest stringRequest = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {


                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String code = jsonObject.optString("code");
                    String status = jsonObject.optString("status");
                    String message = jsonObject.optString("message");

                    GlobalCalls.showToast(message, HistoryDetailActivity.this);

                    Bundle bundle = new Bundle();
                    bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ID-CancelActivity");
                    bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "CancelActivity");
                    bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
                    //mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

                    //Logs an app event.
                    mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

                    //Sets whether analytics collection is enabled for this app on this device.
                    mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);

                    //Sets the minimum engagement time required before starting a session. The default value is 10000 (10 seconds). Let's make it 20 seconds just for the fun
                    mFirebaseAnalytics.setMinimumSessionDuration(20000);

                    //Sets the duration of inactivity that terminates the current session. The default value is 1800000 (30 minutes).
                    mFirebaseAnalytics.setSessionTimeoutDuration(500);

                    //Sets the user ID property.
                    mFirebaseAnalytics.setUserId("Gaadi360");

                    //Sets a user property to a given value.
                    mFirebaseAnalytics.setUserProperty("CancelActivity", "Gaadi360");

                    Intent intent = new Intent(HistoryDetailActivity.this, HistoryActivity.class);
                    startActivity(intent);

                    if (status.equalsIgnoreCase("5038")) {
                        GlobalCalls.showToast(message, HistoryDetailActivity.this);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(HistoryDetailActivity.this);
        requestQueue.add(stringRequest);
    }

    public String convertStringDateToAnotherStringDate(String stringdate, String stringdateformat, String returndateformat) {

        try {
            Date date = new SimpleDateFormat(stringdateformat).parse(stringdate);
            String returndate = new SimpleDateFormat(returndateformat).format(date);
            return returndate;
        } catch (ParseException e) {
            e.printStackTrace();
            return stringdate;
        }

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(HistoryDetailActivity.this, HistoryActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
