package in.g360.owner.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import org.apache.commons.codec.binary.StringUtils;

import java.util.HashMap;

import in.g360.owner.MainActivity;
import in.g360.owner.R;
import in.g360.owner.utils.NetworkChecking;
import in.g360.owner.utils.UserSessionManager;

public class SuccessActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close;
    TextView toolbar_title;
    private boolean checkInternet;
    Typeface regular, bold;

    String accessToken, userEmail;
    UserSessionManager userSessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);
        userEmail = userDetails.get(UserSessionManager.USER_EMAIL);
        Log.d("Email", "->" + userEmail + "-<");

        checkInternet = NetworkChecking.isConnected(this);

        close = findViewById(R.id.close);
        close.setOnClickListener(this);

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);

        if (userEmail == null || userEmail.trim().equalsIgnoreCase("")) {

            AlertDialog alertDialog = new AlertDialog.Builder(SuccessActivity.this).create();
            alertDialog.setTitle("Alert");
            alertDialog.setMessage("Please update your email address in your profile page so we can send your service job card.");
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            Intent intent = new Intent(SuccessActivity.this, EditProfileActivity.class);
                            startActivity(intent);
                        }
                    });

            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "CANCEL",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            Intent intent = new Intent(SuccessActivity.this, MainActivity.class);
                            intent.putExtra("activity", "SuccessActivity");
                            startActivity(intent);
                        }
                    });
            alertDialog.setCancelable(false);
            alertDialog.show();

        } else {
            Intent intent = new Intent(SuccessActivity.this, MainActivity.class);
            intent.putExtra("activity", "SuccessActivity");
            startActivity(intent);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == close) {
            if (checkInternet) {
                Intent intent = new Intent(SuccessActivity.this, MainActivity.class);
                intent.putExtra("activity", "SuccessActivity");
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(SuccessActivity.this, MainActivity.class);
        intent.putExtra("activity", "SuccessActivity");
        startActivity(intent);
    }
}
