package in.g360.owner.activities;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import in.g360.owner.R;
import in.g360.owner.adapters.RescheduleTimeSlotAdapter;
import in.g360.owner.models.TimeSlotsModel;
import in.g360.owner.utils.AppUrls;
import in.g360.owner.utils.GlobalCalls;
import in.g360.owner.utils.NetworkChecking;
import in.g360.owner.utils.UserSessionManager;

public class RescheduleActivity extends AppCompatActivity implements View.OnClickListener {

    private FirebaseAnalytics mFirebaseAnalytics;
    ImageView close;
    TextView toolbar_title, bike_details_txt, bike_brand, bike_model, bike_kilometers, date, time;
    Button next_btn;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    private int mYear, mMonth, mDay, hours, minutes, seconds;
    String accessToken, bookingId, brandName, modelName, currentDate, currentTime, sendStartTime, sendEndTime, sendDisplayTime, kilometerRange,
            preBookingDays, pickupStartTime;

    /*Time Slots*/
    RescheduleTimeSlotAdapter timeSlotAdapter;
    ArrayList<TimeSlotsModel> timeSlotsModels = new ArrayList<TimeSlotsModel>();
    AlertDialog dialog2;
    Typeface regular, bold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reschedule);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        checkInternet = NetworkChecking.isConnected(this);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);

        bookingId = getIntent().getStringExtra("bookingId");
        brandName = getIntent().getStringExtra("brandName");
        modelName = getIntent().getStringExtra("modelName");
        kilometerRange = getIntent().getStringExtra("kilometerRange");

        close = findViewById(R.id.close);
        close.setOnClickListener(this);

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);
        bike_details_txt = findViewById(R.id.bike_details_txt);
        bike_details_txt.setTypeface(bold);
        bike_brand = findViewById(R.id.bike_brand);
        bike_brand.setTypeface(regular);
        bike_brand.setText(brandName);
        bike_model = findViewById(R.id.bike_model);
        bike_model.setTypeface(regular);
        bike_model.setText(modelName);
        bike_kilometers = findViewById(R.id.bike_kilometers);
        bike_kilometers.setTypeface(regular);
        bike_kilometers.setText(kilometerRange);
        date = findViewById(R.id.date);
        date.setTypeface(regular);
        date.setOnClickListener(this);
        time = findViewById(R.id.time);
        time.setTypeface(regular);
        time.setOnClickListener(this);
        next_btn = findViewById(R.id.next_btn);
        next_btn.setTypeface(bold);
        next_btn.setOnClickListener(this);

        currentDate = new SimpleDateFormat("d-M-yyyy", Locale.getDefault()).format(new Date());
        hours = new Time(System.currentTimeMillis()).getHours();
        minutes = new Time(System.currentTimeMillis()).getMinutes();
        seconds = new Time(System.currentTimeMillis()).getSeconds();
        currentTime = hours + ":" + minutes + ":" + seconds;

        Log.d("Current", hours + ":" + minutes + ":" + seconds + "\n" + currentDate);

        getOnGoingActivity();

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ID-RescheduleActivity");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "RescheduleActivity");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
        //mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

        //Logs an app event.
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

        //Sets whether analytics collection is enabled for this app on this device.
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);

        //Sets the minimum engagement time required before starting a session. The default value is 10000 (10 seconds). Let's make it 20 seconds just for the fun
        mFirebaseAnalytics.setMinimumSessionDuration(20000);

        //Sets the duration of inactivity that terminates the current session. The default value is 1800000 (30 minutes).
        mFirebaseAnalytics.setSessionTimeoutDuration(500);

        //Sets the user ID property.
        mFirebaseAnalytics.setUserId("Gaadi360");

        //Sets a user property to a given value.
        mFirebaseAnalytics.setUserProperty("RescheduleActivity", "Gaadi360");
    }

    private void getOnGoingActivity() {

        String url = AppUrls.BASE_URL + AppUrls.CURRENT_STATUS;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String responceCode = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    Log.d("SPLASHRESP", response);

                    if (responceCode.equalsIgnoreCase("2000")) {

                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        String bookingId = jsonObject1.getString("bookingId");
                        String paymentStatus = jsonObject1.getString("paymentStatus");
                        String status = jsonObject1.getString("status");
                        String ongoingBooking = jsonObject1.getString("ongoingBooking");

                        JSONObject jsonObject2 = jsonObject1.getJSONObject("setting");
                        preBookingDays = jsonObject2.getString("preBookingDays");

                        checkTimeSlots();
                        //getDate();

                    } else {

                        GlobalCalls.showToast(message, RescheduleActivity.this);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            GlobalCalls.showToast("Connection Not Found..!", RescheduleActivity.this);

                        } else if (error instanceof AuthFailureError) {

                            GlobalCalls.showToast("User Not Found..!", RescheduleActivity.this);

                        } else if (error instanceof ServerError) {

                            GlobalCalls.showToast("Server Not Found..!", RescheduleActivity.this);

                        } else if (error instanceof NetworkError) {
                            GlobalCalls.showToast("Network Not Found..!", RescheduleActivity.this);
                        } else if (error instanceof ParseError) {
                            GlobalCalls.showToast("Please Try Later..!", RescheduleActivity.this);
                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(RescheduleActivity.this);
        requestQueue.add(stringRequest);

    }


    @Override
    public void onClick(View v) {
        if (v == close) {
            if (checkInternet) {

                finish();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == date) {
            if (checkInternet) {

                checkTimeSlots();
                //getDate();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == time) {
            if (checkInternet) {

                if (date.getText().toString().equalsIgnoreCase("Booking Date")) {
                    GlobalCalls.showToast("Select Booking Date", RescheduleActivity.this);
                } else {
                    getTimeSlots();
                }

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == next_btn) {
            if (checkInternet) {

                rescheduleBooking();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }

    private void checkTimeSlots() {

        checkInternet = NetworkChecking.isConnected(getApplicationContext());

        if (checkInternet) {

            timeSlotsModels.clear();

            String url = AppUrls.BASE_URL + AppUrls.TIME_SLOTS;

            StringRequest string_request = new StringRequest(Request.Method.GET, url,
                    response -> {

                        Log.d("TIMESLOTRESPONCE", response);

                        SimpleDateFormat sdfStopTime = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
                        Calendar now = Calendar.getInstance();
                        now.add(Calendar.HOUR, 1);
                        Date d = now.getTime();
                        String afterime = sdfStopTime.format(d);
                        Log.d("CCHCH", afterime);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String responceCode = jsonObject.getString("status");

                            if (responceCode.equals("2000")) {

                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    TimeSlotsModel tsm = new TimeSlotsModel();
                                    String pickupScheduleId = jsonObject1.getString("pickupScheduleId");
                                    pickupStartTime = jsonObject1.getString("pickupStartTime");
                                    String pickupEndTime = jsonObject1.getString("pickupEndTime");
                                    String displayName = jsonObject1.getString("displayName");

                                    tsm.setPickupScheduleId(pickupScheduleId);
                                    tsm.setPickupStartTime(pickupStartTime);
                                    tsm.setPickupEndTime(pickupEndTime);
                                    tsm.setDisplayName(displayName);

                                    if (date.getText().toString().equals(currentDate)) {
                                        if (checkTimeslots(pickupStartTime, afterime)) {
                                            Log.d("Times", pickupStartTime + "\n" + pickupEndTime + "\n" + afterime);
                                            timeSlotsModels.add(tsm);
                                        }
                                    } else {
                                        timeSlotsModels.add(tsm);
                                    }
                                }

                                if (timeSlotsModels.size() != 0) {

                                    //time.setText("Booking Time");

                                    getDate();

                                } else {

                                    getTomarrowDate();
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }, error -> {

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(string_request);
        } else {
            GlobalCalls.showToast("No Internet Connection...!", RescheduleActivity.this);

        }

    }

    public void getDate() {

        SimpleDateFormat sdfStopTime = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
        Calendar now = Calendar.getInstance();
        now.add(Calendar.HOUR, 1);
        Date d = now.getTime();
        String afterime = sdfStopTime.format(d);
        Log.d("CCHCH", afterime);

        final Calendar calendarMax = Calendar.getInstance();
        calendarMax.add(Calendar.DAY_OF_MONTH, Integer.parseInt(preBookingDays));
        final Calendar calendarToday = Calendar.getInstance();

        if (checkTimeslots(pickupStartTime, afterime)) {

            Log.d("CheckTimes", "IN");


        } else {

            calendarToday.add(Calendar.DAY_OF_MONTH, 1);
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        String strDate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;

                        Log.d("Day", String.valueOf(dayOfMonth));
                        Log.d("Month", String.valueOf(monthOfYear + 1));
                        Log.d("Year", String.valueOf(year));

                        date.setBackgroundResource(R.drawable.rounded_border);
                        date.setTextColor(Color.BLACK);
                        date.setText(strDate);

                        //editor.putString("bookingDate", date.getText().toString());
                        //editor.apply();

                        time.setText("Booking Time");
                        if (time.getText().toString().equalsIgnoreCase("Booking Time")) {
                            getTimeSlots();
                        }

                    }

                }, mYear, mMonth, mDay);

        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {


            }
        });

        //datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.getDatePicker().setMinDate(calendarToday.getTimeInMillis());
        datePickerDialog.getDatePicker().setMaxDate(calendarMax.getTimeInMillis());
        datePickerDialog.show();
    }

    /*private void getTimeSlots() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {

            timeSlotsModels.clear();

            String url = AppUrls.TEST_BASE_URL + AppUrls.TIME_SLOTS;

            StringRequest string_request = new StringRequest(Request.Method.GET, url,
                    response -> {

                        Log.d("TIMESLOTRESPONCE", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String responceCode = jsonObject.getString("status");

                            if (responceCode.equals("2000")) {


                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    TimeSlotsModel tsm = new TimeSlotsModel();
                                    String pickupScheduleId = jsonObject1.getString("pickupScheduleId");
                                    String pickupStartTime = jsonObject1.getString("pickupStartTime");
                                    String pickupEndTime = jsonObject1.getString("pickupEndTime");
                                    String displayName = jsonObject1.getString("displayName");

                                    tsm.setPickupScheduleId(pickupScheduleId);
                                    tsm.setPickupStartTime(pickupStartTime);
                                    tsm.setPickupEndTime(pickupEndTime);
                                    tsm.setDisplayName(displayName);

                                    if (date.getText().toString().equals(currentDate)) {
                                        if (checkTimeslots(pickupStartTime, currentTime)) {
                                            Log.d("Times", pickupStartTime + "\n" + pickupEndTime + "\n" + currentTime);
                                            timeSlotsModels.add(tsm);
                                        }
                                    } else {
                                        timeSlotsModels.add(tsm);
                                    }
                                }

                                if (timeSlotsModels.size() != 0) {
                                    setTimeSlot(timeSlotsModels.get(0).getPickupStartTime(), timeSlotsModels.get(0).getPickupEndTime(),
                                            timeSlotsModels.get(0).getDisplayName());
                                    timeSlotDialog();
                                } else {
                                    Toast.makeText(this, "No TimeSlots Available..!", Toast.LENGTH_SHORT).show();
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }, error -> {

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(string_request);
        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }*/

    private void getTimeSlots() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {

            timeSlotsModels.clear();
            //flipProgressDialog.show(getFragmentManager(), "");

            String url = AppUrls.BASE_URL + AppUrls.TIME_SLOTS;

            StringRequest string_request = new StringRequest(Request.Method.GET, url,
                    response -> {
                        //flipProgressDialog.dismiss();

                        Log.d("TIMESLOTRESPONCE", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String responceCode = jsonObject.getString("status");

                            if (responceCode.equals("2000")) {
                                //flipProgressDialog.dismiss();

                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    TimeSlotsModel tsm = new TimeSlotsModel();
                                    String pickupScheduleId = jsonObject1.getString("pickupScheduleId");
                                    String pickupStartTime = jsonObject1.getString("pickupStartTime");
                                    String pickupEndTime = jsonObject1.getString("pickupEndTime");
                                    String displayName = jsonObject1.getString("displayName");

                                    tsm.setPickupScheduleId(pickupScheduleId);
                                    tsm.setPickupStartTime(pickupStartTime);
                                    tsm.setPickupEndTime(pickupEndTime);
                                    tsm.setDisplayName(displayName);

                                    /*if (date.getText().toString().equals(currentDate)) {
                                        if (checkTimeslots(pickupStartTime, currentTime)) {
                                            Log.d("Times", pickupStartTime + "\n" + pickupEndTime + "\n" + currentTime);
                                            timeSlotsModels.add(tsm);
                                        }
                                    } else {
                                        timeSlotsModels.add(tsm);
                                    }*/

                                    SimpleDateFormat sdfStopTime = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
                                    Calendar now = Calendar.getInstance();
                                    now.add(Calendar.HOUR, 1);
                                    Date d = now.getTime();
                                    String afterime = sdfStopTime.format(d);
                                    Log.d("CCHCH", afterime);

                                    if (date.getText().toString().equals(currentDate)) {
                                        if (checkTimeslots(pickupStartTime, afterime)) {
                                            Log.d("Times", pickupStartTime + "\n" + pickupEndTime + "\n" + afterime);
                                            timeSlotsModels.add(tsm);
                                        }
                                    } else {
                                        timeSlotsModels.add(tsm);
                                    }
                                }

                                if (timeSlotsModels.size() != 0) {
                                    setTimeSlot(timeSlotsModels.get(0).getPickupStartTime(), timeSlotsModels.get(0).getPickupEndTime(),
                                            timeSlotsModels.get(0).getDisplayName());
                                    timeSlotDialog();
                                } else {
                                    GlobalCalls.showToast("No TimeSlots Available..!", RescheduleActivity.this);
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }, error -> {

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(string_request);
        } else {
            GlobalCalls.showToast("No Internet Connection..!", RescheduleActivity.this);
        }

    }

    private void timeSlotDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(RescheduleActivity.this);
        LayoutInflater inflater = RescheduleActivity.this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.timeslot_dialog, null);

        RecyclerView timeslot_recyclerview = dialog_layout.findViewById(R.id.timeslot_recyclerview);
        GridLayoutManager layoutManager = new GridLayoutManager(RescheduleActivity.this, 2);
        layoutManager.setOrientation(GridLayoutManager.VERTICAL);
        timeslot_recyclerview.setLayoutManager(layoutManager);
        timeSlotAdapter = new RescheduleTimeSlotAdapter(timeSlotsModels, RescheduleActivity.this, R.layout.row_time_slots);
        timeslot_recyclerview.setAdapter(timeSlotAdapter);

        builder.setView(dialog_layout).setNegativeButton("Done", (dialogInterface, i) -> {

            dialogInterface.dismiss();
        });

        dialog2 = builder.create();
        dialog2.show();

    }

    public void setTimeSlot(String startTime, String endTime, String displayTime) {

        sendStartTime = startTime;
        sendEndTime = endTime;
        sendDisplayTime = displayTime;

        time.setBackgroundResource(R.drawable.rounded_border);
        time.setTextColor(Color.BLACK);
        time.setText(displayTime);

        Log.d("TIMESLOTS:", sendStartTime + "::::" + sendEndTime);

        Log.d("RESCEDULE", date.getText().toString() + "\n" + time.getText().toString());
    }

    private static boolean checkTimeslots(String startTime, String currentTime) {
        String pattern = "HH:mm:ss";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        try {
            Date date1 = sdf.parse(startTime);
            Date date2 = sdf.parse(currentTime);
            if (date1.before(date2)) {
                return false;
            } else {
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void rescheduleBooking() {

        if (sendStartTime.equalsIgnoreCase("09:00:00")) {
            sendStartTime = "03:30:00";
        } else if (sendStartTime.equalsIgnoreCase("11:00:00")) {
            sendStartTime = "05:30:00";
        } else if (sendStartTime.equalsIgnoreCase("14:00:00")) {
            sendStartTime = "08:30:00";
        } else if (sendStartTime.equalsIgnoreCase("16:00:00")) {
            sendStartTime = "10:30:00";
        }

        String url = AppUrls.BASE_URL + AppUrls.RESCHEDULE;

        String resultDate = convertStringDateToAnotherStringDate(date.getText().toString(), "dd-MM-yyyy", "yyyy-MM-dd");

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("bookingId", bookingId);
            jsonObject.put("bookingDate", resultDate);
            jsonObject.put("bookingTime", sendStartTime);

            Log.d("RESCHEDULE", "" + jsonObject);

        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("SuccessResp", response.toString());

                try {
                    JSONObject jsonObj = new JSONObject(response.toString());
                    String status = jsonObj.getString("status");
                    String message = jsonObj.getString("message");

                    GlobalCalls.showToast(message, RescheduleActivity.this);

                    Intent intent = new Intent(RescheduleActivity.this, HistoryDetailActivity.class);
                    intent.putExtra("bookingId", bookingId);
                    startActivity(intent);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error == null || error.networkResponse == null) {
                    return;
                }

                try {
                    String body = new String(error.networkResponse.data, "UTF-8");

                    JSONObject jsonObject = new JSONObject(body);

                    GlobalCalls.showToast(jsonObject.optString("message"), RescheduleActivity.this);
                } catch (Exception e) {
                    GlobalCalls.showToast("Slot's UnAvailable..!", RescheduleActivity.this);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(RescheduleActivity.this);
        requestQueue.add(request);
    }

    public String convertStringDateToAnotherStringDate(String stringdate, String stringdateformat, String returndateformat) {

        try {
            Date date = new SimpleDateFormat(stringdateformat).parse(stringdate);
            String returndate = new SimpleDateFormat(returndateformat).format(date);
            return returndate;
        } catch (ParseException e) {
            e.printStackTrace();
            return stringdate;
        }

    }

    public void getTomarrowDate() {

        final Calendar calendarMax = Calendar.getInstance();
        calendarMax.add(Calendar.DAY_OF_MONTH, Integer.parseInt(preBookingDays));
        final Calendar calendarToday = Calendar.getInstance();
        calendarToday.add(Calendar.DAY_OF_MONTH, 1);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        String strDate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;

                        Log.d("Day", String.valueOf(dayOfMonth));
                        Log.d("Month", String.valueOf(monthOfYear + 1));
                        Log.d("Year", String.valueOf(year));

                        date.setBackgroundResource(R.drawable.rounded_border);
                        date.setTextColor(Color.BLACK);
                        date.setText(strDate);

                        /*editor.putString("bookingDate", date.getText().toString());
                        editor.apply();*/

                        time.setText("Booking Time");
                        if (time.getText().toString().equalsIgnoreCase("Booking Time")) {
                            getTimeSlots();
                        }

                    }

                }, mYear, mMonth, mDay);

        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {


            }
        });

        datePickerDialog.getDatePicker().setMinDate(calendarToday.getTimeInMillis());
        datePickerDialog.getDatePicker().setMaxDate(calendarMax.getTimeInMillis());
        datePickerDialog.show();
    }
}
