package in.g360.owner.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.g360.owner.R;
import in.g360.owner.utils.AppUrls;
import in.g360.owner.utils.NetworkChecking;
import in.g360.owner.utils.UserSessionManager;

public class MyAccountActivity extends AppCompatActivity implements View.OnClickListener {

    private boolean checkInternet;
    ImageView close, edit_img;
    UserSessionManager userSessionManager;
    String accessToken;
    TextView toolbar_title, name_txt, email_txt, mobile_txt;
    String userId, firstName, lastName, username, authority, email, mobile, emailVerified, mobileVerified;
    Typeface regular, bold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        checkInternet = NetworkChecking.isConnected(this);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);
        mobile = userDetails.get(UserSessionManager.USER_MOBILE);

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);

        close = findViewById(R.id.close);
        close.setOnClickListener(this);
        edit_img = findViewById(R.id.edit_img);
        edit_img.setOnClickListener(this);

        name_txt = findViewById(R.id.name_txt);
        name_txt.setTypeface(regular);
        email_txt = findViewById(R.id.email_txt);
        email_txt.setTypeface(regular);
        mobile_txt = findViewById(R.id.mobile_txt);
        mobile_txt.setTypeface(regular);
        mobile_txt.setText(mobile);

        getProfile();
    }

    private void getProfile() {
        String url = AppUrls.BASE_URL + AppUrls.PROFILE;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    userId = jsonObject.getString("userId");
                    firstName = jsonObject.getString("firstName");
                    lastName = jsonObject.optString("lastName");
                    username = jsonObject.getString("username");
                    authority = jsonObject.getString("authority");
                    email = jsonObject.getString("email");
                    mobile = jsonObject.getString("mobile");
                    emailVerified = jsonObject.getString("emailVerified");
                    mobileVerified = jsonObject.getString("mobileVerified");

                    name_txt.setText(firstName);
                    email_txt.setText(email);
                    mobile_txt.setText(mobile);

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(MyAccountActivity.this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {
        if (v == close) {
            if (checkInternet) {
                Intent intent = new Intent(MyAccountActivity.this, SettingActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
        if (v == edit_img) {
            if (checkInternet) {
                Intent intent = new Intent(MyAccountActivity.this, EditProfileActivity.class);
                intent.putExtra("firstName", firstName);
                intent.putExtra("lastName", lastName);
                intent.putExtra("email", email);
                intent.putExtra("authority", authority);
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(MyAccountActivity.this, SettingActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
