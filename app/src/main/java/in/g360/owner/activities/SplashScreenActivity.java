package in.g360.owner.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.g360.owner.MainActivity;
import in.g360.owner.R;
import in.g360.owner.utils.AppUrls;
import in.g360.owner.utils.GlobalCalls;
import in.g360.owner.utils.NetworkChecking;
import in.g360.owner.utils.UserSessionManager;

public class SplashScreenActivity extends AppCompatActivity {

    private FirebaseAnalytics mFirebaseAnalytics;
    RelativeLayout splash_screen;
    UserSessionManager session;
    String accessToken, device_id, bookingId, paymentStatus, status, ongoingBooking, preBookingDays;
    SharedPreferences locationPref;
    SharedPreferences.Editor locationEditor;

    /*GetArea*/
    double latitude, longitude;
    public LocationManager locationManager;
    public Criteria criteria;
    public String bestProvider;
    Location location;
    Geocoder geocoder;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    AlertDialog versionDialog;
    int version = 0;
    Typeface regular, bold;
    private boolean checkInternet;
    PackageInfo info;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        //Logs an app event.
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ID-SplashScreenActivity");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "SplashScreenActivity");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
        mFirebaseAnalytics.logEvent("Splashpage_PageLoad_New", bundle);

        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(20000);
        mFirebaseAnalytics.setSessionTimeoutDuration(500);

        GlobalCalls.events("Splashpage_PageLoad_New",SplashScreenActivity.this);


        checkInternet = NetworkChecking.isConnected(this);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);

        splash_screen = findViewById(R.id.activity_splash_screen);
        device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        session.createDeviceId(device_id);
        Log.d("Splash", accessToken + "\n" + device_id);
        int SPLASH_TIME_OUT = 1850;

        checkLocationPermission();

        try {
            info = getPackageManager().getPackageInfo("in.g360.owner", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.d("HashKey", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }

        locationPref = getSharedPreferences("Location", Context.MODE_PRIVATE);
        locationEditor = locationPref.edit();

        if (locationPref.getString("latitude", "").equalsIgnoreCase("") ||
                locationPref.getString("longitude", "").equalsIgnoreCase("") ||
                locationPref.getString("locationName", "").equalsIgnoreCase("")) {

            getCurrentLocation();
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (session.checkLogin() != false) {
                        Intent i = new Intent(SplashScreenActivity.this, LoginActivity.class);
                        startActivity(i);
                        finish();
                    } else {

                        checkVersionUpdate();

                    }
                }
            }, SPLASH_TIME_OUT);
        } else {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {

                    if (session.checkLogin() != false) {
                        Intent i = new Intent(SplashScreenActivity.this, LoginActivity.class);
                        startActivity(i);
                        finish();
                    } else {

                        checkVersionUpdate();

                    }
                }
            }, SPLASH_TIME_OUT);
        }
    }

    private void getOnGoingActivity() {

        String url = AppUrls.BASE_URL + AppUrls.CURRENT_STATUS;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String responceCode = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    Log.d("SPLASHRESP", response);

                    if (responceCode.equalsIgnoreCase("2000")) {

                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        bookingId = jsonObject1.getString("bookingId");
                        paymentStatus = jsonObject1.getString("paymentStatus");
                        status = jsonObject1.getString("status");
                        ongoingBooking = jsonObject1.getString("ongoingBooking");

                        JSONObject jsonObject2 = jsonObject1.getJSONObject("setting");
                        preBookingDays = jsonObject2.optString("preBookingDays");

                        Log.d("PREBOOKINGDAYS", preBookingDays);

                        if (ongoingBooking.equalsIgnoreCase("true")) {
                            if (status.equalsIgnoreCase("1")) {

                                Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                                intent.putExtra("activity", "SplashScreenActivity");
                                startActivity(intent);

                            } else if (status.equalsIgnoreCase("2")) {

                                Intent intent = new Intent(SplashScreenActivity.this, TrackingActivity.class);
                                intent.putExtra("bookingId", bookingId);
                                startActivity(intent);

                            } else if (status.equalsIgnoreCase("3")) {

                                Intent intent = new Intent(SplashScreenActivity.this, TrackingActivity.class);
                                intent.putExtra("bookingId", bookingId);
                                startActivity(intent);

                            } else if (status.equalsIgnoreCase("4")) {

                                Intent intent = new Intent(SplashScreenActivity.this, TrackingActivity.class);
                                intent.putExtra("bookingId", bookingId);
                                startActivity(intent);

                            } else if (status.equalsIgnoreCase("5")) {

                                if (paymentStatus.equalsIgnoreCase("1")) {

                                    checkIFC();
                                    /*Intent intent = new Intent(SplashScreenActivity.this, FinalPaymentActivity.class);
                                    intent.putExtra("bookingId", bookingId);
                                    startActivity(intent);*/
                                } else {
                                    Intent intent = new Intent(SplashScreenActivity.this, TrackingActivity.class);
                                    intent.putExtra("bookingId", bookingId);
                                    startActivity(intent);
                                }

                            } else if (status.equalsIgnoreCase("6")) {

                                Intent intent = new Intent(SplashScreenActivity.this, TrackingActivity.class);
                                intent.putExtra("bookingId", bookingId);
                                startActivity(intent);

                            } else if (status.equalsIgnoreCase("7")) {

                                Intent intent = new Intent(SplashScreenActivity.this, RatingActivity.class);
                                intent.putExtra("bookingId", bookingId);
                                startActivity(intent);

                            } else if (status.equalsIgnoreCase("8")) {

                                if (paymentStatus.equalsIgnoreCase("1")) {

                                    checkIFC();

                                    /*Intent intent = new Intent(SplashScreenActivity.this, FinalPaymentActivity.class);
                                    intent.putExtra("bookingId", bookingId);
                                    startActivity(intent);*/
                                } else {
                                    Intent intent = new Intent(SplashScreenActivity.this, TrackingActivity.class);
                                    intent.putExtra("bookingId", bookingId);
                                    startActivity(intent);
                                }

                            } else if (status.equalsIgnoreCase("9")) {

                                Intent intent = new Intent(SplashScreenActivity.this, ActionPageActivity.class);
                                intent.putExtra("bookingId", bookingId);
                                startActivity(intent);
                            } else {
                                Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                                intent.putExtra("activity", "SplashScreenActivity");
                                startActivity(intent);
                            }
                        } else {
                            Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                            intent.putExtra("activity", "SplashScreenActivity");
                            startActivity(intent);
                        }

                    } else {

                        GlobalCalls.showToast(message, SplashScreenActivity.this);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            GlobalCalls.showToast("Connection Not Found..!", SplashScreenActivity.this);

                        } else if (error instanceof AuthFailureError) {

                            GlobalCalls.showToast("User Not Found..!", SplashScreenActivity.this);

                        } else if (error instanceof ServerError) {

                            GlobalCalls.showToast("Server Not Found..!", SplashScreenActivity.this);

                        } else if (error instanceof NetworkError) {
                            GlobalCalls.showToast("Network Not Found..!", SplashScreenActivity.this);
                        } else if (error instanceof ParseError) {
                            GlobalCalls.showToast("Please Try Later..!", SplashScreenActivity.this);
                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(SplashScreenActivity.this);
        requestQueue.add(stringRequest);

    }

    private void checkIFC() {

        String url = AppUrls.BASE_URL + AppUrls.CHECK_IFC + bookingId + "/isfeedback";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.optString("status");
                    String code = jsonObject.optString("code");
                    String message = jsonObject.optString("message");

                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                    String isfeedback = jsonObject1.optString("isfeedback");

                    if (isfeedback.equalsIgnoreCase("true")) {

                        Intent intent = new Intent(SplashScreenActivity.this, FinalPaymentActivity.class);
                        intent.putExtra("bookingId", bookingId);
                        startActivity(intent);

                    } else if (isfeedback.equalsIgnoreCase("false")){

                        Intent intent = new Intent(SplashScreenActivity.this, IFCActivity.class);
                        intent.putExtra("bookingId", bookingId);
                        startActivity(intent);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) /*{
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        }*/;
        RequestQueue requestQueue = Volley.newRequestQueue(SplashScreenActivity.this);
        requestQueue.add(stringRequest);
    }

    private boolean checkLocationPermission() {

        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        List<String> listPermissionsNeeded = new ArrayList<>();

        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);

            return false;
        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void getCurrentLocation() {
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        criteria = new Criteria();
        bestProvider = String.valueOf(locationManager.getBestProvider(criteria, true)).toString();
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    Activity#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for Activity#requestPermissions for more details.
            return;
        }
        location = locationManager.getLastKnownLocation(bestProvider);
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();

            locationEditor.putString("latitude", String.valueOf(latitude));
            locationEditor.putString("longitude", String.valueOf(longitude));
            locationEditor.apply();

        }
        geocoder = new Geocoder(SplashScreenActivity.this);

        Log.d("LATLNG", latitude + "\n" + longitude);

        try {
            List<Address> addressList = geocoder.getFromLocation(latitude, longitude, 1);
            if (addressList != null && addressList.size() > 0) {
                String locality = addressList.get(0).getAddressLine(0);
                String addr[] = locality.split(",", 2);
                Log.d("Locality", locality);

                String country = addressList.get(0).getCountryName();
                String state = addressList.get(0).getAdminArea();
                String city = addressList.get(0).getLocality();
                String area = addressList.get(0).getSubLocality();
                String pincode = addressList.get(0).getPostalCode();
                latitude = addressList.get(0).getLatitude();
                longitude = addressList.get(0).getLongitude();
                if (!locality.isEmpty() && !country.isEmpty()) {

                    locationEditor.putString("locationName", locality);
                    locationEditor.apply();

                } else {
                    locationEditor.putString("locationName", area);
                    locationEditor.apply();
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void checkVersionUpdate() {

        try {
            PackageInfo pInfo = SplashScreenActivity.this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionCode;
            Log.d("VERSION", "" + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        String url = AppUrls.BASE_URL + AppUrls.CHECK_VERSION;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String id = jsonObject.optString("id");
                    String platformId = jsonObject.optString("platformId");
                    int currentVersionCode = jsonObject.optInt("currentVersionCode");
                    int mandatoryUpdateVersionCode = jsonObject.optInt("mandatoryUpdateVersionCode");
                    String versionName = jsonObject.optString("versionName");
                    String mandatory = jsonObject.optString("mandatory");
                    String status = jsonObject.optString("status");
                    String createdTime = jsonObject.optString("createdTime");
                    String modifiedTime = jsonObject.optString("modifiedTime");

                    if (mandatoryUpdateVersionCode > version) {
                        versionUpdateAlert();
                    } else {
                        if (currentVersionCode > version) {
                            optionalVersionUpdateAlert();
                        } else {
                            getOnGoingActivity();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                error -> {

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        GlobalCalls.showToast("Connection Not Found..!", SplashScreenActivity.this);

                    } else if (error instanceof AuthFailureError) {

                        GlobalCalls.showToast("User Not Found..!", SplashScreenActivity.this);

                    } else if (error instanceof ServerError) {

                        GlobalCalls.showToast("Server Not Found..!", SplashScreenActivity.this);


                    } else if (error instanceof NetworkError) {

                        GlobalCalls.showToast("Network Not Found..!", SplashScreenActivity.this);

                    } else if (error instanceof ParseError) {

                        GlobalCalls.showToast("Please Try Later..!", SplashScreenActivity.this);

                    }
                }
        );
        RequestQueue requestQueue = Volley.newRequestQueue(SplashScreenActivity.this);
        requestQueue.add(stringRequest);

    }

    public void versionUpdateAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SplashScreenActivity.this);
        LayoutInflater inflater = SplashScreenActivity.this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.version_update_dialog, null);

        TextView title_txt = dialog_layout.findViewById(R.id.title_txt);
        title_txt.setTypeface(bold);
        TextView msg_txt = dialog_layout.findViewById(R.id.msg_txt);
        msg_txt.setTypeface(regular);
        Button update_btn = dialog_layout.findViewById(R.id.update_btn);
        update_btn.setTypeface(bold);

        update_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkInternet) {
                    final String appPackageName = getPackageName();
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                } else {
                    Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        });

        builder.setView(dialog_layout);
        versionDialog = builder.create();
        versionDialog.setCancelable(false);
        versionDialog.show();
    }

    public void optionalVersionUpdateAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SplashScreenActivity.this);
        LayoutInflater inflater = SplashScreenActivity.this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.version_update_dialog, null);

        TextView title_txt = dialog_layout.findViewById(R.id.title_txt);
        title_txt.setTypeface(bold);
        TextView msg_txt = dialog_layout.findViewById(R.id.msg_txt);
        msg_txt.setTypeface(regular);
        Button update_btn = dialog_layout.findViewById(R.id.update_btn);
        update_btn.setTypeface(bold);

        update_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkInternet) {
                    final String appPackageName = getPackageName();
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                } else {
                    Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        });

        builder.setView(dialog_layout).setNegativeButton("CANCEL", (dialogInterface, i) -> {

            getOnGoingActivity();
            dialogInterface.dismiss();
        });
        versionDialog = builder.create();
        versionDialog.setCancelable(false);
        versionDialog.show();
    }
}
