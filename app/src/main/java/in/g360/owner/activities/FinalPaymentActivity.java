package in.g360.owner.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import in.g360.owner.R;
import in.g360.owner.adapters.ActionPageAdapter;
import in.g360.owner.adapters.AddonAdapter;
import in.g360.owner.adapters.InspAdapter;
import in.g360.owner.adapters.PhotosAdapter;
import in.g360.owner.models.BookingAddonServicesModel;
import in.g360.owner.models.HistoryInspectionModel;
import in.g360.owner.utils.AppUrls;
import in.g360.owner.utils.FinalCheckSum;
import in.g360.owner.utils.GlobalCalls;
import in.g360.owner.utils.NetworkChecking;
import in.g360.owner.utils.UserSessionManager;

public class FinalPaymentActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close,delete_img;
    TextView toolbar_title, bike_txt, brand_txt, model_txt, date_txt, time_txt, sst_txt, sst_name_txt, sst_person,
            sst_timings, sst_price,
            distance_txt, sst_address, service_txt, service_detail_txt, address_txt, pickup_txt, est_txt,
            est_price_txt, disc_txt, booking_txt, dicount_txt, addon_txt, addon_price_txt, tot_txt, final_price_txt,
            total_price_txt, init_txt, init_price_txt, pay_btn, addon_service_txt, insp_txt, photos_txt, moving_txt,
            kilometers_txt, reg_txt, wallet_txt, wallet_price_txt;
    RatingBar sst_rating;
    private boolean checkInternet;
    Typeface regular, bold;
    TableRow discount_tr, wallet_tr,addon_tr;
    String bookingId, accessToken, brandId, brandName, modelId, modelName, status, finalPrice, services,
            addOnServices, inspectionServices, kilometerRangeId, bookingDate, serviceCenterId, bookingTime,
            pickupAddressLatitude, pickupAddressLongitude, promocodeAmount, initialPaidAmount, mobile, estimatedCost,
            resultDate, startRange, endRange, walletAmount,payPrice;
    String orderId, CALLBACK_URL, CHANNEL_ID, CUST_ID, EMAIL, INDUSTRY_TYPE_ID, MID, MOBILE_NO, ORDER_ID, TXN_AMOUNT,
            WEBSITE, checksumValue;
    UserSessionManager userSessionManager;
    RelativeLayout insp_rl, addon_rl, photos_rl,coupon_rl;
    EditText coupon_edt;
    Button verify_btn;
    String couponCodeAppliedStatus = "0",couponCode = "",discount;
    double estValue, couponValue, finalValue;
    AlertDialog.Builder dialogBuilder;
    AlertDialog alertDialog;

    AddonAdapter addonAdapter;
    RecyclerView insp_recyclerview, addon_recyclerview, photos_recyclerview;

    /*Addon's*/
    List<BookingAddonServicesModel> bookingAddonServicesModels = new ArrayList<BookingAddonServicesModel>();
    /*Inspection's*/
    InspAdapter inspAdapter;
    ArrayList<HistoryInspectionModel> historyInspectionModels = new ArrayList<HistoryInspectionModel>();
    /*Photos*/
    PhotosAdapter photosAdapter;
    List<String> mylist = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_payment);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        checkInternet = NetworkChecking.isConnected(this);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);

        Log.d("ACCESSTOKEN", accessToken);

        bookingId = getIntent().getStringExtra("bookingId");

        close = findViewById(R.id.close);
        close.setOnClickListener(this);

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);

        booking_txt = findViewById(R.id.booking_txt);
        booking_txt.setTypeface(bold);
        addon_rl = findViewById(R.id.addon_rl);
        photos_rl = findViewById(R.id.photos_rl);
        addon_service_txt = findViewById(R.id.addon_service_txt);
        addon_service_txt.setTypeface(bold);
        bike_txt = findViewById(R.id.bike_txt);
        bike_txt.setTypeface(bold);
        brand_txt = findViewById(R.id.brand_txt);
        brand_txt.setTypeface(regular);
        model_txt = findViewById(R.id.model_txt);
        model_txt.setTypeface(regular);
        kilometers_txt = findViewById(R.id.kilometers_txt);
        kilometers_txt.setTypeface(regular);
        reg_txt = findViewById(R.id.reg_txt);
        reg_txt.setTypeface(regular);
        date_txt = findViewById(R.id.date_txt);
        date_txt.setTypeface(regular);
        time_txt = findViewById(R.id.time_txt);
        time_txt.setTypeface(regular);

        sst_txt = findViewById(R.id.sst_txt);
        sst_txt.setTypeface(bold);
        sst_name_txt = findViewById(R.id.sst_name_txt);
        sst_name_txt.setTypeface(bold);
        sst_person = findViewById(R.id.sst_person);
        sst_person.setTypeface(regular);
        sst_rating = findViewById(R.id.sst_rating);
        sst_timings = findViewById(R.id.sst_timings);
        sst_timings.setTypeface(regular);
        sst_address = findViewById(R.id.sst_address);
        sst_address.setTypeface(regular);
        distance_txt = findViewById(R.id.distance_txt);
        distance_txt.setTypeface(regular);
        sst_price = findViewById(R.id.sst_price);
        sst_price.setTypeface(bold);

        service_txt = findViewById(R.id.service_txt);
        service_txt.setTypeface(bold);
        service_detail_txt = findViewById(R.id.service_detail_txt);
        service_detail_txt.setTypeface(regular);

        address_txt = findViewById(R.id.address_txt);
        address_txt.setTypeface(bold);
        pickup_txt = findViewById(R.id.pickup_txt);
        pickup_txt.setTypeface(regular);
        est_txt = findViewById(R.id.est_txt);
        est_txt.setTypeface(regular);
        discount_tr = findViewById(R.id.discount_tr);
        addon_tr = findViewById(R.id.addon_tr);
        wallet_tr = findViewById(R.id.wallet_tr);
        dicount_txt = findViewById(R.id.dicount_txt);
        dicount_txt.setTypeface(regular);

        est_price_txt = findViewById(R.id.est_price_txt);
        est_price_txt.setTypeface(regular);
        disc_txt = findViewById(R.id.disc_txt);
        disc_txt.setTypeface(regular);
        wallet_txt = findViewById(R.id.wallet_txt);
        wallet_txt.setTypeface(regular);
        wallet_price_txt = findViewById(R.id.wallet_price_txt);
        wallet_price_txt.setTypeface(regular);
        tot_txt = findViewById(R.id.tot_txt);
        tot_txt.setTypeface(bold);
        final_price_txt = findViewById(R.id.final_price_txt);
        final_price_txt.setTypeface(bold);
        init_txt = findViewById(R.id.init_txt);
        init_txt.setTypeface(regular);
        addon_txt = findViewById(R.id.addon_txt);
        addon_txt.setTypeface(regular);
        addon_price_txt = findViewById(R.id.addon_price_txt);
        addon_price_txt.setTypeface(regular);
        init_price_txt = findViewById(R.id.init_price_txt);
        init_price_txt.setTypeface(regular);
        init_price_txt.setOnClickListener(this);
        total_price_txt = findViewById(R.id.total_price_txt);
        total_price_txt.setTypeface(regular);
        moving_txt = findViewById(R.id.moving_txt);
        moving_txt.setTypeface(bold);

        insp_rl = findViewById(R.id.insp_rl);
        insp_txt = findViewById(R.id.insp_txt);
        insp_txt.setTypeface(bold);
        photos_txt = findViewById(R.id.photos_txt);
        photos_txt.setTypeface(bold);

        coupon_rl = findViewById(R.id.coupon_rl);
        coupon_edt = findViewById(R.id.coupon_edt);
        coupon_edt.setTypeface(regular);
        verify_btn = findViewById(R.id.verify_btn);
        verify_btn.setTypeface(bold);
        verify_btn.setOnClickListener(this);

        delete_img = findViewById(R.id.delete_img);
        delete_img.setOnClickListener(this);

        pay_btn = findViewById(R.id.pay_btn);
        pay_btn.setTypeface(bold);
        pay_btn.setOnClickListener(this);

        insp_recyclerview = findViewById(R.id.insp_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        insp_recyclerview.setLayoutManager(layoutManager);
        inspAdapter = new InspAdapter(historyInspectionModels, FinalPaymentActivity.this, R.layout.row_insp);

        addon_recyclerview = findViewById(R.id.addon_recyclerview);
        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(getApplicationContext());
        addon_recyclerview.setLayoutManager(layoutManager1);
        addonAdapter = new AddonAdapter(bookingAddonServicesModels, FinalPaymentActivity.this, R.layout.row_addon);

        photos_recyclerview = findViewById(R.id.photos_recyclerview);
        photosAdapter = new PhotosAdapter(mylist, FinalPaymentActivity.this, R.layout.row_photos);
        photos_recyclerview.setLayoutManager(new GridLayoutManager(this, 1, GridLayoutManager.HORIZONTAL, false));

        getDetails();
    }

    private void getDetails() {
        String url = AppUrls.BASE_URL + AppUrls.HISTORY_DETAIL + bookingId + "/detail";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObj = new JSONObject(response);
                    String responceCode = jsonObj.optString("status");
                    String message = jsonObj.optString("message");

                    if (responceCode.equalsIgnoreCase("2000")) {

                        JSONObject jsonObject = jsonObj.getJSONObject("data");

                        String bookingId = jsonObject.optString("bookingId");
                        booking_txt.setText("Booking Id : " + bookingId);
                        String userId = jsonObject.optString("userId");
                        String orderId = jsonObject.optString("orderId");
                        serviceCenterId = jsonObject.optString("serviceCenterId");
                        String bookingType = jsonObject.optString("bookingType");
                        brandId = jsonObject.optString("brandId");
                        brandName = jsonObject.optString("brandName");
                        modelId = jsonObject.optString("modelId");
                        modelName = jsonObject.optString("modelName");
                        kilometerRangeId = jsonObject.optString("kilometerRangeId");
                        bookingDate = jsonObject.optString("bookingDate");
                        bookingTime = jsonObject.optString("bookingTime");
                        String pickupAddress = jsonObject.optString("pickupAddress");
                        pickupAddressLatitude = jsonObject.optString("pickupAddressLatitude");
                        pickupAddressLongitude = jsonObject.optString("pickupAddressLongitude");
                        String addressType = jsonObject.optString("addressType");
                        estimatedCost = jsonObject.optString("estimatedCost");
                        finalPrice = jsonObject.optString("finalPrice");
                        promocodeAmount = jsonObject.optString("promocodeAmount");
                        if (promocodeAmount.equalsIgnoreCase("0")){
                            coupon_rl.setVisibility(View.VISIBLE);
                        }else {
                            coupon_rl.setVisibility(View.GONE);
                        }
                        initialPaidAmount = jsonObject.optString("initialPaidAmount");
                        String paymentStatus = jsonObject.optString("paymentStatus");
                        status = jsonObject.optString("status");
                        String deleted = jsonObject.optString("deleted");
                        String createdTime = jsonObject.optString("createdTime");
                        String modifiedTime = jsonObject.optString("modifiedTime");
                        String firstName = jsonObject.optString("firstName");
                        String lastName = jsonObject.optString("lastName");
                        String contactName = jsonObject.optString("contactName");
                        String serviceCenterName = jsonObject.optString("serviceCenterName");
                        String contactMobileNumber = jsonObject.optString("contactMobileNumber");
                        String averageRating = jsonObject.optString("averageRating");
                        mobile = jsonObject.optString("mobile");
                        String serviceCenterLocation = jsonObject.optString("serviceCenterLocation");
                        String registrationNumber = jsonObject.optString("registrationNumber");
                        Boolean movingCondition = jsonObject.optBoolean("movingCondition");
                        walletAmount = jsonObject.optString("walletAmount");

                        reg_txt.setText(registrationNumber);

                        if (movingCondition) {
                            moving_txt.setTextColor(ContextCompat.getColor(FinalPaymentActivity.this, R.color.green));
                            moving_txt.setText("Vehicle Is In Moving Condition");
                        } else {
                            moving_txt.setTextColor(ContextCompat.getColor(FinalPaymentActivity.this, R.color.red));
                            moving_txt.setText("Vehicle Is Not In Moving Condition");
                        }

                        if (jsonObject.has("kilometerRange")) {
                            JSONObject jsonObject1 = jsonObject.getJSONObject("kilometerRange");
                            startRange = jsonObject1.optString("startRange");
                            endRange = jsonObject1.optString("endRange");

                            kilometers_txt.setText(startRange + " - " + endRange + " km");
                        }

                        if (jsonObject.has("services")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("services");

                            services = IntStream.range(0, jsonArray.length()).mapToObj(i -> {
                                try {
                                    return jsonArray.getJSONObject(i).optString("serviceName");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    return "";
                                }
                            }).collect(Collectors.joining("\n"));

                            service_detail_txt.setText(services);
                        }

                        if (jsonObject.has("bookingAddonServices")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("bookingAddonServices");

                            addOnServices = IntStream.range(0, jsonArray.length()).mapToObj(i -> {
                                try {
                                    return jsonArray.getJSONObject(i).optString("serviceName");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    return "";
                                }
                            }).collect(Collectors.joining("\n"));

                            if (jsonArray.length() > 0) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    BookingAddonServicesModel basm = new BookingAddonServicesModel();
                                    basm.setBookingId(jsonObject1.optString("bookingId"));
                                    basm.setServiceId(jsonObject1.optString("serviceId"));
                                    basm.setServiceName(jsonObject1.optString("serviceName"));
                                    basm.setCost(jsonObject1.optString("cost"));
                                    basm.setStatus(jsonObject1.optString("status"));

                                    bookingAddonServicesModels.add(basm);

                                }
                                addon_recyclerview.setAdapter(addonAdapter);
                                addonAdapter.notifyDataSetChanged();
                            } else {
                                addon_rl.setVisibility(View.GONE);
                            }

                        }

                        if (jsonObject.has("inspections")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("inspections");

                            inspectionServices = IntStream.range(0, jsonArray.length()).mapToObj(i -> {
                                try {
                                    return jsonArray.getJSONObject(i).optString("inspectionName");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    return "";
                                }
                            }).collect(Collectors.joining("\n"));

                            if (jsonArray.length() > 0) {

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    HistoryInspectionModel him = new HistoryInspectionModel();
                                    him.setBookingId(jsonObject1.getString("bookingId"));
                                    him.setInspectionId(jsonObject1.getString("inspectionId"));
                                    him.setState(jsonObject1.getString("state"));
                                    him.setInspectionName(jsonObject1.getString("inspectionName"));

                                    historyInspectionModels.add(him);
                                }

                                insp_recyclerview.setAdapter(inspAdapter);
                                inspAdapter.notifyDataSetChanged();
                            } else {
                                insp_rl.setVisibility(View.GONE);
                            }
                        }

                        if (jsonObject.has("fileIds")) {

                            photos_txt.setVisibility(View.VISIBLE);
                            photos_rl.setVisibility(View.VISIBLE);
                            photos_recyclerview.setVisibility(View.VISIBLE);

                            JSONArray jsonArray = jsonObject.getJSONArray("fileIds");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                String data = jsonArray.get(i).toString();
                                mylist.add(AppUrls.IMAGE_URL + data);
                            }

                            Log.d("PHOTOS", String.valueOf(mylist));

                            photos_recyclerview.setAdapter(photosAdapter);
                            photosAdapter.notifyDataSetChanged();

                        } else {
                            photos_txt.setVisibility(View.GONE);
                            photos_rl.setVisibility(View.GONE);
                            photos_recyclerview.setVisibility(View.GONE);
                        }

                        brand_txt.setText(brandName);
                        model_txt.setText(modelName);
                        resultDate = convertStringDateToAnotherStringDate(bookingDate, "yyyy-MM-dd", "dd-MM-yyyy");
                        date_txt.setText(resultDate);

                        if (bookingTime.equalsIgnoreCase("03:30:00")) {
                            bookingTime = "9 AM";
                        } else if (bookingTime.equalsIgnoreCase("05:30:00")) {
                            bookingTime = "11 AM";
                        } else if (bookingTime.equalsIgnoreCase("08:30:00")) {
                            bookingTime = "2 PM";
                        } else if (bookingTime.equalsIgnoreCase("10:30:00")) {
                            bookingTime = "4 PM";
                        }

                        time_txt.setText(bookingTime);
                        sst_name_txt.setText(serviceCenterName);
                        sst_person.setText(contactName);
                        sst_price.setText("\u20B9 " + finalPrice);
                        sst_address.setText(serviceCenterLocation);
                        sst_rating.setRating(Float.parseFloat(averageRating));
                        pickup_txt.setText(pickupAddress);

                        est_price_txt.setText("\u20B9 " + estimatedCost);
                        init_price_txt.setText("\u20B9 " + initialPaidAmount);
                        if (promocodeAmount.equalsIgnoreCase("0")) {
                            discount_tr.setVisibility(View.GONE);
                        } else {
                            discount_tr.setVisibility(View.VISIBLE);
                            dicount_txt.setText("\u20B9 " + promocodeAmount);
                        }

                        if (walletAmount.equalsIgnoreCase("0")) {
                            wallet_tr.setVisibility(View.GONE);
                        } else {
                            wallet_tr.setVisibility(View.VISIBLE);
                            wallet_price_txt.setText("\u20B9 " + walletAmount);
                        }

                        Double estPrice = Double.valueOf(estimatedCost);
                        Double price = Double.valueOf(finalPrice);
                        Double initPrice = Double.valueOf(initialPaidAmount);
                        Double promoPrice = Double.valueOf(promocodeAmount);
                        Double walletPrice = Double.valueOf(walletAmount);
                        Double removePrice = initPrice + promoPrice + walletPrice;
                        Double addOnPrice = price - estPrice;

                        if (addOnPrice == 0) {
                            addon_tr.setVisibility(View.GONE);
                        } else {
                            addon_tr.setVisibility(View.VISIBLE);
                            addon_price_txt.setText("\u20B9 " + addOnPrice);
                        }

                        payPrice = String.valueOf((price - removePrice));
                        final_price_txt.setText("\u20B9 " + payPrice);
                        total_price_txt.setText("Final Payment \u20B9 " + payPrice);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(FinalPaymentActivity.this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {
        if (v == close) {
            if (checkInternet) {
                Intent intent = new Intent(FinalPaymentActivity.this, IFCActivity.class);
                intent.putExtra("bookingId", bookingId);
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == verify_btn) {
            couponCodeAppliedStatus = "0";
            verifyCoupon();
        }

        if (v == delete_img) {

            showClearDialog(R.layout.warning_dialog);

        }

        if (v == pay_btn) {
            if (checkInternet) {

                pay();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }

    private void verifyCoupon() {

        String coupon = coupon_edt.getText().toString().trim();
        if (!coupon.equalsIgnoreCase("")) {

            String url = AppUrls.BASE_URL + AppUrls.VERIFY_START_COUPON + coupon + AppUrls.VERIFY_END_COUPON + serviceCenterId;

            Log.d("VERIFYCOUPON", url);

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");

                        if (status.equalsIgnoreCase("2000")) {

                            coupon_rl.setVisibility(View.GONE);
                            discount_tr.setVisibility(View.VISIBLE);

                            couponCodeAppliedStatus = "1";

                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                            String couponId = jsonObject1.getString("couponId");
                            couponCode = jsonObject1.getString("couponCode");
                            discount = jsonObject1.getString("discount");
                            String startDateTime = jsonObject1.getString("startDateTime");
                            String expiryDateTime = jsonObject1.getString("expiryDateTime");
                            String type = jsonObject1.getString("type");
                            String deleted = jsonObject1.getString("deleted");
                            String createdTime = jsonObject1.getString("createdTime");
                            String modifiedTime = jsonObject1.getString("modifiedTime");

                            dicount_txt.setText("\u20B9 " + discount);

                            if (!dicount_txt.getText().toString().isEmpty()) {
                                couponValue = Double.parseDouble(discount);
                                estValue = Double.parseDouble(finalPrice);

                                Log.d("Prices", couponValue + "\n" + estValue);
                                finalValue = estValue - couponValue;
                                finalPrice = String.valueOf(finalValue);
                                discount_tr.setVisibility(View.VISIBLE);
                                final_price_txt.setText("\u20B9 " + finalPrice);

                                total_price_txt.setText("Final Payment \u20B9 " + finalPrice);

                            } else {
                                final_price_txt.setText("");
                                GlobalCalls.showToast(message, FinalPaymentActivity.this);

                            }

                            showPositiveAlertDialog(R.layout.dialog_postive_layout);
                        }
                        if (status.equalsIgnoreCase("5035")) {
                            final_price_txt.setText("");
                            GlobalCalls.showToast(message, FinalPaymentActivity.this);
                            showNegativeAlertDialog(R.layout.dialog_negative_layout);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            Log.d("COUPON", error.toString());
                            couponCodeAppliedStatus = "0";
                            showNegativeAlertDialog(R.layout.dialog_negative_layout);

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization", "Bearer " + accessToken);
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(FinalPaymentActivity.this);
            requestQueue.add(stringRequest);
        } else {

            GlobalCalls.showToast("Please Enter Coupon..!", FinalPaymentActivity.this);

        }
    }

    private void showPositiveAlertDialog(int layout) {
        dialogBuilder = new AlertDialog.Builder(FinalPaymentActivity.this);
        View layoutView = getLayoutInflater().inflate(layout, null);
        TextView message_txt = layoutView.findViewById(R.id.message_txt);
        message_txt.setText("You Just Saved \u20B9 " + discount);
        Button dialogButton = layoutView.findViewById(R.id.btnDialog);
        dialogBuilder.setView(layoutView);
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();

                coupon_rl.setVisibility(View.GONE);
                discount_tr.setVisibility(View.VISIBLE);
            }
        });
        alertDialog.setCancelable(false);
    }

    private void showNegativeAlertDialog(int layout) {
        dialogBuilder = new AlertDialog.Builder(FinalPaymentActivity.this);
        View layoutView = getLayoutInflater().inflate(layout, null);
        TextView message_txt = layoutView.findViewById(R.id.message_txt);
        Button dialogButton = layoutView.findViewById(R.id.btnDialog);
        dialogBuilder.setView(layoutView);
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                couponCode = "";
                couponCodeAppliedStatus = "0";
                coupon_rl.setVisibility(View.VISIBLE);
                discount_tr.setVisibility(View.GONE);
            }
        });
        alertDialog.setCancelable(false);
    }

    private void showClearDialog(int layout) {
        dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(FinalPaymentActivity.this);
        View layoutView = getLayoutInflater().inflate(layout, null);
        TextView title_txt = layoutView.findViewById(R.id.title_txt);
        title_txt.setTypeface(bold);
        title_txt.setText("Are you sure?");
        TextView msg_txt = layoutView.findViewById(R.id.msg_txt);
        msg_txt.setTypeface(regular);
        msg_txt.setVisibility(View.GONE);
        Button ok_btn = layoutView.findViewById(R.id.ok_btn);
        ok_btn.setTypeface(bold);
        ok_btn.setText("Yes, Remove");
        Button cancel_btn = layoutView.findViewById(R.id.cancel_btn);
        cancel_btn.setTypeface(bold);
        cancel_btn.setText("No, Keep It");
        dialogBuilder.setView(layoutView);
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();

        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialog.dismiss();

                discount_tr.setVisibility(View.GONE);
                discount = "0";
                couponValue = 0.0;
                couponCodeAppliedStatus = "0";
                couponCode = "";
                //finalPrice = sstPrice;
                finalPrice = estimatedCost;
                final_price_txt.setText("\u20B9 " + finalPrice);
                coupon_rl.setVisibility(View.VISIBLE);

                total_price_txt.setText("Final Payment \u20B9 " + payPrice);

            }
        });

        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialog.dismiss();

            }
        });
        alertDialog.setCancelable(false);
    }

    private void pay() {

        String resultDate = convertStringDateToAnotherStringDate(bookingDate, "dd-MM-yyyy", "yyyy-MM-dd");
        Log.d("ResultDate", resultDate);

        String url = AppUrls.BASE_URL + AppUrls.PAY;

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("bookingId", bookingId);
            jsonObject.put("promoCode", couponCode);
            jsonObject.put("promocodeAmount", discount);
            jsonObject.put("couponCodeAppliedStatus", couponCodeAppliedStatus);
            /*jsonObject.put("serviceCenterId", serviceCenterId);
            jsonObject.put("brandId", brandId);
            jsonObject.put("brandName", brandName);
            jsonObject.put("modelId", modelId);
            jsonObject.put("modelName", modelName);
            jsonObject.put("bookingDate", resultDate);
            jsonObject.put("bookingTime", bookingTime);
            jsonObject.put("pickupAddress", pickup_txt.getText().toString());
            jsonObject.put("pickupLandmark", "");
            jsonObject.put("pickupAddressLatitude", pickupAddressLatitude);
            jsonObject.put("pickupAddressLongitude", pickupAddressLongitude);
            jsonObject.put("addressType", "2");
            jsonObject.put("bookingType", "1");
            jsonObject.put("estimatedCost", estimatedCost);
            jsonObject.put("kilometerRangeId", kilometerRangeId);
            jsonObject.put("promocodeAmount", promocodeAmount);
            jsonObject.put("initialPaidAmount", initialPaidAmount);
            jsonObject.put("paymentStatus", "0");
            jsonObject.put("contactMobileNumber", mobile);*/

            Log.d("ServicesObject", "" + jsonObject.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("PayResp", response.toString());

                try {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    orderId = jsonObject.optString("orderId");
                    checksumValue = jsonObject.optString("checksumValue");

                    JSONObject jsonObject1 = jsonObject.getJSONObject("paytmParms");
                    CALLBACK_URL = jsonObject1.optString("CALLBACK_URL");
                    CHANNEL_ID = jsonObject1.optString("CHANNEL_ID");
                    CUST_ID = jsonObject1.optString("CUST_ID");
                    EMAIL = jsonObject1.optString("EMAIL");
                    INDUSTRY_TYPE_ID = jsonObject1.optString("INDUSTRY_TYPE_ID");
                    MID = jsonObject1.optString("MID");
                    MOBILE_NO = jsonObject1.optString("MOBILE_NO");
                    ORDER_ID = jsonObject1.optString("ORDER_ID");
                    TXN_AMOUNT = jsonObject1.optString("TXN_AMOUNT");
                    WEBSITE = jsonObject1.optString("WEBSITE");

                    Log.d("PayDetails", orderId + "\n" + checksumValue + "\n" + CALLBACK_URL + "\n" + CHANNEL_ID + "\n" + CUST_ID + "\n" +
                            EMAIL + "\n" + INDUSTRY_TYPE_ID + "\n" + MID + "\n" + MOBILE_NO + "\n" + ORDER_ID + "\n" + TXN_AMOUNT + "\n" + WEBSITE);

                    Intent intent = new Intent(FinalPaymentActivity.this, FinalCheckSum.class);
                    intent.putExtra("CALLBACK_URL", CALLBACK_URL);
                    intent.putExtra("CHANNEL_ID", CHANNEL_ID);
                    intent.putExtra("CUST_ID", CUST_ID);
                    intent.putExtra("EMAIL", EMAIL);
                    intent.putExtra("INDUSTRY_TYPE_ID", INDUSTRY_TYPE_ID);
                    intent.putExtra("MID", MID);
                    intent.putExtra("MOBILE_NO", MOBILE_NO);
                    intent.putExtra("ORDER_ID", ORDER_ID);
                    intent.putExtra("TXN_AMOUNT", TXN_AMOUNT);
                    intent.putExtra("WEBSITE", WEBSITE);
                    intent.putExtra("checksumValue", checksumValue);
                    intent.putExtra("bookingId", bookingId);
                    startActivity(intent);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (ContextCompat.checkSelfPermission(FinalPaymentActivity.this,
                        Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(FinalPaymentActivity.this,
                            new String[]{Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS}, 101);

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(FinalPaymentActivity.this);
        requestQueue.add(request);
    }

    public String convertStringDateToAnotherStringDate(String stringdate, String stringdateformat, String returndateformat) {

        try {
            Date date = new SimpleDateFormat(stringdateformat).parse(stringdate);
            String returndate = new SimpleDateFormat(returndateformat).format(date);
            return returndate;
        } catch (ParseException e) {
            e.printStackTrace();
            return stringdate;
        }

    }

    @Override
    public void onBackPressed() {

        GlobalCalls.showToast("Please Complete Payment..!", FinalPaymentActivity.this);

        /*if (checkInternet) {
            Intent intent = new Intent(FinalPaymentActivity.this, IFCActivity.class);
            intent.putExtra("bookingId", bookingId);
            startActivity(intent);

        } else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
            snackbar.show();
        }*/

    }
}
