package in.g360.owner.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import in.g360.owner.MainActivity;
import in.g360.owner.R;
import in.g360.owner.adapters.SourceAdapter;
import in.g360.owner.models.SourceModel;
import in.g360.owner.utils.AppUrls;
import in.g360.owner.utils.CheckSum;
import in.g360.owner.utils.GlobalCalls;
import in.g360.owner.utils.NetworkChecking;
import in.g360.owner.utils.SimpleTooltip;
import in.g360.owner.utils.UserSessionManager;

public class ConfirmationDetailActivity extends AppCompatActivity implements View.OnClickListener {

    private FirebaseAnalytics mFirebaseAnalytics;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    SharedPreferences preferences, locationPref, sstPreferences, servicePreferences, inspectionPreferences;
    SharedPreferences.Editor locationEditor;
    String accessToken, userMobile, brandId, brandName, modelId, modelName, kilometerRangeId, bookingDate, bookingStartTime, bookingEndTime,
            bookingDisplayTime, sstId, sstName, sstAddress, sstDistance, sstInitPrice, sstPrice, jArray, sstRating, sstOpenTime, couponCode = "",
            sstCloseTime, sstPerson, serviceId, serviceName, serviceType, inspections, finalPrice, discount, latitude, longitude, locationName,
            kilometer, inspectionNames, sstDisplayTime, sstNumber, condStr, sourceId, sourceName;
    double estValue, couponValue, finalValue;
    ImageView close, edit_bike_details_img, edit_sst_details_img, edit_services_img, edit_address_img, delete_img, info_img;
    TextView toolbar_title, brand_txt, bike_txt, model_txt, date_txt, time_txt, sst_name_txt, sst_person, sst_timings, sst_txt, service_txt,
            sst_address, distance_txt, sst_price, service_detail_txt, pickup_txt, dicount_txt, verify_btn, est_price_txt, address_txt, disc_txt,
            final_price_txt, init_price_txt, pay_btn, est_txt, tot_txt, inspection_txt, inspection_detail_txt, pick_txt, pick_price_txt, free_txt,
            source_title_txt, source_txt;
    RatingBar sst_rating;
    EditText coupon_edt;
    TableRow discount_tr;
    private final static int PLACE_PICKER_REQUEST = 999;
    AlertDialog.Builder dialogBuilder;
    AlertDialog alertDialog;
    String orderId, CALLBACK_URL, CHANNEL_ID, CUST_ID, EMAIL, INDUSTRY_TYPE_ID, MID, MOBILE_NO, ORDER_ID, TXN_AMOUNT, WEBSITE, checksumValue;
    SweetAlertDialog sweetAlertDialog;
    RelativeLayout coupon_rl;
    Typeface regular, bold;
    String couponCodeAppliedStatus = "0";
    String activity;
    private long mLastClickTime = 0;

    AlertDialog dialog;
    RecyclerView source_recyclerview;
    SourceAdapter sourceAdapter;
    ArrayList<SourceModel> sourceModels = new ArrayList<SourceModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation_detail);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        activity = getIntent().getStringExtra("activity");

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        checkInternet = NetworkChecking.isConnected(this);

        locationPref = getSharedPreferences("Location", Context.MODE_PRIVATE);
        //locationPref = PreferenceManager.getDefaultSharedPreferences(this);
        locationEditor = locationPref.edit();

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);
        userMobile = userDetails.get(UserSessionManager.USER_MOBILE);

        Log.d("ACCESS", accessToken);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        brandId = preferences.getString("brandId", "");
        brandName = preferences.getString("brandName", "");
        modelId = preferences.getString("modelId", "");
        modelName = preferences.getString("modelName", "");
        kilometer = preferences.getString("kilometer", "");
        kilometerRangeId = preferences.getString("kilometerRangeId", "");
        bookingDate = preferences.getString("bookingDate", "");
        bookingStartTime = preferences.getString("bookingStartTime", "");
        bookingEndTime = preferences.getString("bookingEndTime", "");
        bookingDisplayTime = preferences.getString("bookingDisplayTime", "");

        Log.d("Preferences", brandId + "\n" + modelId + "\n" + kilometerRangeId + "\n" + bookingDate + "\n" +
                bookingStartTime + "\n" + bookingEndTime + "\n" + bookingDisplayTime);

        locationPref = getSharedPreferences("Location", Context.MODE_PRIVATE);
        //locationPref = PreferenceManager.getDefaultSharedPreferences(this);
        latitude = locationPref.getString("latitude", "");
        longitude = locationPref.getString("longitude", "");
        locationName = locationPref.getString("locationName", "");

        Log.d("locationPref", latitude + "\n" + longitude + "\n" + locationName);

        sstPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sstId = sstPreferences.getString("sstId", "");
        sstName = sstPreferences.getString("sstName", "");
        sstAddress = sstPreferences.getString("sstAddress", "");
        sstDistance = sstPreferences.getString("sstDistance", "");
        sstInitPrice = sstPreferences.getString("sstInitPrice", "");
        sstPrice = sstPreferences.getString("sstPrice", "");
        jArray = sstPreferences.getString("jArray", "");
        sstRating = sstPreferences.getString("sstRating", "");
        sstOpenTime = sstPreferences.getString("sstOpenTime", "");
        sstCloseTime = sstPreferences.getString("sstCloseTime", "");
        sstPerson = sstPreferences.getString("sstPerson", "");
        sstDisplayTime = sstPreferences.getString("sstDisplayTime", "");
        sstNumber = sstPreferences.getString("sstNumber", "");

        Log.d("jArray", jArray);
        Log.d("SSTPreferences", sstId + "\n" + sstName + "\n" + sstAddress + "\n" + sstDistance + "\n" + sstInitPrice + "\n" + sstPrice
                + "\n" + sstDisplayTime + "\n" + sstNumber);

        servicePreferences = PreferenceManager.getDefaultSharedPreferences(this);
        serviceId = servicePreferences.getString("serviceId", "");
        serviceName = servicePreferences.getString("serviceName", "");
        serviceType = servicePreferences.getString("serviceType", "");

        Log.d("Services", serviceId + "\n" + serviceName);

        inspectionPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        inspections = inspectionPreferences.getString("inspections", "");
        inspectionNames = inspectionPreferences.getString("inspectionNames", "");

        Log.d("Inspections", inspections + "\n" + inspectionNames);

        close = findViewById(R.id.close);
        close.setOnClickListener(this);
        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);
        bike_txt = findViewById(R.id.bike_txt);
        bike_txt.setTypeface(bold);
        brand_txt = findViewById(R.id.brand_txt);
        brand_txt.setTypeface(regular);
        brand_txt.setText(brandName);
        model_txt = findViewById(R.id.model_txt);
        model_txt.setTypeface(regular);
        model_txt.setText(modelName);
        date_txt = findViewById(R.id.date_txt);
        date_txt.setTypeface(regular);
        date_txt.setText(bookingDate);
        time_txt = findViewById(R.id.time_txt);
        time_txt.setTypeface(regular);
        time_txt.setText(bookingDisplayTime);
        //time_txt.setText(bookingStartTime + " - " + bookingEndTime);

        sst_txt = findViewById(R.id.sst_txt);
        sst_txt.setTypeface(bold);
        sst_name_txt = findViewById(R.id.sst_name_txt);
        sst_name_txt.setTypeface(bold);
        sst_name_txt.setText(sstName);
        sst_person = findViewById(R.id.sst_person);
        sst_person.setTypeface(regular);
        sst_person.setText(sstPerson);
        sst_rating = findViewById(R.id.sst_rating);
        sst_rating.setRating(Float.parseFloat(sstRating));
        sst_timings = findViewById(R.id.sst_timings);
        sst_timings.setTypeface(regular);
        //sst_timings.setText(sstOpenTime + " - " + sstCloseTime);
        sst_timings.setText(sstDisplayTime);
        sst_address = findViewById(R.id.sst_address);
        sst_address.setTypeface(regular);
        sst_address.setText(sstAddress);
        distance_txt = findViewById(R.id.distance_txt);
        distance_txt.setTypeface(regular);
        distance_txt.setText(sstDistance + " Kms");
        sst_price = findViewById(R.id.sst_price);
        sst_price.setTypeface(bold);
        sst_price.setText("\u20B9 " + sstPrice);

        service_txt = findViewById(R.id.service_txt);
        service_txt.setTypeface(bold);
        service_detail_txt = findViewById(R.id.service_detail_txt);
        inspection_txt = findViewById(R.id.inspection_txt);
        inspection_txt.setTypeface(bold);
        inspection_detail_txt = findViewById(R.id.inspection_detail_txt);
        inspection_detail_txt.setTypeface(regular);
        String serviceString = serviceName.replace(",", "\n");
        String inspectionString = inspectionNames.replace(",", "\n");
        service_detail_txt.setTypeface(regular);
        service_detail_txt.setText(serviceString);
        if (inspectionString.equalsIgnoreCase("")) {
            inspection_txt.setVisibility(View.GONE);
            inspection_detail_txt.setVisibility(View.GONE);
        } else {
            inspection_txt.setVisibility(View.VISIBLE);
            inspection_detail_txt.setVisibility(View.VISIBLE);
            inspection_detail_txt.setText(inspectionString);
        }

        address_txt = findViewById(R.id.address_txt);
        address_txt.setTypeface(bold);
        info_img = findViewById(R.id.info_img);
        info_img.setOnClickListener(this);
        coupon_rl = findViewById(R.id.coupon_rl);
        edit_sst_details_img = findViewById(R.id.edit_sst_details_img);
        edit_sst_details_img.setOnClickListener(this);
        edit_bike_details_img = findViewById(R.id.edit_bike_details_img);
        edit_bike_details_img.setOnClickListener(this);
        edit_services_img = findViewById(R.id.edit_services_img);
        edit_services_img.setOnClickListener(this);
        edit_address_img = findViewById(R.id.edit_address_img);
        edit_address_img.setOnClickListener(this);
        pickup_txt = findViewById(R.id.pickup_txt);
        pickup_txt.setTypeface(regular);
        pickup_txt.setText(locationName);
        coupon_edt = findViewById(R.id.coupon_edt);
        coupon_edt.setTypeface(regular);
        est_txt = findViewById(R.id.est_txt);
        est_txt.setTypeface(regular);
        dicount_txt = findViewById(R.id.dicount_txt);
        dicount_txt.setTypeface(regular);
        source_title_txt = findViewById(R.id.source_title_txt);
        source_title_txt.setTypeface(bold);
        source_txt = findViewById(R.id.source_txt);
        source_txt.setTypeface(regular);
        source_txt.setOnClickListener(this);
        verify_btn = findViewById(R.id.verify_btn);
        verify_btn.setTypeface(bold);
        verify_btn.setOnClickListener(this);
        pick_txt = findViewById(R.id.pick_txt);
        pick_txt.setTypeface(regular);
        free_txt = findViewById(R.id.free_txt);
        free_txt.setTypeface(bold);
        pick_price_txt = findViewById(R.id.pick_price_txt);
        pick_price_txt.setTypeface(bold);
        est_price_txt = findViewById(R.id.est_price_txt);
        est_price_txt.setTypeface(regular);
        est_price_txt.setText("\u20B9 " + sstPrice);
        disc_txt = findViewById(R.id.disc_txt);
        disc_txt.setTypeface(regular);
        tot_txt = findViewById(R.id.tot_txt);
        tot_txt.setTypeface(bold);
        final_price_txt = findViewById(R.id.final_price_txt);
        final_price_txt.setTypeface(bold);
        final_price_txt.setText("\u20B9 " + sstPrice);
        finalPrice = sstPrice;

        discount_tr = findViewById(R.id.discount_tr);
        delete_img = findViewById(R.id.delete_img);
        delete_img.setOnClickListener(this);

        pay_btn = findViewById(R.id.pay_btn);
        pay_btn.setTypeface(bold);
        pay_btn.setOnClickListener(this);

        init_price_txt = findViewById(R.id.init_price_txt);
        init_price_txt.setTypeface(regular);
        init_price_txt.setOnClickListener(this);

        if (sstInitPrice.equalsIgnoreCase("0") || sstInitPrice.equalsIgnoreCase("") ||
                sstInitPrice.equalsIgnoreCase(null)) {

            info_img.setVisibility(View.GONE);
            init_price_txt.setVisibility(View.GONE);
            pay_btn.setText("Book");

        } else {

            info_img.setVisibility(View.VISIBLE);
            init_price_txt.setVisibility(View.VISIBLE);
            init_price_txt.setText("Advance Payment : \u20B9 " + sstInitPrice);
            pay_btn.setText("Pay");

            new SimpleTooltip.Builder(this)
                    .anchorView(info_img)
                    .text("Please note that this amount is an advance payment for your service and will be adjusted against the final bill.")
                    .gravity(Gravity.TOP)
                    .animated(true)
                    .build()
                    .show();
        }


        if (activity.equalsIgnoreCase("ServicesListActivity") || activity.equalsIgnoreCase("ServiceCenterAdapter") ||
                activity.equalsIgnoreCase("ServiceCenterActivity")) {

            condStr = getIntent().getStringExtra("condStr");

            Log.d("condStr", condStr);

            if (condStr.equalsIgnoreCase("true")) {
                pick_price_txt.setText(" \u20B9 150 ");
                pick_price_txt.setPaintFlags(pick_price_txt.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                free_txt.setText("FREE");
            } else if (condStr.equalsIgnoreCase("false")) {
                pick_price_txt.setText("Charges may apply");
            }

        }

        if (activity.equalsIgnoreCase("TestActivity")) {

            latitude = getIntent().getStringExtra("latitude");
            longitude = getIntent().getStringExtra("longitude");
            locationName = getIntent().getStringExtra("location");
            pickup_txt.setText(locationName);

            Log.d("LLLL", latitude + "\n" + longitude + "\n" + locationName);

        }

        if (activity.equalsIgnoreCase("CheckSum")) {
            couponCode = getIntent().getStringExtra("couponCode");
            condStr = getIntent().getStringExtra("condStr");
            coupon_edt.setText(couponCode);

            if (couponCode.equalsIgnoreCase("")) {
                coupon_edt.setText("");
            } else {
                verifyCouponCode();
            }

            if (condStr.equalsIgnoreCase("true")) {
                pick_price_txt.setText(" \u20B9 150 ");
                pick_price_txt.setPaintFlags(pick_price_txt.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                free_txt.setText("FREE");
            } else if (condStr.equalsIgnoreCase("false")) {
                pick_price_txt.setText("Charges may apply");
            }

        }



        //Logs an app event.
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ID-ConfirmationActivity");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "ConfirmationActivity");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
        mFirebaseAnalytics.logEvent("Confirmationpage_PageLoad_New", bundle);

        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(20000);
        mFirebaseAnalytics.setSessionTimeoutDuration(500);
        mFirebaseAnalytics.setUserId(userMobile);
        mFirebaseAnalytics.setUserProperty("ConfirmationActivity", userMobile);

        GlobalCalls.events("Confirmationpage_PageLoad_New", ConfirmationDetailActivity.this);


        sourceModels = new ArrayList<SourceModel>();
        source_recyclerview = findViewById(R.id.source_recyclerview);
        sourceAdapter = new SourceAdapter(sourceModels, ConfirmationDetailActivity.this, R.layout.row_source_type);
        source_recyclerview.setLayoutManager(new GridLayoutManager(this, 2, GridLayoutManager.HORIZONTAL, false));

    }

    @Override
    public void onClick(View v) {
        if (v == close) {

            finish();

        }
        if (v == edit_bike_details_img) {

            Intent intent = new Intent(ConfirmationDetailActivity.this, MainActivity.class);
            intent.putExtra("activity", "ConfirmationDetailActivity");
            intent.putExtra("brandId", brandId);
            intent.putExtra("modelId", modelId);
            intent.putExtra("kilometerRangeId", kilometerRangeId);
            intent.putExtra("brandName", brandName);
            intent.putExtra("modelName", modelName);
            intent.putExtra("kilometer", kilometer);
            intent.putExtra("bookingDate", bookingDate);
            intent.putExtra("bookingStartTime", bookingStartTime);
            intent.putExtra("bookingEndTime", bookingEndTime);
            intent.putExtra("bookingDisplayTime", bookingDisplayTime);
            startActivity(intent);

        }
        if (v == edit_sst_details_img) {
            Intent intent = new Intent(ConfirmationDetailActivity.this, ServiceCenterActivity.class);
            intent.putExtra("condStr", condStr);
            startActivity(intent);
            finish();
        }
        if (v == edit_services_img) {

            finish();

        }
        if (v == edit_address_img) {
            if (checkInternet) {

                Intent intent = new Intent(ConfirmationDetailActivity.this, TestActivity.class);
                intent.putExtra("activity", "ConfirmationDetailActivity");
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == source_txt) {
            getSource();
        }

        if (v == verify_btn) {
            couponCodeAppliedStatus = "0";
            verifyCoupon();
        }

        if (v == delete_img) {

            showClearDialog(R.layout.warning_dialog);

            /*sweetAlertDialog = new SweetAlertDialog(ConfirmationDetailActivity.this, SweetAlertDialog.WARNING_TYPE);
            sweetAlertDialog.setTitleText("Are you sure?");
            //sweetAlertDialog.setContentText("You want to Remove Promocode!");
            sweetAlertDialog.setCancelText("No, Keep It");
            sweetAlertDialog.setConfirmText("Yes, Remove");
            sweetAlertDialog.showCancelButton(true);

            sweetAlertDialog.setConfirmClickListener(sweetAlertDialog -> {

                sweetAlertDialog.cancel();

                discount_tr.setVisibility(View.GONE);
                discount = "0";
                couponValue = 0.0;
                couponCodeAppliedStatus = "0";
                couponCode = "";
                finalPrice = sstPrice;
                final_price_txt.setText("\u20B9 " + finalPrice);
                coupon_rl.setVisibility(View.VISIBLE);

            });

            sweetAlertDialog.setCancelClickListener(sDialog -> sDialog.cancel());
            sweetAlertDialog.show();*/
        }

        if (v == info_img) {

            new SimpleTooltip.Builder(this)
                    .anchorView(info_img)
                    .text("Please note that this amount is an advance payment for your service and will be adjusted against the final bill.")
                    .gravity(Gravity.TOP)
                    .animated(true)
                    .build()
                    .show();
        }

        if (v == init_price_txt) {

            new SimpleTooltip.Builder(this)
                    .anchorView(info_img)
                    .text("Please note that this amount is an advance payment for your service and will be adjusted against the final bill.")
                    .gravity(Gravity.TOP)
                    .animated(true)
                    .build()
                    .show();
        }

        if (v == pay_btn) {

            Bundle bundle = new Bundle();
            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ID-PayButton");
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "PayButton");
            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
            mFirebaseAnalytics.logEvent("Confirmationpage_Event_PayButton_New", bundle);

            GlobalCalls.events("Confirmationpage_Event_PayButton_New", ConfirmationDetailActivity.this);

            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();

            Log.d("SystemCheck", String.valueOf(SystemClock.elapsedRealtime() - mLastClickTime));

            if (!source_txt.getText().toString().equalsIgnoreCase("Select Source Type")) {

                pay();

            } else {

                GlobalCalls.showToast("Please Select Source Type", ConfirmationDetailActivity.this);

            }
        }
    }

    private void getSource() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            sourceModels.clear();
            //flipProgressDialog.show(getFragmentManager(), "");
            String url = AppUrls.BASE_URL + AppUrls.SOURCE_TYPE;

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    response -> {

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            Log.d("RESP", response);

                            String status = jsonObject.getString("status");
                            String code = jsonObject.getString("code");
                            String message = jsonObject.getString("message");
                            //if (status.equalsIgnoreCase("2000")) {
                            //flipProgressDialog.dismiss();

                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                            JSONArray jsonArray = jsonObject1.getJSONArray("sources");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                                SourceModel sourceModel = new SourceModel();
                                sourceModel.setId(jsonObject2.optString("id"));
                                String name = jsonObject2.optString("name");
                                sourceModel.setName(name);

                                sourceModels.add(sourceModel);
                            }

                            sourceDialog();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    },
                    error -> {

                    }) {
            };
            RequestQueue requestQueue = Volley.newRequestQueue(ConfirmationDetailActivity.this);
            requestQueue.add(stringRequest);

        } else {

            GlobalCalls.showToast("Please Check Internet Connection..!", ConfirmationDetailActivity.this);

        }

    }

    private void sourceDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ConfirmationDetailActivity.this);
        LayoutInflater inflater = ConfirmationDetailActivity.this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.source_dialog, null);

        TextView source_title = dialog_layout.findViewById(R.id.source_title);
        source_title.setTypeface(regular);

        RecyclerView source_recyclerview = dialog_layout.findViewById(R.id.source_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        source_recyclerview.setLayoutManager(layoutManager);

        sourceAdapter = new SourceAdapter(sourceModels, ConfirmationDetailActivity.this, R.layout.row_source_type);
        source_recyclerview.setAdapter(sourceAdapter);

        builder.setView(dialog_layout).setNegativeButton("CANCEL", (dialogInterface, i) -> {

            dialogInterface.dismiss();
        });

        dialog = builder.create();
        dialog.show();
    }

    public void setSourceType(String id, String name) {

        dialog.dismiss();
        sourceAdapter.notifyDataSetChanged();
        sourceId = id;
        sourceName = name;

        source_txt.setText(sourceName);

    }

    private void showClearDialog(int layout) {
        dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(ConfirmationDetailActivity.this);
        View layoutView = getLayoutInflater().inflate(layout, null);
        TextView title_txt = layoutView.findViewById(R.id.title_txt);
        title_txt.setTypeface(bold);
        title_txt.setText("Are you sure?");
        TextView msg_txt = layoutView.findViewById(R.id.msg_txt);
        msg_txt.setTypeface(regular);
        msg_txt.setVisibility(View.GONE);
        Button ok_btn = layoutView.findViewById(R.id.ok_btn);
        ok_btn.setTypeface(bold);
        ok_btn.setText("Yes, Remove");
        Button cancel_btn = layoutView.findViewById(R.id.cancel_btn);
        cancel_btn.setTypeface(bold);
        cancel_btn.setText("No, Keep It");
        dialogBuilder.setView(layoutView);
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();

        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialog.dismiss();

                discount_tr.setVisibility(View.GONE);
                discount = "0";
                couponValue = 0.0;
                couponCodeAppliedStatus = "0";
                couponCode = "";
                finalPrice = sstPrice;
                final_price_txt.setText("\u20B9 " + finalPrice);
                coupon_rl.setVisibility(View.VISIBLE);


            }
        });

        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialog.dismiss();

            }
        });
        alertDialog.setCancelable(false);
    }

    private void verifyCouponCode() {

        String coupon = coupon_edt.getText().toString().trim();
        if (!coupon.equalsIgnoreCase("")) {

            String url = AppUrls.BASE_URL + AppUrls.VERIFY_START_COUPON + coupon + AppUrls.VERIFY_END_COUPON;

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");

                        if (status.equalsIgnoreCase("2000")) {

                            coupon_rl.setVisibility(View.GONE);
                            discount_tr.setVisibility(View.VISIBLE);

                            couponCodeAppliedStatus = "1";

                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                            String couponId = jsonObject1.getString("couponId");
                            couponCode = jsonObject1.getString("couponCode");
                            discount = jsonObject1.getString("discount");
                            String startDateTime = jsonObject1.getString("startDateTime");
                            String expiryDateTime = jsonObject1.getString("expiryDateTime");
                            String type = jsonObject1.getString("type");
                            String deleted = jsonObject1.getString("deleted");
                            String createdTime = jsonObject1.getString("createdTime");
                            String modifiedTime = jsonObject1.getString("modifiedTime");

                            dicount_txt.setText("\u20B9 " + discount);

                            if (!dicount_txt.getText().toString().isEmpty()) {
                                couponValue = Double.parseDouble(discount);
                                estValue = Double.parseDouble(sstPrice);
                                Log.d("Prices", couponValue + "\n" + estValue);
                                finalValue = estValue - couponValue;
                                finalPrice = String.valueOf(finalValue);
                                discount_tr.setVisibility(View.VISIBLE);
                                final_price_txt.setText("\u20B9 " + finalPrice);
                            } else {
                                final_price_txt.setText("");

                                GlobalCalls.showToast(message, ConfirmationDetailActivity.this);

                            }

                            coupon_rl.setVisibility(View.GONE);
                            discount_tr.setVisibility(View.VISIBLE);
                        }
                        if (status.equalsIgnoreCase("5035")) {
                            couponCode = "";
                            final_price_txt.setText("");
                            GlobalCalls.showToast(message, ConfirmationDetailActivity.this);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            Log.d("COUPON", error.toString());
                            couponCodeAppliedStatus = "0";
                            showNegativeAlertDialog(R.layout.dialog_negative_layout);

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization", "Bearer " + accessToken);
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(ConfirmationDetailActivity.this);
            requestQueue.add(stringRequest);
        } else {

            GlobalCalls.showToast("Please Enter Coupon..!", ConfirmationDetailActivity.this);
        }
    }

    private void verifyCoupon() {

        String coupon = coupon_edt.getText().toString().trim();
        if (!coupon.equalsIgnoreCase("")) {

            String url = AppUrls.BASE_URL + AppUrls.VERIFY_START_COUPON + coupon + AppUrls.VERIFY_END_COUPON + sstId;

            Log.d("VERIFYCOUPON", url);

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");

                        if (status.equalsIgnoreCase("2000")) {

                            coupon_rl.setVisibility(View.GONE);
                            discount_tr.setVisibility(View.VISIBLE);

                            couponCodeAppliedStatus = "1";

                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                            String couponId = jsonObject1.getString("couponId");
                            couponCode = jsonObject1.getString("couponCode");
                            discount = jsonObject1.getString("discount");
                            String startDateTime = jsonObject1.getString("startDateTime");
                            String expiryDateTime = jsonObject1.getString("expiryDateTime");
                            String type = jsonObject1.getString("type");
                            String deleted = jsonObject1.getString("deleted");
                            String createdTime = jsonObject1.getString("createdTime");
                            String modifiedTime = jsonObject1.getString("modifiedTime");

                            dicount_txt.setText("\u20B9 " + discount);

                            if (!dicount_txt.getText().toString().isEmpty()) {
                                couponValue = Double.parseDouble(discount);
                                estValue = Double.parseDouble(sstPrice);
                                Log.d("Prices", couponValue + "\n" + estValue);
                                finalValue = estValue - couponValue;
                                finalPrice = String.valueOf(finalValue);
                                discount_tr.setVisibility(View.VISIBLE);
                                final_price_txt.setText("\u20B9 " + finalPrice);
                            } else {
                                final_price_txt.setText("");
                                GlobalCalls.showToast(message, ConfirmationDetailActivity.this);

                            }

                            showPositiveAlertDialog(R.layout.dialog_postive_layout);
                        }
                        if (status.equalsIgnoreCase("5035")) {
                            final_price_txt.setText("");
                            GlobalCalls.showToast(message, ConfirmationDetailActivity.this);
                            showNegativeAlertDialog(R.layout.dialog_negative_layout);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            Log.d("COUPON", error.toString());
                            couponCodeAppliedStatus = "0";
                            showNegativeAlertDialog(R.layout.dialog_negative_layout);

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization", "Bearer " + accessToken);
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(ConfirmationDetailActivity.this);
            requestQueue.add(stringRequest);
        } else {

            GlobalCalls.showToast("Please Enter Coupon..!", ConfirmationDetailActivity.this);

        }
    }

    private void showPositiveAlertDialog(int layout) {
        dialogBuilder = new AlertDialog.Builder(ConfirmationDetailActivity.this);
        View layoutView = getLayoutInflater().inflate(layout, null);
        TextView message_txt = layoutView.findViewById(R.id.message_txt);
        message_txt.setText("You Just Saved \u20B9 " + discount);
        Button dialogButton = layoutView.findViewById(R.id.btnDialog);
        dialogBuilder.setView(layoutView);
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();

                coupon_rl.setVisibility(View.GONE);
                discount_tr.setVisibility(View.VISIBLE);
            }
        });
        alertDialog.setCancelable(false);
    }

    private void showNegativeAlertDialog(int layout) {
        dialogBuilder = new AlertDialog.Builder(ConfirmationDetailActivity.this);
        View layoutView = getLayoutInflater().inflate(layout, null);
        TextView message_txt = layoutView.findViewById(R.id.message_txt);
        Button dialogButton = layoutView.findViewById(R.id.btnDialog);
        dialogBuilder.setView(layoutView);
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                couponCode = "";
                couponCodeAppliedStatus = "0";
                coupon_rl.setVisibility(View.VISIBLE);
                discount_tr.setVisibility(View.GONE);
            }
        });
        alertDialog.setCancelable(false);
    }

    @SuppressLint("DefaultLocale")
    private void pay() {

        if (bookingStartTime.equalsIgnoreCase("09:00:00")) {
            bookingStartTime = "03:30:00";
        } else if (bookingStartTime.equalsIgnoreCase("11:00:00")) {
            bookingStartTime = "05:30:00";
        } else if (bookingStartTime.equalsIgnoreCase("14:00:00")) {
            bookingStartTime = "08:30:00";
        } else if (bookingStartTime.equalsIgnoreCase("16:00:00")) {
            bookingStartTime = "10:30:00";
        }

        Log.d("Diff", bookingStartTime);

        String resultDate = convertStringDateToAnotherStringDate(bookingDate, "dd-MM-yyyy", "yyyy-MM-dd");
        Log.d("ResultDate", resultDate);

        Double sendPrice = Double.valueOf(finalPrice) + couponValue;

        String url = AppUrls.BASE_URL + AppUrls.PAY;

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("serviceCenterId", sstId);
            jsonObject.put("brandId", brandId);
            jsonObject.put("brandName", brandName);
            jsonObject.put("modelId", modelId);
            jsonObject.put("modelName", modelName);
            jsonObject.put("bookingDate", resultDate);
            jsonObject.put("bookingTime", bookingStartTime);
            jsonObject.put("pickupAddress", pickup_txt.getText().toString());
            jsonObject.put("pickupLandmark", pickup_txt.getText().toString());
            jsonObject.put("pickupAddressLatitude", latitude);
            jsonObject.put("pickupAddressLongitude", longitude);
            jsonObject.put("addressType", "3");
            jsonObject.put("bookingType", "1");
            jsonObject.put("promoCode", couponCode);
            jsonObject.put("couponCodeAppliedStatus", couponCodeAppliedStatus);
            jsonObject.put("estimatedCost", sendPrice);
            jsonObject.put("kilometerRangeId", kilometerRangeId);
            jsonObject.put("promocodeAmount", couponValue);
            jsonObject.put("initialPaidAmount", sstInitPrice);
            jsonObject.put("paymentStatus", "0");
            jsonObject.put("platformId", "1");
            jsonObject.put("movingCondition", condStr);
            jsonObject.put("contactMobileNumber", sstNumber);
            jsonObject.put("pickup", "true");
            jsonObject.put("dropValue", "true");
            jsonObject.put("sourceId", sourceId);
            jsonObject.put("sourceName", sourceName);

            JSONArray jsonArray = new JSONArray(jArray);
            jsonObject.put("services", jsonArray);

            JSONArray jsonArray1 = new JSONArray(inspections);
            jsonObject.put("inspections", jsonArray1);

            Log.d("Source", sourceId + "\n" + sourceName);
            Log.d("Array", "" + jsonArray);
            Log.d("Array1", "" + jsonArray1);
            Log.d("Object", "" + jsonObject.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("PayResp", response.toString());

                try {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    orderId = jsonObject.getString("orderId");
                    checksumValue = jsonObject.getString("checksumValue");

                    JSONObject jsonObject1 = jsonObject.getJSONObject("paytmParms");
                    CALLBACK_URL = jsonObject1.getString("CALLBACK_URL");
                    CHANNEL_ID = jsonObject1.getString("CHANNEL_ID");
                    CUST_ID = jsonObject1.getString("CUST_ID");
                    EMAIL = jsonObject1.getString("EMAIL");
                    INDUSTRY_TYPE_ID = jsonObject1.getString("INDUSTRY_TYPE_ID");
                    MID = jsonObject1.getString("MID");
                    MOBILE_NO = jsonObject1.getString("MOBILE_NO");
                    ORDER_ID = jsonObject1.getString("ORDER_ID");
                    TXN_AMOUNT = jsonObject1.getString("TXN_AMOUNT");
                    WEBSITE = jsonObject1.getString("WEBSITE");

                    Log.d("PayDetails", orderId + "\n" + checksumValue + "\n" + CALLBACK_URL + "\n" + CHANNEL_ID + "\n" + CUST_ID + "\n" +
                            EMAIL + "\n" + INDUSTRY_TYPE_ID + "\n" + MID + "\n" + MOBILE_NO + "\n" + ORDER_ID + "\n" + TXN_AMOUNT + "\n" + WEBSITE);

                    if (sstInitPrice.equalsIgnoreCase("0") || sstInitPrice.equalsIgnoreCase("") ||
                            sstInitPrice.equalsIgnoreCase(null)) {

                        GlobalCalls.showToast("Booking done successfully", ConfirmationDetailActivity.this);

                        Intent intent = new Intent(ConfirmationDetailActivity.this, MainActivity.class);
                        intent.putExtra("activity", "ConfirmationDetailActivity");
                        intent.putExtra("brandId", brandId);
                        intent.putExtra("modelId", modelId);
                        intent.putExtra("kilometerRangeId", kilometerRangeId);
                        intent.putExtra("brandName", brandName);
                        intent.putExtra("modelName", modelName);
                        intent.putExtra("kilometer", kilometer);
                        intent.putExtra("bookingDate", bookingDate);
                        intent.putExtra("bookingStartTime", bookingStartTime);
                        intent.putExtra("bookingEndTime", bookingEndTime);
                        intent.putExtra("bookingDisplayTime", bookingDisplayTime);
                        startActivity(intent);

                    } else {

                        Intent intent = new Intent(ConfirmationDetailActivity.this, CheckSum.class);
                        intent.putExtra("CALLBACK_URL", CALLBACK_URL);
                        intent.putExtra("CHANNEL_ID", CHANNEL_ID);
                        intent.putExtra("CUST_ID", CUST_ID);
                        intent.putExtra("EMAIL", EMAIL);
                        intent.putExtra("INDUSTRY_TYPE_ID", INDUSTRY_TYPE_ID);
                        intent.putExtra("MID", MID);
                        intent.putExtra("MOBILE_NO", MOBILE_NO);
                        intent.putExtra("ORDER_ID", ORDER_ID);
                        intent.putExtra("TXN_AMOUNT", TXN_AMOUNT);
                        intent.putExtra("WEBSITE", WEBSITE);
                        intent.putExtra("checksumValue", checksumValue);
                        intent.putExtra("couponCode", couponCode);
                        intent.putExtra("condStr", condStr);
                        startActivity(intent);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (ContextCompat.checkSelfPermission(ConfirmationDetailActivity.this,
                        Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(ConfirmationDetailActivity.this,
                            new String[]{Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS}, 101);

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(ConfirmationDetailActivity.this);
        requestQueue.add(request);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PLACE_PICKER_REQUEST && resultCode == RESULT_OK) {
            Place place = PlacePicker.getPlace(this, data);
            String placeName = String.format("" + place.getName());
            Double latitude = place.getLatLng().latitude;
            Double longitude = place.getLatLng().longitude;
            getAddress(latitude, longitude);
        }

    }

    public void getAddress(double lat, double lng) {
        Geocoder geocoder = new Geocoder(ConfirmationDetailActivity.this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            Address obj = addresses.get(0);
            String add = obj.getAddressLine(0);
            add = add + "\n" + obj.getCountryName();
            add = add + "\n" + obj.getCountryCode();
            add = add + "\n" + obj.getAdminArea();
            add = add + "\n" + obj.getPostalCode();
            add = add + "\n" + obj.getSubAdminArea();
            add = add + "\n" + obj.getLocality();
            add = add + "\n" + obj.getSubThoroughfare();

            Log.d("Address", obj.toString());
            if (obj.getLocality() != null) {
                pickup_txt.setText(obj.getAddressLine(0));
            } else {
                pickup_txt.setText(obj.getCountryName());
            }

            locationEditor.putString("latitude", String.valueOf(lat));
            locationEditor.putString("longitude", String.valueOf(lng));
            locationEditor.putString("locationName", pickup_txt.getText().toString());
            locationEditor.apply();

        } catch (IOException e) {
            e.printStackTrace();
            GlobalCalls.showToast(e.getMessage(), ConfirmationDetailActivity.this);
        }
    }

    public String convertStringDateToAnotherStringDate(String stringdate, String stringdateformat, String returndateformat) {

        try {
            Date date = new SimpleDateFormat(stringdateformat).parse(stringdate);
            String returndate = new SimpleDateFormat(returndateformat).format(date);
            return returndate;
        } catch (ParseException e) {
            e.printStackTrace();
            return stringdate;
        }

    }

    @Override
    public void onBackPressed() {

        finish();

    }
}
