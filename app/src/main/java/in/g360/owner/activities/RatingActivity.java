package in.g360.owner.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.hsalf.smilerating.SmileRating;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.g360.owner.MainActivity;
import in.g360.owner.R;
import in.g360.owner.utils.AppUrls;
import in.g360.owner.utils.GlobalCalls;
import in.g360.owner.utils.NetworkChecking;
import in.g360.owner.utils.UserSessionManager;

public class RatingActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close;
    private boolean checkInternet;
    TextView toolbar_title, rate_txt, rate_play_txt, service_txt, pick_txt, price_txt, comments_txt;
    SmileRating service_rating, pickup_rating, cost_rating;
    Button submit_btn;
    Button fab;
    AlertDialog.Builder dialogBuilder;
    AlertDialog alertDialog;
    EditText review_edt, concern_edt;
    UserSessionManager userSessionManager;
    String accessToken, bookingId;
    Typeface regular, bold;

    String serviceRating = "0", pickUpRating = "0", costingRating = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        checkInternet = NetworkChecking.isConnected(this);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);

        bookingId = getIntent().getStringExtra("bookingId");

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);

        close = findViewById(R.id.close);
        close.setOnClickListener(this);

        rate_txt = findViewById(R.id.rate_txt);
        rate_txt.setTypeface(bold);
        service_txt = findViewById(R.id.service_txt);
        service_txt.setTypeface(bold);
        pick_txt = findViewById(R.id.pick_txt);
        pick_txt.setTypeface(bold);
        price_txt = findViewById(R.id.price_txt);
        price_txt.setTypeface(bold);
        comments_txt = findViewById(R.id.comments_txt);
        comments_txt.setTypeface(bold);
        rate_play_txt = findViewById(R.id.rate_play_txt);
        rate_play_txt.setTypeface(regular);
        rate_play_txt.setPaintFlags(rate_play_txt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        rate_play_txt.setOnClickListener(this);
        review_edt = findViewById(R.id.review_edt);
        service_rating = findViewById(R.id.service_rating);
        pickup_rating = findViewById(R.id.pickup_rating);
        cost_rating = findViewById(R.id.cost_rating);
        submit_btn = findViewById(R.id.submit_btn);
        submit_btn.setOnClickListener(this);
        fab = findViewById(R.id.fab);
        fab.setTypeface(bold);
        fab.setOnClickListener(this);

        service_rating.setOnRatingSelectedListener(new SmileRating.OnRatingSelectedListener() {
            @Override
            public void onRatingSelected(int level, boolean reselected) {
                serviceRating = String.valueOf(level);

                Log.d("ServiceRatings", serviceRating);
            }
        });

        pickup_rating.setOnRatingSelectedListener(new SmileRating.OnRatingSelectedListener() {
            @Override
            public void onRatingSelected(int level, boolean reselected) {
                pickUpRating = String.valueOf(level);
                Log.d("PickUpRatings", pickUpRating);
            }
        });

        cost_rating.setOnRatingSelectedListener(new SmileRating.OnRatingSelectedListener() {
            @Override
            public void onRatingSelected(int level, boolean reselected) {
                costingRating = String.valueOf(level);
                Log.d("CostingRatings", costingRating);
            }
        });

    }

    @Override
    public void onClick(View v) {
        if (v == close) {
            if (checkInternet) {
                Intent intent = new Intent(RatingActivity.this, MainActivity.class);
                intent.putExtra("activity", "RatingActivity");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == rate_play_txt) {
            if (checkInternet) {
                final String appPackageName = getPackageName();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == submit_btn) {
            if (checkInternet) {

                if (serviceRating.equalsIgnoreCase("0") || pickUpRating.equalsIgnoreCase("0") || costingRating.equalsIgnoreCase("0")) {

                    GlobalCalls.showToast("Please Give Rating For All Services..!", RatingActivity.this);


                } else {

                    if (serviceRating.equalsIgnoreCase("1") || pickUpRating.equalsIgnoreCase("1") ||
                            costingRating.equalsIgnoreCase("1")) {

                        showConcernAlertDialog(R.layout.dialog_concern_layout);

                    } else {
                        sendRating();
                    }
                }

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
        if (v == fab) {
            if (checkInternet) {

                showConcernAlertDialog(R.layout.dialog_concern_layout);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }

    private void sendRating() {

        String url = AppUrls.BASE_URL + AppUrls.RATING;

        JSONObject jsonObj = new JSONObject();

        try {
            jsonObj.put("bookingId", bookingId);
            jsonObj.put("costingRating", costingRating);
            jsonObj.put("pickupRating", pickUpRating);
            jsonObj.put("serviceRating", serviceRating);
            jsonObj.put("review", review_edt.getText().toString());

            Log.d("Ratings", serviceRating + "\n" + pickUpRating + "\n" + costingRating);

        } catch (Exception e) {

        }

        Log.d("JsonObj", jsonObj.toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("RatingStatus", response.toString());

                try {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                    String ratingId = jsonObject1.getString("ratingId");

                    Log.d("Status", status + "\n" + message);

                    if (status.equalsIgnoreCase("2000")) {

                        GlobalCalls.showToast("Successfully Rated..!", RatingActivity.this);

                        Intent intent = new Intent(RatingActivity.this, MainActivity.class);
                        intent.putExtra("activity", "RatingActivity");
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();

                    } else {
                        GlobalCalls.showToast("Something went wrong..!", RatingActivity.this);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                    Log.d("ResError", e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(RatingActivity.this);
        requestQueue.add(request);

    }

    private void showConcernAlertDialog(int layout) {
        dialogBuilder = new AlertDialog.Builder(RatingActivity.this);
        View layoutView = getLayoutInflater().inflate(layout, null);
        concern_edt = layoutView.findViewById(R.id.concern_edt);
        Button cancel_btn = layoutView.findViewById(R.id.cancel_btn);
        Button submit_btn = layoutView.findViewById(R.id.submit_btn);
        dialogBuilder.setView(layoutView);
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!concern_edt.getText().toString().equalsIgnoreCase("")) {
                    sendConcern();
                } else {
                    concern_edt.setError("Please Enter Concern");
                }

            }
        });
        alertDialog.setCancelable(false);
    }

    private void sendConcern() {
        String url = AppUrls.BASE_URL + AppUrls.CONCERN;

        JSONObject jsonObj = new JSONObject();

        try {
            jsonObj.put("bookingId", bookingId);
            jsonObj.put("concernMessage", concern_edt.getText().toString());

        } catch (Exception e) {

        }

        Log.d("JsonObj", jsonObj.toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("ConcernStatus", response.toString());

                try {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    Log.d("Status", status + "\n" + message);

                    if (status.equalsIgnoreCase("2000")) {
                        alertDialog.dismiss();
                        GlobalCalls.showToast("Your Concern Submitted Successfully..!", RatingActivity.this);


                        sendRating();

                    } else {
                        GlobalCalls.showToast("Something went wrong..!", RatingActivity.this);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                    Log.d("ResError", e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

                Log.d("EROORRR", error.getMessage());

            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(RatingActivity.this);
        requestQueue.add(request);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(RatingActivity.this, MainActivity.class);
        intent.putExtra("activity", "RatingActivity");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
