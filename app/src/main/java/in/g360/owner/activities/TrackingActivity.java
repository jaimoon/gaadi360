package in.g360.owner.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import in.g360.owner.R;
import in.g360.owner.utils.AppUrls;
import in.g360.owner.utils.NetworkChecking;
import in.g360.owner.utils.UserSessionManager;

public class TrackingActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close, assign_img, pick_com_img, ser_start_img, ser_com_img, drop_start_img, delivered_img;
    TextView toolbar_title, booking_id_txt, otp_txt, assign_time_txt, pick_com_txt, ser_start_txt, ser_com_txt, drop_start_txt, delivered_txt, assign_time, pick_com, ser_start,
            ser_com, drop_start, delivered;
    private boolean checkInternet;
    String accessToken, bookingId, brandName, modelName, registrationNumber, pickupAssignTime, pickedUpTime, mechanicAssignTime,
            serviceCompletedTime, paymentPaidTime, dropOffTime, deliveredTime, status, paymentStatus, pickupOtp, dropoffOtp;
    Typeface regular, bold;
    UserSessionManager userSessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        checkInternet = NetworkChecking.isConnected(this);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);

        bookingId = getIntent().getStringExtra("bookingId");

        //String resultDate = convertStringDateToAnotherStringDate(bookingDate, "yyyy-MM-dd", "dd-MM-yyyy");

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);

        close = findViewById(R.id.close);
        close.setOnClickListener(this);
        booking_id_txt = findViewById(R.id.booking_id_txt);
        booking_id_txt.setTypeface(bold);
        booking_id_txt.setText("Booking ID : " + bookingId);
        otp_txt = findViewById(R.id.otp_txt);
        otp_txt.setTypeface(bold);
        assign_time_txt = findViewById(R.id.assign_time_txt);
        assign_time_txt.setTypeface(regular);

        pick_com_txt = findViewById(R.id.pick_com_txt);
        pick_com_txt.setTypeface(regular);
        ser_start_txt = findViewById(R.id.ser_start_txt);
        ser_start_txt.setTypeface(regular);
        ser_com_txt = findViewById(R.id.ser_com_txt);
        ser_com_txt.setTypeface(regular);
        drop_start_txt = findViewById(R.id.drop_start_txt);
        drop_start_txt.setTypeface(regular);
        delivered_txt = findViewById(R.id.delivered_txt);
        delivered_txt.setTypeface(regular);
        assign_time = findViewById(R.id.assign_time);
        assign_time.setTypeface(regular);
        pick_com = findViewById(R.id.pick_com);
        pick_com.setTypeface(regular);
        ser_start = findViewById(R.id.ser_start);
        ser_start.setTypeface(regular);
        ser_com = findViewById(R.id.ser_com);
        ser_com.setTypeface(regular);
        drop_start = findViewById(R.id.drop_start);
        drop_start.setTypeface(regular);
        delivered = findViewById(R.id.delivered);
        delivered.setTypeface(regular);

        assign_img = findViewById(R.id.assign_img);
        pick_com_img = findViewById(R.id.pick_com_img);
        ser_start_img = findViewById(R.id.ser_start_img);
        ser_com_img = findViewById(R.id.ser_com_img);
        drop_start_img = findViewById(R.id.drop_start_img);
        delivered_img = findViewById(R.id.delivered_img);

        getTrackingDetails();
    }

    private void getTrackingDetails() {
        String url = AppUrls.BASE_URL + AppUrls.TRACKING + bookingId + "/tracking-details";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String responceCode = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    if (responceCode.equalsIgnoreCase("2000")) {

                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                        bookingId = jsonObject1.getString("bookingId");
                        brandName = jsonObject1.getString("brandName");
                        modelName = jsonObject1.getString("modelName");
                        registrationNumber = jsonObject1.getString("registrationNumber");
                        pickupAssignTime = jsonObject1.getString("pickupAssignTime");
                        pickedUpTime = jsonObject1.getString("pickedUpTime");
                        mechanicAssignTime = jsonObject1.getString("mechanicAssignTime");
                        serviceCompletedTime = jsonObject1.getString("serviceCompletedTime");
                        paymentPaidTime = jsonObject1.getString("paymentPaidTime");
                        dropOffTime = jsonObject1.getString("dropOffTime");
                        deliveredTime = jsonObject1.getString("deliveredTime");
                        status = jsonObject1.getString("status");
                        paymentStatus = jsonObject1.getString("paymentStatus");
                        pickupOtp = jsonObject1.getString("pickupOtp");
                        dropoffOtp = jsonObject1.getString("dropoffOtp");

                        int otpStatus = Integer.parseInt(status);
                        if (otpStatus < 3) {
                            otp_txt.setVisibility(View.VISIBLE);
                            otp_txt.setText("Pick Up Otp : " + pickupOtp);
                        } else if (otpStatus != 7 && otpStatus != 12) {
                            otp_txt.setVisibility(View.VISIBLE);
                            otp_txt.setText("Drop Off Otp : " + dropoffOtp);
                        }



                        getTracking();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(TrackingActivity.this);
        requestQueue.add(stringRequest);
    }

    private void getTracking() {
        if (status.equalsIgnoreCase("1")) {

            assign_time.setTextColor(Color.parseColor("#003171"));
            assign_img.setBackgroundResource(R.drawable.round_small_theme_bg);

            assign_time_txt.setText(pickupAssignTime);

        } else if (status.equalsIgnoreCase("2")) {

            assign_time.setTextColor(Color.parseColor("#003171"));
            assign_img.setBackgroundResource(R.drawable.round_small_theme_bg);

            assign_time_txt.setText(pickupAssignTime);

        } else if (status.equalsIgnoreCase("3")) {

            assign_time.setTextColor(Color.parseColor("#003171"));
            pick_com.setTextColor(Color.parseColor("#003171"));
            assign_img.setBackgroundResource(R.drawable.round_small_theme_bg);
            pick_com_img.setBackgroundResource(R.drawable.round_small_theme_bg);

            assign_time_txt.setText(pickupAssignTime);
            pick_com_txt.setText(pickedUpTime);

        } else if (status.equalsIgnoreCase("4")) {

            assign_time.setTextColor(Color.parseColor("#003171"));
            pick_com.setTextColor(Color.parseColor("#003171"));
            ser_start.setTextColor(Color.parseColor("#003171"));
            assign_img.setBackgroundResource(R.drawable.round_small_theme_bg);
            pick_com_img.setBackgroundResource(R.drawable.round_small_theme_bg);
            ser_start_img.setBackgroundResource(R.drawable.round_small_theme_bg);

            assign_time_txt.setText(pickupAssignTime);
            pick_com_txt.setText(pickedUpTime);
            ser_start_txt.setText(mechanicAssignTime);

        } else if (status.equalsIgnoreCase("5")) {

            assign_time.setTextColor(Color.parseColor("#003171"));
            pick_com.setTextColor(Color.parseColor("#003171"));
            ser_start.setTextColor(Color.parseColor("#003171"));
            ser_com.setTextColor(Color.parseColor("#003171"));
            assign_img.setBackgroundResource(R.drawable.round_small_theme_bg);
            pick_com_img.setBackgroundResource(R.drawable.round_small_theme_bg);
            ser_start_img.setBackgroundResource(R.drawable.round_small_theme_bg);
            ser_com_img.setBackgroundResource(R.drawable.round_small_theme_bg);

            assign_time_txt.setText(pickupAssignTime);
            pick_com_txt.setText(pickedUpTime);
            ser_start_txt.setText(mechanicAssignTime);
            ser_com_txt.setText(serviceCompletedTime);

        } else if (status.equalsIgnoreCase("6")) {

            assign_time.setTextColor(Color.parseColor("#003171"));
            pick_com.setTextColor(Color.parseColor("#003171"));
            ser_start.setTextColor(Color.parseColor("#003171"));
            ser_com.setTextColor(Color.parseColor("#003171"));
            drop_start.setTextColor(Color.parseColor("#003171"));
            assign_img.setBackgroundResource(R.drawable.round_small_theme_bg);
            pick_com_img.setBackgroundResource(R.drawable.round_small_theme_bg);
            ser_start_img.setBackgroundResource(R.drawable.round_small_theme_bg);
            ser_com_img.setBackgroundResource(R.drawable.round_small_theme_bg);
            drop_start_img.setBackgroundResource(R.drawable.round_small_theme_bg);

            assign_time_txt.setText(pickupAssignTime);
            pick_com_txt.setText(pickedUpTime);
            ser_start_txt.setText(mechanicAssignTime);
            ser_com_txt.setText(serviceCompletedTime);
            drop_start_txt.setText(dropOffTime);

        } else if (status.equalsIgnoreCase("7")) {

            assign_time.setTextColor(Color.parseColor("#003171"));
            pick_com.setTextColor(Color.parseColor("#003171"));
            ser_start.setTextColor(Color.parseColor("#003171"));
            ser_com.setTextColor(Color.parseColor("#003171"));
            drop_start.setTextColor(Color.parseColor("#003171"));
            delivered.setTextColor(Color.parseColor("#003171"));
            assign_img.setBackgroundResource(R.drawable.round_small_theme_bg);
            pick_com_img.setBackgroundResource(R.drawable.round_small_theme_bg);
            ser_start_img.setBackgroundResource(R.drawable.round_small_theme_bg);
            ser_com_img.setBackgroundResource(R.drawable.round_small_theme_bg);
            drop_start_img.setBackgroundResource(R.drawable.round_small_theme_bg);
            delivered_img.setBackgroundResource(R.drawable.round_small_theme_bg);

            assign_time_txt.setText(pickupAssignTime);
            pick_com_txt.setText(pickedUpTime);
            ser_start_txt.setText(mechanicAssignTime);
            ser_com_txt.setText(serviceCompletedTime);
            drop_start_txt.setText(dropOffTime);
            delivered_txt.setText(deliveredTime);

        } else if (status.equalsIgnoreCase("8")) {

            assign_time.setTextColor(Color.parseColor("#003171"));
            pick_com.setTextColor(Color.parseColor("#003171"));
            ser_start.setTextColor(Color.parseColor("#003171"));
            ser_com.setTextColor(Color.parseColor("#003171"));
            drop_start.setTextColor(Color.parseColor("#003171"));

            assign_img.setBackgroundResource(R.drawable.round_small_theme_bg);
            pick_com_img.setBackgroundResource(R.drawable.round_small_theme_bg);
            ser_start_img.setBackgroundResource(R.drawable.round_small_theme_bg);
            ser_com_img.setBackgroundResource(R.drawable.round_small_theme_bg);
            drop_start_img.setBackgroundResource(R.drawable.round_small_theme_bg);

            assign_time_txt.setText(pickupAssignTime);
            pick_com_txt.setText(pickedUpTime);
            ser_start_txt.setText(mechanicAssignTime);
            ser_com_txt.setText(serviceCompletedTime);
            drop_start_txt.setText(dropOffTime);

        }
    }

    @Override
    public void onClick(View v) {
        if (checkInternet) {
            Intent intent = new Intent(TrackingActivity.this, HistoryActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();

        } else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    public String convertStringDateToAnotherStringDate(String stringdate, String stringdateformat, String returndateformat) {

        try {
            Date date = new SimpleDateFormat(stringdateformat).parse(stringdate);
            String returndate = new SimpleDateFormat(returndateformat).format(date);
            return returndate;
        } catch (ParseException e) {
            e.printStackTrace();
            return stringdate;
        }

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(TrackingActivity.this, HistoryActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
