package in.g360.owner.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import in.g360.owner.R;
import in.g360.owner.adapters.ServiceCenterAdapter;
import in.g360.owner.models.ServiceCenterModel;
import in.g360.owner.utils.AppUrls;
import in.g360.owner.utils.GlobalCalls;
import in.g360.owner.utils.UserSessionManager;

public class ServiceCenterActivity extends AppCompatActivity implements View.OnClickListener {

    private FirebaseAnalytics mFirebaseAnalytics;
    ImageView close;
    RecyclerView service_center_recyclervew;
    /*Service Detail*/
    ArrayList<ServiceCenterModel> serviceCenterModels = new ArrayList<ServiceCenterModel>();
    ServiceCenterAdapter serviceCenterAdapter;

    SharedPreferences preferences, sstPreferences, locationPref;
    String brandId, modelId, kilometerRangeId, bookingDate, bookingStartTime, bookingEndTime, bookingDisplayTime, sstId, sstName, sstAddress,
            sstDistance, sstInitPrice, sstPrice, jArray, latitude, longitude, locationName,condStr,accessToken, userMobile;
    Typeface regular, bold;
    UserSessionManager userSessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_center);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);
        userMobile = userDetails.get(UserSessionManager.USER_MOBILE);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        //Logs an app event.
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ID-ServiceCenterActivity");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "ServiceCenterActivity");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
        mFirebaseAnalytics.logEvent("ServiceCenter_PageLoad_New", bundle);

        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(20000);
        mFirebaseAnalytics.setSessionTimeoutDuration(500);
        mFirebaseAnalytics.setUserId(userMobile);
        mFirebaseAnalytics.setUserProperty("ServiceCenterActivity", userMobile);

        GlobalCalls.events("ServiceCenter_PageLoad_New",ServiceCenterActivity.this);


        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        condStr = getIntent().getStringExtra("condStr");

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        brandId = preferences.getString("brandId", "");
        modelId = preferences.getString("modelId", "");
        kilometerRangeId = preferences.getString("kilometerRangeId", "");
        bookingDate = preferences.getString("bookingDate", "");
        bookingStartTime = preferences.getString("bookingStartTime", "");
        bookingEndTime = preferences.getString("bookingEndTime", "");
        bookingDisplayTime = preferences.getString("bookingDisplayTime", "");

        Log.d("BikePreferences", brandId + "\n" + modelId + "\n" + bookingDate + "\n" + bookingStartTime + "\n" + bookingEndTime + "\n" + bookingDisplayTime);

        sstPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sstId = sstPreferences.getString("sstId", "");
        sstName = sstPreferences.getString("sstName", "");
        sstAddress = sstPreferences.getString("sstAddress", "");
        sstDistance = sstPreferences.getString("sstDistance", "");
        sstInitPrice = sstPreferences.getString("sstInitPrice", "");
        sstPrice = sstPreferences.getString("sstPrice", "");
        jArray = sstPreferences.getString("jArray", "");

        Log.d("SSTPreferences", sstName + "\n" + sstAddress + "\n" + sstDistance + "\n" + sstInitPrice + "\n" + sstPrice + "\n" + jArray);

        locationPref = getSharedPreferences("Location", Context.MODE_PRIVATE);
        latitude = locationPref.getString("latitude", "");
        longitude = locationPref.getString("longitude", "");
        locationName = locationPref.getString("locationName", "");

        Log.d("GetLATLNG", latitude + "\n" + longitude + "\n" + locationName);

        close = findViewById(R.id.close);
        close.setOnClickListener(this);

        service_center_recyclervew = findViewById(R.id.service_center_recyclervew);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        service_center_recyclervew.setLayoutManager(layoutManager);
        serviceCenterAdapter = new ServiceCenterAdapter(condStr,serviceCenterModels, ServiceCenterActivity.this, R.layout.row_service_center);

        getServiceCenters();

    }

    private void getServiceCenters() {

        String resultDate = convertStringDateToAnotherStringDate(bookingDate, "dd-MM-yyyy", "yyyy-MM-dd");
        Log.d("ResultDate", bookingDate + "\n" + resultDate);

        String url = AppUrls.BASE_URL + AppUrls.SERVICECENTERS;
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("bookingDate", resultDate);
            jsonObject.put("bookingTime", bookingEndTime);
            jsonObject.put("brandId", brandId);
            jsonObject.put("modelId", modelId);
            jsonObject.put("bookingType", 1);
            jsonObject.put("pickupAddressLatitude", latitude);
            jsonObject.put("pickupAddressLongitude", longitude);
            JSONArray jsonArray = new JSONArray(jArray);
            jsonObject.put("services", jsonArray);

            Log.d("ServicesArray", "" + jsonArray);
            Log.d("ServicesObject", "" + jsonObject.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                serviceCenterModels.clear();

                Log.d("SuccessResp", response.toString());

                try {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    if (status.equalsIgnoreCase("2000")) {

                        JSONArray jsonArray = jsonObject.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            ServiceCenterModel scm = new ServiceCenterModel();
                            scm.setServiceCenterId(jsonObject1.optString("serviceCenterId"));
                            scm.setServiceCenterName(jsonObject1.optString("serviceCenterName"));
                            scm.setContactName(jsonObject1.optString("contactName"));
                            scm.setUserId(jsonObject1.optString("userId"));
                            scm.setContactMobileNumber(jsonObject1.optString("contactMobileNumber"));
                            scm.setContactEmail(jsonObject1.optString("contactEmail"));
                            scm.setAddress(jsonObject1.optString("address"));
                            scm.setLatitude(jsonObject1.optString("latitude"));
                            scm.setLongitude(jsonObject1.optString("longitude"));
                            scm.setOpenTime(jsonObject1.optString("openTime"));
                            scm.setCloseTime(jsonObject1.optString("closeTime"));
                            scm.setDeleted(jsonObject1.optString("deleted"));
                            scm.setCreatedTime(jsonObject1.optString("createdTime"));
                            scm.setModifiedTime(jsonObject1.optString("modifiedTime"));
                            scm.setDisplayTime(jsonObject1.optString("displayTime"));
                            scm.setEstimatedCost(jsonObject1.optString("estimatedCost"));
                            scm.setInitialPaidAmount(jsonObject1.optString("initialPaidAmount"));

                            Double dd = (Double.valueOf(jsonObject1.optString("distance"))) / 1000;
                            scm.setDistance(String.format("%.2f", dd));
                            scm.setAverageRating(jsonObject1.optString("averageRating"));

                            serviceCenterModels.add(scm);
                        }

                        service_center_recyclervew.setAdapter(serviceCenterAdapter);
                        serviceCenterAdapter.notifyDataSetChanged();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(ServiceCenterActivity.this);
        requestQueue.add(request);
    }

    public String convertStringDateToAnotherStringDate(String stringdate, String stringdateformat, String returndateformat) {

        try {
            Date date = new SimpleDateFormat(stringdateformat).parse(stringdate);
            String returndate = new SimpleDateFormat(returndateformat).format(date);
            return returndate;
        } catch (ParseException e) {
            e.printStackTrace();
            return stringdate;
        }

    }

    @Override
    public void onClick(View v) {

        if (v == close) {

            Intent intent = new Intent(ServiceCenterActivity.this, ConfirmationDetailActivity.class);
            intent.putExtra("activity", "ServiceCenterActivity");
            intent.putExtra("condStr", condStr);
            startActivity(intent);
            finish();
        }

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ServiceCenterActivity.this, ConfirmationDetailActivity.class);
        intent.putExtra("activity", "ServiceCenterActivity");
        intent.putExtra("condStr", condStr);
        startActivity(intent);
        finish();
    }
}
