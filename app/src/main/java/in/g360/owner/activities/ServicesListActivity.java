package in.g360.owner.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.stream.Collectors;

import in.g360.owner.MainActivity;
import in.g360.owner.R;
import in.g360.owner.adapters.GeneralServiceAdapter;
import in.g360.owner.adapters.InfoAdapter;
import in.g360.owner.adapters.InspectionAdapter;
import in.g360.owner.adapters.RecServiceAdapter;
import in.g360.owner.models.GeneralServiceInfoModel;
import in.g360.owner.models.GetInspectionModel;
import in.g360.owner.models.GetServiceCenterModel;
import in.g360.owner.models.InspectionModel;
import in.g360.owner.models.RecServiceModel;
import in.g360.owner.models.ServiceCenterModel;
import in.g360.owner.models.ServiceDetailModel;
import in.g360.owner.utils.AppUrls;
import in.g360.owner.utils.GlobalCalls;
import in.g360.owner.utils.NetworkChecking;
import in.g360.owner.utils.UserSessionManager;

public class ServicesListActivity extends AppCompatActivity implements View.OnClickListener {

    private FirebaseAnalytics mFirebaseAnalytics;
    private boolean checkInternet;
    ImageView close, sst_img, location_img, distance_img;
    CheckBox gen_chk, free_chk;
    TextView toolbar_title, sst_name_txt, sst_address, distance_txt, price_txt, next_txt, deal_title, inspection_title, brand_txt, date_txt,
            model_txt, time_txt, bike_txt, cond_txt, strike_txt, free_txt, charges_txt;
    RadioGroup cond_rg;
    RadioButton yes_rb, no_rb;
    RecyclerView gen_recyclervew, rec_recyclervew, inspection_recyclervew;
    AlertDialog dialog;
    RelativeLayout service_center_rl;
    LinearLayout free_ll;

    /*General Service*/
    ArrayList<GeneralServiceInfoModel> generalServiceInfoModels;
    GeneralServiceAdapter generalServiceAdapter;

    /*RecServices*/
    ArrayList<RecServiceModel> recServiceModels;
    RecServiceAdapter recServiceAdapter;

    /*Service Detail*/
    ArrayList<ServiceDetailModel> serviceDetailModels;
    InfoAdapter infoAdapter;

    /*Inspections*/
    ArrayList<InspectionModel> inspectionModels;
    InspectionAdapter inspectionAdapter;

    /*Service Detail*/
    ArrayList<ServiceCenterModel> serviceCenterModels = new ArrayList<ServiceCenterModel>();

    /*GetServices*/
    ArrayList<GetServiceCenterModel> getServiceCenterModels = new ArrayList<GetServiceCenterModel>();

    /*GetInspections*/
    ArrayList<GetInspectionModel> getInspectionModels = new ArrayList<GetInspectionModel>();

    SharedPreferences preferences, locationPref, sstPreferences, servicePreferences, inspectionPreferences;
    SharedPreferences.Editor sstEditor, serviceEditor, inspectionEditor;
    String latitude, longitude, locationName, brandId, modelId, kilometerRangeId, bookingDate, bookingStartTime, bookingEndTime, bookingDisplayTime,
            genServiceId, genServiceName, sstId, sstName, sstAddress, sstDistance, sstInitPrice, sstPrice, sstRating, sstOpenTime, sstCloseTime,
            sstPerson, serviceId, serviceName, serviceType, inspectionId, inspectionName, inspectionState, brandName, modelName, kilometer,
            sstDisplayTime, sstNumber, condStr, accessToken, userMobile;
    Typeface regular, bold;
    UserSessionManager userSessionManager;

    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services_list);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        sstPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sstEditor = sstPreferences.edit();
        //sstPrice = sstPreferences.getString("sstPrice", "");

        servicePreferences = PreferenceManager.getDefaultSharedPreferences(this);
        serviceEditor = servicePreferences.edit();

        inspectionPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        inspectionEditor = inspectionPreferences.edit();

        checkInternet = NetworkChecking.isConnected(this);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        brandId = preferences.getString("brandId", "");
        brandName = preferences.getString("brandName", "");
        modelId = preferences.getString("modelId", "");
        modelName = preferences.getString("modelName", "");
        kilometer = preferences.getString("kilometer", "");
        kilometerRangeId = preferences.getString("kilometerRangeId", "");
        bookingDate = preferences.getString("bookingDate", "");
        bookingStartTime = preferences.getString("bookingStartTime", "");
        bookingEndTime = preferences.getString("bookingEndTime", "");
        bookingDisplayTime = preferences.getString("bookingDisplayTime", "");

        Log.d("Preferences", brandId + "\n" + brandName + "\n" + modelId + "\n" + modelName + "\n" + kilometer + "\n" +
                kilometerRangeId + "\n" + bookingDate + "\n" + bookingStartTime + "\n" + bookingEndTime + "\n" + bookingDisplayTime);


        locationPref = getSharedPreferences("Location", Context.MODE_PRIVATE);
        //locationPref = PreferenceManager.getDefaultSharedPreferences(this);
        latitude = locationPref.getString("latitude", "");
        longitude = locationPref.getString("longitude", "");
        locationName = locationPref.getString("locationName", "");

        Log.d("GetLATLNG", latitude + "\n" + longitude + "\n" + locationName);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);
        userMobile = userDetails.get(UserSessionManager.USER_MOBILE);

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);
        close = findViewById(R.id.close);
        close.setOnClickListener(this);
        location_img = findViewById(R.id.location_img);
        distance_img = findViewById(R.id.distance_img);
        sst_img = findViewById(R.id.sst_img);
        gen_chk = findViewById(R.id.gen_chk);
        gen_chk.setTypeface(regular);
        deal_title = findViewById(R.id.deal_title);
        deal_title.setTypeface(bold);
        inspection_title = findViewById(R.id.inspection_title);
        inspection_title.setTypeface(bold);
        sst_name_txt = findViewById(R.id.sst_name_txt);
        sst_name_txt.setTypeface(bold);
        sst_address = findViewById(R.id.sst_address);
        sst_address.setTypeface(regular);
        distance_txt = findViewById(R.id.distance_txt);
        distance_txt.setTypeface(regular);
        price_txt = findViewById(R.id.price_txt);
        price_txt.setTypeface(bold);
        next_txt = findViewById(R.id.next_txt);
        next_txt.setTypeface(bold);
        next_txt.setOnClickListener(this);
        service_center_rl = findViewById(R.id.service_center_rl);
        free_ll = findViewById(R.id.free_ll);
        bike_txt = findViewById(R.id.bike_txt);
        bike_txt.setTypeface(bold);
        brand_txt = findViewById(R.id.brand_txt);
        brand_txt.setTypeface(regular);
        brand_txt.setText(brandName);
        date_txt = findViewById(R.id.date_txt);
        date_txt.setTypeface(regular);
        date_txt.setText(bookingDate);
        model_txt = findViewById(R.id.model_txt);
        model_txt.setTypeface(regular);
        model_txt.setText(modelName);
        time_txt = findViewById(R.id.time_txt);
        time_txt.setTypeface(regular);
        time_txt.setText(bookingDisplayTime);
        free_chk = findViewById(R.id.free_chk);
        free_chk.setTypeface(bold);
        strike_txt = findViewById(R.id.strike_txt);
        strike_txt.setTypeface(bold);
        strike_txt.setPaintFlags(strike_txt.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        free_txt = findViewById(R.id.free_txt);
        free_txt.setTypeface(bold);
        charges_txt = findViewById(R.id.charges_txt);
        charges_txt.setTypeface(bold);
        cond_txt = findViewById(R.id.cond_txt);
        cond_txt.setTypeface(bold);
        cond_rg = findViewById(R.id.cond_rg);
        yes_rb = findViewById(R.id.yes_rb);
        no_rb = findViewById(R.id.no_rb);

        cond_rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {

                    case R.id.yes_rb:

                        free_ll.setVisibility(View.VISIBLE);
                        strike_txt.setVisibility(View.VISIBLE);
                        free_txt.setVisibility(View.VISIBLE);
                        charges_txt.setVisibility(View.GONE);
                        condStr = "true";
                        break;

                    case R.id.no_rb:

                        free_ll.setVisibility(View.VISIBLE);
                        strike_txt.setVisibility(View.GONE);
                        free_txt.setVisibility(View.GONE);
                        charges_txt.setVisibility(View.VISIBLE);
                        condStr = "false";
                        break;

                }
            }
        });

        //service_center_rl.setOnClickListener(this);

        generalServiceInfoModels = new ArrayList<GeneralServiceInfoModel>();
        gen_recyclervew = findViewById(R.id.gen_recyclervew);
        generalServiceAdapter = new GeneralServiceAdapter(generalServiceInfoModels, ServicesListActivity.this, R.layout.row_general_service);
        gen_recyclervew.setLayoutManager(new GridLayoutManager(this, 5, GridLayoutManager.HORIZONTAL, false));

        serviceDetailModels = new ArrayList<ServiceDetailModel>();

        recServiceModels = new ArrayList<RecServiceModel>();
        rec_recyclervew = findViewById(R.id.rec_recyclervew);
        recServiceAdapter = new RecServiceAdapter(recServiceModels, ServicesListActivity.this, R.layout.row_rec_service);
        rec_recyclervew.setLayoutManager(new GridLayoutManager(this, 2, GridLayoutManager.HORIZONTAL, false));

        inspectionModels = new ArrayList<InspectionModel>();
        inspection_recyclervew = findViewById(R.id.inspection_recyclervew);
        inspectionAdapter = new InspectionAdapter(inspectionModels, ServicesListActivity.this, R.layout.row_inspection);
        inspection_recyclervew.setLayoutManager(new GridLayoutManager(this, 2, GridLayoutManager.HORIZONTAL, false));

        getGeneralService();

        gen_chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (compoundButton.isChecked()) {
                    gen_chk.setTextColor(Color.BLACK);
                    gen_chk.setButtonTintList(ColorStateList.valueOf(Color.BLACK));
                    gen_recyclervew.setBackgroundResource(R.drawable.recycler_boarder);

                    String checked = "1";
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        getServiceIds(checked, genServiceId, genServiceName);
                    }

                } else {
                    String checked = "0";
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        getServiceIds(checked, genServiceId, genServiceName);
                    }
                    gen_chk.setTextColor(Color.GRAY);
                    gen_chk.setButtonTintList(ColorStateList.valueOf(Color.GRAY));
                    gen_recyclervew.setBackgroundResource(R.drawable.searchview_boarder);
                }
            }
        });

        //Logs an app event.
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ID-ServicesListActivity");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "ServicesListActivity");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
        mFirebaseAnalytics.logEvent("Servicepage_PageLoad_New", bundle);

        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(20000);
        mFirebaseAnalytics.setSessionTimeoutDuration(500);
        mFirebaseAnalytics.setUserId(userMobile);
        mFirebaseAnalytics.setUserProperty("ServicesListActivity", userMobile);

        GlobalCalls.events("Servicepage_PageLoad_New",ServicesListActivity.this);


    }

    private void getGeneralService() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.GENERAL_SERVICE + modelId,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        generalServiceInfoModels.clear();

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String responceCode = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (responceCode.equalsIgnoreCase("2000")) {

                                getRecServices();

                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                genServiceId = jsonObject1.getString("serviceId");
                                genServiceName = jsonObject1.getString("serviceName");
                                gen_chk.setText(genServiceName);

                                JSONArray jsonArray = jsonObject1.getJSONArray("serviceDetails");

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                    GeneralServiceInfoModel gsim = new GeneralServiceInfoModel();
                                    gsim.setPerform(jsonObject2.getString("perform"));
                                    gsim.setServiceItem(jsonObject2.getString("serviceItem"));
                                    generalServiceInfoModels.add(gsim);
                                }

                                String checked = "1";
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    getServiceIds(checked, genServiceId, genServiceName);
                                }

                                gen_recyclervew.setAdapter(generalServiceAdapter);
                                generalServiceAdapter.notifyDataSetChanged();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        );
        RequestQueue requestQueue = Volley.newRequestQueue(ServicesListActivity.this);
        requestQueue.add(stringRequest);
    }

    private void getRecServices() {

        String url = AppUrls.BASE_URL + AppUrls.REC_SERVICE;

        JSONObject jsonObj = new JSONObject();

        try {
            jsonObj.put("modelId", modelId);
            jsonObj.put("kilometerRangeId", kilometerRangeId);
            //jsonObj.put("manufactureYear", "2016");

        } catch (Exception e) {

        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("Responce", response.toString());

                recServiceModels.clear();

                try {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    String responceCode = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    Log.d("Status", responceCode);

                    if (responceCode.equalsIgnoreCase("2000")) {

                        getInspections();

                        JSONArray jsonArray = jsonObject.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            RecServiceModel rsm = new RecServiceModel();
                            rsm.setServiceId(jsonObject1.getString("serviceId"));
                            rsm.setServiceName(jsonObject1.getString("serviceName"));
                            rsm.setFileId(AppUrls.IMAGE_URL + jsonObject1.getString("fileId"));

                            Log.d("MainServiceDetails", jsonObject1.getString("serviceName"));

                            JSONArray jsonArray1 = jsonObject1.getJSONArray("serviceDetails");

                            for (int j = 0; j < jsonArray1.length(); j++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(j);
                                ServiceDetailModel sdm = new ServiceDetailModel();
                                sdm.setServiceId(jsonObject2.getString("serviceId"));
                                sdm.setPerform(jsonObject2.getString("perform"));
                                sdm.setServiceItem(jsonObject2.getString("serviceItem"));

                                Log.d("ServiceDetails", jsonObject2.getString("serviceItem"));
                                serviceDetailModels.add(sdm);
                            }

                            recServiceModels.add(rsm);

                        }

                        rec_recyclervew.setAdapter(recServiceAdapter);
                        recServiceAdapter.notifyDataSetChanged();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(ServicesListActivity.this);
        requestQueue.add(request);

    }

    private void getInspections() {

        String url = AppUrls.BASE_URL + AppUrls.INSPECTIONS;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        inspectionModels.clear();

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String responceCode = jsonObject.getString("status");

                            if (responceCode.equals("2000")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    InspectionModel im = new InspectionModel();
                                    im.setInspectionId(jsonObject1.getString("inspectionId"));
                                    im.setInspectionName(jsonObject1.getString("inspectionName"));
                                    im.setFileId(AppUrls.IMAGE_URL + jsonObject1.getString("fileId"));
                                    inspectionModels.add(im);
                                }

                                inspection_recyclervew.setAdapter(inspectionAdapter);
                                inspectionAdapter.notifyDataSetChanged();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        );
        RequestQueue requestQueue = Volley.newRequestQueue(ServicesListActivity.this);
        requestQueue.add(stringRequest);
    }

    public void getServiceCentre() {
        if (getServiceCenterModels.size() > 0) {

            String resultDate = convertStringDateToAnotherStringDate(bookingDate, "dd-MM-yyyy", "yyyy-MM-dd");
            Log.d("ResultDate", bookingDate + "\n" + resultDate);

            JSONArray jArray = new JSONArray();

            String url = AppUrls.BASE_URL + AppUrls.SERVICECENTERS;
            JSONObject jsonObject = new JSONObject();

            try {
                jsonObject.put("bookingDate", resultDate);
                jsonObject.put("bookingTime", bookingStartTime);
                jsonObject.put("brandId", brandId);
                jsonObject.put("modelId", modelId);
                jsonObject.put("bookingType", 1);
                jsonObject.put("pickupAddressLatitude", latitude);
                jsonObject.put("pickupAddressLongitude", longitude);


                for (int i = 0; i < getServiceCenterModels.size(); i++) {
                    JSONObject jsonObject1 = new JSONObject();
                    jsonObject1.put("serviceId", getServiceCenterModels.get(i).getServiceId());
                    jsonObject1.put("serviceName", getServiceCenterModels.get(i).getServiceName());
                    jsonObject1.put("type", getServiceCenterModels.get(i).getType());
                    jArray.put(jsonObject1);
                }

                jsonObject.put("services", jArray);

                Log.d("JAYYYY", "" + jsonObject);

                Log.d("ARRAYDATA", "" + jArray);
            } catch (Exception e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void onResponse(JSONObject response) {

                    Log.d("SuccessResp", response.toString());

                    try {
                        JSONObject jsonObj = new JSONObject(response.toString());
                        String status = jsonObj.optString("status");
                        String message = jsonObj.optString("message");

                        if (status.equalsIgnoreCase("2000")) {

                            JSONArray jsonArray = jsonObj.getJSONArray("data");

                            if (jsonArray.length() > 0) {
                                ServiceCenterModel scm = new ServiceCenterModel();
                                JSONObject jsonObject2 = jsonArray.getJSONObject(0);
                                scm.setServiceCenterId(jsonObject2.optString("serviceCenterId"));
                                scm.setServiceCenterName(jsonObject2.optString("serviceCenterName"));
                                scm.setContactName(jsonObject2.optString("contactName"));
                                scm.setUserId(jsonObject2.optString("userId"));
                                scm.setContactMobileNumber(jsonObject2.optString("contactMobileNumber"));
                                scm.setContactEmail(jsonObject2.optString("contactEmail"));
                                scm.setAddress(jsonObject2.optString("address"));
                                scm.setLatitude(jsonObject2.optString("latitude"));
                                scm.setLongitude(jsonObject2.optString("longitude"));
                                scm.setOpenTime(jsonObject2.optString("openTime"));
                                scm.setCloseTime(jsonObject2.optString("closeTime"));
                                scm.setDeleted(jsonObject2.optString("deleted"));
                                scm.setCreatedTime(jsonObject2.optString("createdTime"));
                                scm.setModifiedTime(jsonObject2.optString("modifiedTime"));
                                scm.setDisplayTime(jsonObject2.optString("displayTime"));
                                scm.setEstimatedCost(jsonObject2.optString("estimatedCost"));
                                scm.setInitialPaidAmount(jsonObject2.optString("initialPaidAmount"));
                                scm.setDistance(jsonObject2.optString("distance"));
                                scm.setAverageRating(jsonObject2.optString("averageRating"));
                                serviceCenterModels.add(scm);

                                Double dd = (Double.valueOf(jsonObject2.optString("distance"))) / 1000;
                                Log.d("Distance", String.valueOf(dd));
                                sstId = jsonObject2.optString("serviceCenterId");
                                sstName = jsonObject2.optString("serviceCenterName");
                                sstAddress = jsonObject2.optString("address");
                                //sstDistance = jsonObject2.getString("distance");
                                sstDistance = String.format("%.2f", dd);
                                sstInitPrice = jsonObject2.optString("initialPaidAmount");
                                sstPrice = jsonObject2.optString("estimatedCost");
                                sstRating = jsonObject2.optString("averageRating");
                                sstOpenTime = jsonObject2.optString("openTime");
                                sstCloseTime = jsonObject2.optString("closeTime");
                                sstPerson = jsonObject2.optString("contactName");
                                sstDisplayTime = jsonObject2.optString("displayTime");
                                sstNumber = jsonObject2.optString("contactMobileNumber");

                                sstEditor.putString("sstId", sstId);
                                sstEditor.putString("sstName", sstName);
                                sstEditor.putString("sstAddress", sstAddress);
                                sstEditor.putString("sstDistance", sstDistance);
                                sstEditor.putString("sstInitPrice", sstInitPrice);
                                sstEditor.putString("sstPrice", sstPrice);
                                sstEditor.putString("jArray", "" + jArray);
                                sstEditor.putString("sstRating", sstRating);
                                sstEditor.putString("sstOpenTime", sstOpenTime);
                                sstEditor.putString("sstCloseTime", sstCloseTime);
                                sstEditor.putString("sstPerson", sstPerson);
                                sstEditor.putString("sstDisplayTime", sstDisplayTime);
                                sstEditor.putString("sstNumber", sstNumber);
                                sstEditor.apply();

                                sst_name_txt.setText(sstName);
                                sst_address.setText(sstAddress);
                                distance_txt.setText(sstDistance + " km");
                                price_txt.setText("Estimated price \u20B9 " + sstPrice);

                                Log.d("SSTDetails", sstName + "\n" + sstAddress + "\n" + sstDistance + "\n" + sstPrice);

                                if (getServiceCenterModels.size() > 0) {
                                    next_txt.setTextColor(Color.parseColor("#FF8000"));
                                    location_img.setVisibility(View.VISIBLE);
                                    distance_img.setVisibility(View.VISIBLE);
                                    price_txt.setVisibility(View.VISIBLE);
                                    service_center_rl.setVisibility(View.VISIBLE);
                                } else {
                                    next_txt.setTextColor(Color.parseColor("#C0C0C0"));
                                    location_img.setVisibility(View.GONE);
                                    distance_img.setVisibility(View.GONE);
                                    price_txt.setVisibility(View.GONE);
                                    service_center_rl.setVisibility(View.GONE);
                                }

                                if (sst_name_txt.getText().toString().equalsIgnoreCase("No Service Center Available")) {
                                    noSSTDialog();
                                    //GlobalCalls.showToast("Please Select Atleast One Service..!",ServicesListActivity.this);
                                }

                            } else {

                                next_txt.setTextColor(Color.parseColor("#C0C0C0"));
                                service_center_rl.setVisibility(View.GONE);
                                location_img.setVisibility(View.GONE);
                                distance_img.setVisibility(View.GONE);
                                price_txt.setVisibility(View.GONE);

                                noSSTDialog();
                                //GlobalCalls.showToast("Please Select Atleast One Service..!",ServicesListActivity.this);

                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(ServicesListActivity.this);
            requestQueue.add(request);
        } else {
            next_txt.setTextColor(Color.parseColor("#C0C0C0"));
            price_txt.setVisibility(View.GONE);
            service_center_rl.setVisibility(View.GONE);
            price_txt.setVisibility(View.GONE);

            GlobalCalls.showToast("Please Select Atleast One Service..!", ServicesListActivity.this);
        }
    }

    public String convertStringDateToAnotherStringDate(String stringdate, String stringdateformat, String returndateformat) {

        try {
            Date date = new SimpleDateFormat(stringdateformat).parse(stringdate);
            String returndate = new SimpleDateFormat(returndateformat).format(date);
            return returndate;
        } catch (ParseException e) {
            e.printStackTrace();
            return stringdate;
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View v) {

        if (v == close) {
            if (checkInternet) {

                new Thread(new Runnable() {
                    public void run() {
                        getServiceNames();
                        getInspectionNames();
                    }
                }).start();

                Intent intent = new Intent(ServicesListActivity.this, MainActivity.class);
                intent.putExtra("activity", "ServiceListActivity");
                intent.putExtra("brandId", brandId);
                intent.putExtra("modelId", modelId);
                intent.putExtra("kilometerRangeId", kilometerRangeId);
                intent.putExtra("brandName", brandName);
                intent.putExtra("modelName", modelName);
                intent.putExtra("kilometer", kilometer);
                intent.putExtra("bookingDate", bookingDate);
                intent.putExtra("bookingStartTime", bookingStartTime);
                intent.putExtra("bookingEndTime", bookingEndTime);
                intent.putExtra("bookingDisplayTime", bookingDisplayTime);
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == service_center_rl) {
            if (checkInternet) {
                Intent intent = new Intent(ServicesListActivity.this, ServiceCenterActivity.class);
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == next_txt) {
            if (checkInternet) {

                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ID-ServicesListSubmit");
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "ServicesListSubmit");
                bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
                mFirebaseAnalytics.logEvent("Servicepage_Event_ServicesListSubmit_New", bundle);

                GlobalCalls.events("Servicepage_Event_ServicesListSubmit_New",ServicesListActivity.this);

                if (cond_rg.getCheckedRadioButtonId() == -1) {
                    Toast.makeText(this, "Please Check Vehicle Moving Condition..!", Toast.LENGTH_SHORT).show();
                    //GlobalCalls.showToast("Please Check Vehicle Moving Condition..!", ServicesListActivity.this);
                } else {
                    if (sst_name_txt.getText().toString().equalsIgnoreCase("No Service Center Available")) {

                        noSSTDialog();

                    } else {
                        if (getServiceCenterModels.size() > 0) {
                            if (sst_name_txt.getText().toString().equalsIgnoreCase("No Service Center Available")) {

                                noSSTDialog();

                            } else {
                                next_txt.setTextColor(Color.parseColor("#FF8000"));
                                price_txt.setVisibility(View.VISIBLE);
                                service_center_rl.setVisibility(View.VISIBLE);

                                getServiceNames();
                                getInspectionNames();

                                Intent intent = new Intent(ServicesListActivity.this, ConfirmationDetailActivity.class);
                                intent.putExtra("activity", "ServicesListActivity");
                                intent.putExtra("condStr", condStr);
                                startActivity(intent);
                            }

                        } else {
                            next_txt.setTextColor(Color.parseColor("#C0C0C0"));
                            price_txt.setVisibility(View.GONE);
                            service_center_rl.setVisibility(View.GONE);
                            price_txt.setVisibility(View.GONE);

                            GlobalCalls.showToast("Please Select Atleast One Service..!", ServicesListActivity.this);
                        }
                    }
                }

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onBackPressed() {

        new Thread(new Runnable() {
            public void run() {
                getServiceNames();
                getInspectionNames();
            }
        }).start();


        Intent intent = new Intent(ServicesListActivity.this, MainActivity.class);
        intent.putExtra("activity", "ServiceListActivity");
        intent.putExtra("brandId", brandId);
        intent.putExtra("modelId", modelId);
        intent.putExtra("kilometerRangeId", kilometerRangeId);
        intent.putExtra("brandName", brandName);
        intent.putExtra("modelName", modelName);
        intent.putExtra("kilometer", kilometer);
        intent.putExtra("bookingDate", bookingDate);
        intent.putExtra("bookingStartTime", bookingStartTime);
        intent.putExtra("bookingEndTime", bookingEndTime);
        intent.putExtra("bookingDisplayTime", bookingDisplayTime);
        startActivity(intent);
    }

    public void infoDialog(String serviceId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ServicesListActivity.this);
        LayoutInflater inflater = ServicesListActivity.this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.info_dialog, null);

        TextView service_detail_title = dialog_layout.findViewById(R.id.service_detail_title);

        final SearchView info_search = dialog_layout.findViewById(R.id.info_search);
        EditText searchEditText = info_search.findViewById(androidx.appcompat.R.id.search_src_text);

        RecyclerView info_recyclerview = dialog_layout.findViewById(R.id.info_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        info_recyclerview.setLayoutManager(layoutManager);

        infoAdapter = new InfoAdapter(serviceId, serviceDetailModels, ServicesListActivity.this, R.layout.row_info);
        info_recyclerview.setAdapter(infoAdapter);

        info_search.setOnClickListener(v -> info_search.setIconified(false));

        builder.setView(dialog_layout).setNegativeButton("OK", (dialogInterface, i) -> {
            dialogInterface.dismiss();
        });

        dialog = builder.create();

        info_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                infoAdapter.getFilter().filter(query);
                return false;
            }
        });
        dialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void getServiceIds(String checked, String serviceId, String serviceName) {

        GetServiceCenterModel gscm = new GetServiceCenterModel();
        if (checked.equalsIgnoreCase("1")) {

            gscm.setServiceId(serviceId);
            gscm.setType("1");
            gscm.setServiceName(serviceName);
            getServiceCenterModels.add(gscm);

            getServiceCentre();

        } else if (checked.equalsIgnoreCase("0")) {

            getServiceCenterModels.removeIf(e -> e.getServiceId().equals(serviceId));

            getServiceCentre();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void getInspectionIds(String checked, String inspectionId, String inspectionName) {

        GetInspectionModel gim = new GetInspectionModel();
        if (checked.equalsIgnoreCase("1")) {

            gim.setInspectionId(inspectionId);
            gim.setState("0");
            gim.setInspectionName(inspectionName);
            getInspectionModels.add(gim);
            Log.d("Inspection", "Add->" + gim.getInspectionId());

        } else if (checked.equalsIgnoreCase("0")) {

            getInspectionModels.removeIf(e -> e.getInspectionId().equals(inspectionId));
            Log.d("Inspection", "Remove->" + getInspectionModels.removeIf(e -> e.getInspectionId().equals(inspectionId)));

        }

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void getServiceNames() {
        if (getServiceCenterModels.size() > 0) {

            /*StringBuilder stringBuilder = new StringBuilder();
            StringBuilder stringBuilder1 = new StringBuilder();
            for (int i = 0; i < getServiceCenterModels.size(); i++) {
                serviceId = getServiceCenterModels.get(i).getServiceId();
                serviceType = getServiceCenterModels.get(i).getType();
                serviceName = getServiceCenterModels.get(i).getServiceName();
                stringBuilder.append(serviceId);
                stringBuilder.append(",");
                stringBuilder1.append(serviceName);
                stringBuilder1.append(",");
            }*/

            /*StringJoiner stringJoiner = new StringJoiner(",");
            StringJoiner stringJoiner1 = new StringJoiner(",");
            for (int i = 0; i < getServiceCenterModels.size(); i++) {
                serviceId = getServiceCenterModels.get(i).getServiceId();
                serviceType = getServiceCenterModels.get(i).getType();
                serviceName = getServiceCenterModels.get(i).getServiceName();
                stringJoiner.add(serviceId);
                stringJoiner1.add(serviceName);
            }*/

            serviceId = getServiceCenterModels.stream().map(GetServiceCenterModel::getServiceId).collect(Collectors.joining(","));
            serviceName = getServiceCenterModels.stream().map(GetServiceCenterModel::getServiceName).collect(Collectors.joining(","));
            serviceType = getServiceCenterModels.stream().map(GetServiceCenterModel::getType).collect(Collectors.joining(","));

            String prefServiceId = "," + servicePreferences.getString("serviceId", "") + ",";
            String getId = "," + serviceId + ",";

            if (getId.equalsIgnoreCase(prefServiceId)) {
                Log.d("SameService", getId + "\n" + prefServiceId);
            } else {
                serviceEditor.putString("serviceId", serviceId);
                serviceEditor.putString("serviceName", serviceName);
                serviceEditor.putString("serviceType", serviceType);
                serviceEditor.apply();
            }


        } /*else {

            Toast toast = Toast.makeText(ServicesListActivity.this, "Please choose your Required service..!", Toast.LENGTH_LONG);
            View view = toast.getView();
            view.getBackground().setColorFilter(Color.parseColor("#FF8000"), PorterDuff.Mode.SRC_IN);
            TextView text = view.findViewById(android.R.id.message);
            text.setTextColor(Color.parseColor("#FFFFFF"));
            toast.show();

            //GlobalCalls.showToast("Please choose your Required service..!", ServicesListActivity.this);
        }*/
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void getInspectionNames() {
        if (getInspectionModels.size() > 0) {

            inspectionId = getInspectionModels.stream().map(GetInspectionModel::getInspectionId).collect(Collectors.joining(","));
            inspectionName = getInspectionModels.stream().map(GetInspectionModel::getInspectionName).collect(Collectors.joining(","));
            inspectionState = getInspectionModels.stream().map(GetInspectionModel::getState).collect(Collectors.joining(","));

            Log.d("INSPECTIONS", inspectionId);

            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < getInspectionModels.size(); i++) {
                JSONObject jsonObject1 = new JSONObject();
                try {
                    jsonObject1.put("inspectionId", getInspectionModels.get(i).getInspectionId());
                    jsonObject1.put("inspectionName", getInspectionModels.get(i).getInspectionName());
                    jsonObject1.put("state", getInspectionModels.get(i).getState());
                    jsonArray.put(jsonObject1);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            String prefInspectionId = "," + inspectionPreferences.getString("inspectionId", "") + ",";
            String getId = "," + inspectionId + ",";
            if (getId.equalsIgnoreCase(prefInspectionId)) {

                Log.d("SameInspection", "Same->" + getId + "\n" + prefInspectionId);

            } else {

                Log.d("SameInspection", "New->" + getId + "\n" + prefInspectionId);
                inspectionEditor.putString("inspections", "" + jsonArray);
                inspectionEditor.putString("inspectionId", inspectionId);
                inspectionEditor.putString("inspectionNames", inspectionName);
                inspectionEditor.apply();
            }

        } else {
            inspectionEditor.putString("inspections", "");
            inspectionEditor.putString("inspectionId", "");
            inspectionEditor.putString("inspectionNames", "");
            inspectionEditor.apply();
        }

    }

    public void noSSTDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(ServicesListActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Sorry, Gaadi360 service stations are closed on the day of your booking or not YET available near your area. Please select a different booking date and try. Thank you!");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.setCancelable(false);
        alertDialog.show();
    }
}