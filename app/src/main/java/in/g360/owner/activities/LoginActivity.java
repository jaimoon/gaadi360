package in.g360.owner.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.JsonObject;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;
import com.taishi.flipprogressdialog.FlipProgressDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.aabhasjindal.otptextview.OtpTextView;
import in.g360.owner.MainActivity;
import in.g360.owner.R;
import in.g360.owner.adapters.ServiceTypeAdapter;
import in.g360.owner.models.ServiceTypeImageModel;
import in.g360.owner.models.ServiceTypeModel;
import in.g360.owner.utils.AppUrls;
import in.g360.owner.utils.GlobalCalls;
import in.g360.owner.utils.NetworkChecking;
import in.g360.owner.utils.SliderAdapterExample;
import in.g360.owner.utils.SliderItem;
import in.g360.owner.utils.UserSessionManager;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    private FirebaseAnalytics mFirebaseAnalytics;
    public String user_image, otp;
    EditText mobile_edt;
    Button login_btn;
    LinearLayout g_Login, f_Login;
    private boolean checkInternet;
    String mobile, firstName, lastName, username, authority, email, createdTime, modifiedTime, accessToken, mediaSecret, id,
            initialPaymentAmount, adImageId, radius, sstShowCount, ratingInterval, cancellationHours, preBookingDays,
            rescheduleCount, registrationAmount, referedAmount, walletDeductionPercentage;
    Boolean locked, enabled, expired, emailVerified, mobileVerified;
    Integer userId, profilePicId;
    AlertDialog dialog;
    UserSessionManager userSessionManager;
    OtpTextView otpTextView;
    TextInputLayout mobile_til;
    RecyclerView type_recyclerview;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    SliderView sliderView;
    private SliderAdapterExample adapter;
    SmsVerifyCatcher smsVerifyCatcher;

    /*Service Type Images*/
    ArrayList<ServiceTypeImageModel> serviceTypeImageModels;
    ServiceTypeAdapter serviceTypeAdapter;

    /*Service Types*/
    ArrayList<ServiceTypeModel> serviceTypeModels;

    /*Firebase*/
    //private FirebaseAuth mAuth;
    private GoogleApiClient googleApiClient;
    private static final int RC_SIGN_IN = 1;
    private int RESOLVE_HINT = 2;

    /*ProgressDialog*/
    List<Integer> imageList = new ArrayList<Integer>();
    FlipProgressDialog flipProgressDialog;

    String fcmToken, imei, bookingId;
    Typeface regular, bold;

    AlertDialog.Builder dialogBuilder;
    AlertDialog alertDialog;

    @SuppressLint("HardwareIds")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        checkInternet = NetworkChecking.isConnected(this);

        userSessionManager = new UserSessionManager(this);

        imei = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d("IMEI", imei);

        FirebaseApp.initializeApp(this);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(LoginActivity.this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                fcmToken = instanceIdResult.getToken();
                Log.d("TOKEN", fcmToken);
            }
        });

        sliderView = findViewById(R.id.imageSlider);

        adapter = new SliderAdapterExample(this);
        sliderView.setSliderAdapter(adapter);

        sliderView.setIndicatorAnimation(IndicatorAnimations.SLIDE); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_RIGHT);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(3);
        sliderView.setAutoCycle(true);
        sliderView.startAutoCycle();

        imageList.add(R.mipmap.ic_launcher);
        flipProgressDialog = new FlipProgressDialog();
        flipProgressDialog.setImageList(imageList);
        flipProgressDialog.setCanceledOnTouchOutside(true);
        flipProgressDialog.setDimAmount(0.8f); //0.0f
        flipProgressDialog.setBackgroundColor(Color.parseColor("#00FFFFFF"));
        flipProgressDialog.setBackgroundAlpha(0.2f);
        flipProgressDialog.setBorderStroke(0);
        flipProgressDialog.setBorderColor(-1);
        flipProgressDialog.setCornerRadius(16);
        flipProgressDialog.setImageSize(200);
        flipProgressDialog.setImageMargin(10);
        flipProgressDialog.setOrientation("rotationY");
        flipProgressDialog.setDuration(600);
        flipProgressDialog.setStartAngle(0.0f);
        flipProgressDialog.setEndAngle(180.0f);
        flipProgressDialog.setMinAlpha(0.0f);
        flipProgressDialog.setMaxAlpha(1.0f);

        type_recyclerview = findViewById(R.id.type_recyclerview);
        serviceTypeModels = new ArrayList<ServiceTypeModel>();
        serviceTypeAdapter = new ServiceTypeAdapter(serviceTypeModels, LoginActivity.this, R.layout.row_service_type);
        type_recyclerview.setLayoutManager(new GridLayoutManager(this, 2, GridLayoutManager.HORIZONTAL, false));

        mobile_til = findViewById(R.id.mobile_til);
        mobile_edt = findViewById(R.id.mobile_edt);
        mobile_edt.setTypeface(regular);

        login_btn = findViewById(R.id.login_btn);
        login_btn.setTypeface(bold);
        login_btn.setOnClickListener(this);

        g_Login = findViewById(R.id.g_Login);
        g_Login.setOnClickListener(this);
        f_Login = findViewById(R.id.f_Login);
        f_Login.setOnClickListener(this);

        try {
            @SuppressLint("PackageManagerGetSignatures") PackageInfo info = getPackageManager().getPackageInfo("in.g360.owner", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException ignored) {

        } catch (NoSuchAlgorithmException ignored) {

        }

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addApi(Auth.CREDENTIALS_API)
                .build();

        getHintMobileNumber();

        checkLocationPermission();

        getServiceType();

        smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                otp = parseCode(message);
                otpTextView.setOTP(otp);
            }
        });

        smsVerifyCatcher.onStart();

        //Logs an app event.
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ID-LoginActivity");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "LoginActivity");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
        mFirebaseAnalytics.logEvent("Loginpage_PageLoad_New", bundle);

        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(20000);
        mFirebaseAnalytics.setSessionTimeoutDuration(500);


    }

    private void getServiceType() {
        if (checkInternet) {
            //flipProgressDialog.show(getFragmentManager(), "");
            String url = AppUrls.BASE_URL + AppUrls.SERVICETYPE;

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                String status = jsonObject.getString("status");

                                if (status.equalsIgnoreCase("2000")) {
                                    //flipProgressDialog.dismiss();

                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONArray jsonArray = jsonObject1.getJSONArray("files");
                                    JSONArray jsonArray1 = jsonObject1.getJSONArray("services");

                                    List<SliderItem> sliderItemList = new ArrayList<>();

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        ServiceTypeImageModel serviceTypeImageModel = new ServiceTypeImageModel();
                                        serviceTypeImageModel.setDesc(jsonObject2.optString("desc"));
                                        serviceTypeImageModel.setFileId(AppUrls.IMAGE_URL + jsonObject2.getString("fileId"));

                                        SliderItem sliderItem = new SliderItem();

                                        user_image = AppUrls.IMAGE_URL + jsonObject2.getString("fileId");

                                        sliderItem.setImageUrl(user_image);
                                        sliderItem.setDescription(jsonObject1.optString("desc"));
                                        sliderItemList.add(sliderItem);
                                    }

                                    adapter.renewItems(sliderItemList);

                                    for (int j = 0; j < jsonArray1.length(); j++) {
                                        JSONObject jsonObject2 = jsonArray1.getJSONObject(j);
                                        ServiceTypeModel stm = new ServiceTypeModel();
                                        stm.setId(jsonObject2.getString("id"));
                                        stm.setName(jsonObject2.getString("name"));
                                        serviceTypeModels.add(stm);
                                    }

                                    type_recyclerview.setAdapter(serviceTypeAdapter);
                                    serviceTypeAdapter.notifyDataSetChanged();

                                } else {

                                    Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Invalid Data..!", Snackbar.LENGTH_LONG);
                                    snackbar.show();

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.getMessage();
                        }
                    });

            RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    @Override
    public void onClick(View v) {

        if (v == login_btn) {

            Bundle bundle = new Bundle();
            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ID-LoginSubmit");
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "LoginSubmit");
            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
            mFirebaseAnalytics.logEvent("Loginpage_Event_LoginSubmit_New", bundle);

            login();
        }

        if (v == g_Login) {
            Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
            startActivityForResult(intent, RC_SIGN_IN);
        }

        if (v == f_Login) {

            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            intent.putExtra("activity", "LoginActivity");
            startActivity(intent);
        }
    }

    private void login() {

        mobile = mobile_edt.getText().toString();
        if (validateMobile()) {
            if (checkInternet) {
                flipProgressDialog.show(getFragmentManager(), "");
                String url = AppUrls.BASE_URL + AppUrls.LOGIN + mobile;
                Log.d("LOGINURL", url);
                StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    Log.d("LOGIN", response);
                                    String status = jsonObject.optString("status");

                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    String userType = jsonObject1.optString("userType");

                                    if (userType.equalsIgnoreCase("1")) {

                                        flipProgressDialog.dismiss();

                                        getReferalAmount();

                                    } else if (userType.equalsIgnoreCase("2")) {

                                        flipProgressDialog.dismiss();

                                        verifyOtp();

                                    } else {
                                        Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Invalid Details..!", Snackbar.LENGTH_LONG);
                                        snackbar.show();
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                if (error == null || error.networkResponse == null) {
                                    return;
                                }

                                try {
                                    String body = new String(error.networkResponse.data, "UTF-8");

                                    JSONObject jsonObject = new JSONObject(body);

                                    GlobalCalls.showToast(jsonObject.optString("message"), LoginActivity.this);
                                } catch (Exception e) {
                                    GlobalCalls.showToast("You Are Not Registered..!", LoginActivity.this);
                                }
                            }
                        });

                stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
                requestQueue.add(stringRequest);
            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }

    private void getReferalAmount() {

        String url = AppUrls.BASE_URL + AppUrls.REFERAL_AMOUNT;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    id = jsonObject.optString("id");
                    initialPaymentAmount = jsonObject.optString("initialPaymentAmount");
                    adImageId = jsonObject.optString("adImageId");
                    radius = jsonObject.optString("radius");
                    sstShowCount = jsonObject.optString("sstShowCount");
                    ratingInterval = jsonObject.optString("ratingInterval");
                    cancellationHours = jsonObject.optString("cancellationHours");
                    String createdTime = jsonObject.optString("createdTime");
                    String modifiedTime = jsonObject.optString("modifiedTime");
                    preBookingDays = jsonObject.optString("preBookingDays");
                    rescheduleCount = jsonObject.optString("rescheduleCount");
                    registrationAmount = jsonObject.optString("registrationAmount");
                    referedAmount = jsonObject.optString("referedAmount");
                    walletDeductionPercentage = jsonObject.optString("walletDeductionPercentage");

                    showReferalDialog(R.layout.referal_dialog);


                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        );
        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
        requestQueue.add(stringRequest);
    }

    private void showReferalDialog(int layout) {
        dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(LoginActivity.this);
        View layoutView = getLayoutInflater().inflate(layout, null);
        TextView ref_title_txt = layoutView.findViewById(R.id.ref_title_txt);
        ref_title_txt.setTypeface(bold);
        TextView ref_txt = layoutView.findViewById(R.id.ref_txt);
        ref_txt.setTypeface(regular);
        ref_txt.setText("Hi, save \u20B9 " + registrationAmount + " by entering a referral code if you have one. If not, complete registration and refer your friends to Gaadi360 by sharing our app. Each of you will save \u20B9 " + referedAmount + " on your next bike service through Gaadi360 app.");
        EditText referal_edt = layoutView.findViewById(R.id.referal_edt);
        referal_edt.setTypeface(regular);
        Button skip_btn = layoutView.findViewById(R.id.skip_btn);
        skip_btn.setTypeface(bold);
        Button verify_btn = layoutView.findViewById(R.id.verify_btn);
        verify_btn.setTypeface(bold);
        dialogBuilder.setView(layoutView);
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();

        //referalCode = referal_edt.getText().toString().trim();

        verify_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //alertDialog.dismiss();

                verifyReferal(referal_edt.getText().toString().trim());


            }
        });

        skip_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialog.dismiss();

                verifyOtp();

            }
        });
        alertDialog.setCancelable(true);
    }

    private void verifyOtp() {

        String url = AppUrls.BASE_URL + AppUrls.VALIDATE_REFERAL + mobile;

        Log.d("URL", url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.optString("status");
                    String code = jsonObject.optString("code");
                    String message = jsonObject.optString("message");

                    if (status.equalsIgnoreCase("2000")) {

                        otpDialog("empty");

                    } else {
                        GlobalCalls.showToast("Invalid Details", LoginActivity.this);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        );
        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
        requestQueue.add(stringRequest);

    }

    private void verifyReferal(String referalCode) {

        String url = AppUrls.BASE_URL + AppUrls.VALIDATE_REFERAL + mobile + "&referralCode=" + referalCode;

        Log.d("URL", url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.optString("status");
                    String code = jsonObject.optString("code");
                    String message = jsonObject.optString("message");

                    if (status.equalsIgnoreCase("2000")) {

                        showPositiveAlertDialog(R.layout.dialog_postive_layout, referalCode);

                    } else {
                        GlobalCalls.showToast("Invalid ReferalCode", LoginActivity.this);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        );
        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
        requestQueue.add(stringRequest);

    }

    private void showPositiveAlertDialog(int layout, String referalCode) {
        dialogBuilder = new AlertDialog.Builder(LoginActivity.this);
        View layoutView = getLayoutInflater().inflate(layout, null);
        TextView message_txt = layoutView.findViewById(R.id.message_txt);
        message_txt.setText("Referal code applied successfully");
        Button dialogButton = layoutView.findViewById(R.id.btnDialog);
        dialogBuilder.setView(layoutView);
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialog.dismiss();

                otpDialog(referalCode);

            }
        });
        alertDialog.setCancelable(false);
    }

    private boolean validateMobile() {
        boolean result = true;
        if (mobile.length() != 10) {
            mobile_til.setError("Invalid Mobile Number");
            result = false;
        } else {
            mobile_til.setError(null);
        }
        return result;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }

        if (requestCode == RESOLVE_HINT) {
            if (resultCode == Activity.RESULT_OK) {
                Credential credential = data.getParcelableExtra(Credential.EXTRA_KEY);
                Log.d("HintNumber", credential.getId());
                String hintNumber = credential.getId();
                String to_remove = "+91";
                String mobileNumber = hintNumber.replace(to_remove, "");
                mobile_edt.setText(mobileNumber);
            }
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            gotoProfile();
        } else {

            GlobalCalls.showToast("Sign in cancel", LoginActivity.this);

        }
    }

    private void gotoProfile() {
        Intent intent = new Intent(LoginActivity.this, SocialLoginDetailsActivity.class);
        startActivity(intent);
    }

    private void otpDialog(String check) {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setCancelable(false);
        LayoutInflater inflater = LoginActivity.this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.otp_dialog, null);

        TextView otp_txt = dialog_layout.findViewById(R.id.otp_txt);
        otp_txt.setTypeface(bold);
        otpTextView = dialog_layout.findViewById(R.id.otpTextView);
        TextView resend_txt = dialog_layout.findViewById(R.id.resend_txt);
        resend_txt.setTypeface(regular);
        resend_txt.setPaintFlags(resend_txt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        /*smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                otp = parseCode(message);
                otpTextView.setOTP(otp);
            }
        });

        smsVerifyCatcher.onStart();*/

        resend_txt.setOnClickListener(view -> {
            mobile = mobile_edt.getText().toString();
            otpTextView.setOTP("");
            if (validateMobile()) {
                if (checkInternet) {
                    flipProgressDialog.show(getFragmentManager(), "");

                    //String URL = AppUrls.BASE_URL + AppUrls.LOGIN + mobile;
                    String URL = AppUrls.BASE_URL + AppUrls.CALL_OTP + mobile + "&retrytype=text";

                    StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        Log.d("LOGIN", response);
                                        String status = jsonObject.getString("status");
                                        String message = jsonObject.getString("message");

                                        if (status.equalsIgnoreCase("2000")) {

                                            flipProgressDialog.dismiss();

                                        } else {

                                            flipProgressDialog.dismiss();
                                            GlobalCalls.showToast(message, LoginActivity.this);

                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    if (error == null || error.networkResponse == null) {
                                        return;
                                    }

                                    try {
                                        String body = new String(error.networkResponse.data, "UTF-8");

                                        JSONObject jsonObject = new JSONObject(body);

                                        GlobalCalls.showToast(jsonObject.optString("message"), LoginActivity.this);
                                    } catch (Exception e) {
                                        GlobalCalls.showToast("Invalid Details..!", LoginActivity.this);
                                    }
                                }
                            });

                    RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
                    requestQueue.add(stringRequest);
                } else {
                    Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        });

        TextView call_txt = dialog_layout.findViewById(R.id.call_txt);
        Button call_btn = dialog_layout.findViewById(R.id.call_btn);
        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {

                call_txt.setText("Seconds Remaining : " + millisUntilFinished / 1000);

            }

            public void onFinish() {
                call_txt.setText("done!");
                call_txt.setVisibility(View.GONE);
                resend_txt.setVisibility(View.VISIBLE);
                call_btn.setVisibility(View.VISIBLE);

                call_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        call_btn.setTextColor(ContextCompat.getColor(LoginActivity.this, R.color.white));
                        call_btn.setBackgroundResource(R.drawable.rounded_orange_border);

                        flipProgressDialog.show(getFragmentManager(), "");
                        StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.CALL_OTP + mobile + "&retrytype=voice",
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        try {
                                            JSONObject jsonObject = new JSONObject(response);
                                            Log.d("CALL", response);

                                            String status = jsonObject.getString("status");
                                            String message = jsonObject.getString("message");

                                            if (status.equalsIgnoreCase("2000")) {

                                                flipProgressDialog.dismiss();

                                            } else {

                                                flipProgressDialog.dismiss();
                                                GlobalCalls.showToast(message, LoginActivity.this);

                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        if (error == null || error.networkResponse == null) {
                                            return;
                                        }

                                        try {
                                            String body = new String(error.networkResponse.data, "UTF-8");

                                            JSONObject jsonObject = new JSONObject(body);

                                            GlobalCalls.showToast(jsonObject.optString("message"), LoginActivity.this);
                                        } catch (Exception e) {
                                            GlobalCalls.showToast("Invalid Details..!", LoginActivity.this);
                                        }
                                    }
                                });
                        stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
                        requestQueue.add(stringRequest);
                    }
                });

            }

        }.start();

        Button verify_btn = dialog_layout.findViewById(R.id.verify_btn);
        verify_btn.setTypeface(bold);
        verify_btn.setOnClickListener(view -> {
            flipProgressDialog.show(getFragmentManager(), "");
            otp = otpTextView.getOTP();

            String url;

            if (check.equalsIgnoreCase("empty")) {
                url = AppUrls.BASE_URL + AppUrls.VERIFYOTP + mobile + "&otp=" + otp;
            } else {
                url = AppUrls.BASE_URL + AppUrls.VERIFYOTP + mobile + "&otp=" + otp + "&referralCode=" + check;
            }


            Log.d("VERIFY", url);

            if (validate()) {
                if (checkInternet) {
                    StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);

                                        Log.d("OTPRESP", response);
                                        flipProgressDialog.dismiss();

                                        String status = jsonObject.optString("status");
                                        String message = jsonObject.optString("message");


                                        if (status.equalsIgnoreCase("2000")) {
                                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                                            userId = jsonObject1.optInt("userId");
                                            firstName = jsonObject1.optString("firstName");
                                            lastName = jsonObject1.optString("lastName");
                                            username = jsonObject1.optString("username");
                                            locked = jsonObject1.getBoolean("locked");
                                            enabled = jsonObject1.optBoolean("enabled");
                                            expired = jsonObject1.optBoolean("expired");
                                            authority = jsonObject1.optString("authority");
                                            email = jsonObject1.optString("email");
                                            mobile = jsonObject1.optString("mobile");
                                            //profilePicId = jsonObject1.getInt("profilePicId");
                                            emailVerified = jsonObject1.optBoolean("emailVerified");
                                            mobileVerified = jsonObject1.optBoolean("mobileVerified");
                                            createdTime = jsonObject1.optString("createdTime");
                                            modifiedTime = jsonObject1.optString("modifiedTime");
                                            accessToken = jsonObject1.optString("accessToken");
                                            mediaSecret = jsonObject1.optString("mediaSecret");

                                            Log.d("LoginDetails", userId + "\n" + firstName + "\n" + username + "\n" + locked + "\n" +
                                                    enabled + "\n" + expired + "\n" + authority + "\n" + email + "\n" + mobile + "\n" +
                                                    profilePicId + "\n" + emailVerified + "\n" + mobileVerified + "" + "\n" + createdTime + "\n" +
                                                    modifiedTime + "\n" + accessToken + "\n" + mediaSecret);

                                            userSessionManager.createUserLoginSession(firstName, username, email, mobile, accessToken, mediaSecret);

                                            sendFCM();

                                            Bundle bundle = new Bundle();
                                            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ID-LoginVerifyButton");
                                            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "LoginVerifyButton");
                                            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
                                            mFirebaseAnalytics.logEvent("Loginpage_Event_LoginVerifyButton_New", bundle);

                                            if (checkLocationPermission()) {
                                                /*Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                                intent.putExtra("activity", "LoginActivity");
                                                startActivity(intent);*/

                                                getOnGoingActivity();

                                            }


                                        } else {
                                            otpTextView.showError();
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    otpTextView.showError();
                                    if (error == null || error.networkResponse == null) {
                                        return;
                                    }

                                    try {
                                        String body = new String(error.networkResponse.data, "UTF-8");

                                        JSONObject jsonObject = new JSONObject(body);

                                        GlobalCalls.showToast(jsonObject.optString("message"), LoginActivity.this);
                                    } catch (Exception e) {
                                        GlobalCalls.showToast("Invalid Otp..!", LoginActivity.this);
                                    }
                                }
                            });
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
                    requestQueue.add(stringRequest);
                } else {
                    Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        });

        builder.setView(dialog_layout).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        dialog = builder.create();
        dialog.show();
    }

    private boolean validate() {
        boolean result = true;
        //String otp = otpTextView.getText().toString().trim();
        otp = otpTextView.getOTP();
        Log.d("OTP", otp);
        if ((otp == null || otp.equals("")) || otp.length() != 6) {
            //otpTextView.setError("Invalid OTP");
            otpTextView.showError();
            result = false;
        }
        return result;
    }

    private boolean checkLocationPermission() {

        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        List<String> listPermissionsNeeded = new ArrayList<>();

        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);

            return false;
        }
        return true;
    }

    public void getHintMobileNumber() {
        HintRequest hintRequest = new HintRequest.Builder().setPhoneNumberIdentifierSupported(true).build();
        PendingIntent mIntent = Auth.CredentialsApi.getHintPickerIntent(googleApiClient, hintRequest);
        try {
            startIntentSenderForResult(mIntent.getIntentSender(), RESOLVE_HINT, null, 0, 0, 0);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }

    private String parseCode(String message) {
        Pattern p = Pattern.compile("\\b\\d{6}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }

    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        smsVerifyCatcher.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void sendFCM() {

        String url = AppUrls.BASE_URL + AppUrls.FCM;
        JSONObject jsonObj = new JSONObject();

        Log.d("TOKENNNN", "" + fcmToken);

        try {
            jsonObj.put("platformId", "1");
            jsonObj.put("pushRegId", fcmToken);
            jsonObj.put("imei", imei);
            jsonObj.put("roleType", "1");
            jsonObj.put("userId", userId);

        } catch (Exception e) {

        }

        Log.d("JsonObj", jsonObj.toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("FCMStatus", response.toString());

                try {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    Log.d("Status", status + "\n" + message);

                    if (status.equalsIgnoreCase("2000")) {

                        //Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                    Log.d("ResError", e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

                //Log.d("EROORRR", error.getMessage());

            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
        requestQueue.add(request);
    }

    private void getOnGoingActivity() {

        String url = AppUrls.BASE_URL + AppUrls.CURRENT_STATUS;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String responceCode = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    Log.d("SPLASHRESP", response);

                    if (responceCode.equalsIgnoreCase("2000")) {

                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        bookingId = jsonObject1.getString("bookingId");
                        String paymentStatus = jsonObject1.getString("paymentStatus");
                        String status = jsonObject1.getString("status");
                        String ongoingBooking = jsonObject1.getString("ongoingBooking");

                        JSONObject jsonObject2 = jsonObject1.getJSONObject("setting");
                        String preBookingDays = jsonObject2.optString("preBookingDays");

                        Log.d("PREBOOKINGDAYS", preBookingDays);

                        if (ongoingBooking.equalsIgnoreCase("true")) {
                            if (status.equalsIgnoreCase("1")) {

                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                intent.putExtra("activity", "SplashScreenActivity");
                                startActivity(intent);

                            } else if (status.equalsIgnoreCase("2")) {

                                Intent intent = new Intent(LoginActivity.this, TrackingActivity.class);
                                intent.putExtra("bookingId", bookingId);
                                startActivity(intent);

                            } else if (status.equalsIgnoreCase("3")) {

                                Intent intent = new Intent(LoginActivity.this, TrackingActivity.class);
                                intent.putExtra("bookingId", bookingId);
                                startActivity(intent);

                            } else if (status.equalsIgnoreCase("4")) {

                                Intent intent = new Intent(LoginActivity.this, TrackingActivity.class);
                                intent.putExtra("bookingId", bookingId);
                                startActivity(intent);

                            } else if (status.equalsIgnoreCase("5")) {

                                if (paymentStatus.equalsIgnoreCase("1")) {

                                    checkIFC();
                                    /*Intent intent = new Intent(SplashScreenActivity.this, FinalPaymentActivity.class);
                                    intent.putExtra("bookingId", bookingId);
                                    startActivity(intent);*/
                                } else {
                                    Intent intent = new Intent(LoginActivity.this, TrackingActivity.class);
                                    intent.putExtra("bookingId", bookingId);
                                    startActivity(intent);
                                }

                            } else if (status.equalsIgnoreCase("6")) {

                                Intent intent = new Intent(LoginActivity.this, TrackingActivity.class);
                                intent.putExtra("bookingId", bookingId);
                                startActivity(intent);

                            } else if (status.equalsIgnoreCase("7")) {

                                Intent intent = new Intent(LoginActivity.this, RatingActivity.class);
                                intent.putExtra("bookingId", bookingId);
                                startActivity(intent);

                            } else if (status.equalsIgnoreCase("8")) {

                                if (paymentStatus.equalsIgnoreCase("1")) {

                                    checkIFC();

                                    /*Intent intent = new Intent(SplashScreenActivity.this, FinalPaymentActivity.class);
                                    intent.putExtra("bookingId", bookingId);
                                    startActivity(intent);*/
                                } else {
                                    Intent intent = new Intent(LoginActivity.this, TrackingActivity.class);
                                    intent.putExtra("bookingId", bookingId);
                                    startActivity(intent);
                                }

                            } else if (status.equalsIgnoreCase("9")) {

                                Intent intent = new Intent(LoginActivity.this, ActionPageActivity.class);
                                intent.putExtra("bookingId", bookingId);
                                startActivity(intent);
                            } else {
                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                intent.putExtra("activity", "SplashScreenActivity");
                                startActivity(intent);
                            }
                        } else {
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            intent.putExtra("activity", "LoginActivity");
                            startActivity(intent);
                        }

                    } else {

                        GlobalCalls.showToast(message, LoginActivity.this);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            GlobalCalls.showToast("Connection Not Found..!", LoginActivity.this);

                        } else if (error instanceof AuthFailureError) {

                            GlobalCalls.showToast("User Not Found..!", LoginActivity.this);

                        } else if (error instanceof ServerError) {

                            GlobalCalls.showToast("Server Not Found..!", LoginActivity.this);

                        } else if (error instanceof NetworkError) {
                            GlobalCalls.showToast("Network Not Found..!", LoginActivity.this);
                        } else if (error instanceof ParseError) {
                            GlobalCalls.showToast("Please Try Later..!", LoginActivity.this);
                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
        requestQueue.add(stringRequest);

    }

    private void checkIFC() {

        String url = AppUrls.BASE_URL + AppUrls.CHECK_IFC + bookingId + "/isfeedback";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.optString("status");
                    String code = jsonObject.optString("code");
                    String message = jsonObject.optString("message");

                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                    String isfeedback = jsonObject1.optString("isfeedback");

                    if (isfeedback.equalsIgnoreCase("true")) {

                        Intent intent = new Intent(LoginActivity.this, FinalPaymentActivity.class);
                        intent.putExtra("bookingId", bookingId);
                        startActivity(intent);

                    } else if (isfeedback.equalsIgnoreCase("false")) {

                        Intent intent = new Intent(LoginActivity.this, IFCActivity.class);
                        intent.putExtra("bookingId", bookingId);
                        startActivity(intent);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) /*{
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        }*/;
        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }
}
