package in.g360.owner.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;

import in.g360.owner.R;
import in.g360.owner.utils.GlobalCalls;
import in.g360.owner.utils.PrefManager;
import in.g360.owner.utils.UserSessionManager;

public class GetStartedScreensActivity extends AppCompatActivity {

    private FirebaseAnalytics mFirebaseAnalytics;
    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private int[] layouts;
    private TextView btnSkip, btnNext;
    private PrefManager prefManager;
    Typeface regular, bold;
    UserSessionManager sessionManager;
    SharedPreferences preferences, sstPreferences, servicePreferences, inspectionPreferences;
    SharedPreferences.Editor editor, sstEditor, serviceEditor, inspectionEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_started_screens);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();
        sstPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sstEditor = sstPreferences.edit();
        servicePreferences = PreferenceManager.getDefaultSharedPreferences(this);
        serviceEditor = servicePreferences.edit();
        inspectionPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        inspectionEditor = inspectionPreferences.edit();

        editor.clear();
        editor.commit();
        sstEditor.clear();
        sstEditor.commit();
        serviceEditor.clear();
        serviceEditor.commit();
        inspectionEditor.clear();
        inspectionEditor.commit();

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        sessionManager = new UserSessionManager(getApplicationContext());

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        prefManager = new PrefManager(this);
        if (!prefManager.isFirstTimeLaunch()) {
            launchHomeScreen();
            finish();
        }

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        viewPager = findViewById(R.id.view_pager);
        dotsLayout = findViewById(R.id.layoutDots);
        btnSkip = findViewById(R.id.btn_skip);
        btnSkip.setTypeface(bold);
        btnNext = findViewById(R.id.btn_next);
        btnNext.setTypeface(bold);

        layouts = new int[]{
                R.layout.get_started_screen1,
                R.layout.get_started_screen2,
                R.layout.get_started_screen3,
                R.layout.get_started_screen4
        };

        addBottomDots(0);
        changeStatusBarColor();

        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        //Logs an app event.
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ID-IntroScreenActivity");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "IntroScreenActivity");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
        mFirebaseAnalytics.logEvent("Intropage_PageLoad_New", bundle);

        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(20000);
        mFirebaseAnalytics.setSessionTimeoutDuration(500);

        GlobalCalls.events("Intropage_PageLoad_New",GetStartedScreensActivity.this);

        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ID-IntroSkipButton");
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "IntroSkipButton");
                bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
                mFirebaseAnalytics.logEvent("Intropage_Event_IntroSkipButton_New", bundle);

                launchHomeScreen();
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int current = getItem(+1);
                if (current < layouts.length) {

                    Bundle bundle = new Bundle();
                    bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ID-IntroNextButton");
                    bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "IntroNextButton");
                    bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
                    mFirebaseAnalytics.logEvent("Intropage_Event_IntroNextButton_New", bundle);

                    viewPager.setCurrentItem(current);

                } else {

                    Bundle bundle = new Bundle();
                    bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ID-IntroGotitButton");
                    bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "IntroGotitButton");
                    bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
                    mFirebaseAnalytics.logEvent("Intropage_Event_IntroGotitButton_New", bundle);

                    launchHomeScreen();
                }
            }
        });

    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    private void launchHomeScreen() {
        sessionManager.createIsFirstTimeAppLunch();
        prefManager.setFirstTimeLaunch(false);
        startActivity(new Intent(GetStartedScreensActivity.this, SplashScreenActivity.class));
        finish();
    }

    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);

            if (position == layouts.length - 1) {

                btnNext.setText(getString(R.string.start));
                btnSkip.setVisibility(View.GONE);
            } else {

                btnNext.setText(getString(R.string.next));
                btnSkip.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);
            return view;

        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
}
