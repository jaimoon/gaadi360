package in.g360.owner.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import in.g360.owner.MainActivity;
import in.g360.owner.R;
import in.g360.owner.adapters.ActionPageAdapter;
import in.g360.owner.models.BookingAddonServicesModel;
import in.g360.owner.models.HistoryInspectionModel;
import in.g360.owner.utils.AppUrls;
import in.g360.owner.utils.GlobalCalls;
import in.g360.owner.utils.NetworkChecking;
import in.g360.owner.utils.UserSessionManager;

public class ActionPageActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close;
    TextView toolbar_title, bike_txt, brand_txt, model_txt, date_txt, time_txt, price_txt, est_txt, est_price_txt, init_txt, init_price_txt,
            disc_txt, dicount_txt, addon_txt, addon_price_txt, tot_txt, final_price_txt;
    TableRow discount_tr, addon_tr;
    View view1, view2;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    String bookingId, accessToken, addonCost, brandId, brandName, modelId, modelName, status, finalPrice, promocodeAmount;
    Typeface regular, bold;
    RecyclerView addon_recyclerview;
    Button sumbit_btn;

    /*Addon's*/
    ActionPageAdapter actionPageAdapter;
    ArrayList<BookingAddonServicesModel> bookingAddonServicesModels = new ArrayList<BookingAddonServicesModel>();
    ArrayList<BookingAddonServicesModel> alreadyAddedServices = new ArrayList<BookingAddonServicesModel>();

    /*Inspection's*/
    ArrayList<HistoryInspectionModel> historyInspectionModels = new ArrayList<HistoryInspectionModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action_page);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        checkInternet = NetworkChecking.isConnected(this);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);

        Log.d("ACCESSTOKEN", accessToken);

        bookingId = getIntent().getStringExtra("bookingId");

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);

        discount_tr = findViewById(R.id.discount_tr);
        addon_tr = findViewById(R.id.addon_tr);
        bike_txt = findViewById(R.id.bike_txt);
        bike_txt.setTypeface(bold);
        brand_txt = findViewById(R.id.brand_txt);
        brand_txt.setTypeface(regular);
        model_txt = findViewById(R.id.model_txt);
        model_txt.setTypeface(regular);
        date_txt = findViewById(R.id.date_txt);
        date_txt.setTypeface(regular);
        time_txt = findViewById(R.id.time_txt);
        time_txt.setTypeface(regular);

        close = findViewById(R.id.close);
        close.setOnClickListener(this);

        price_txt = findViewById(R.id.price_txt);
        price_txt.setTypeface(bold);

        view1 = findViewById(R.id.view1);
        view2 = findViewById(R.id.view2);

        est_txt = findViewById(R.id.est_txt);
        est_txt.setTypeface(regular);
        est_price_txt = findViewById(R.id.est_price_txt);
        est_price_txt.setTypeface(regular);
        init_txt = findViewById(R.id.init_txt);
        init_txt.setTypeface(regular);
        init_price_txt = findViewById(R.id.init_price_txt);
        init_price_txt.setTypeface(regular);
        disc_txt = findViewById(R.id.disc_txt);
        disc_txt.setTypeface(regular);
        dicount_txt = findViewById(R.id.dicount_txt);
        dicount_txt.setTypeface(regular);
        addon_txt = findViewById(R.id.addon_txt);
        addon_txt.setTypeface(regular);
        addon_price_txt = findViewById(R.id.addon_price_txt);
        addon_price_txt.setTypeface(regular);
        tot_txt = findViewById(R.id.tot_txt);
        tot_txt.setTypeface(regular);
        final_price_txt = findViewById(R.id.final_price_txt);
        final_price_txt.setTypeface(bold);

        sumbit_btn = findViewById(R.id.sumbit_btn);
        sumbit_btn.setTypeface(bold);
        sumbit_btn.setOnClickListener(this);

        addon_recyclerview = findViewById(R.id.addon_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        addon_recyclerview.setLayoutManager(layoutManager);
        actionPageAdapter = new ActionPageAdapter(bookingAddonServicesModels, ActionPageActivity.this, R.layout.row_action_page);

        getDetails();
    }

    private void getDetails() {
        String url = AppUrls.BASE_URL + AppUrls.HISTORY_DETAIL + bookingId + "/detail";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObj = new JSONObject(response);
                    String responceCode = jsonObj.getString("status");
                    String message = jsonObj.getString("message");

                    if (responceCode.equalsIgnoreCase("2000")) {

                        JSONObject jsonObject = jsonObj.getJSONObject("data");

                        String bookingId = jsonObject.optString("bookingId");
                        String userId = jsonObject.optString("userId");
                        String orderId = jsonObject.optString("orderId");
                        String serviceCenterId = jsonObject.optString("serviceCenterId");
                        String bookingType = jsonObject.optString("bookingType");
                        brandId = jsonObject.optString("brandId");
                        brandName = jsonObject.optString("brandName");
                        modelId = jsonObject.optString("modelId");
                        modelName = jsonObject.optString("modelName");
                        String kilometerRangeId = jsonObject.optString("kilometerRangeId");
                        String bookingDate = jsonObject.optString("bookingDate");
                        String bookingTime = jsonObject.optString("bookingTime");
                        String pickupAddress = jsonObject.optString("pickupAddress");
                        String pickupAddressLatitude = jsonObject.optString("pickupAddressLatitude");
                        String pickupAddressLongitude = jsonObject.optString("pickupAddressLongitude");
                        String addressType = jsonObject.optString("addressType");
                        String estimatedCost = jsonObject.optString("estimatedCost");
                        finalPrice = jsonObject.optString("finalPrice");

                        if (jsonObject.has("promocodeAmount")) {
                            promocodeAmount = jsonObject.optString("promocodeAmount");
                        } else {
                            promocodeAmount = "0";
                        }
                        String initialPaidAmount = jsonObject.optString("initialPaidAmount");
                        String paymentStatus = jsonObject.optString("paymentStatus");
                        status = jsonObject.optString("status");
                        String deleted = jsonObject.optString("deleted");
                        String createdTime = jsonObject.optString("createdTime");
                        String modifiedTime = jsonObject.optString("modifiedTime");
                        String firstName = jsonObject.optString("firstName");
                        String lastName = jsonObject.optString("lastName");
                        String contactName = jsonObject.optString("contactName");
                        String serviceCenterName = jsonObject.optString("serviceCenterName");
                        String contactMobileNumber = jsonObject.optString("contactMobileNumber");
                        String averageRating = jsonObject.optString("averageRating");
                        String mobile = jsonObject.optString("mobile");
                        String serviceCenterLocation = jsonObject.optString("serviceCenterLocation");
                        String walletAmount = jsonObject.optString("walletAmount");

                        if (jsonObject.has("services")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("services");

                            String services = IntStream.range(0, jsonArray.length()).mapToObj(i -> {
                                try {
                                    return jsonArray.getJSONObject(i).optString("serviceName");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    return "";
                                }
                            }).collect(Collectors.joining("\n"));

                        }

                        if (jsonObject.has("inspections")) {

                            JSONArray jsonArray = jsonObject.getJSONArray("inspections");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                HistoryInspectionModel him = new HistoryInspectionModel();
                                him.setBookingId(jsonObject1.optString("bookingId"));
                                him.setInspectionId(jsonObject1.optString("inspectionId"));
                                him.setState(jsonObject1.optString("state"));
                                him.setInspectionName(jsonObject1.optString("inspectionName"));

                                historyInspectionModels.add(him);
                            }

                        }


                        if (jsonObject.has("bookingAddonServices")) {

                            JSONArray jsonArray = jsonObject.getJSONArray("bookingAddonServices");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                BookingAddonServicesModel basm = new BookingAddonServicesModel();
                                basm.setBookingId(jsonObject1.optString("bookingId"));
                                basm.setServiceId(jsonObject1.optString("serviceId"));
                                basm.setServiceName(jsonObject1.optString("serviceName"));
                                basm.setCost(jsonObject1.optString("cost"));
                                basm.setStatus(jsonObject1.optString("status"));

                                if (jsonObject1.optString("status").equalsIgnoreCase("0")) {
                                    bookingAddonServicesModels.add(basm);
                                } else {
                                    alreadyAddedServices.add(basm);
                                }
                            }
                            addon_recyclerview.setAdapter(actionPageAdapter);
                            actionPageAdapter.notifyDataSetChanged();

                        }

                        String resultDate = convertStringDateToAnotherStringDate(bookingDate, "yyyy-MM-dd", "dd-MM-yyyy");
                        Log.d("ResultDate", resultDate);

                        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
                        Date d = null;
                        try {
                            d = df.parse(bookingTime);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(d);
                        cal.add(Calendar.HOUR, 5);
                        cal.add(Calendar.MINUTE, 30);
                        String newTime = df.format(cal.getTime());

                        try {
                            SimpleDateFormat dateFormatter = new SimpleDateFormat("HH:mm:ss");
                            Date date = dateFormatter.parse(newTime);
                            SimpleDateFormat timeFormatter = new SimpleDateFormat("h:mm a");
                            //SimpleDateFormat timeFormatter = new SimpleDateFormat("h a");
                            newTime = timeFormatter.format(date);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        brand_txt.setText(brandName);
                        model_txt.setText(modelName);
                        date_txt.setText(resultDate);
                        time_txt.setText(newTime);

                        Double addOnPrice = bookingAddonServicesModels.stream().mapToDouble(t -> {
                            return Double.valueOf(t.getCost());
                        }).sum();
                        Double finalCost = Double.valueOf(finalPrice) - addOnPrice;

                        if (promocodeAmount.equalsIgnoreCase("0")) {
                            discount_tr.setVisibility(View.GONE);
                        } else {
                            discount_tr.setVisibility(View.VISIBLE);
                            dicount_txt.setText("\u20B9 " + promocodeAmount);
                        }

                        est_price_txt.setText("\u20B9 " + finalCost);
                        finalPrice = String.valueOf(finalCost);

                        Double discount = Double.valueOf(promocodeAmount);
                        finalPrice = String.valueOf(finalCost - discount);
                        final_price_txt.setText("\u20B9 " + finalPrice);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            GlobalCalls.showToast("Connection Not Found..!", ActionPageActivity.this);

                        } else if (error instanceof AuthFailureError) {

                            GlobalCalls.showToast("User Not Found..!", ActionPageActivity.this);

                        } else if (error instanceof ServerError) {

                            GlobalCalls.showToast("Server Not Found..!", ActionPageActivity.this);

                        } else if (error instanceof NetworkError) {

                            GlobalCalls.showToast("Network Not Found..!", ActionPageActivity.this);

                        } else if (error instanceof ParseError) {

                            GlobalCalls.showToast("Please Try Later..!", ActionPageActivity.this);

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(ActionPageActivity.this);
        requestQueue.add(stringRequest);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View v) {

        if (v == close) {
            Intent intent = new Intent(ActionPageActivity.this, MainActivity.class);
            intent.putExtra("activity", "ActionPageActivity");
            startActivity(intent);
        }

        if (v == sumbit_btn) {

            boolean status = bookingAddonServicesModels.stream().anyMatch(t -> t.getStatus().equalsIgnoreCase("0"));
            if (!status) {
                sendAddons();
            } else {

                GlobalCalls.showToast("Please Select All Services..!", ActionPageActivity.this);
            }
        }

    }

    public String convertStringDateToAnotherStringDate(String stringdate, String stringdateformat, String returndateformat) {

        try {
            Date date = new SimpleDateFormat(stringdateformat).parse(stringdate);
            String returndate = new SimpleDateFormat(returndateformat).format(date);
            return returndate;
        } catch (ParseException e) {
            e.printStackTrace();
            return stringdate;
        }

    }

    private void sendAddons() {

        ArrayList<BookingAddonServicesModel> sendServices = new ArrayList<BookingAddonServicesModel>();
        sendServices.addAll(bookingAddonServicesModels);
        sendServices.addAll(alreadyAddedServices);

        String url = AppUrls.BASE_URL + AppUrls.SEND_ADDON_SERVICES;
        JSONObject jsonObject = new JSONObject();
        JSONArray jArray = new JSONArray();

        try {
            jsonObject.put("bookingId", bookingId);

            for (int i = 0; i < sendServices.size(); i++) {
                JSONObject jsonObject1 = new JSONObject();
                jsonObject1.put("bookingId", sendServices.get(i).getBookingId());
                jsonObject1.put("serviceId", sendServices.get(i).getServiceId());
                jsonObject1.put("serviceName", sendServices.get(i).getServiceName());
                jsonObject1.put("cost", sendServices.get(i).getCost());
                jsonObject1.put("status", sendServices.get(i).getStatus());
                jArray.put(jsonObject1);
            }

            jsonObject.put("bookingAddonServices", jArray);

            Log.d("JAYYYY", "" + jsonObject);

            Log.d("ARRAYDATA", "" + jArray);

        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("SuccessResp", response.toString());

                try {
                    JSONObject jsonObj = new JSONObject(response.toString());
                    String status = jsonObj.getString("status");
                    String message = jsonObj.getString("message");
                    String code = jsonObj.getString("code");

                    GlobalCalls.showToast(message, ActionPageActivity.this);

                    Intent intent = new Intent(ActionPageActivity.this, MainActivity.class);
                    intent.putExtra("activity", "ActionPageActivity");
                    startActivity(intent);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(ActionPageActivity.this);
        requestQueue.add(request);

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void getTotalPrice() {

        addonCost = "" + bookingAddonServicesModels.stream().filter(t -> t.getStatus().equalsIgnoreCase("1")).mapToDouble(t -> {
            return Double.valueOf(t.getCost());
        }).sum();

        view1.setVisibility(View.VISIBLE);
        view2.setVisibility(View.VISIBLE);
        price_txt.setVisibility(View.VISIBLE);
        addon_tr.setVisibility(View.VISIBLE);
        price_txt.setText("Add On Price : \u20B9 " + addonCost);

        Double addOn, total, totalPrice;
        addOn = Double.valueOf(addonCost);
        total = Double.valueOf(finalPrice);
        totalPrice = addOn + total;
        String addOnPrice = String.valueOf(totalPrice);
        addon_price_txt.setText("\u20B9 " + addonCost);
        final_price_txt.setText("\u20B9 " + addOnPrice);
    }

    @Override
    public void onBackPressed() {

        GlobalCalls.showToast("Please Select Services..!", ActionPageActivity.this);

    }
}
