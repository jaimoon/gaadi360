package in.g360.owner.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.g360.owner.R;
import in.g360.owner.utils.AppUrls;
import in.g360.owner.utils.NetworkChecking;
import in.g360.owner.utils.UserSessionManager;

public class ReferFriendActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close;
    TextView toolbar_title, ref_title_txt, ref_txt, ref_code_txt;
    Button share_btn;
    private boolean checkInternet;
    Typeface regular, bold;
    UserSessionManager userSessionManager;
    String accessToken, userMobile, id, initialPaymentAmount, adImageId, radius, sstShowCount, ratingInterval, cancellationHours,
            createdTime, modifiedTime, preBookingDays, rescheduleCount, registrationAmount, referedAmount, walletDeductionPercentage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refer_friend);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        checkInternet = NetworkChecking.isConnected(this);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);
        userMobile = userDetails.get(UserSessionManager.USER_MOBILE);

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);

        close = findViewById(R.id.close);
        close.setOnClickListener(this);

        ref_title_txt = findViewById(R.id.ref_title_txt);
        ref_title_txt.setTypeface(regular);

        ref_txt = findViewById(R.id.ref_txt);
        ref_txt.setTypeface(bold);

        ref_code_txt = findViewById(R.id.ref_code_txt);
        ref_code_txt.setTypeface(bold);
        ref_code_txt.setText(userMobile);

        share_btn = findViewById(R.id.share_btn);
        share_btn.setTypeface(bold);
        share_btn.setOnClickListener(this);

        getReferalAmount();
    }

    private void getReferalAmount() {

        String url = AppUrls.BASE_URL + AppUrls.REFERAL_AMOUNT_DETAIL;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    id = jsonObject.optString("id");
                    initialPaymentAmount = jsonObject.optString("initialPaymentAmount");
                    adImageId = jsonObject.optString("adImageId");
                    radius = jsonObject.optString("radius");
                    sstShowCount = jsonObject.optString("sstShowCount");
                    ratingInterval = jsonObject.optString("ratingInterval");
                    cancellationHours = jsonObject.optString("cancellationHours");
                    createdTime = jsonObject.optString("createdTime");
                    modifiedTime = jsonObject.optString("modifiedTime");
                    preBookingDays = jsonObject.optString("preBookingDays");
                    rescheduleCount = jsonObject.optString("rescheduleCount");
                    registrationAmount = jsonObject.optString("registrationAmount");
                    referedAmount = jsonObject.optString("referedAmount");
                    walletDeductionPercentage = jsonObject.optString("walletDeductionPercentage");

                    ref_title_txt.setText("Refer a friend and earn \u20B9 "+ referedAmount  +" for yourself and save another \u20B9 "+ registrationAmount +" for your friend. It is quite simple - your friend earns \u20B9 "+ registrationAmount +" when he/she enters your phone number as referral code. When they complete their bike service through Gaadi360, you will earn \u20B9 "+ referedAmount +" for the referral that can be redeemed with your next bike service. Help us spread the word about our professional bike servicing app and experience while you save money for your friends and you.");

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(ReferFriendActivity.this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {

        if (v == close) {
            if (checkInternet) {
                Intent intent = new Intent(ReferFriendActivity.this, SettingActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == share_btn) {

            if (checkInternet) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Gaadi360");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Hi, save \u20B9 " + registrationAmount + " by getting your next bike service using Gaadi360 app at http://app.Gaadi360.com . Please enter my phone number " + userMobile + " as referral code when you register so I can save on my next bike service. Happy and safe riding!");
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }

        }
    }

    @Override
    public void onBackPressed() {

        if (checkInternet) {
            Intent intent = new Intent(ReferFriendActivity.this, SettingActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();

        } else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }
}
