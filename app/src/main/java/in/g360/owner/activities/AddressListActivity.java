package in.g360.owner.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.g360.owner.R;
import in.g360.owner.models.AddressListModel;
import in.g360.owner.utils.AppUrls;
import in.g360.owner.utils.NetworkChecking;
import in.g360.owner.utils.UserSessionManager;

public class AddressListActivity extends AppCompatActivity implements View.OnClickListener {

    private boolean checkInternet;
    ImageView close,home_edit_img,work_edit_img,other_edit_img;
    UserSessionManager userSessionManager;
    String accessToken;
    TextView home,home_address_txt,work,work_address_txt,other,other_address_txt;
    LinearLayout home_ll,work_ll,other_ll;

    /*AddressList*/
    ArrayList<AddressListModel> addressListModels = new ArrayList<AddressListModel>();

    Map<String,String> address = new HashMap<String,String>();

    Typeface regular, bold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_list);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        checkInternet = NetworkChecking.isConnected(this);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);

        close = findViewById(R.id.close);
        close.setOnClickListener(this);
        home_edit_img = findViewById(R.id.home_edit_img);
        home_edit_img.setOnClickListener(this);
        work_edit_img = findViewById(R.id.work_edit_img);
        work_edit_img.setOnClickListener(this);
        other_edit_img = findViewById(R.id.other_edit_img);
        other_edit_img.setOnClickListener(this);

        home_ll = findViewById(R.id.home_ll);
        work_ll = findViewById(R.id.work_ll);
        other_ll = findViewById(R.id.other_ll);

        home = findViewById(R.id.home);
        work = findViewById(R.id.work);
        other = findViewById(R.id.other);
        home_address_txt = findViewById(R.id.home_address_txt);
        work_address_txt = findViewById(R.id.work_address_txt);
        other_address_txt = findViewById(R.id.other_address_txt);

        getAddress();


    }

    private void getAddress() {
        String url = AppUrls.BASE_URL + AppUrls.ADDRESSES;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                addressListModels.clear();

                try {
                    JSONArray jsonArray = new JSONArray(response);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        AddressListModel alm = new AddressListModel();
                        alm.setUserAddressId(jsonObject.getString("userAddressId"));
                        alm.setUserId(jsonObject.getString("userId"));
                        alm.setAddress(jsonObject.getString("address"));
                        alm.setLatitude(jsonObject.getString("latitude"));
                        alm.setLongitude(jsonObject.getString("longitude"));
                        alm.setLandmark(jsonObject.getString("landmark"));
                        alm.setType(jsonObject.getString("type"));

                        address.put(jsonObject.getString("type"),jsonObject.getString("address"));

                        alm.setCreatedTime(jsonObject.getString("createdTime"));
                        alm.setModifiedTime(jsonObject.getString("modifiedTime"));
                        addressListModels.add(alm);
                    }

                    if (address.containsKey("1")){
                        home_address_txt.setText(address.get("1"));
                    }else {
                        home.setVisibility(View.GONE);
                        home_edit_img.setVisibility(View.GONE);
                        home_ll.setVisibility(View.GONE);
                    }

                    if (address.containsKey("2")){
                        work_address_txt.setText(address.get("2"));
                    }else {
                        work.setVisibility(View.GONE);
                        work_edit_img.setVisibility(View.GONE);
                        work_ll.setVisibility(View.GONE);
                    }
                    if (address.containsKey("3")){
                        other_address_txt.setText(address.get("3"));
                    }else {
                        other.setVisibility(View.GONE);
                        other_edit_img.setVisibility(View.GONE);
                        other_ll.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(AddressListActivity.this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {

        if (v == close) {
            if (checkInternet) {
                Intent intent = new Intent(AddressListActivity.this, MyAccountActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == home_edit_img) {
            if (checkInternet) {
                Intent intent = new Intent(AddressListActivity.this, MapActivity.class);
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == work_edit_img) {
            if (checkInternet) {
                Intent intent = new Intent(AddressListActivity.this, MapActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == other_edit_img) {
            if (checkInternet) {
                Intent intent = new Intent(AddressListActivity.this, MapActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(AddressListActivity.this, MyAccountActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
