package in.g360.owner.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.taishi.flipprogressdialog.FlipProgressDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.g360.owner.MainActivity;
import in.g360.owner.R;
import in.g360.owner.utils.AppUrls;
import in.g360.owner.utils.GlobalCalls;
import in.g360.owner.utils.NetworkChecking;
import in.g360.owner.utils.UserSessionManager;

public class IFCActivity extends AppCompatActivity implements View.OnClickListener {

    private boolean checkInternet;
    ImageView close;
    TextView toolbar_title, thank_txt, que_one_txt, que_two_txt, que_three_txt, que_four_txt, que_five_txt, update_txt;
    RadioGroup que_one_rg, que_two_rg, que_three_rg, que_four_rg;
    RadioButton yes_rb, no_rb, l_rb, a_rb, h_rb, good_rb, bad_rb, very_good_rb, f_good_rb, average_rb, f_bad_rb;
    EditText suggestions_edt, name_edt, email_edt;
    Button submit_btn;
    UserSessionManager userSessionManager;
    String accessToken, name, userEmail, userName, userMobile, mediaSecret, bookingId, strOne, strTwo, strThree, strFour;

    /*ProgressDialog*/
    //List<Integer> imageList = new ArrayList<Integer>();
    //FlipProgressDialog flipProgressDialog;

    Typeface regular, bold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ifc);

        getProfile();

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        checkInternet = NetworkChecking.isConnected(this);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);
        /*name = userDetails.get(UserSessionManager.FIRST_NAME);
        userEmail = userDetails.get(UserSessionManager.USER_EMAIL);
        userName = userDetails.get(UserSessionManager.USER_NAME);
        userMobile = userDetails.get(UserSessionManager.USER_MOBILE);
        mediaSecret = userDetails.get(UserSessionManager.MEDIA_SECRET);
*/
        bookingId = getIntent().getStringExtra("bookingId");

        Log.d("ACCESSTOKEN", accessToken + "\n" + bookingId);
        //Log.d("NAMEEEE", name);

        //imageList.add(R.mipmap.ic_launcher);
        /*flipProgressDialog = new FlipProgressDialog();
        flipProgressDialog.setImageList(imageList);
        flipProgressDialog.setCanceledOnTouchOutside(true);
        flipProgressDialog.setDimAmount(0.8f); //0.0f
        flipProgressDialog.setBackgroundColor(Color.parseColor("#00FFFFFF"));
        flipProgressDialog.setBackgroundAlpha(0.2f);
        flipProgressDialog.setBorderStroke(0);
        flipProgressDialog.setBorderColor(-1);
        flipProgressDialog.setCornerRadius(16);
        flipProgressDialog.setImageSize(200);
        flipProgressDialog.setImageMargin(10);
        flipProgressDialog.setOrientation("rotationY");
        flipProgressDialog.setDuration(600);
        flipProgressDialog.setStartAngle(0.0f);
        flipProgressDialog.setEndAngle(180.0f);
        flipProgressDialog.setMinAlpha(0.0f);
        flipProgressDialog.setMaxAlpha(1.0f);
        flipProgressDialog.show(getFragmentManager(), "");
        flipProgressDialog.dismiss();*/

        close = findViewById(R.id.close);
        close.setOnClickListener(this);

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);
        thank_txt = findViewById(R.id.thank_txt);
        thank_txt.setTypeface(bold);
        que_one_txt = findViewById(R.id.que_one_txt);
        que_one_txt.setTypeface(bold);
        que_two_txt = findViewById(R.id.que_two_txt);
        que_two_txt.setTypeface(bold);
        que_three_txt = findViewById(R.id.que_three_txt);
        que_three_txt.setTypeface(bold);
        que_four_txt = findViewById(R.id.que_four_txt);
        que_four_txt.setTypeface(bold);
        que_five_txt = findViewById(R.id.que_five_txt);
        que_five_txt.setTypeface(bold);

        que_one_rg = findViewById(R.id.que_one_rg);
        que_two_rg = findViewById(R.id.que_two_rg);
        que_three_rg = findViewById(R.id.que_three_rg);
        que_four_rg = findViewById(R.id.que_four_rg);

        yes_rb = findViewById(R.id.yes_rb);
        yes_rb.setTypeface(regular);
        no_rb = findViewById(R.id.no_rb);
        no_rb.setTypeface(regular);
        l_rb = findViewById(R.id.l_rb);
        l_rb.setTypeface(regular);
        a_rb = findViewById(R.id.a_rb);
        a_rb.setTypeface(regular);
        h_rb = findViewById(R.id.h_rb);
        h_rb.setTypeface(regular);
        good_rb = findViewById(R.id.good_rb);
        good_rb.setTypeface(regular);
        bad_rb = findViewById(R.id.bad_rb);
        bad_rb.setTypeface(regular);
        very_good_rb = findViewById(R.id.very_good_rb);
        very_good_rb.setTypeface(regular);
        f_good_rb = findViewById(R.id.f_good_rb);
        f_good_rb.setTypeface(regular);
        average_rb = findViewById(R.id.average_rb);
        average_rb.setTypeface(regular);
        f_bad_rb = findViewById(R.id.f_bad_rb);
        f_bad_rb.setTypeface(regular);

        suggestions_edt = findViewById(R.id.suggestions_edt);
        suggestions_edt.setTypeface(regular);
        update_txt = findViewById(R.id.update_txt);
        update_txt.setTypeface(bold);
        name_edt = findViewById(R.id.name_edt);
        name_edt.setTypeface(regular);
        email_edt = findViewById(R.id.email_edt);
        email_edt.setTypeface(regular);

        submit_btn = findViewById(R.id.submit_btn);
        submit_btn.setTypeface(bold);
        submit_btn.setOnClickListener(this);

        que_one_rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.yes_rb:
                        strOne = String.valueOf(yes_rb.getText());
                        break;
                    case R.id.no_rb:
                        strOne = String.valueOf(no_rb.getText());
                        break;

                }
            }
        });

        que_two_rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.l_rb:
                        strTwo = String.valueOf(l_rb.getText());
                        break;
                    case R.id.a_rb:
                        strTwo = String.valueOf(a_rb.getText());
                        break;
                    case R.id.h_rb:
                        strTwo = String.valueOf(h_rb.getText());
                        break;

                }
            }
        });

        que_three_rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.good_rb:
                        strThree = String.valueOf(good_rb.getText());
                        break;
                    case R.id.bad_rb:
                        strThree = String.valueOf(bad_rb.getText());
                        break;

                }
            }
        });

        que_four_rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.very_good_rb:
                        strFour = String.valueOf(very_good_rb.getText());
                        break;
                    case R.id.f_good_rb:
                        strFour = String.valueOf(f_good_rb.getText());
                        break;
                    case R.id.average_rb:
                        strFour = String.valueOf(average_rb.getText());
                        break;
                    case R.id.f_bad_rb:
                        strFour = String.valueOf(f_bad_rb.getText());
                        break;

                }
            }
        });

    }

    @Override
    public void onClick(View v) {
        if (v == close) {
            if (checkInternet) {
                Intent intent = new Intent(IFCActivity.this, MainActivity.class);
                intent.putExtra("activity", "IFCActivity");
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == submit_btn) {
            if (checkInternet) {

                if (que_one_rg.getCheckedRadioButtonId() == -1 || que_two_rg.getCheckedRadioButtonId() == -1 ||
                        que_three_rg.getCheckedRadioButtonId() == -1 || que_four_rg.getCheckedRadioButtonId() == -1) {

                    GlobalCalls.showToast("Please Fill All Details..!", IFCActivity.this);

                } else {

                    sendIFC();

                }
            }
        } else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    private void getProfile() {
        String url = AppUrls.BASE_URL + AppUrls.PROFILE;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    name = jsonObject.optString("firstName");
                    userEmail = jsonObject.optString("email");
                    userMobile = jsonObject.optString("mobile");

                    if (jsonObject.has("firstName")) {
                        name_edt.setText(name);
                    }
                    if (jsonObject.has("email")) {
                        email_edt.setText(userEmail);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(IFCActivity.this);
        requestQueue.add(stringRequest);
    }

    private void updateProfile() {
        String url = AppUrls.BASE_URL + AppUrls.EDIT_PROFILE;

        JSONObject jsonObj = new JSONObject();

        try {
            jsonObj.put("firstName", name_edt.getText().toString());
            jsonObj.put("profilePicId", "");
            jsonObj.put("email", email_edt.getText().toString());

        } catch (Exception e) {

        }

        Log.d("JsonObj", jsonObj.toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("ProfileStatus", response.toString());

                try {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    Log.d("Status", status + "\n" + message);

                    if (status.equalsIgnoreCase("2000")) {

                        GlobalCalls.showToast(message, IFCActivity.this);

                        userSessionManager.createUserLoginSession(name_edt.getText().toString(), userName, email_edt.getText().toString(), userMobile,
                                accessToken, mediaSecret);

                        Intent intent = new Intent(IFCActivity.this, FinalPaymentActivity.class);
                        intent.putExtra("bookingId", bookingId);
                        startActivity(intent);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                    Log.d("ResError", e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

                Log.d("EROORRR", error.getMessage());

            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(IFCActivity.this);
        requestQueue.add(request);
    }

    private void sendIFC() {

        String url = AppUrls.BASE_URL + AppUrls.SEND_IFC;

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("bookingId", bookingId);
            jsonObject.put("complaintsSolved", strOne);
            jsonObject.put("serviceCost", strTwo);
            jsonObject.put("waterWash", strThree);
            jsonObject.put("overAllService", strFour);
            jsonObject.put("suggestions", suggestions_edt.getText().toString());

            Log.d("JAYYYY", "" + jsonObject);

        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("SuccessResp", response.toString());

                try {
                    JSONObject jsonObj = new JSONObject(response.toString());
                    String status = jsonObj.getString("status");
                    String message = jsonObj.getString("message");
                    String code = jsonObj.getString("code");

                    if (name_edt.getText().toString() == null || name_edt.getText().toString().trim().equalsIgnoreCase("")) {

                        GlobalCalls.showToast("Enter Name", IFCActivity.this);

                    } else if (isValidEmail(email_edt.getText().toString().trim())) {

                        updateProfile();

                    } else {

                        GlobalCalls.showToast("Enter Valid Email", IFCActivity.this);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(IFCActivity.this);
        requestQueue.add(request);
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }


    @Override
    public void onBackPressed() {
        GlobalCalls.showToast("Please Fill Above Form..!", IFCActivity.this);
    }
}
