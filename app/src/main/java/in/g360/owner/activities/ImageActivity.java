package in.g360.owner.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.VideoView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import in.g360.owner.MainActivity;
import in.g360.owner.R;
import in.g360.owner.adapters.BikeImageAdapter;
import in.g360.owner.adapters.BikeVideoAdapter;
import in.g360.owner.dbhelper.ImageDbHelper;
import in.g360.owner.models.BikeImagesModel;
import in.g360.owner.models.BikeVideosModel;
import in.g360.owner.utils.CameraUtils;
import in.g360.owner.utils.GlobalCalls;
import in.g360.owner.utils.NetworkChecking;

public class ImageActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;
    public static final String KEY_IMAGE_STORAGE_PATH = "image_path";
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    public static final int BITMAP_SAMPLE_SIZE = 8;
    public static final String GALLERY_DIRECTORY_NAME = "Hello Camera";
    public static final String IMAGE_EXTENSION = "jpg";
    public static final String VIDEO_EXTENSION = "mp4";
    private static String imageStoragePath;
    private ImageView close,preview_img, saved_img;
    private VideoView preview_video;
    private Button btnCapturePicture, btnRecordVideo, saved_btn,saved_v_btn;

    ImageDbHelper imageDbHelper;
    ArrayList<BikeImagesModel> bikeImagesModels;
    ArrayList<BikeVideosModel> bikeVideosModels;
    RecyclerView recyclerview_img, recyclerview_video;
    BikeImageAdapter bikeImageAdapter;
    BikeVideoAdapter bikeVideoAdapter;
    private boolean checkInternet;
    Typeface regular, bold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        checkInternet = NetworkChecking.isConnected(this);

        imageDbHelper = new ImageDbHelper(ImageActivity.this);

        if (!CameraUtils.isDeviceSupportCamera(getApplicationContext())) {

            GlobalCalls.showToast("Sorry! Your device doesn't support camera", ImageActivity.this);
            finish();
        }

        close = findViewById(R.id.close);
        close.setOnClickListener(this);
        saved_img = findViewById(R.id.saved_img);
        preview_img = findViewById(R.id.preview_img);
        preview_video = findViewById(R.id.preview_video);
        btnCapturePicture = findViewById(R.id.btnCapturePicture);
        btnCapturePicture.setOnClickListener(this);
        btnRecordVideo = findViewById(R.id.btnRecordVideo);
        btnRecordVideo.setOnClickListener(this);
        saved_btn = findViewById(R.id.saved_btn);
        saved_btn.setOnClickListener(this);
        saved_v_btn = findViewById(R.id.saved_v_btn);
        saved_v_btn.setOnClickListener(this);

        bikeImagesModels = new ArrayList<BikeImagesModel>();
        recyclerview_img = findViewById(R.id.recyclerview_img);
        bikeImageAdapter = new BikeImageAdapter(bikeImagesModels, ImageActivity.this, R.layout.row_bike_images);
        //recyclerview_img.setLayoutManager(new LinearLayoutManager(ImageActivity.this, LinearLayoutManager.HORIZONTAL, false));
        LinearLayoutManager layoutManager = new LinearLayoutManager(ImageActivity.this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerview_img.setLayoutManager(layoutManager);
        //recyclerview_img.setAdapter( adapter );

        bikeVideosModels = new ArrayList<BikeVideosModel>();
        recyclerview_video = findViewById(R.id.recyclerview_video);
        bikeVideoAdapter = new BikeVideoAdapter(bikeVideosModels, ImageActivity.this, R.layout.row_bike_videos);
        //recyclerview_video.setLayoutManager(new LinearLayoutManager(ImageActivity.this, LinearLayoutManager.HORIZONTAL, false));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ImageActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerview_video.setLayoutManager(linearLayoutManager);

        restoreFromBundle(savedInstanceState);
    }

    @Override
    public void onClick(View v) {

        if (v == close){
            if (checkInternet) {
                Intent intent = new Intent(ImageActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
        if (v == saved_btn) {
            getBikeImagesList();

        }

        if (v == saved_v_btn) {
            getBikeVideosList();
        }

        if (v == btnCapturePicture) {
            if (CameraUtils.checkPermissions(getApplicationContext())) {
                captureImage();
            } else {
                requestCameraPermission(MEDIA_TYPE_IMAGE);
            }
        }

        if (v == btnRecordVideo) {
            if (CameraUtils.checkPermissions(getApplicationContext())) {
                captureVideo();
            } else {
                requestCameraPermission(MEDIA_TYPE_VIDEO);
            }
        }

    }

    private void restoreFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(KEY_IMAGE_STORAGE_PATH)) {
                imageStoragePath = savedInstanceState.getString(KEY_IMAGE_STORAGE_PATH);
                if (!TextUtils.isEmpty(imageStoragePath)) {
                    if (imageStoragePath.substring(imageStoragePath.lastIndexOf(".")).equals("." + IMAGE_EXTENSION)) {
                        previewCapturedImage();
                    } else if (imageStoragePath.substring(imageStoragePath.lastIndexOf(".")).equals("." + VIDEO_EXTENSION)) {
                        previewVideo();
                    }
                }
            }
        }
    }

    private void requestCameraPermission(final int type) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.RECORD_AUDIO)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {

                            if (type == MEDIA_TYPE_IMAGE) {
                                captureImage();
                            } else {
                                captureVideo();
                            }

                        } else if (report.isAnyPermissionPermanentlyDenied()) {
                            showPermissionsAlert();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<com.karumi.dexter.listener.PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }

                }).check();
    }

    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file = CameraUtils.getOutputMediaFile(MEDIA_TYPE_IMAGE);
        if (file != null) {
            imageStoragePath = file.getAbsolutePath();
        }

        Uri fileUri = CameraUtils.getOutputMediaFileUri(getApplicationContext(), file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(KEY_IMAGE_STORAGE_PATH, imageStoragePath);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        imageStoragePath = savedInstanceState.getString(KEY_IMAGE_STORAGE_PATH);
    }

    private void captureVideo() {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

        File file = CameraUtils.getOutputMediaFile(MEDIA_TYPE_VIDEO);
        if (file != null) {
            imageStoragePath = file.getAbsolutePath();
        }

        Uri fileUri = CameraUtils.getOutputMediaFileUri(getApplicationContext(), file);
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file
        startActivityForResult(intent, CAMERA_CAPTURE_VIDEO_REQUEST_CODE);
    }

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                CameraUtils.refreshGallery(getApplicationContext(), imageStoragePath);
                previewCapturedImage();
            } else if (resultCode == RESULT_CANCELED) {
                GlobalCalls.showToast("User cancelled image capture", ImageActivity.this);
            } else {

                GlobalCalls.showToast("Sorry! Failed to capture image", ImageActivity.this);
            }
        } else if (requestCode == CAMERA_CAPTURE_VIDEO_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                CameraUtils.refreshGallery(getApplicationContext(), imageStoragePath);
                previewVideo();
            } else if (resultCode == RESULT_CANCELED) {

                GlobalCalls.showToast("User cancelled video recording", ImageActivity.this);

            } else {

                GlobalCalls.showToast("Sorry! Failed to record video", ImageActivity.this);

            }
        }
    }

    private void previewCapturedImage() {
        try {
            preview_video.setVisibility(View.GONE);
            preview_img.setVisibility(View.VISIBLE);
            Bitmap bitmap = CameraUtils.optimizeBitmap(BITMAP_SAMPLE_SIZE, imageStoragePath);
            Log.d("IMAGEPATH", imageStoragePath);

            saved_img.setImageBitmap(bitmap);

            ContentValues values = new ContentValues();
            values.put(ImageDbHelper.BIKE_IMAGE, imageStoragePath);
            imageDbHelper.addBikeImages(values);

            preview_img.setImageBitmap(bitmap);

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void previewVideo() {
        try {
            preview_img.setVisibility(View.GONE);
            preview_video.setVisibility(View.VISIBLE);

            Log.d("VIDEOPATH", imageStoragePath);

            ContentValues values = new ContentValues();
            values.put(ImageDbHelper.BIKE_VIDEO, imageStoragePath);
            imageDbHelper.addBikeVideos(values);
            preview_video.setVideoPath(imageStoragePath);
            preview_video.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showPermissionsAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Permissions required!")
                .setMessage("Camera needs few permissions to work properly. Grant them in settings.")
                .setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        CameraUtils.openSettings(ImageActivity.this);
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
    }

    private void getBikeImagesList() {

        bikeImagesModels.clear();

        ImageDbHelper db = new ImageDbHelper(this);
        List<String> bikeImageIdList = db.getBikeImageId();
        List<String> bikeImageList = db.getBikeImage();

        for (int i = 0; i < bikeImageIdList.size(); i++) {
            BikeImagesModel bim = new BikeImagesModel();
            bim.setId(bikeImageIdList.get(i));
            bim.setBikeImage(bikeImageList.get(i));
            bikeImagesModels.add(bim);
        }
        recyclerview_img.setAdapter(bikeImageAdapter);
        db.close();
    }

    private void getBikeVideosList() {

        bikeVideosModels.clear();

        ImageDbHelper db = new ImageDbHelper(this);
        List<String> bikeVideoIdList = db.getBikeVideoId();
        List<String> bikeVideoList = db.getBikeVideo();

        for (int i = 0; i < bikeVideoIdList.size(); i++) {

            BikeVideosModel bvm = new BikeVideosModel();
            bvm.setId(bikeVideoIdList.get(i));
            bvm.setBikeVideo(bikeVideoList.get(i));
            bikeVideosModels.add(bvm);
        }
        recyclerview_video.setAdapter(bikeVideoAdapter);
        db.close();
    }
}
