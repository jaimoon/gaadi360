package in.g360.owner.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.taishi.flipprogressdialog.FlipProgressDialog;

import java.util.ArrayList;
import java.util.List;

import in.g360.owner.R;
import in.g360.owner.utils.NetworkChecking;

public class TermsAndConditionsActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close;
    TextView toolbar_title;
    private boolean checkInternet;
    WebView terms_web;

    /*ProgressDialog*/
    List<Integer> imageList = new ArrayList<Integer>();
    FlipProgressDialog flipProgressDialog;
    Typeface regular, bold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_conditions);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        checkInternet = NetworkChecking.isConnected(this);

        imageList.add(R.mipmap.ic_launcher);
        flipProgressDialog = new FlipProgressDialog();
        flipProgressDialog.setImageList(imageList);
        flipProgressDialog.setCanceledOnTouchOutside(true);
        flipProgressDialog.setDimAmount(0.8f); //0.0f
        flipProgressDialog.setBackgroundColor(Color.parseColor("#00FFFFFF"));
        flipProgressDialog.setBackgroundAlpha(0.2f);
        flipProgressDialog.setBorderStroke(0);
        flipProgressDialog.setBorderColor(-1);
        flipProgressDialog.setCornerRadius(16);
        flipProgressDialog.setImageSize(200);
        flipProgressDialog.setImageMargin(10);
        flipProgressDialog.setOrientation("rotationY");
        flipProgressDialog.setDuration(600);
        flipProgressDialog.setStartAngle(0.0f);
        flipProgressDialog.setEndAngle(180.0f);
        flipProgressDialog.setMinAlpha(0.0f);
        flipProgressDialog.setMaxAlpha(1.0f);
        flipProgressDialog.show(getFragmentManager(), "");
        //flipProgressDialog.dismiss();

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);

        close = findViewById(R.id.close);
        close.setOnClickListener(this);

        terms_web = findViewById(R.id.terms_web);

        terms_web.setInitialScale(1);
        terms_web.getSettings().setJavaScriptEnabled(true);
        terms_web.getSettings().setLoadWithOverviewMode(true);
        terms_web.getSettings().setUseWideViewPort(true);
        terms_web.getSettings().setBuiltInZoomControls(true);
        terms_web.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        terms_web.setScrollbarFadingEnabled(false);
        terms_web.setWebViewClient(new WebViewClient());
        CookieManager.getInstance().setAcceptCookie(true);
        terms_web.loadUrl("http://www.gaadi360.com/terms-and-conditions.html");

        terms_web.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {

                flipProgressDialog.dismiss();

            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == close){
            if (checkInternet) {
                Intent intent = new Intent(TermsAndConditionsActivity.this, SettingActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            }else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (checkInternet) {
            Intent intent = new Intent(TermsAndConditionsActivity.this, SettingActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();

        }else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }
}
