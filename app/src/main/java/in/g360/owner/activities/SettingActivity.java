package in.g360.owner.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import in.g360.owner.MainActivity;
import in.g360.owner.R;
import in.g360.owner.utils.AppUrls;
import in.g360.owner.utils.NetworkChecking;
import in.g360.owner.utils.UserSessionManager;

public class SettingActivity extends AppCompatActivity implements View.OnClickListener {

    private boolean checkInternet;
    ImageView close;
    TextView toolbar_title, wallet_txt, acc_txt, about_txt, t_txt, help_txt, privacy_txt, can_txt, refer_txt, wallet_price_txt,
            logout_txt, version_txt;
    RelativeLayout my_accountRL, aboutUsRL, tcRL, help_centerRL, privacy_policyRL, refund_policyRL, refer_RL, wallet_RL,
            logoutRL;
    UserSessionManager userSessionManager;
    Typeface regular, bold;
    String accessToken, userMobile,versionName, userId, firstName, lastName, username, authority, email, mobile, emailVerified,
            mobileVerified, wallet,id, initialPaymentAmount, adImageId, radius, sstShowCount, ratingInterval, cancellationHours,
            createdTime, modifiedTime, preBookingDays, rescheduleCount, registrationAmount, referedAmount, walletDeductionPercentage;
    int versionCode;
    AlertDialog.Builder dialogBuilder;
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        checkInternet = NetworkChecking.isConnected(this);
        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);
        userMobile = userDetails.get(UserSessionManager.USER_MOBILE);

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);
        wallet_txt = findViewById(R.id.wallet_txt);
        wallet_txt.setTypeface(bold);
        wallet_txt.setOnClickListener(this);

        close = findViewById(R.id.close);
        close.setOnClickListener(this);

        acc_txt = findViewById(R.id.acc_txt);
        acc_txt.setTypeface(regular);
        about_txt = findViewById(R.id.about_txt);
        about_txt.setTypeface(regular);
        t_txt = findViewById(R.id.t_txt);
        t_txt.setTypeface(regular);
        help_txt = findViewById(R.id.help_txt);
        help_txt.setTypeface(regular);
        privacy_txt = findViewById(R.id.privacy_txt);
        privacy_txt.setTypeface(regular);
        can_txt = findViewById(R.id.can_txt);
        can_txt.setTypeface(regular);
        refer_txt = findViewById(R.id.refer_txt);
        refer_txt.setTypeface(regular);
        wallet_price_txt = findViewById(R.id.wallet_price_txt);
        wallet_price_txt.setTypeface(regular);
        logout_txt = findViewById(R.id.logout_txt);
        logout_txt.setTypeface(regular);

        my_accountRL = findViewById(R.id.my_accountRL);
        my_accountRL.setOnClickListener(this);
        aboutUsRL = findViewById(R.id.aboutUsRL);
        aboutUsRL.setOnClickListener(this);
        tcRL = findViewById(R.id.tcRL);
        tcRL.setOnClickListener(this);
        help_centerRL = findViewById(R.id.help_centerRL);
        help_centerRL.setOnClickListener(this);
        privacy_policyRL = findViewById(R.id.privacy_policyRL);
        privacy_policyRL.setOnClickListener(this);
        refund_policyRL = findViewById(R.id.refund_policyRL);
        refund_policyRL.setOnClickListener(this);
        refer_RL = findViewById(R.id.refer_RL);
        refer_RL.setOnClickListener(this);
        wallet_RL = findViewById(R.id.wallet_RL);
        wallet_RL.setOnClickListener(this);
        logoutRL = findViewById(R.id.logoutRL);
        logoutRL.setOnClickListener(this);

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionName = pInfo.versionName;
            versionCode = pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        StringBuilder builder = new StringBuilder();
        builder.append("Android : ").append(Build.VERSION.RELEASE);

        Field[] fields = Build.VERSION_CODES.class.getFields();
        for (Field field : fields) {
            String fieldName = field.getName();
            int fieldValue = -1;

            try {
                fieldValue = field.getInt(new Object());
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            if (fieldValue == Build.VERSION.SDK_INT) {
                builder.append("(").append(fieldName).append(")");
                //builder.append("sdk=").append(fieldValue);
            }
        }

        Log.d("OS: ", builder.toString());

        version_txt = findViewById(R.id.version_txt);
        version_txt.setTypeface(regular);
        version_txt.setText("Version " + versionName + " - (" + builder.toString() + ")");

        getProfile();

        getReferalAmount();

    }

    private void getProfile() {
        String url = AppUrls.BASE_URL + AppUrls.PROFILE;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    userId = jsonObject.optString("userId");
                    firstName = jsonObject.optString("firstName");
                    lastName = jsonObject.optString("lastName");
                    username = jsonObject.optString("username");
                    authority = jsonObject.optString("authority");
                    email = jsonObject.optString("email");
                    mobile = jsonObject.optString("mobile");
                    emailVerified = jsonObject.optString("emailVerified");
                    mobileVerified = jsonObject.optString("mobileVerified");
                    wallet = jsonObject.optString("wallet");

                    wallet_txt.setText("Wallet : \u20B9 " + wallet);

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(SettingActivity.this);
        requestQueue.add(stringRequest);
    }

    private void getReferalAmount() {

        String url = AppUrls.BASE_URL + AppUrls.REFERAL_AMOUNT_DETAIL;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    id = jsonObject.optString("id");
                    initialPaymentAmount = jsonObject.optString("initialPaymentAmount");
                    adImageId = jsonObject.optString("adImageId");
                    radius = jsonObject.optString("radius");
                    sstShowCount = jsonObject.optString("sstShowCount");
                    ratingInterval = jsonObject.optString("ratingInterval");
                    cancellationHours = jsonObject.optString("cancellationHours");
                    createdTime = jsonObject.optString("createdTime");
                    modifiedTime = jsonObject.optString("modifiedTime");
                    preBookingDays = jsonObject.optString("preBookingDays");
                    rescheduleCount = jsonObject.optString("rescheduleCount");
                    registrationAmount = jsonObject.optString("registrationAmount");
                    referedAmount = jsonObject.optString("referedAmount");
                    walletDeductionPercentage = jsonObject.optString("walletDeductionPercentage");


                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(SettingActivity.this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {

        if (v == close) {
            if (checkInternet) {
                Intent intent = new Intent(SettingActivity.this, MainActivity.class);
                intent.putExtra("activity", "SettingActivity");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == wallet_txt) {
            if (checkInternet) {

                showReferalDialog(R.layout.wallet_dialog);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == my_accountRL) {
            if (checkInternet) {
                Intent intent = new Intent(SettingActivity.this, MyAccountActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == aboutUsRL) {
            if (checkInternet) {
                Intent intent = new Intent(SettingActivity.this, AboutUsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == tcRL) {
            if (checkInternet) {
                Intent intent = new Intent(SettingActivity.this, TermsAndConditionsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == help_centerRL) {
            if (checkInternet) {
                Intent intent = new Intent(SettingActivity.this, HelpCenterActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == privacy_policyRL) {
            if (checkInternet) {
                Intent intent = new Intent(SettingActivity.this, PrivacyAndPolicyActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == refund_policyRL) {
            if (checkInternet) {
                Intent intent = new Intent(SettingActivity.this, CancellationAndRefundPolicyActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
        if (v == refer_RL) {
            if (checkInternet) {
                Intent intent = new Intent(SettingActivity.this, ReferFriendActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == wallet_RL) {
            if (checkInternet) {
                Intent intent = new Intent(SettingActivity.this, WalletActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == logoutRL) {
            if (checkInternet) {

                showLogoutDialog(R.layout.warning_dialog);

                /*sweetAlertDialog = new SweetAlertDialog(SettingActivity.this, SweetAlertDialog.WARNING_TYPE);
                sweetAlertDialog.setTitleText("Are you sure?");
                sweetAlertDialog.setCancelText("No, Stay");
                sweetAlertDialog.setConfirmText("Yes, Log Out");
                sweetAlertDialog.showCancelButton(true);

                sweetAlertDialog.setConfirmClickListener(sweetAlertDialog -> {

                    sweetAlertDialog.cancel();
                    userSessionManager.logoutUser();

                    Intent intent = new Intent(SettingActivity.this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();

                });

                sweetAlertDialog.setCancelClickListener(sDialog -> sDialog.cancel());
                sweetAlertDialog.show();*/

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }

    private void showReferalDialog(int layout) {
        dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(SettingActivity.this);
        View layoutView = getLayoutInflater().inflate(layout, null);
        TextView wallet_title = layoutView.findViewById(R.id.wallet_title);
        wallet_title.setTypeface(bold);
        TextView wallet_txt = layoutView.findViewById(R.id.wallet_txt);
        wallet_txt.setTypeface(regular);

        if (wallet.equalsIgnoreCase("0")){

            wallet_txt.setText("Refer a friend and save money for both.");


        }else {

            wallet_txt.setText("Thank you for referring Gaadi360 app. \n You earned \u20B9 " + wallet + " so far.");

        }

        Button cancel_btn = layoutView.findViewById(R.id.cancel_btn);
        cancel_btn.setTypeface(bold);
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();

            }
        });

        Button share_btn = layoutView.findViewById(R.id.share_btn);
        share_btn.setTypeface(bold);
        share_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkInternet) {
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Gaadi360");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Hi, save \u20B9 " + referedAmount + " by getting your next bike service using Gaadi360 app at http://app.Gaadi360.com . Please enter my phone number " + userMobile + " as referral code when you register so I can save on my next bike service. Happy and safe riding!");
                    startActivity(Intent.createChooser(sharingIntent, "Share via"));
                } else {
                    Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }

                alertDialog.dismiss();

            }
        });

        dialogBuilder.setView(layoutView);
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        alertDialog.setCancelable(true);
    }

    private void showLogoutDialog(int layout) {
        dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(SettingActivity.this);
        View layoutView = getLayoutInflater().inflate(layout, null);
        TextView title_txt = layoutView.findViewById(R.id.title_txt);
        title_txt.setTypeface(bold);
        title_txt.setText("Are you sure?");
        TextView msg_txt = layoutView.findViewById(R.id.msg_txt);
        msg_txt.setVisibility(View.GONE);
        Button ok_btn = layoutView.findViewById(R.id.ok_btn);
        ok_btn.setTypeface(bold);
        ok_btn.setText("Yes, Log Out");
        Button cancel_btn = layoutView.findViewById(R.id.cancel_btn);
        cancel_btn.setTypeface(bold);
        cancel_btn.setText("No, Stay");
        dialogBuilder.setView(layoutView);
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();

        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialog.dismiss();

                userSessionManager.logoutUser();

                Intent intent = new Intent(SettingActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            }
        });

        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialog.dismiss();

            }
        });
        alertDialog.setCancelable(false);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(SettingActivity.this, MainActivity.class);
        intent.putExtra("activity", "SettingActivity");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
