package in.g360.owner.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import in.g360.owner.MainActivity;
import in.g360.owner.R;
import in.g360.owner.utils.AppUrls;
import in.g360.owner.utils.GlobalCalls;
import in.g360.owner.utils.NetworkChecking;
import in.g360.owner.utils.UserSessionManager;

public class MapActivity extends FragmentActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener, OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    UserSessionManager session;
    private boolean checkInternet;

    ImageView close;
    private TextView save_txt, textSearch;
    EditText name_edt, address_1_edt, address_2_edt, area_edt, city_edt, state_edt, country_edt, pincode_edt, mobile_edt, alternate_mob_edt;
    TextInputLayout name_til, address_one_til, address_two_til, pincode_til, mobile_til, alt_mobile_til, area_til, city_til, state_til, country_til;
    String deviceId, token, user_name, mobile, send_default_value, user_id = "100";
    RadioGroup rg_default;
    RadioButton yes_rb, no_rb;

    /*Get Location*/
    SupportMapFragment mapFragment;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private GoogleMap mMap;
    private GoogleMap.OnCameraIdleListener onCameraIdleListener;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    double latitute, longitiu;
    List<Address> addresses;
    Geocoder geocoder;
    String latitude1, longitude1, activity;
    LatLng latLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        checkInternet = NetworkChecking.isConnected(this);

        session = new UserSessionManager(this);
        HashMap<String, String> userDetails = session.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_name = userDetails.get(UserSessionManager.USER_NAME);
        mobile = userDetails.get(UserSessionManager.USER_MOBILE);

        close = findViewById(R.id.close);
        close.setOnClickListener(this);

        name_edt = findViewById(R.id.name_edt);
        address_1_edt = findViewById(R.id.address_1_edt);
        address_2_edt = findViewById(R.id.address_2_edt);
        area_edt = findViewById(R.id.area_edt);
        city_edt = findViewById(R.id.city_edt);
        state_edt = findViewById(R.id.state_edt);
        country_edt = findViewById(R.id.country_edt);
        pincode_edt = findViewById(R.id.pincode_edt);
        mobile_edt = findViewById(R.id.mobile_edt);
        alternate_mob_edt = findViewById(R.id.alternate_mob_edt);
        save_txt = findViewById(R.id.save_txt);
        save_txt.setOnClickListener(this);

        textSearch = findViewById(R.id.textSearch);
        textSearch.setOnClickListener(this);

        geocoder = new Geocoder(this, Locale.getDefault());
        name_til = findViewById(R.id.name_til);
        address_one_til = findViewById(R.id.address_one_til);
        address_two_til = findViewById(R.id.address_two_til);
        pincode_til = findViewById(R.id.pincode_til);
        mobile_til = findViewById(R.id.mobile_til);
        alt_mobile_til = findViewById(R.id.alt_mobile_til);
        area_til = findViewById(R.id.area_til);
        city_til = findViewById(R.id.city_til);
        state_til = findViewById(R.id.state_til);
        country_til = findViewById(R.id.country_til);

        rg_default = findViewById(R.id.rg_default);
        rg_default.setOnCheckedChangeListener(this);
        yes_rb = findViewById(R.id.yes_rb);
        no_rb = findViewById(R.id.no_rb);
        send_default_value = "Yes";

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        configureCameraIdle();
    }


    private void configureCameraIdle() {
        onCameraIdleListener = new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {

                latLng = mMap.getCameraPosition().target;
                Geocoder geocoder = new Geocoder(MapActivity.this);

                try {
                    List<Address> addressList = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                    if (addressList != null && addressList.size() > 0) {
                        String locality = addressList.get(0).getAddressLine(0);
                        String addr[] = locality.split(",", 2);
                        Log.d("LOClity", locality);
                        Log.d("ARYA", addr[0] + "////" + addr[1]);

                        String country = addressList.get(0).getCountryName();
                        String state = addressList.get(0).getAdminArea();
                        String city = addressList.get(0).getLocality();
                        String area = addressList.get(0).getSubLocality();
                        String pincode = addressList.get(0).getPostalCode();
                        latitute = addressList.get(0).getLatitude();
                        longitiu = addressList.get(0).getLongitude();
                        if (!locality.isEmpty() && !country.isEmpty())

                            name_edt.setText(user_name);
                        address_1_edt.setText(addr[0]);
                        address_2_edt.setText(addr[1]);
                        area_edt.setText(area);
                        city_edt.setText(city);
                        state_edt.setText(state);
                        country_edt.setText(country);
                        pincode_edt.setText(pincode);

                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        };
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
        mMap.setOnCameraIdleListener(onCameraIdleListener);

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));

        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {


        if (checkedId == R.id.yes_rb) {
            yes_rb.setChecked(true);
            send_default_value = "Yes";
        } else {
            no_rb.setChecked(true);
            send_default_value = "No";
        }
    }

    @Override
    public void onClick(View v) {

        if (v == close) {
            if (checkInternet) {
                Intent intent = new Intent(MapActivity.this, MainActivity.class);
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == textSearch) {
            try {
                Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                        .build(this);
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
            } catch (GooglePlayServicesRepairableException e) {

            } catch (GooglePlayServicesNotAvailableException e) {

            }
        }

        if (v == save_txt) {

            if (checkInternet) {
                if (validate()) {
                    String url = AppUrls.BASE_URL + AppUrls.ADDADDRESS + user_id + "/addresses";
                    Log.d("ADDSAVEURL", url);
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {

                                    Log.d("ADDRESPRESP", response);
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        String status = jsonObject.getString("status");
                                        String message = jsonObject.getString("message");
                                        if (status.equals("10100")) {
                                            GlobalCalls.showToast(message, MapActivity.this);

                                            assert activity != null;
                                            if (activity.equalsIgnoreCase("ChooseAddressAdapter")) {
                                                Intent intent = new Intent(MapActivity.this, MainActivity.class);
                                                intent.putExtra("activity", "Order");
                                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(intent);
                                            }

                                            if (activity.equalsIgnoreCase("PreOrderCartActivity")) {

                                                Intent intent = new Intent(MapActivity.this, MainActivity.class);
                                                intent.putExtra("activity", "PreOrderCartActivity");
                                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(intent);

                                            }

                                            if (activity.equalsIgnoreCase("MyCartActivity")) {

                                                Intent intent = new Intent(MapActivity.this, MainActivity.class);
                                                intent.putExtra("activity", "MyCartActivity");
                                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(intent);

                                            }

                                            if (activity.equalsIgnoreCase("ChooseAddress")) {

                                                Intent intent = new Intent(MapActivity.this, MainActivity.class);
                                                intent.putExtra("activity", "ChooseAddress");
                                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(intent);

                                            }


                                        }
                                        if (status.equals("10200")) {

                                            GlobalCalls.showToast(message, MapActivity.this);
                                        }
                                        if (status.equals("10300")) {

                                            GlobalCalls.showToast(message, MapActivity.this);
                                        }
                                        if (status.equals("10400")) {

                                            GlobalCalls.showToast(message, MapActivity.this);
                                        }
                                        if (status.equals("10500")) {

                                            GlobalCalls.showToast(message, MapActivity.this);
                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();

                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    error.getMessage();
                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("name", name_edt.getText().toString());
                            params.put("address_line1", address_1_edt.getText().toString());
                            params.put("address_line2", address_2_edt.getText().toString());
                            params.put("area", area_edt.getText().toString());
                            params.put("city", city_edt.getText().toString());
                            params.put("state", state_edt.getText().toString());
                            params.put("pincode", pincode_edt.getText().toString());
                            params.put("contact_no", mobile_edt.getText().toString());
                            params.put("alternate_contact_no", alternate_mob_edt.getText().toString());
                            params.put("is_default", send_default_value);
                            params.put("latitude", String.valueOf(latitute));
                            params.put("longitude", String.valueOf(longitiu));
                            Log.d("SAVEPARAM", params.toString());
                            return params;
                        }

                        @Override
                        public Map<String, String> getHeaders() {
                            Map<String, String> headers = new HashMap<>();
                            headers.put("Authorization-Basic", token);
                            headers.put("X-Deviceid", deviceId);
                            headers.put("x-device-platform", "ANDROID");
                            return headers;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    RequestQueue requestQueue = Volley.newRequestQueue(MapActivity.this);
                    requestQueue.add(stringRequest);
                }
            }

        }
    }

    public boolean validate() {
        boolean result = true;

        String name = name_edt.getText().toString();
        if (name.isEmpty() || name.equals("") || name.equals(null)) {
            name_til.setError("Please Enter Name");
            result = false;
        }

        String addline_one = address_1_edt.getText().toString();
        if (addline_one.isEmpty() || addline_one.equals("") || addline_one.equals(null)) {
            address_one_til.setError("Please Enter Address 1");
            result = false;
        }

        String addline_two = address_2_edt.getText().toString();
        if (addline_two.isEmpty() || addline_two.equals("") || addline_two.equals(null)) {
            address_two_til.setError("Please Enter Address 2");
            result = false;
        }

        String area = area_edt.getText().toString();
        if (area.isEmpty() || area.equals("") || area.equals(null)) {
            area_til.setError("Please Enter Area");
            result = false;
        }

        String city = city_edt.getText().toString();
        if (city.isEmpty() || city.equals("") || city.equals(null)) {
            city_til.setError("Please Enter City");
            result = false;
        }

        String state = state_edt.getText().toString();
        if (state.isEmpty() || state.equals("") || state.equals(null)) {
            state_til.setError("Please Enter State");
            result = false;
        }

        String country = country_edt.getText().toString();
        if (country.isEmpty() || country.equals("") || country.equals(null)) {
            country_til.setError("Please Enter Country");
            result = false;
        }

        String pincode = pincode_edt.getText().toString();
        if (pincode.isEmpty() || pincode.equals("") || pincode.equals(null) || pincode.length() != 6) {
            pincode_til.setError("Please Enter Pincode");
            result = false;
        }

        String mobile = mobile_edt.getText().toString();
        if (mobile.isEmpty() || mobile.equals("") || mobile.equals(null)) {
            mobile_til.setError("Please Enter Mobile");
            result = false;
        }
        return result;
    }

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.d("PPPPPP", "" + place.getAddress() + "---" + place.getLatLng());
                String stringlat = place.getLatLng().toString();
                CharSequence name = place.getName();
                final CharSequence address = place.getAddress();
                String attributions = (String) place.getAttributions();
                if (attributions == null) {
                    attributions = "";
                }
                if (name.toString().contains("°")) {
                    name = "";
                }
                stringlat = stringlat.substring(stringlat.indexOf("(") + 1);
                stringlat = stringlat.substring(0, stringlat.indexOf(")"));
                String latValue = stringlat.split(",")[0];
                latitude1 = latValue;
                String lngValue = stringlat.split(",")[1];
                longitude1 = lngValue;

                Double lat = Double.valueOf(latitude1);
                Double lng = Double.valueOf(longitude1);
                String fulladd = address.toString();
                String addr[] = fulladd.split(",", 2);
                if (name != null && name.length() > 0)
                    address_1_edt.setText(name);
                else
                    address_1_edt.setText(addr[0]);

                try {
                    addresses = geocoder.getFromLocation(lat, lng, 1);
                    Log.d("LOXXX", addresses.get(0).getAddressLine(0));
                    String addLine = addresses.get(0).getAddressLine(0);
                    String city = addresses.get(0).getLocality();
                    city_edt.setText(city);
                    String area = addresses.get(0).getSubLocality();
                    area_edt.setText(area);
                    String state = addresses.get(0).getAdminArea();
                    state_edt.setText(state);
                    String country = addresses.get(0).getCountryName();
                    country_edt.setText(country);

                    String postalCode = addresses.get(0).getPostalCode();
                    pincode_edt.setText(postalCode);
                    String addLineTwo = addresses.get(0).getThoroughfare();
                    String addLineOne = addresses.get(0).getFeatureName();

                    Log.d("ADDRESSES", addLine + "\n" + city + "\n" + state + "\n" + country + "\n" + postalCode + "\n" + address_1_edt);
                    Log.d("REMAININGADDRESS", addresses.get(0).getSubAdminArea() + "\n" + addresses.get(0).getSubLocality() + "\n" + addresses.get(0).getPremises() + "\n" + addresses.get(0).getSubThoroughfare()
                            + "\n" + addresses.get(0).getThoroughfare());
                    Log.v("FFFFFFF", "//" + address.toString());

                    if (mCurrLocationMarker != null) {
                        mCurrLocationMarker.remove();
                    }
                    latLng = new LatLng(Double.parseDouble(latitude1), Double.parseDouble(longitude1));
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(latLng);
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(19));

                    if (mGoogleApiClient != null) {
                        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }


            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                Log.i("PPPPPP", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {

            }
        }
    }
}
