package in.g360.owner.models;

public class BikeBrandModel
{
    public String id;
    public String bike_brand_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBike_brand_name() {
        return bike_brand_name;
    }

    public void setBike_brand_name(String bike_brand_name) {
        this.bike_brand_name = bike_brand_name;
    }
}
