package in.g360.owner.models;

public class BikeVideosModel {

    String id;
    String bikeVideo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBikeVideo() {
        return bikeVideo;
    }

    public void setBikeVideo(String bikeVideo) {
        this.bikeVideo = bikeVideo;
    }
}
