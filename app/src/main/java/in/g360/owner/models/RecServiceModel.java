package in.g360.owner.models;

import java.util.List;

public class RecServiceModel {

    private boolean isChecked = false;
    public String serviceId;
    public String serviceName;
    public String fileId;


    public List<ServiceDetailModel> serviceDetailModels;

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public List<ServiceDetailModel> getServiceDetailModels() {
        return serviceDetailModels;
    }

    public void setServiceDetailModels(List<ServiceDetailModel> serviceDetailModels) {
        this.serviceDetailModels = serviceDetailModels;
    }
}
