package in.g360.owner.models;

public class GeneralServiceInfoModel {

    public String perform;
    public String serviceItem;

    public String getPerform() {
        return perform;
    }

    public void setPerform(String perform) {
        this.perform = perform;
    }

    public String getServiceItem() {
        return serviceItem;
    }

    public void setServiceItem(String serviceItem) {
        this.serviceItem = serviceItem;
    }
}
