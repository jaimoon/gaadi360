package in.g360.owner.models;

public class FeaturedModel
{
    public int id;
    public String name;
    public String msg;

    public FeaturedModel( int id, String name,String msg) {

        this.id = id;
        this.name = name;
        this.msg = msg;
    }

}
