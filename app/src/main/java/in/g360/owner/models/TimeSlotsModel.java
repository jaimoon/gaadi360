package in.g360.owner.models;

public class TimeSlotsModel
{
    public String pickupScheduleId;
    public String pickupStartTime;
    public String pickupEndTime;
    public String displayName;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPickupScheduleId() {
        return pickupScheduleId;
    }

    public void setPickupScheduleId(String pickupScheduleId) {
        this.pickupScheduleId = pickupScheduleId;
    }

    public String getPickupStartTime() {
        return pickupStartTime;
    }

    public void setPickupStartTime(String pickupStartTime) {
        this.pickupStartTime = pickupStartTime;
    }

    public String getPickupEndTime() {
        return pickupEndTime;
    }

    public void setPickupEndTime(String pickupEndTime) {
        this.pickupEndTime = pickupEndTime;
    }
}
