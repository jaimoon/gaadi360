package in.g360.owner.models;

public class BikeKilometersModel
{
    public String kilometerRangeId;
    public String startRange;
    public String endRange;

    public String getKilometerRangeId() {
        return kilometerRangeId;
    }

    public void setKilometerRangeId(String kilometerRangeId) {
        this.kilometerRangeId = kilometerRangeId;
    }

    public String getStartRange() {
        return startRange;
    }

    public void setStartRange(String startRange) {
        this.startRange = startRange;
    }

    public String getEndRange() {
        return endRange;
    }

    public void setEndRange(String endRange) {
        this.endRange = endRange;
    }
}
