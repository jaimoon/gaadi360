package in.g360.owner.models;

public class SliderModel
{
    String id;
    String highlighted_text;
    String image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHighlighted_text() {
        return highlighted_text;
    }

    public void setHighlighted_text(String highlighted_text) {
        this.highlighted_text = highlighted_text;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
