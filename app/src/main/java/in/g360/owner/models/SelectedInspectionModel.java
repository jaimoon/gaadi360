package in.g360.owner.models;

public class SelectedInspectionModel {

    public String inspectionId;

    public String getInspectionId() {
        return inspectionId;
    }

    public void setInspectionId(String inspectionId) {
        this.inspectionId = inspectionId;
    }

}
