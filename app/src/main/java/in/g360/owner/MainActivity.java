package in.g360.owner;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.eightbitlab.bottomnavigationbar.BottomBarItem;
import com.eightbitlab.bottomnavigationbar.BottomNavigationBar;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import in.g360.owner.activities.HistoryActivity;
import in.g360.owner.activities.NotificationActivity;
import in.g360.owner.activities.ServicesListActivity;
import in.g360.owner.activities.SettingActivity;
import in.g360.owner.activities.TestActivity;
import in.g360.owner.adapters.BikeBrandAdapter;
import in.g360.owner.adapters.BikeKilometerAdapter;
import in.g360.owner.adapters.BikeModelAdapter;
import in.g360.owner.adapters.FeaturedAdapter;
import in.g360.owner.adapters.HistoryAdapter;
import in.g360.owner.adapters.OnGoingHistoryAdapter;
import in.g360.owner.adapters.RebookAdapter;
import in.g360.owner.adapters.TestimonialAdapter;
import in.g360.owner.adapters.TimeSlotAdapter;
import in.g360.owner.models.BikeBrandModel;
import in.g360.owner.models.BikeKilometersModel;
import in.g360.owner.models.BikeModelModel;
import in.g360.owner.models.FeaturedModel;
import in.g360.owner.models.HistoryModel;
import in.g360.owner.models.RebookModel;
import in.g360.owner.models.ServiceTypeImageModel;
import in.g360.owner.models.TimeSlotsModel;
import in.g360.owner.utils.AppUrls;
import in.g360.owner.utils.GlobalCalls;
import in.g360.owner.utils.GpsUtils;
import in.g360.owner.utils.NetworkChecking;
import in.g360.owner.utils.SliderAdapterExample;
import in.g360.owner.utils.SliderItem;
import in.g360.owner.utils.UserSessionManager;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private FirebaseAnalytics mFirebaseAnalytics;
    ImageView share_img, notification_img;
    RecyclerView rebook_recyclerview, featured_recyclerview, testimonial_recyclerview,history_recyclerview;
    SliderView sliderView;
    private SliderAdapterExample adapter;
    public String user_image;
    private boolean checkInternet;
    LinearLayout booking_detail_ll;
    TextView location_txt, bike_brand, bike_model, bike_kilometers, date, time, bike_details_txt,testmonials_txt;
    Button next_btn;
    FloatingActionButton fab;

    private final static int PLACE_PICKER_REQUEST = 999;
    private static final int REQUEST_PICK_PLACE = 2;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;

    UserSessionManager userSessionManager;
    String accessToken, userMobile, currentDate, currentTime;

    /*GetArea*/
    double latitude, longitude;
    public LocationManager locationManager;
    public Criteria criteria;
    public String bestProvider;
    Location location;
    Geocoder geocoder;

    SharedPreferences preferences, locationPref;
    SharedPreferences.Editor editor, locationEditor;

    /*ProgressDialog*/
    List<Integer> imageList = new ArrayList<Integer>();
    //FlipProgressDialog flipProgressDialog;

    /*Rebook*/
    RebookAdapter rebookAdapter;
    ArrayList<RebookModel> rebookModels = new ArrayList<RebookModel>();

    FeaturedAdapter featuredAdapter;
    TestimonialAdapter testimonialAdapter;

    /*BikeBrand*/
    BikeBrandAdapter bikeBrandAdapter;
    ArrayList<BikeBrandModel> bikeBrandModels = new ArrayList<BikeBrandModel>();

    /*Bike Model*/
    BikeModelAdapter bikeModelAdapter;
    ArrayList<BikeModelModel> bikeModelModels = new ArrayList<BikeModelModel>();

    /*Bike Kilometer*/
    BikeKilometerAdapter bikeKilometerAdapter;
    ArrayList<BikeKilometersModel> bikeKilometerModels = new ArrayList<BikeKilometersModel>();

    /*Time Slots*/
    TimeSlotAdapter timeSlotAdapter;
    ArrayList<TimeSlotsModel> timeSlotsModels = new ArrayList<TimeSlotsModel>();

    private Boolean exit = false;
    private int mYear, mMonth, mDay, hours, minutes, seconds;
    AlertDialog dialog, dialog2, dialog3, versionDialog;
    String activity, sendBikeBrand, sendBikeBrandId, sendBikeModel, sendBikeModelId, kilometerRangeId, sendstartRange,
            sendendRange, sendStartTime, sendEndTime, sendDisplayTime;
    Typeface regular, bold;

    private static final String[] INITIAL_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    private static final int INITIAL_REQUEST = 1337;

    boolean gps_enabled = false;
    boolean network_enabled = false;

    String brandId, modelId, bookingDate, bookingStartTime, bookingEndTime, bookingDisplayTime, brandName, modelName, kilometer, preBookingDays,
            pickupStartTime, locationName;

    /*Enable GPS Automatically*/
    private FusedLocationProviderClient mFusedLocationClient;
    private double wayLatitude = 0.0, wayLongitude = 0.0;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private android.widget.Button btnLocation;
    private TextView txtLocation;
    private android.widget.Button btnContinueLocation;
    private TextView txtContinueLocation;
    private StringBuilder stringBuilder;
    private boolean isContinue = false;
    private boolean isGPS = false;
    int version = 0;

    String id, initialPaymentAmount, adImageId, radius, sstShowCount, ratingInterval, cancellationHours, createdTime,
            modifiedTime, rescheduleCount, registrationAmount, referedAmount, walletDeductionPercentage;

    /*History*/
    OnGoingHistoryAdapter onGoingHistoryAdapter;
    ArrayList<HistoryModel> historyModels = new ArrayList<HistoryModel>();

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkLocationPermission();

        if (!canAccessLocation()) {
            requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
        }

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);
        userMobile = userDetails.get(UserSessionManager.USER_MOBILE);

        checkInternet = NetworkChecking.isConnected(this);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();

        brandId = preferences.getString("brandId", "");
        brandName = preferences.getString("brandName", "");
        modelId = preferences.getString("modelId", "");
        modelName = preferences.getString("modelName", "");
        kilometer = preferences.getString("kilometer", "");
        kilometerRangeId = preferences.getString("kilometerRangeId", "");
        bookingDate = preferences.getString("bookingDate", "");
        bookingStartTime = preferences.getString("bookingStartTime", "");
        bookingEndTime = preferences.getString("bookingEndTime", "");
        bookingDisplayTime = preferences.getString("bookingDisplayTime", "");

        Log.d("Preferences", brandId + "\n" + brandName + "\n" + modelId + "\n" + modelName + "\n" + kilometer + "\n" +
                kilometerRangeId + "\n" + bookingDate + "\n" + bookingStartTime + "\n" + bookingEndTime + "\n" + bookingDisplayTime);

        sendBikeBrandId = brandId;
        sendBikeBrand = brandName;
        sendBikeModelId = modelId;
        sendBikeModel = modelName;
        kilometerRangeId = kilometerRangeId;
        sendStartTime = bookingStartTime;
        sendEndTime = bookingEndTime;
        sendDisplayTime = bookingDisplayTime;

        locationPref = getSharedPreferences("Location", Context.MODE_PRIVATE);
        //locationPref = PreferenceManager.getDefaultSharedPreferences(this);
        locationEditor = locationPref.edit();

        activity = getIntent().getStringExtra("activity");

        /*imageList.add(R.mipmap.ic_launcher);
        flipProgressDialog = new FlipProgressDialog();
        flipProgressDialog.setImageList(imageList);
        flipProgressDialog.setCanceledOnTouchOutside(true);
        flipProgressDialog.setDimAmount(0.8f); //0.0f
        flipProgressDialog.setBackgroundColor(Color.parseColor("#00FFFFFF"));
        flipProgressDialog.setBackgroundAlpha(0.2f);
        flipProgressDialog.setBorderStroke(0);
        flipProgressDialog.setBorderColor(-1);
        flipProgressDialog.setCornerRadius(16);
        flipProgressDialog.setImageSize(200);
        flipProgressDialog.setImageMargin(10);
        flipProgressDialog.setOrientation("rotationY");
        flipProgressDialog.setDuration(600);
        flipProgressDialog.setStartAngle(0.0f);
        flipProgressDialog.setEndAngle(180.0f);
        flipProgressDialog.setMinAlpha(0.0f);
        flipProgressDialog.setMaxAlpha(1.0f);*/

        share_img = findViewById(R.id.share_img);
        share_img.setOnClickListener(this);
        notification_img = findViewById(R.id.notification_img);
        notification_img.setOnClickListener(this);

        location_txt = findViewById(R.id.location_txt);
        location_txt.setOnClickListener(this);
        location_txt.setTypeface(bold);

        sliderView = findViewById(R.id.imageSlider);

        adapter = new SliderAdapterExample(this);
        sliderView.setSliderAdapter(adapter);

        sliderView.setIndicatorAnimation(IndicatorAnimations.SLIDE);
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_RIGHT);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(4);
        sliderView.setAutoCycle(true);
        sliderView.startAutoCycle();

        testmonials_txt = findViewById(R.id.testmonials_txt);
        testmonials_txt.setTypeface(bold);
        bike_details_txt = findViewById(R.id.bike_details_txt);
        bike_details_txt.setTypeface(bold);
        booking_detail_ll = findViewById(R.id.booking_detail_ll);
        bike_brand = findViewById(R.id.bike_brand);
        bike_brand.setTypeface(bold);
        if (brandName.equalsIgnoreCase("") || brandName.equalsIgnoreCase("Brand")) {
            bike_brand.setBackgroundResource(R.drawable.rounded_border);
            bike_brand.setTextColor(ContextCompat.getColor(this, R.color.black));
            bike_brand.setText("Brand");
        } else {
            bike_brand.setBackgroundResource(R.drawable.selected_boarder);
            bike_brand.setTextColor(ContextCompat.getColor(this, R.color.white));
            bike_brand.setText(brandName);
        }
        bike_brand.setOnClickListener(this);
        bike_model = findViewById(R.id.bike_model);
        bike_model.setTypeface(bold);
        if (modelName.equalsIgnoreCase("") || modelName.equalsIgnoreCase("Model")) {
            bike_model.setBackgroundResource(R.drawable.rounded_border);
            bike_model.setTextColor(ContextCompat.getColor(this, R.color.black));
            bike_model.setText("Model");
        } else {
            bike_model.setBackgroundResource(R.drawable.selected_boarder);
            bike_model.setTextColor(ContextCompat.getColor(this, R.color.white));
            bike_model.setText(modelName);
        }
        bike_model.setOnClickListener(this);
        bike_kilometers = findViewById(R.id.bike_kilometers);
        bike_kilometers.setTypeface(bold);
        if (kilometer.equalsIgnoreCase("") || kilometer.equalsIgnoreCase("Kilometers")) {
            bike_kilometers.setBackgroundResource(R.drawable.rounded_border);
            bike_kilometers.setTextColor(ContextCompat.getColor(this, R.color.black));
            bike_kilometers.setText("Kilometers");
        } else {
            bike_kilometers.setBackgroundResource(R.drawable.selected_boarder);
            bike_kilometers.setTextColor(ContextCompat.getColor(this, R.color.white));
            bike_kilometers.setText(kilometer);
        }
        bike_kilometers.setOnClickListener(this);
        date = findViewById(R.id.date);
        date.setTypeface(bold);

        date.setOnClickListener(this);
        time = findViewById(R.id.time);
        time.setTypeface(bold);

        time.setOnClickListener(this);

        next_btn = findViewById(R.id.next_btn);
        next_btn.setTypeface(bold);
        next_btn.setOnClickListener(this);

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(this);

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds

        new GpsUtils(this).turnGPSOn(new GpsUtils.onGpsListener() {
            @Override
            public void gpsStatus(boolean isGPSEnable) {
                // turn on GPS
                isGPS = isGPSEnable;
            }
        });

        if (locationPref.getString("latitude", "").equalsIgnoreCase("")) {

        } else {
            latitude = Double.parseDouble(locationPref.getString("latitude", ""));

            locationEditor.putString("latitude", String.valueOf(latitude));
            locationEditor.apply();
        }

        if (locationPref.getString("longitude", "").equalsIgnoreCase("")) {

        } else {
            longitude = Double.parseDouble(locationPref.getString("longitude", ""));

            locationEditor.putString("longitude", String.valueOf(longitude));
            locationEditor.apply();
        }

        if (locationPref.getString("locationName", "").equalsIgnoreCase("")) {
            locationName = "";
        } else {
            locationName = locationPref.getString("locationName", "");

            locationEditor.putString("locationName", location_txt.getText().toString());
            locationEditor.apply();
        }

        if (locationName.equalsIgnoreCase("")) {
            location_txt.setText("Select Location");
        } else {
            location_txt.setText(locationName);

            locationEditor.putString("locationName", location_txt.getText().toString());
            locationEditor.apply();
        }

        history_recyclerview = findViewById(R.id.history_recyclerview);
        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(getApplicationContext());
        history_recyclerview.setLayoutManager(layoutManager2);
        onGoingHistoryAdapter = new OnGoingHistoryAdapter(historyModels, MainActivity.this, R.layout.row_ongoing_history);

        getHistory();

        rebook_recyclerview = findViewById(R.id.rebook_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        rebook_recyclerview.setLayoutManager(layoutManager);
        rebookAdapter = new RebookAdapter(rebookModels, MainActivity.this, R.layout.row_rebook);

        featured_recyclerview = findViewById(R.id.featured_recyclerview);
        ArrayList<FeaturedModel> data = new ArrayList<>();
        data.add(new FeaturedModel(R.drawable.test_img_one, "first","one"));
        data.add(new FeaturedModel(R.drawable.test_img_two, "second","two"));
        data.add(new FeaturedModel(R.drawable.test_img_three, "third","three"));
        data.add(new FeaturedModel(R.drawable.test_img_four, "fourth","four"));
        featured_recyclerview.setLayoutManager(new GridLayoutManager(this, 1, GridLayoutManager.HORIZONTAL, false));
        featuredAdapter = new FeaturedAdapter(MainActivity.this, data);
        featured_recyclerview.setAdapter(featuredAdapter);
        adapter.notifyDataSetChanged();

        testimonial_recyclerview = findViewById(R.id.testimonial_recyclerview);
        ArrayList<FeaturedModel> fm = new ArrayList<>();
        //fm.add(new FeaturedModel(R.drawable.img_one, "Kongari Naresh","Just wanted to drop Gaadi360 a note to say thanks for the great work getting my bike running smooth again. Great service, very friendly and very professional. I'd highly recommend Gaadi360's services and will be spreading the word with my network. See you for my next service, guys"));
        fm.add(new FeaturedModel(R.drawable.img_two, "J C Prasad Babu","The guys from Gaadi360 are fast, friendly and thoroughly professional. They picked my bike up even dropped it off at another location that I wanted all fixed and ready to go and all at a reasonable price! Highly Recommended ! Cheers and way to go!"));
        fm.add(new FeaturedModel(R.drawable.img_three, "M Srinivas Reddy","I happened to nootice a advert for bike service. Gave it a try and scheduled a bike service on the G360 app. Within an hour my bike was picked up from my home address and there after I was kept informed at every stage of the bikes assessment before any work was carried out, the bike was returned fully fixed and ready to go as arranged at my convenience, I even got a few tips on how to maintain all our bikes and all for a fantastic price."));
        fm.add(new FeaturedModel(R.drawable.img_four, "K B Reddy","Gave Gaadi360 a try for the first time, their time were quick to respond and picked up my bike from the office. I got my bike back in pristine shape, delivered back to my office, all the time while I was working. Fast, friendly and easy on the wallet ! Go on, give it a try today"));
        testimonial_recyclerview.setLayoutManager(new GridLayoutManager(this, 1, GridLayoutManager.HORIZONTAL, false));
        testimonialAdapter = new TestimonialAdapter(MainActivity.this, fm);
        testimonial_recyclerview.setAdapter(testimonialAdapter);
        adapter.notifyDataSetChanged();

        checkVersionUpdate();
        getOnGoingActivity();
        getRebookDetails();
        addBanners();

        showContent(0);
        setupBottomBar();

        currentDate = new SimpleDateFormat("d-M-yyyy", Locale.getDefault()).format(new Date());
        hours = new Time(System.currentTimeMillis()).getHours();
        minutes = new Time(System.currentTimeMillis()).getMinutes();
        seconds = new Time(System.currentTimeMillis()).getSeconds();
        currentTime = hours + ":" + minutes + ":" + seconds;

        Log.d("Current", hours + ":" + minutes + ":" + seconds + "\n" + currentDate);

        if (activity.equalsIgnoreCase("HistoryAdapter") || activity.equalsIgnoreCase("HistoryDetailActivity")) {

            String brandId = getIntent().getStringExtra("brandId");
            String modelId = getIntent().getStringExtra("modelId");
            String brandName = getIntent().getStringExtra("brandName");
            String modelName = getIntent().getStringExtra("modelName");

            getRebookData(brandId, brandName, modelId, modelName);

        }

        if (activity.equalsIgnoreCase("ServiceListActivity") || activity.equalsIgnoreCase("ConfirmationDetailActivity")) {

            String brandName = getIntent().getStringExtra("brandName");
            String modelName = getIntent().getStringExtra("modelName");
            String kilometer = getIntent().getStringExtra("kilometer");
            String bookingDate = getIntent().getStringExtra("bookingDate");
            String bookingStartTime = getIntent().getStringExtra("bookingStartTime");
            String bookingEndTime = getIntent().getStringExtra("bookingEndTime");
            String bookingDisplayTime = getIntent().getStringExtra("bookingDisplayTime");
            String brandId = getIntent().getStringExtra("brandId");
            String modelId = getIntent().getStringExtra("modelId");
            String kilometerRange = getIntent().getStringExtra("kilometerRangeId");

            bike_brand.setBackgroundResource(R.drawable.selected_boarder);
            bike_brand.setTextColor(ContextCompat.getColor(this, R.color.white));
            bike_brand.setText(brandName);

            bike_model.setBackgroundResource(R.drawable.selected_boarder);
            bike_model.setTextColor(ContextCompat.getColor(this, R.color.white));
            bike_model.setText(modelName);

            bike_kilometers.setBackgroundResource(R.drawable.selected_boarder);
            bike_kilometers.setTextColor(ContextCompat.getColor(this, R.color.white));
            bike_kilometers.setText(kilometer);

            date.setBackgroundResource(R.drawable.selected_boarder);
            date.setTextColor(ContextCompat.getColor(this, R.color.white));
            date.setText(bookingDate);
            if (bookingDisplayTime.equalsIgnoreCase("")) {
                time.setText("Booking Time");
            } else {
                time.setBackgroundResource(R.drawable.selected_boarder);
                time.setTextColor(ContextCompat.getColor(this, R.color.white));
                time.setText(bookingDisplayTime);
            }


            sendBikeBrandId = brandId;
            sendBikeModelId = modelId;
            kilometerRangeId = kilometerRange;
            sendStartTime = bookingStartTime;
            sendEndTime = bookingEndTime;
            sendDisplayTime = bookingDisplayTime;

        }

        if (activity.equalsIgnoreCase("TestActivity")) {

            String lat = getIntent().getStringExtra("latitude");
            String lng = getIntent().getStringExtra("longitude");
            String location = getIntent().getStringExtra("location");
            location_txt.setText(location);

            latitude = Double.parseDouble(lat);
            longitude = Double.parseDouble(lng);

            Log.d("LLLL", latitude + "\n" + longitude + "\n" + location);

            String brandName = getIntent().getStringExtra("brandName");
            String modelName = getIntent().getStringExtra("modelName");
            String kilometer = getIntent().getStringExtra("kilometer");
            String bookingDate = getIntent().getStringExtra("bookingDate");
            String bookingStartTime = getIntent().getStringExtra("bookingStartTime");
            String bookingEndTime = getIntent().getStringExtra("bookingEndTime");
            String bookingDisplayTime = getIntent().getStringExtra("bookingDisplayTime");
            String brandId = getIntent().getStringExtra("brandId");
            String modelId = getIntent().getStringExtra("modelId");
            String kilometerRange = getIntent().getStringExtra("kilometerRangeId");

            if (brandName.equalsIgnoreCase("")) {
                bike_brand.setBackgroundResource(R.drawable.rounded_border);
                bike_brand.setTextColor(ContextCompat.getColor(this, R.color.black));
                bike_brand.setText("Brand");
            } else {
                bike_brand.setBackgroundResource(R.drawable.selected_boarder);
                bike_brand.setTextColor(ContextCompat.getColor(this, R.color.white));
                bike_brand.setText(brandName);
            }

            if (modelName.equalsIgnoreCase("")) {
                bike_model.setBackgroundResource(R.drawable.rounded_border);
                bike_model.setTextColor(ContextCompat.getColor(this, R.color.black));
                bike_model.setText("Model");
            } else {
                bike_model.setBackgroundResource(R.drawable.selected_boarder);
                bike_model.setTextColor(ContextCompat.getColor(this, R.color.white));
                bike_model.setText(modelName);
            }

            if (kilometer.equalsIgnoreCase("")) {
                bike_kilometers.setBackgroundResource(R.drawable.rounded_border);
                bike_kilometers.setTextColor(ContextCompat.getColor(this, R.color.black));
                bike_kilometers.setText("Kilometers");
            } else {
                bike_kilometers.setBackgroundResource(R.drawable.selected_boarder);
                bike_kilometers.setTextColor(ContextCompat.getColor(this, R.color.white));
                bike_kilometers.setText(kilometer);
            }

            if (bookingDate.equalsIgnoreCase("")) {
                date.setBackgroundResource(R.drawable.rounded_border);
                date.setTextColor(ContextCompat.getColor(this, R.color.black));
                date.setText("Booking Date");
            } else {
                date.setBackgroundResource(R.drawable.selected_boarder);
                date.setTextColor(ContextCompat.getColor(this, R.color.white));
                date.setText(bookingDate);
            }

            if (bookingDisplayTime.equalsIgnoreCase("")) {
                time.setBackgroundResource(R.drawable.rounded_border);
                time.setTextColor(ContextCompat.getColor(this, R.color.black));
                time.setText("Booking Time");
            } else {
                time.setBackgroundResource(R.drawable.selected_boarder);
                time.setTextColor(ContextCompat.getColor(this, R.color.white));
                time.setText(bookingDisplayTime);
            }

            sendBikeBrandId = brandId;
            sendBikeModelId = modelId;
            kilometerRangeId = kilometerRange;
            sendStartTime = bookingStartTime;
            sendEndTime = bookingEndTime;
            sendDisplayTime = bookingDisplayTime;

        }

        //Logs an app event.
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ID-MainActivity");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "MainActivity");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
        mFirebaseAnalytics.logEvent("Homepage_PageLoad_New", bundle);

        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(20000);
        mFirebaseAnalytics.setSessionTimeoutDuration(500);
        mFirebaseAnalytics.setUserId(userMobile);
        mFirebaseAnalytics.setUserProperty("MainActivity", userMobile);

        GlobalCalls.events("Homepage_PageLoad_New",MainActivity.this);

        getReferalAmount();

    }

    private void getHistory() {

        String url = AppUrls.BASE_URL + AppUrls.HISTORY;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                historyModels.clear();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    if (status.equalsIgnoreCase("2000")) {

                        JSONArray jsonArray = jsonObject.getJSONArray("data");

                        if (jsonArray.length()!=0) {

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                HistoryModel rm = new HistoryModel();
                                rm.setBookingId(jsonObject1.getString("bookingId"));
                                rm.setBrandId(jsonObject1.getString("brandId"));
                                rm.setBrandName(jsonObject1.getString("brandName"));
                                rm.setModelId(jsonObject1.getString("modelId"));
                                rm.setModelName(jsonObject1.getString("modelName"));
                                rm.setRegistrationNumber(jsonObject1.getString("registrationNumber"));
                                rm.setBookingDate(jsonObject1.getString("bookingDate"));
                                rm.setBookingTime(jsonObject1.getString("bookingTime"));
                                rm.setFinalPrice(jsonObject1.getString("finalPrice"));
                                rm.setServices(jsonObject1.getString("services"));
                                rm.setPickupAddress(jsonObject1.getString("pickupAddress"));
                                rm.setStatus(jsonObject1.getString("status"));
                                rm.setServiceCenterName(jsonObject1.getString("serviceCenterName"));
                                rm.setPaymentStatus(jsonObject1.getString("paymentStatus"));
                                rm.setPromocodeAmount(jsonObject1.getString("promocodeAmount"));
                                rm.setPaidAmount(jsonObject1.getString("paidAmount"));

                                String req = jsonObject1.getString("status");

                                if ((!req.equalsIgnoreCase("7")) && (!req.equalsIgnoreCase("12"))) {

                                    historyModels.add(rm);

                                }
                            }

                            history_recyclerview.setAdapter(onGoingHistoryAdapter);
                            onGoingHistoryAdapter.notifyDataSetChanged();
                        }else {
                            history_recyclerview.setVisibility(View.GONE);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        requestQueue.add(stringRequest);

    }

    private void getReferalAmount() {

        String url = AppUrls.BASE_URL + AppUrls.REFERAL_AMOUNT_DETAIL;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    id = jsonObject.optString("id");
                    initialPaymentAmount = jsonObject.optString("initialPaymentAmount");
                    adImageId = jsonObject.optString("adImageId");
                    radius = jsonObject.optString("radius");
                    sstShowCount = jsonObject.optString("sstShowCount");
                    ratingInterval = jsonObject.optString("ratingInterval");
                    cancellationHours = jsonObject.optString("cancellationHours");
                    createdTime = jsonObject.optString("createdTime");
                    modifiedTime = jsonObject.optString("modifiedTime");
                    preBookingDays = jsonObject.optString("preBookingDays");
                    rescheduleCount = jsonObject.optString("rescheduleCount");
                    registrationAmount = jsonObject.optString("registrationAmount");
                    referedAmount = jsonObject.optString("referedAmount");
                    walletDeductionPercentage = jsonObject.optString("walletDeductionPercentage");

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        requestQueue.add(stringRequest);
    }

    private void checkVersionUpdate() {


        try {
            PackageInfo pInfo = MainActivity.this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionCode;

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        String url = AppUrls.BASE_URL + AppUrls.CHECK_VERSION;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String id = jsonObject.optString("id");
                    String platformId = jsonObject.optString("platformId");
                    int currentVersionCode = jsonObject.optInt("currentVersionCode");
                    int mandatoryUpdateVersionCode = jsonObject.optInt("mandatoryUpdateVersionCode");
                    String versionName = jsonObject.optString("versionName");
                    String mandatory = jsonObject.optString("mandatory");
                    String status = jsonObject.optString("status");
                    String createdTime = jsonObject.optString("createdTime");
                    String modifiedTime = jsonObject.optString("modifiedTime");

                    Log.d("VERSION", mandatoryUpdateVersionCode + "\n" + version);
                    if (mandatoryUpdateVersionCode > version) {
                        versionUpdateAlert();
                    } else {
                        if (currentVersionCode > version) {
                            optionalVersionUpdateAlert();
                        } else {
                            getOnGoingActivity();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                error -> {

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        GlobalCalls.showToast("Connection Not Found..!", MainActivity.this);

                    } else if (error instanceof AuthFailureError) {

                        GlobalCalls.showToast("User Not Found..!", MainActivity.this);

                    } else if (error instanceof ServerError) {

                        GlobalCalls.showToast("Server Not Found..!", MainActivity.this);


                    } else if (error instanceof NetworkError) {

                        GlobalCalls.showToast("Network Not Found..!", MainActivity.this);

                    } else if (error instanceof ParseError) {

                        GlobalCalls.showToast("Please Try Later..!", MainActivity.this);

                    }
                }
        );
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        requestQueue.add(stringRequest);

    }

    public void optionalVersionUpdateAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = MainActivity.this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.version_update_dialog, null);

        TextView title_txt = dialog_layout.findViewById(R.id.title_txt);
        title_txt.setTypeface(bold);
        TextView msg_txt = dialog_layout.findViewById(R.id.msg_txt);
        msg_txt.setTypeface(regular);
        Button update_btn = dialog_layout.findViewById(R.id.update_btn);
        update_btn.setTypeface(bold);

        update_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkInternet) {
                    final String appPackageName = getPackageName();
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                } else {
                    Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        });

        builder.setView(dialog_layout).setNegativeButton("CANCEL", (dialogInterface, i) -> {

            getOnGoingActivity();
            dialogInterface.dismiss();
        });
        versionDialog = builder.create();
        versionDialog.setCancelable(false);
        versionDialog.show();
    }

    public void versionUpdateAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = MainActivity.this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.version_update_dialog, null);

        TextView title_txt = dialog_layout.findViewById(R.id.title_txt);
        title_txt.setTypeface(bold);
        TextView msg_txt = dialog_layout.findViewById(R.id.msg_txt);
        msg_txt.setTypeface(regular);
        Button update_btn = dialog_layout.findViewById(R.id.update_btn);
        update_btn.setTypeface(bold);

        update_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkInternet) {
                    final String appPackageName = getPackageName();
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                } else {
                    Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        });

        builder.setView(dialog_layout);
        versionDialog = builder.create();
        versionDialog.setCancelable(false);
        versionDialog.show();
    }

    private void getOnGoingActivity() {

        String url = AppUrls.BASE_URL + AppUrls.CURRENT_STATUS;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String responceCode = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    Log.d("SPLASHRESP", response);

                    if (responceCode.equalsIgnoreCase("2000")) {

                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        String bookingId = jsonObject1.getString("bookingId");
                        String paymentStatus = jsonObject1.getString("paymentStatus");
                        String status = jsonObject1.getString("status");
                        String ongoingBooking = jsonObject1.getString("ongoingBooking");

                        JSONObject jsonObject2 = jsonObject1.getJSONObject("setting");
                        preBookingDays = jsonObject2.optString("preBookingDays");

                    } else {

                        GlobalCalls.showToast(message, MainActivity.this);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            GlobalCalls.showToast("Connection Not Found..!", MainActivity.this);

                        } else if (error instanceof AuthFailureError) {

                            GlobalCalls.showToast("User Not Found..!", MainActivity.this);

                        } else if (error instanceof ServerError) {

                            GlobalCalls.showToast("Server Not Found..!", MainActivity.this);


                        } else if (error instanceof NetworkError) {

                            GlobalCalls.showToast("Network Not Found..!", MainActivity.this);

                        } else if (error instanceof ParseError) {

                            GlobalCalls.showToast("Please Try Later..!", MainActivity.this);

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        requestQueue.add(stringRequest);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm));
    }

    private boolean checkLocationPermission() {

        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        List<String> listPermissionsNeeded = new ArrayList<>();

        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);

            return false;
        }
        return true;
    }

    private void getRebookDetails() {

        String url = AppUrls.BASE_URL + AppUrls.REBOOK;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                rebookModels.clear();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    if (status.equalsIgnoreCase("2000")) {

                        JSONArray jsonArray = jsonObject.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            RebookModel rm = new RebookModel();
                            rm.setBookingId(jsonObject1.getString("bookingId"));
                            rm.setBrandId(jsonObject1.getString("brandId"));
                            rm.setBrandName(jsonObject1.getString("brandName"));
                            rm.setModelId(jsonObject1.getString("modelId"));
                            rm.setModelName(jsonObject1.getString("modelName"));
                            rm.setRegistrationNumber(jsonObject1.getString("registrationNumber"));
                            rm.setBookingDate(jsonObject1.getString("bookingDate"));
                            rm.setBookingTime(jsonObject1.getString("bookingTime"));
                            rm.setFinalPrice(jsonObject1.getString("finalPrice"));
                            rm.setServices(jsonObject1.getString("services"));
                            rebookModels.add(rm);
                        }

                        rebook_recyclerview.setAdapter(rebookAdapter);
                        rebookAdapter.notifyDataSetChanged();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        requestQueue.add(stringRequest);

    }


    public void addBanners() {
        if (checkInternet) {
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.PROMOTION_SLIDERS,
                    response -> {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            //flipProgressDialog.show(getFragmentManager(), "");

                            String status = jsonObject.getString("status");

                            if (status.equals("2000")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                //flipProgressDialog.dismiss();

                                List<SliderItem> sliderItemList = new ArrayList<>();

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    ServiceTypeImageModel slider_model = new ServiceTypeImageModel();
                                    slider_model.setDesc(jsonObject1.optString("desc"));
                                    slider_model.setFileId(AppUrls.IMAGE_URL + jsonObject1.getString("fileId"));
                                    SliderItem sliderItem = new SliderItem();
                                    user_image = AppUrls.IMAGE_URL + "" + jsonObject1.getString("fileId");
                                    sliderItem.setImageUrl(user_image);
                                    //sliderItem.setImageScaleType(ImageView.ScaleType.FIT_XY);
                                    sliderItem.setDescription(jsonObject1.optString("desc"));

                                    /*final int finalI = i;*/

                                    sliderItemList.add(sliderItem);
                                }

                                adapter.renewItems(sliderItemList);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    },
                    error -> error.getMessage());

            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
            requestQueue.add(stringRequest);

        } else {

            GlobalCalls.showToast("Check Internet Connection..!", MainActivity.this);

        }

    }

    public void getRebookData(String brandId, String brandName, String modelId, String modelName) {

        fab.setVisibility(View.GONE);
        booking_detail_ll.setVisibility(View.VISIBLE);

        sendBikeBrandId = brandId;
        sendBikeModelId = modelId;
        bike_brand.setBackgroundResource(R.drawable.selected_boarder);
        bike_brand.setTextColor(ContextCompat.getColor(this, R.color.white));
        bike_brand.setText(brandName);
        bike_model.setBackgroundResource(R.drawable.selected_boarder);
        bike_model.setTextColor(ContextCompat.getColor(this, R.color.white));
        bike_model.setText(modelName);

        getBikeKilometers();

    }

    @Override
    public void onClick(View v) {

        if (v == share_img) {

            if (checkInternet) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Gaadi360");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Hi, save \u20B9 " + registrationAmount + " by getting your next bike service using Gaadi360 app at http://app.Gaadi360.com . Please enter my phone number " + userMobile + " as referral code when you register so I can save on my next bike service. Happy and safe riding!");
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }

        }

        if (v == notification_img) {

            if (checkInternet) {
                Intent intent = new Intent(MainActivity.this, NotificationActivity.class);
                startActivity(intent);
                finish();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }

        }

        if (v == location_txt) {

            if (checkInternet) {

                checkLocationPermission();

                if (gps_enabled) {

                    new GpsUtils(this).turnGPSOn(new GpsUtils.onGpsListener() {
                        @Override
                        public void gpsStatus(boolean isGPSEnable) {
                            // turn on GPS
                            isGPS = isGPSEnable;
                        }
                    });

                } else {
                    Intent intent = new Intent(MainActivity.this, TestActivity.class);
                    intent.putExtra("activity", "MainActivity");
                    startActivity(intent);
                }


                /*PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    // for activty
                    startActivityForResult(builder.build(MainActivity.this), PLACE_PICKER_REQUEST);
                    // for fragment
                    //startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }*/

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }

        }

        if (v == fab) {
            if (checkInternet) {

                bike_model.setText("Model");
                getBikeBrands();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == bike_brand) {
            if (checkInternet) {

                bike_model.setBackgroundResource(R.drawable.rounded_border);
                bike_model.setTextColor(ContextCompat.getColor(this, R.color.black));
                bike_model.setText("Model");
                getBikeBrands();


            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == bike_model) {
            if (checkInternet) {
                String bikeBrand = bike_brand.getText().toString().trim();
                if (!bikeBrand.equals("Brand")) {

                    getBikeModels(sendBikeBrandId);


                } else {

                    GlobalCalls.showToast("Select Bike Brand", MainActivity.this);

                }

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == bike_kilometers) {
            if (checkInternet) {

                String bikeModel = bike_model.getText().toString().trim();
                if (!bikeModel.equals("Model")) {

                    getBikeKilometers();


                } else {

                    GlobalCalls.showToast("Select Bike Model", MainActivity.this);
                }

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == date) {
            if (checkInternet) {

                if (bike_kilometers.getText().toString().equalsIgnoreCase("Kilometers")) {

                    GlobalCalls.showToast("Select Kilometers", MainActivity.this);

                } else {
                    checkTimeSlots();

                }

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == time) {
            if (checkInternet) {

                if (date.getText().toString().equalsIgnoreCase("Booking Date")) {

                    GlobalCalls.showToast("Select Booking Date", MainActivity.this);

                } else {

                    getTimeSlots();

                }

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == next_btn) {
            if (checkInternet) {

                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ID-BookingDetailSubmit");
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "BookingDetailSubmit");
                bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
                mFirebaseAnalytics.logEvent("Homepage_Event_BookingDetailSubmit_New", bundle);

                GlobalCalls.events("Homepage_Event_BookingDetailSubmit_New",MainActivity.this);

                locationEditor.putString("latitude", String.valueOf(latitude));
                locationEditor.putString("longitude", String.valueOf(longitude));
                locationEditor.putString("locationName", location_txt.getText().toString());
                locationEditor.apply();

                editor.putString("brandId", sendBikeBrandId);
                editor.putString("brandName", bike_brand.getText().toString());
                editor.putString("modelId", sendBikeModelId);
                editor.putString("modelName", bike_model.getText().toString());
                editor.putString("kilometerRangeId", kilometerRangeId);
                editor.putString("kilometer", bike_kilometers.getText().toString());
                editor.putString("bookingDate", date.getText().toString());
                editor.putString("bookingStartTime", sendStartTime);
                editor.putString("bookingEndTime", sendEndTime);
                editor.putString("bookingDisplayTime", sendDisplayTime);
                editor.apply();

                Log.d("SendLATLNG", "" + latitude + "\n" + longitude);

                if (bike_brand.getText().toString().equalsIgnoreCase("Brand") ||
                        bike_model.getText().toString().equalsIgnoreCase("Model") ||
                        bike_kilometers.getText().toString().equalsIgnoreCase("Kilometers") ||
                        date.getText().toString().equalsIgnoreCase("Booking Date") ||
                        time.getText().toString().equalsIgnoreCase("Booking Time")) {

                    GlobalCalls.showToast("Please Select All Details..!", MainActivity.this);

                } else {
                    if (latitude == 0.0 || longitude == 0.0) {
                        checkLocationPermission();

                        if (!isGPS) {
                            Toast.makeText(this, "Please turn on GPS", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        isContinue = false;

                        Intent intent = new Intent(MainActivity.this, TestActivity.class);
                        intent.putExtra("activity", "MainActivity2");
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(MainActivity.this, ServicesListActivity.class);
                        startActivity(intent);

                    }
                }

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

    }

    private void getBikeBrands() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            bikeBrandModels.clear();
            //flipProgressDialog.show(getFragmentManager(), "");
            String url = AppUrls.BASE_URL + AppUrls.BIKE_BRANDS;

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    response -> {

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");
                            if (status.equals("2000")) {
                                //flipProgressDialog.dismiss();

                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                    BikeBrandModel bikeBrandModel = new BikeBrandModel();
                                    bikeBrandModel.setId(jsonObject1.getString("brandId"));
                                    String category_name = jsonObject1.getString("brandName");
                                    bikeBrandModel.setBike_brand_name(category_name);

                                    bikeBrandModels.add(bikeBrandModel);
                                }

                                bikeBrandDialog();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    },
                    error -> {

                    }) {
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
            requestQueue.add(stringRequest);

        } else {

            GlobalCalls.showToast("Please Check Internet Connection..!", MainActivity.this);

        }

    }

    private void bikeBrandDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = MainActivity.this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.bike_brand_dialog, null);

        TextView bike_brand_title = dialog_layout.findViewById(R.id.bike_brand_title);
        bike_brand_title.setTypeface(regular);

        final SearchView bike_brand_search = dialog_layout.findViewById(R.id.bike_brand_search);
        EditText searchEditText = bike_brand_search.findViewById(androidx.appcompat.R.id.search_src_text);

        RecyclerView bike_brand_recyclerview = dialog_layout.findViewById(R.id.bike_brand_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        bike_brand_recyclerview.setLayoutManager(layoutManager);

        bikeBrandAdapter = new BikeBrandAdapter(bikeBrandModels, MainActivity.this, R.layout.row_bike_brand);
        bike_brand_recyclerview.setAdapter(bikeBrandAdapter);

        bike_brand_search.setOnClickListener(v -> bike_brand_search.setIconified(false));

        builder.setView(dialog_layout).setNegativeButton("CANCEL", (dialogInterface, i) -> {
            bike_brand.setBackgroundResource(R.drawable.rounded_border);
            bike_brand.setTextColor(ContextCompat.getColor(this, R.color.black));
            bike_brand.setText("Brand");
            dialogInterface.dismiss();
        });

        dialog = builder.create();

        bike_brand_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                bikeBrandAdapter.getFilter().filter(query);
                return false;
            }
        });
        dialog.show();
    }

    public void setBikeBrandName(String bikeBrandName, String id) {

        dialog.dismiss();
        bikeBrandAdapter.notifyDataSetChanged();
        sendBikeBrand = bikeBrandName;
        sendBikeBrandId = id;

        /*bike_brand.setBackgroundResource(R.drawable.rounded_border);
        bike_brand.setTextColor(Color.BLACK);*/
        bike_brand.setBackgroundResource(R.drawable.selected_boarder);
        bike_brand.setTextColor(ContextCompat.getColor(this, R.color.white));
        bike_brand.setText(sendBikeBrand);

        fab.setVisibility(View.GONE);
        booking_detail_ll.setVisibility(View.VISIBLE);

        editor.putString("brandId", sendBikeBrandId);
        editor.putString("brandName", sendBikeBrand);
        editor.apply();

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ID-Bikebrand");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, bike_brand.getText().toString().trim());
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
        mFirebaseAnalytics.logEvent("Homepage_Event_Bikebrand_New", bundle);

        GlobalCalls.events("Homepage_Event_Bikebrand_New",MainActivity.this);

        getBikeModels(id);

        Log.d("BIKEBRANDDETAILS:", sendBikeBrand + "::::" + sendBikeBrandId);
    }

    private void getBikeModels(String id) {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {

            bikeModelModels.clear();
            //flipProgressDialog.show(getFragmentManager(), "");

            String url = AppUrls.BASE_URL + AppUrls.BIKE_MODELS + id;

            StringRequest string_request = new StringRequest(Request.Method.GET, url,
                    response -> {
                        //flipProgressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String responceCode = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            if (responceCode.equals("2000")) {
                                //flipProgressDialog.dismiss();

                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    BikeModelModel bmm = new BikeModelModel();
                                    String modelId = jsonObject1.getString("modelId");
                                    String brandId = jsonObject1.getString("brandId");
                                    String modelName = jsonObject1.getString("modelName");
                                    bmm.setModelId(modelId);
                                    bmm.setBrandId(brandId);
                                    bmm.setModelName(modelName);

                                    bikeModelModels.add(bmm);
                                }

                                bikeModelDialog();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }, error -> {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(string_request);
        } else {

            GlobalCalls.showToast("Please Check Internet Connection..!", MainActivity.this);

        }

    }

    private void bikeModelDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = MainActivity.this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.bike_model_dialog, null);

        TextView bike_model_title = dialog_layout.findViewById(R.id.bike_model_title);
        bike_model_title.setText(sendBikeBrand + " Models");

        final SearchView bike_model_search = dialog_layout.findViewById(R.id.bike_model_search);
        EditText searchEditText = bike_model_search.findViewById(androidx.appcompat.R.id.search_src_text);

        RecyclerView bike_model_recyclerview = dialog_layout.findViewById(R.id.bike_model_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        bike_model_recyclerview.setLayoutManager(layoutManager);

        bikeModelAdapter = new BikeModelAdapter(bikeModelModels, MainActivity.this, R.layout.row_bike_model);

        bike_model_recyclerview.setAdapter(bikeModelAdapter);
        bike_model_search.setOnClickListener(v -> bike_model_search.setIconified(false));

        builder.setView(dialog_layout).setNegativeButton("Done", (dialogInterface, i) -> {
            //progressDialog.dismiss();
            dialogInterface.dismiss();
        });

        dialog2 = builder.create();

        bike_model_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                bikeModelAdapter.getFilter().filter(query);
                return false;
            }
        });
        dialog2.show();

    }

    public void setBikeModelName(String bikeModelName, String id) {

        dialog2.dismiss();
        bikeModelAdapter.notifyDataSetChanged();
        sendBikeModel = bikeModelName;
        sendBikeModelId = id;

        bike_model.setBackgroundResource(R.drawable.selected_boarder);
        bike_model.setTextColor(ContextCompat.getColor(this, R.color.white));
        bike_model.setText(sendBikeModel);

        editor.putString("modelId", sendBikeModelId);
        editor.putString("modelName", sendBikeModel);
        editor.apply();

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ID-Bikemodel");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, bike_model.getText().toString().trim());
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
        mFirebaseAnalytics.logEvent("Homepage_Event_Bikemodel_New", bundle);

        GlobalCalls.events("Homepage_Event_Bikemodel_New",MainActivity.this);

        getBikeKilometers();

        Log.d("BIKEMODELDETAIL:", sendBikeModel + "::::" + sendBikeModelId);
    }

    private void getBikeKilometers() {

        checkInternet = NetworkChecking.isConnected(getApplicationContext());

        if (checkInternet) {
            //flipProgressDialog.show(getFragmentManager(), "");
            String url = AppUrls.BASE_URL + AppUrls.BIKE_KILOMETERS;
            StringRequest string_request = new StringRequest(Request.Method.GET, url,
                    response -> {

                        //flipProgressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String responceCode = jsonObject.getString("status");
                            String Success = jsonObject.getString("message");

                            if (responceCode.equals("2000")) {
                                //flipProgressDialog.dismiss();

                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    BikeKilometersModel bkm = new BikeKilometersModel();
                                    kilometerRangeId = jsonObject1.getString("kilometerRangeId");
                                    String startRange = jsonObject1.getString("startRange");
                                    String endRange = jsonObject1.getString("endRange");

                                    bkm.setKilometerRangeId(kilometerRangeId);
                                    bkm.setStartRange(startRange);
                                    bkm.setEndRange(endRange);

                                    bikeKilometerModels.add(bkm);
                                }

                                //if (bike_kilometers.getText().toString().equalsIgnoreCase("Kilometers")) {
                                bikeKilometerDialog();
                                //}

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }, error -> {

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(string_request);

        } else {

            GlobalCalls.showToast("Please Check Internet Connection..!", MainActivity.this);
        }

    }

    private void bikeKilometerDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = MainActivity.this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.bike_kilometer_dialog, null);

        TextView bike_kilometer_title = dialog_layout.findViewById(R.id.bike_kilometer_title);

        final SearchView bike_kilometer_search = dialog_layout.findViewById(R.id.bike_kilometer_search);
        EditText searchEditText = bike_kilometer_search.findViewById(androidx.appcompat.R.id.search_src_text);

        RecyclerView bike_kilometer_recyclerview = dialog_layout.findViewById(R.id.bike_kilometer_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        bike_kilometer_recyclerview.setLayoutManager(layoutManager);

        bikeKilometerAdapter = new BikeKilometerAdapter(bikeKilometerModels, MainActivity.this, R.layout.row_bike_kilometer);

        bike_kilometer_recyclerview.setAdapter(bikeKilometerAdapter);
        bike_kilometer_search.setOnClickListener(v -> bike_kilometer_search.setIconified(false));

        builder.setView(dialog_layout).setNegativeButton("Done", (dialogInterface, i) -> {
            //progressDialog.dismiss();
            dialogInterface.dismiss();
        });

        dialog3 = builder.create();

        bike_kilometer_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                bikeKilometerAdapter.getFilter().filter(query);
                return false;
            }
        });
        dialog3.show();

    }

    public void setBikeKilometerName(String rangeId, String startRange, String endRange) {

        dialog3.dismiss();
        bikeKilometerAdapter.notifyDataSetChanged();
        sendstartRange = startRange;
        sendendRange = endRange;
        kilometerRangeId = rangeId;

        bike_kilometers.setBackgroundResource(R.drawable.selected_boarder);
        bike_kilometers.setTextColor(ContextCompat.getColor(this, R.color.white));
        bike_kilometers.setText(sendstartRange + " - " + sendendRange);

        editor.putString("kilometerRangeId", kilometerRangeId);
        editor.putString("kilometer", bike_kilometers.getText().toString());
        editor.apply();

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ID-Bikekilometers");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, bike_kilometers.getText().toString().trim());
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
        mFirebaseAnalytics.logEvent("Homepage_Event_Bikekilometers_New", bundle);

        GlobalCalls.events("Homepage_Event_Bikekilometers_New",MainActivity.this);

        checkTimeSlots();


        Log.d("BIKEMODELDETAIL:", sendBikeModel + "::::" + sendBikeModelId);
    }

    private void checkTimeSlots() {

        checkInternet = NetworkChecking.isConnected(getApplicationContext());

        if (checkInternet) {

            timeSlotsModels.clear();

            String url = AppUrls.BASE_URL + AppUrls.TIME_SLOTS;

            StringRequest string_request = new StringRequest(Request.Method.GET, url,
                    response -> {

                        Log.d("TIMESLOTRESPONCE", response);

                        SimpleDateFormat sdfStopTime = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
                        Calendar now = Calendar.getInstance();
                        now.add(Calendar.HOUR, 1);
                        Date d = now.getTime();
                        String afterime = sdfStopTime.format(d);
                        Log.d("CCHCH", afterime);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String responceCode = jsonObject.getString("status");

                            if (responceCode.equals("2000")) {

                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    TimeSlotsModel tsm = new TimeSlotsModel();
                                    String pickupScheduleId = jsonObject1.getString("pickupScheduleId");
                                    pickupStartTime = jsonObject1.getString("pickupStartTime");
                                    String pickupEndTime = jsonObject1.getString("pickupEndTime");
                                    String displayName = jsonObject1.getString("displayName");

                                    tsm.setPickupScheduleId(pickupScheduleId);
                                    tsm.setPickupStartTime(pickupStartTime);
                                    tsm.setPickupEndTime(pickupEndTime);
                                    tsm.setDisplayName(displayName);

                                    if (date.getText().toString().equals(currentDate)) {
                                        if (checkTimeslots(pickupStartTime, afterime)) {
                                            Log.d("Times", pickupStartTime + "\n" + pickupEndTime + "\n" + afterime);
                                            timeSlotsModels.add(tsm);
                                        }
                                    } else {
                                        timeSlotsModels.add(tsm);
                                    }
                                }

                                if (timeSlotsModels.size() != 0) {

                                    //time.setText("Booking Time");

                                    getDate();

                                } else {

                                    getTomarrowDate();
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }, error -> {

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(string_request);

        } else {

            GlobalCalls.showToast("Please Check Internet Connection..!", MainActivity.this);
        }

    }

    public void getDate() {

        SimpleDateFormat sdfStopTime = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
        Calendar now = Calendar.getInstance();
        now.add(Calendar.HOUR, 1);
        Date d = now.getTime();
        String afterime = sdfStopTime.format(d);
        Log.d("CCHCH", afterime);

        final Calendar calendarMax = Calendar.getInstance();
        calendarMax.add(Calendar.DAY_OF_MONTH, Integer.parseInt(preBookingDays));
        final Calendar calendarToday = Calendar.getInstance();

        if (checkTimeslots(pickupStartTime, afterime)) {

            Log.d("CheckTimes", "IN");


        } else {

            calendarToday.add(Calendar.DAY_OF_MONTH, 1);
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        String strDate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;

                        Log.d("Day", String.valueOf(dayOfMonth));
                        Log.d("Month", String.valueOf(monthOfYear + 1));
                        Log.d("Year", String.valueOf(year));

                        date.setBackgroundResource(R.drawable.selected_boarder);
                        date.setTextColor(Color.parseColor("#FFFFFF"));
                        date.setText(strDate);

                        editor.putString("bookingDate", date.getText().toString());
                        editor.apply();

                        Bundle bundle = new Bundle();
                        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ID-Bookingdate");
                        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, date.getText().toString().trim());
                        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
                        mFirebaseAnalytics.logEvent("Homepage_Event_Bookingdate_New", bundle);

                        GlobalCalls.events("Homepage_Event_Bookingdate_New",MainActivity.this);

                        time.setBackgroundResource(R.drawable.rounded_border);
                        time.setTextColor(Color.parseColor("#000000"));
                        time.setText("Booking Time");
                        if (time.getText().toString().equalsIgnoreCase("Booking Time")) {
                            getTimeSlots();
                        }

                    }

                }, mYear, mMonth, mDay);

        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {


            }
        });

        //datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.getDatePicker().setMinDate(calendarToday.getTimeInMillis());
        datePickerDialog.getDatePicker().setMaxDate(calendarMax.getTimeInMillis());
        datePickerDialog.show();
    }

    public void getTomarrowDate() {

        final Calendar calendarMax = Calendar.getInstance();
        calendarMax.add(Calendar.DAY_OF_MONTH, Integer.parseInt(preBookingDays));
        final Calendar calendarToday = Calendar.getInstance();
        calendarToday.add(Calendar.DAY_OF_MONTH, 1);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        String strDate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;

                        Log.d("Day", String.valueOf(dayOfMonth));
                        Log.d("Month", String.valueOf(monthOfYear + 1));
                        Log.d("Year", String.valueOf(year));

                        date.setBackgroundResource(R.drawable.selected_boarder);
                        date.setTextColor(Color.parseColor("#FFFFFF"));
                        date.setText(strDate);

                        editor.putString("bookingDate", date.getText().toString());
                        editor.apply();

                        Bundle bundle = new Bundle();
                        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ID-Bookingdate");
                        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, date.getText().toString().trim());
                        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
                        mFirebaseAnalytics.logEvent("Homepage_Event_Bookingdate_New", bundle);

                        GlobalCalls.events("Homepage_Event_Bookingdate_New",MainActivity.this);

                        time.setBackgroundResource(R.drawable.rounded_border);
                        time.setTextColor(Color.parseColor("#000000"));
                        time.setText("Booking Time");
                        if (time.getText().toString().equalsIgnoreCase("Booking Time")) {
                            getTimeSlots();
                        }

                    }

                }, mYear, mMonth, mDay);

        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {


            }
        });

        datePickerDialog.getDatePicker().setMinDate(calendarToday.getTimeInMillis());
        datePickerDialog.getDatePicker().setMaxDate(calendarMax.getTimeInMillis());
        datePickerDialog.show();
    }

    private void getTimeSlots() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {

            timeSlotsModels.clear();
            //flipProgressDialog.show(getFragmentManager(), "");

            String url = AppUrls.BASE_URL + AppUrls.TIME_SLOTS;

            StringRequest string_request = new StringRequest(Request.Method.GET, url,
                    response -> {
                        //flipProgressDialog.dismiss();

                        Log.d("TIMESLOTRESPONCE", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String responceCode = jsonObject.getString("status");

                            if (responceCode.equals("2000")) {
                                //flipProgressDialog.dismiss();

                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    TimeSlotsModel tsm = new TimeSlotsModel();
                                    String pickupScheduleId = jsonObject1.getString("pickupScheduleId");
                                    String pickupStartTime = jsonObject1.getString("pickupStartTime");
                                    String pickupEndTime = jsonObject1.getString("pickupEndTime");
                                    String displayName = jsonObject1.getString("displayName");

                                    tsm.setPickupScheduleId(pickupScheduleId);
                                    tsm.setPickupStartTime(pickupStartTime);
                                    tsm.setPickupEndTime(pickupEndTime);
                                    tsm.setDisplayName(displayName);

                                    /*if (date.getText().toString().equals(currentDate)) {
                                        if (checkTimeslots(pickupStartTime, currentTime)) {
                                            Log.d("Times", pickupStartTime + "\n" + pickupEndTime + "\n" + currentTime);
                                            timeSlotsModels.add(tsm);
                                        }
                                    } else {
                                        timeSlotsModels.add(tsm);
                                    }*/

                                    SimpleDateFormat sdfStopTime = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
                                    Calendar now = Calendar.getInstance();
                                    now.add(Calendar.HOUR, 1);
                                    Date d = now.getTime();
                                    String afterime = sdfStopTime.format(d);
                                    Log.d("CCHCH", afterime);

                                    if (date.getText().toString().equals(currentDate)) {
                                        if (checkTimeslots(pickupStartTime, afterime)) {
                                            Log.d("Times", pickupStartTime + "\n" + pickupEndTime + "\n" + afterime);
                                            timeSlotsModels.add(tsm);
                                        }
                                    } else {
                                        timeSlotsModels.add(tsm);
                                    }
                                }

                                if (timeSlotsModels.size() != 0) {
                                    setTimeSlot(timeSlotsModels.get(0).getPickupStartTime(), timeSlotsModels.get(0).getPickupEndTime(),
                                            timeSlotsModels.get(0).getDisplayName());
                                    timeSlotDialog();

                                } else {

                                    GlobalCalls.showToast("No TimeSlots Available..!", MainActivity.this);

                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }, error -> {

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(string_request);

        } else {

            GlobalCalls.showToast("No Internet Connection..!", MainActivity.this);

        }

    }

    private static boolean checkTimeslots(String startTime, String currentTime) {
        String pattern = "HH:mm:ss";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        try {
            Date date1 = sdf.parse(startTime);
            Date date2 = sdf.parse(currentTime);
            if (date1.before(date2)) {
                return false;
            } else {
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void timeSlotDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = MainActivity.this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.timeslot_dialog, null);

        RecyclerView timeslot_recyclerview = dialog_layout.findViewById(R.id.timeslot_recyclerview);
        GridLayoutManager layoutManager = new GridLayoutManager(MainActivity.this, 2);
        layoutManager.setOrientation(GridLayoutManager.VERTICAL);
        timeslot_recyclerview.setLayoutManager(layoutManager);
        timeSlotAdapter = new TimeSlotAdapter(timeSlotsModels, MainActivity.this, R.layout.row_time_slots);
        timeslot_recyclerview.setAdapter(timeSlotAdapter);

        builder.setView(dialog_layout).setNegativeButton("Done", (dialogInterface, i) -> {

            dialogInterface.dismiss();
        });

        dialog2 = builder.create();
        dialog2.show();

    }

    public void setTimeSlot(String startTime, String endTime, String displayTime) {

        sendStartTime = startTime;
        sendEndTime = endTime;
        sendDisplayTime = displayTime;

        time.setBackgroundResource(R.drawable.selected_boarder);
        time.setTextColor(ContextCompat.getColor(this, R.color.white));
        time.setText(displayTime);

        editor.putString("bookingStartTime", sendStartTime);
        editor.putString("bookingEndTime", sendEndTime);
        editor.putString("bookingDisplayTime", time.getText().toString());
        editor.apply();

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ID-Bookingtime");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, time.getText().toString().trim());
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
        mFirebaseAnalytics.logEvent("Homepage_Event_Bookingtime_New", bundle);

        GlobalCalls.events("Homepage_Event_Bookingtime_New",MainActivity.this);

        Log.d("TIMESLOTS:", sendStartTime + "::::" + sendEndTime);

    }

    private void setupBottomBar() {
        BottomNavigationBar bottomNavigationBar = findViewById(R.id.bottom_bar);

        BottomBarItem home = new BottomBarItem(R.drawable.ic_house_black_silhouette_without_door, R.string.home);
        BottomBarItem history = new BottomBarItem(R.drawable.ic_diploma, R.string.history);
        BottomBarItem settings = new BottomBarItem(R.drawable.ic_repairing_service, R.string.setting);

        bottomNavigationBar
                .addTab(home)
                .addTab(history)
                .addTab(settings);

        bottomNavigationBar.setOnSelectListener(new BottomNavigationBar.OnSelectListener() {
            @Override
            public void onSelect(int position) {
                showContent(position);
            }
        });
    }

    void showContent(int position) {

        if (position == 0) {

        }
        if (position == 1) {
            Intent intent = new Intent(MainActivity.this, HistoryActivity.class);
            startActivity(intent);
        }
        if (position == 2) {
            Intent intent = new Intent(MainActivity.this, SettingActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        if (exit) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            moveTaskToBack(true);

        } else {

            GlobalCalls.showToast("Press Back again to Exit.!", MainActivity.this);

            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 2 * 1000);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //checkPermissionOnActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_PICKER_REQUEST && resultCode == RESULT_OK) {
            Place place = PlacePicker.getPlace(this, data);
            String placeName = String.format("" + place.getName());
            latitude = place.getLatLng().latitude;
            longitude = place.getLatLng().longitude;
            getAddress(latitude, longitude);
        }

    }

    public void getAddress(double lat, double lng) {
        Geocoder geocoder = new Geocoder(MainActivity.this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            Address obj = addresses.get(0);
            String add = obj.getAddressLine(0);
            add = add + "\n" + obj.getCountryName();
            add = add + "\n" + obj.getCountryCode();
            add = add + "\n" + obj.getAdminArea();
            add = add + "\n" + obj.getPostalCode();
            add = add + "\n" + obj.getSubAdminArea();
            add = add + "\n" + obj.getLocality();
            add = add + "\n" + obj.getSubThoroughfare();

            Log.d("Address", obj.toString());
            if (obj.getLocality() != null) {
                //location_txt.setText(obj.getSubAdminArea() + ", " + obj.getAdminArea());
                location_txt.setText(obj.getAddressLine(0));
            } else if (obj.getSubAdminArea() != null) {
                /*if (obj.getSubAdminArea().contains("Greater London")) {
                    location_txt.setText("London" + ", " + obj.getCountryName());
                } else {
                    location_txt.setText(obj.getSubAdminArea() + ", " + obj.getCountryName());
                }*/
            } else {
                location_txt.setText(obj.getCountryName());
            }

            locationEditor.putString("latitude", String.valueOf(lat));
            locationEditor.putString("longitude", String.valueOf(lng));
            locationEditor.putString("locationName", location_txt.getText().toString());
            locationEditor.apply();

            /*Intent intent = new Intent(MainActivity.this, NewMainActivity.class);
            intent.putExtra("latitude", String.valueOf(lat));
            intent.putExtra("longitude", String.valueOf(lng));
            intent.putExtra("locationname", location_txt.getText().toString());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();*/


        } catch (IOException e) {

            e.printStackTrace();

            GlobalCalls.showToast(e.getMessage(), MainActivity.this);

        }
    }

}
