package in.g360.owner.dbhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class ImageDbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "gaadi360.db";
    private static final String TABLE_BIKE_IMAGES = "bikeimages";
    private static final String TABLE_BIKE_VIDEOS = "bikevideos";

    /*BikeImages*/
    public static final String BIKE_IMAGE_ID = "bike_id";
    public static final String BIKE_IMAGE = "bike_img";

    /*BikeVideos*/
    public static final String BIKE_VIDEO_ID = "bike_id";
    public static final String BIKE_VIDEO = "bike_video";

    public ImageDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public ImageDbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_BIKE_IMAGES_TABLE = "CREATE TABLE " + TABLE_BIKE_IMAGES + "("
                + BIKE_IMAGE_ID + " TEXT ,"
                + BIKE_IMAGE + " TEXT )";

        String CREATE_BIKE_VIDEO_TABLE = "CREATE TABLE " + TABLE_BIKE_VIDEOS + "("
                + BIKE_VIDEO_ID + " TEXT ,"
                + BIKE_VIDEO + " TEXT )";

        db.execSQL(CREATE_BIKE_IMAGES_TABLE);
        db.execSQL(CREATE_BIKE_VIDEO_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BIKE_IMAGES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BIKE_VIDEOS);
        onCreate(db);

    }

    /*bikeImages*/
    public void addBikeImages(ContentValues contentValues) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_BIKE_IMAGES, null, contentValues);
        db.close();
    }

    /*bikeVideos*/
    public void addBikeVideos(ContentValues contentValues) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_BIKE_VIDEOS, null, contentValues);
        db.close();

    }

    /*bikeImages*/
    public List<String> getBikeImageId() {

        List<String> bikeImageId = new ArrayList<String>();
        String selectQuery = "SELECT * FROM " + TABLE_BIKE_IMAGES;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(selectQuery, null);
        } finally {
            if (cursor.moveToFirst()) {
                do {
                    bikeImageId.add(cursor.getString(0));

                } while (cursor.moveToNext());
            }
        }

        return bikeImageId;

    }

    public List<String> getBikeImage() {

        List<String> bikeImage = new ArrayList<String>();
        String selectQuery = "SELECT * FROM " + TABLE_BIKE_IMAGES;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(selectQuery, null);
        } finally {
            if (cursor.moveToFirst()) {
                do {
                    bikeImage.add(cursor.getString(1));

                } while (cursor.moveToNext());
            }
            return bikeImage;
        }

    }

    /*bikeVideos*/

    public List<String> getBikeVideoId() {

        List<String> bikeVideoId = new ArrayList<String>();
        String selectQuery = "SELECT * FROM " + TABLE_BIKE_VIDEOS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(selectQuery, null);
        } finally {
            if (cursor.moveToFirst()) {
                do {
                    bikeVideoId.add(cursor.getString(0));

                } while (cursor.moveToNext());
            }
        }

        return bikeVideoId;

    }

    public List<String> getBikeVideo() {

        List<String> bikeVideo = new ArrayList<String>();
        String selectQuery = "SELECT * FROM " + TABLE_BIKE_VIDEOS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(selectQuery, null);
        } finally {
            if (cursor.moveToFirst()) {
                do {
                    bikeVideo.add(cursor.getString(1));

                } while (cursor.moveToNext());
            }
            return bikeVideo;
        }

    }

    public void deleteBikeImages() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_BIKE_IMAGES);
        Log.d("CHECK", "DELETEBIKEIMAGES");
        db.close();
    }

    public void deleteBikeVideos() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_BIKE_VIDEOS);
        Log.d("CHECK", "DELETEBIKEVIDEOS");//delete all rows in a table
        db.close();
    }
}
