package in.g360.owner.interfaces;

import android.view.View;

public interface ServiceTypeItemClickListener
{
   void onItemClick(View v, int pos);
}
