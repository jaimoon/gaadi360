package in.g360.owner.interfaces;

import android.view.View;

public interface BikeModelItemClickListener
{
   void onItemClick(View v, int pos);
}
