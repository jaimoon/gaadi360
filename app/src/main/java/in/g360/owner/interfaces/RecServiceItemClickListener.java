package in.g360.owner.interfaces;

import android.view.View;

public interface RecServiceItemClickListener
{
    void onItemClick(View v, int pos);
}

