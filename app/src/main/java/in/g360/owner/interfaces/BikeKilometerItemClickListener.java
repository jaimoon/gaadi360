package in.g360.owner.interfaces;

import android.view.View;

public interface BikeKilometerItemClickListener
{
   void onItemClick(View v, int pos);
}
