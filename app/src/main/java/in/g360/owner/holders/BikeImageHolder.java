package in.g360.owner.holders;

import android.view.View;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import in.g360.owner.R;
import in.g360.owner.interfaces.BikeImageItemClickListener;

public class BikeImageHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView bike_img;

    BikeImageItemClickListener bikeImageItemClickListener;

    public BikeImageHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);

        bike_img = itemView.findViewById(R.id.bike_img);
    }

    @Override
    public void onClick(View view) {

        this.bikeImageItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(BikeImageItemClickListener ic) {
        this.bikeImageItemClickListener = ic;
    }
}
