package in.g360.owner.holders;

import android.view.View;
import android.widget.VideoView;

import androidx.recyclerview.widget.RecyclerView;

import in.g360.owner.R;
import in.g360.owner.interfaces.BikeVideoItemClickListener;

public class BikeVideoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public VideoView bike_video;

    BikeVideoItemClickListener bikeVideoItemClickListener;

    public BikeVideoHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);

        bike_video = itemView.findViewById(R.id.bike_video);
    }

    @Override
    public void onClick(View view) {

        this.bikeVideoItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(BikeVideoItemClickListener ic) {
        this.bikeVideoItemClickListener = ic;
    }
}
