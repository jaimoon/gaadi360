package in.g360.owner.holders;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import in.g360.owner.R;
import in.g360.owner.interfaces.InfoItemClickListener;

public class InfoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView info_txt;

    private InfoItemClickListener infoItemClickListener;

    public InfoHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        info_txt = itemView.findViewById(R.id.info_txt);

    }

    @Override
    public void onClick(View view) {

        this.infoItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(InfoItemClickListener ic) {
        this.infoItemClickListener = ic;
    }
}
