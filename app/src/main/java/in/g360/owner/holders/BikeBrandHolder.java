package in.g360.owner.holders;

import android.view.View;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import in.g360.owner.R;
import in.g360.owner.interfaces.BikeBrandItemClickListener;

public class BikeBrandHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView bike_brand_name_txt;

    private BikeBrandItemClickListener bikeBrandItemClickListener;

    public BikeBrandHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        bike_brand_name_txt = itemView.findViewById(R.id.bike_brand_name_txt);

    }

    @Override
    public void onClick(View view) {

        this.bikeBrandItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(BikeBrandItemClickListener ic) {
        this.bikeBrandItemClickListener = ic;
    }
}
