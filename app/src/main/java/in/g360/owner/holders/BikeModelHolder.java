package in.g360.owner.holders;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import in.g360.owner.R;
import in.g360.owner.interfaces.BikeModelItemClickListener;

public class BikeModelHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView bike_model_name_txt;
    BikeModelItemClickListener bikeModelItemClickListener;

    public BikeModelHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        bike_model_name_txt = itemView.findViewById(R.id.bike_model_name_txt);

    }

    @Override
    public void onClick(View view) {

        this.bikeModelItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(BikeModelItemClickListener ic)
    {
        this.bikeModelItemClickListener = ic;
    }
}
