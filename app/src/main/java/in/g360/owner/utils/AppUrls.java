package in.g360.owner.utils;

public class AppUrls {

    public static String BASE_URL = "https://admin.gaadi360.com/gaadi2/api/";
    //public static String BASE_URL = "http://test.gaadi360.com/gaadi/api/";
    //public static String BASE_URL = "http://192.168.32.97:8080/gaadi/api/";
    //public static String BASE_URL = "http://192.168.1.5:8080/gaadi/api/";

    public static String IMAGE_URL = BASE_URL + "media/file/display/";
    public static String CHECK_VERSION = "build/info/1/1";
    public static String PROMOTION_SLIDERS = "promotion/images";
    public static String SERVICETYPE = "service/type";
    public static String LOGIN = "V2/login/generate/otp?mobile=";
    public static String VERIFYOTP = "V2/login/verify/otp?mobile=";
    public static String ADDADDRESS = "api/user/";
    public static String FCM = "secure/user/device";
    public static String CURRENT_STATUS = "secure/consumer/ongoing/booking-status";
    public static String PROFILE = "secure/profile/get";
    public static String EDIT_PROFILE = "secure/profile/update";
    public static String EDIT_PROFILE_PIC = "media/file/upload";
    public static String ADDRESSES = "secure/consumer/address";
    public static String NOTIFICATIONS = "secure/notifications";
    public static String CLEAR_NOTIFICATIONS = "secure/notification/all/clear";
    public static String READ_UNREAD = "secure/notification/";
    public static String HISTORY = "secure/V2/consumer/bookings";
    public static String HISTORY_DETAIL = "secure/V2/booking/";
    public static String REBOOK = "secure/consumer/rebook";
    public static String BIKE_BRANDS = "V2/brands";
    public static String BIKE_MODELS = "models?brandId=";
    public static String BIKE_KILOMETERS = "kilometer-ranges";
    public static String TIME_SLOTS = "pickupSchedules";
    public static String GENERAL_SERVICE = "V2/quick-services?modelId=";
    public static String REC_SERVICE = "V2/suggested-services";
    public static String INSPECTIONS = "V2/inspections";
    public static String SERVICECENTERS = "service-centers";
    public static String VERIFY_START_COUPON = "secure/v2/promocode/";
    public static String VERIFY_END_COUPON = "/validate?serviceCenterId=";
    public static String TRACKING = "secure/booking/";
    public static String PAY = "secure/paytm/payload-and-checksum";
    public static String PAY_SAVE = "secure/book/save";
    public static String FINAL_PAY_SAVE = "secure/booking/";
    public static String RATING = "secure/rating";
    public static String CONCERN = "secure/booking/concern";
    public static String HELP_CENTER = "secure/help-center/get";
    public static String CANCEL_BOOKING = "secure/booking/";
    public static String SEND_ADDON_SERVICES = "secure/addon-service/submit";
    public static String RESCHEDULE = "secure/booking/reschedule";
    public static String CALL_OTP = "V2/login/resend/otp?mobile=";
    public static String CHECK_IFC = "booking/";
    public static String SEND_IFC = "secure/booking/feedback";
    public static String SOURCE_TYPE = "sources";
    public static String VALIDATE_REFERAL = "login/validate/referralCode?mobile=";
    public static String WALLET_HISTORY = "secure/wallet/history";
    public static String REFERAL_AMOUNT = "setting/get";
    public static String REFERAL_AMOUNT_DETAIL = "secure/setting/get";

}

