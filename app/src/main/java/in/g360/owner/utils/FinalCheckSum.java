package in.g360.owner.utils;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.g360.owner.MainActivity;
import in.g360.owner.activities.FinalPaymentActivity;
import in.g360.owner.activities.SuccessActivity;

public class FinalCheckSum extends AppCompatActivity implements PaytmPaymentTransactionCallback {

    private FirebaseAnalytics mFirebaseAnalytics;
    String CALLBACK_URL, CHANNEL_ID, CUST_ID, EMAIL, INDUSTRY_TYPE_ID, MID, MOBILE_NO, ORDER_ID, TXN_AMOUNT, WEBSITE, bookingId,
            checksumValue;
    String accessToken, status, orderId, userEmail;
    UserSessionManager userSessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);
        userEmail = userDetails.get(UserSessionManager.USER_EMAIL);

        CALLBACK_URL = getIntent().getStringExtra("CALLBACK_URL");
        CHANNEL_ID = getIntent().getStringExtra("CHANNEL_ID");
        CUST_ID = getIntent().getStringExtra("CUST_ID");
        EMAIL = getIntent().getStringExtra("EMAIL");
        INDUSTRY_TYPE_ID = getIntent().getStringExtra("INDUSTRY_TYPE_ID");
        MID = getIntent().getStringExtra("MID");
        MOBILE_NO = getIntent().getStringExtra("MOBILE_NO");
        ORDER_ID = getIntent().getStringExtra("ORDER_ID");
        TXN_AMOUNT = getIntent().getStringExtra("TXN_AMOUNT");
        WEBSITE = getIntent().getStringExtra("WEBSITE");
        checksumValue = getIntent().getStringExtra("checksumValue");
        bookingId = getIntent().getStringExtra("bookingId");

        sendUserDetailTOServerdd dl = new sendUserDetailTOServerdd();
        dl.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        //Logs an app event.
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ID-PaytmFinalActivity");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "PaytmFinalActivity");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
        mFirebaseAnalytics.logEvent("PaytmFinalpage_PageLoad_New", bundle);

        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(20000);
        mFirebaseAnalytics.setSessionTimeoutDuration(500);
        mFirebaseAnalytics.setUserId(MOBILE_NO);
        mFirebaseAnalytics.setUserProperty("PaytmFinalActivity", MOBILE_NO);

        GlobalCalls.events("PaytmFinalpage_PageLoad_New",FinalCheckSum.this);


    }

    public class sendUserDetailTOServerdd extends AsyncTask<ArrayList<String>, Void, String> {

        private ProgressDialog dialog = new ProgressDialog(FinalCheckSum.this);

        @Override
        protected void onPreExecute() {
            this.dialog.setMessage("Please wait");
            this.dialog.show();
        }

        protected String doInBackground(ArrayList<String>... alldata) {

            return checksumValue;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.e(" setup acc ", "  signup result  " + result);
            if (dialog.isShowing()) {
                dialog.dismiss();
            }

            //PaytmPGService Service = PaytmPGService.getStagingService();  //  Stagging
            PaytmPGService  Service = PaytmPGService.getProductionService(); // Production

            HashMap<String, String> paramMap = new HashMap<String, String>();
            paramMap.put("MID", MID);
            paramMap.put("ORDER_ID", ORDER_ID);
            paramMap.put("CUST_ID", CUST_ID);
            paramMap.put("CHANNEL_ID", CHANNEL_ID);
            paramMap.put("TXN_AMOUNT", TXN_AMOUNT);
            paramMap.put("WEBSITE", WEBSITE);
            paramMap.put("CALLBACK_URL", CALLBACK_URL);
            paramMap.put("EMAIL", EMAIL);   // no need
            paramMap.put("MOBILE_NO", MOBILE_NO);  // no need
            paramMap.put("CHECKSUMHASH", checksumValue);
            paramMap.put("INDUSTRY_TYPE_ID", INDUSTRY_TYPE_ID);

            PaytmOrder Order = new PaytmOrder(paramMap);
            Service.initialize(Order, null);
            Service.startPaymentTransaction(FinalCheckSum.this, true, true,
                    FinalCheckSum.this);

        }
    }

    @Override
    public void onTransactionResponse(Bundle bundle) {
        Log.d("Bundle", bundle.toString());
        status = bundle.getString("STATUS");
        orderId = bundle.getString("ORDERID");
        Log.d("CHECCCCCC", status + "\n" + orderId);

        if (status.equalsIgnoreCase("TXN_SUCCESS")) {

            Bundle fbundle = new Bundle();
            fbundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ID-FinalPaySuccess");
            fbundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "FinalPaySuccess");
            fbundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
            mFirebaseAnalytics.logEvent("PaytmFinalpage_Event_FinalPaySuccess_New", fbundle);

            GlobalCalls.events("PaytmFinalpage_Event_FinalPaySuccess_New",FinalCheckSum.this);

            saveDetails();

        } else if (status.equalsIgnoreCase("TXN_FAILURE")) {

        } else if (status.equalsIgnoreCase("PENDING")) {

        }
    }

    @Override
    public void networkNotAvailable() {

        Log.d("Network", "++++");
    }

    @Override
    public void clientAuthenticationFailed(String s) {
        Log.d("Client", s);
    }

    @Override
    public void someUIErrorOccurred(String s) {
        Log.d("UIError", s);
    }

    @Override
    public void onErrorLoadingWebPage(int i, String s, String s1) {

        Log.d("WebpageError", s + "\n" + s1);
    }

    @Override
    public void onBackPressedCancelTransaction() {

        Log.d("BackCancel", "Back Button Click");

        /*Intent intent = new Intent(FinalCheckSum.this, FinalPaymentActivity.class);
        intent.putExtra("bookingId", bookingId);
        startActivity(intent);*/

        finish();
    }

    @Override
    public void onTransactionCancel(String s, Bundle bundle) {
        Log.d("TransCancel", s + "\n" + bundle.toString());

        Bundle fbundle = new Bundle();
        fbundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ID-FinalPayFailure");
        fbundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "FinalPayFailure");
        fbundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
        mFirebaseAnalytics.logEvent("PaytmFinalpage_Event_FinalPayFailure_New", fbundle);

        GlobalCalls.events("PaytmFinalpage_Event_FinalPayFailure_New",FinalCheckSum.this);

    }

    private void saveDetails() {

        String url = AppUrls.BASE_URL + AppUrls.FINAL_PAY_SAVE + bookingId + "/" + ORDER_ID + "/payment?promocodeAmount=0";

        Log.d("FinalSave", url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                Log.d("PaymentStatus", response);

                try {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");
                    String code = jsonObject.getString("code");

                    Log.d("Status", status + "\n" + message + "\n" + code);

                    if (status.equalsIgnoreCase("2000")) {

                        GlobalCalls.showToast(message, FinalCheckSum.this);

                        Intent intent = new Intent(FinalCheckSum.this, SuccessActivity.class);
                        startActivity(intent);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                    Log.d("ResError", e.getMessage());
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(FinalCheckSum.this);
        requestQueue.add(stringRequest);

    }
}
