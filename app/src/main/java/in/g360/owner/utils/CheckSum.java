package in.g360.owner.utils;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.g360.owner.MainActivity;
import in.g360.owner.activities.ConfirmationDetailActivity;

public class CheckSum extends AppCompatActivity {

    private FirebaseAnalytics mFirebaseAnalytics;
    String accessToken, respMsg, status, orderId, condStr, CALLBACK_URL, CHANNEL_ID, CUST_ID, EMAIL, INDUSTRY_TYPE_ID, MID, MOBILE_NO, ORDER_ID,
            TXN_AMOUNT, WEBSITE, checksumValue, couponCode;
    UserSessionManager userSessionManager;
    SharedPreferences preferences, sstPreferences, servicePreferences, inspectionPreferences;
    SharedPreferences.Editor editor, sstEditor, serviceEditor, inspectionEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();
        sstPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sstEditor = sstPreferences.edit();
        servicePreferences = PreferenceManager.getDefaultSharedPreferences(this);
        serviceEditor = servicePreferences.edit();
        inspectionPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        inspectionEditor = inspectionPreferences.edit();

        condStr = getIntent().getStringExtra("condStr");
        CALLBACK_URL = getIntent().getStringExtra("CALLBACK_URL");
        CHANNEL_ID = getIntent().getStringExtra("CHANNEL_ID");
        CUST_ID = getIntent().getStringExtra("CUST_ID");
        EMAIL = getIntent().getStringExtra("EMAIL");
        INDUSTRY_TYPE_ID = getIntent().getStringExtra("INDUSTRY_TYPE_ID");
        MID = getIntent().getStringExtra("MID");
        MOBILE_NO = getIntent().getStringExtra("MOBILE_NO");
        ORDER_ID = getIntent().getStringExtra("ORDER_ID");
        TXN_AMOUNT = getIntent().getStringExtra("TXN_AMOUNT");
        WEBSITE = getIntent().getStringExtra("WEBSITE");
        checksumValue = getIntent().getStringExtra("checksumValue");
        couponCode = getIntent().getStringExtra("couponCode");

        sendUserDetailTOServerdd dl = new sendUserDetailTOServerdd();
        dl.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        //Logs an app event.
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ID-PaytmInitActivity");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "PaytmInitActivity");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
        mFirebaseAnalytics.logEvent("PaytmInitpage_PageLoad_New", bundle);

        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(20000);
        mFirebaseAnalytics.setSessionTimeoutDuration(500);
        mFirebaseAnalytics.setUserId(MOBILE_NO);
        mFirebaseAnalytics.setUserProperty("PaytmInitActivity", MOBILE_NO);

        GlobalCalls.events("PaytmInitpage_PageLoad_New",CheckSum.this);

    }

    public class sendUserDetailTOServerdd extends AsyncTask<ArrayList<String>, Void, String> {

        private ProgressDialog dialog = new ProgressDialog(CheckSum.this);

        @Override
        protected void onPreExecute() {
            this.dialog.setMessage("Please wait");
            this.dialog.show();
        }

        protected String doInBackground(ArrayList<String>... alldata) {

            return checksumValue;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.e(" setup acc ", "  signup result  " + result);
            if (dialog.isShowing()) {
                dialog.dismiss();
            }


            //PaytmPGService Service = PaytmPGService.getStagingService();  //  Stagging
            PaytmPGService Service = PaytmPGService.getProductionService(); // Production

            HashMap<String, String> paramMap = new HashMap<String, String>();
            paramMap.put("MID", MID);
            paramMap.put("ORDER_ID", ORDER_ID);
            paramMap.put("CUST_ID", CUST_ID);
            paramMap.put("CHANNEL_ID", CHANNEL_ID);
            paramMap.put("TXN_AMOUNT", TXN_AMOUNT);
            paramMap.put("WEBSITE", WEBSITE);
            paramMap.put("CALLBACK_URL", CALLBACK_URL);
            paramMap.put("EMAIL", EMAIL);   // no need
            paramMap.put("MOBILE_NO", MOBILE_NO);  // no need
            paramMap.put("CHECKSUMHASH", checksumValue);
            paramMap.put("INDUSTRY_TYPE_ID", INDUSTRY_TYPE_ID);

            PaytmOrder Order = new PaytmOrder(paramMap);
            Service.initialize(Order, null);

            Service.startPaymentTransaction(CheckSum.this, true, true,
                    new PaytmPaymentTransactionCallback() {
                        /*Call Backs*/
                        public void someUIErrorOccurred(String inErrorMessage) {
                            Log.d("Network", "1");
                        }

                        public void onTransactionResponse(Bundle inResponse) {
                            Log.d("Network", "2");

                            respMsg = inResponse.getString("RESPMSG");
                            status = inResponse.getString("STATUS");
                            orderId = inResponse.getString("ORDERID");
                            Log.d("CHECCCCCC", status + "\n" + orderId);

                            if (status.equalsIgnoreCase("TXN_SUCCESS")) {

                                Bundle bundle = new Bundle();
                                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ID-InitPaySuccess");
                                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "InitPaySuccess");
                                bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
                                mFirebaseAnalytics.logEvent("PaytmInitpage_Event_InitPaySuccess_New", bundle);

                                GlobalCalls.events("PaytmInitpage_Event_InitPaySuccess_New",CheckSum.this);

                                saveDetails();

                            } else if (status.equalsIgnoreCase("TXN_FAILURE")) {

                            } else if (status.equalsIgnoreCase("PENDING")) {

                            }

                        }

                        public void networkNotAvailable() {
                            Log.d("Network", "3");
                        }

                        public void clientAuthenticationFailed(String inErrorMessage) {
                            Log.d("Network", "4");
                        }

                        public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl) {
                            Log.d("Network", "5");
                        }

                        public void onBackPressedCancelTransaction() {

                            Log.d("Network", "6");

                            Intent intent = new Intent(CheckSum.this, ConfirmationDetailActivity.class);
                            intent.putExtra("activity", "CheckSum");
                            intent.putExtra("couponCode", couponCode);
                            intent.putExtra("condStr", condStr);
                            startActivity(intent);
                            finish();

                        }

                        public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
                            Log.d("Network", "7");

                            Bundle bundle = new Bundle();
                            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "ID-InitPayFailure");
                            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "InitPayFailure");
                            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
                            mFirebaseAnalytics.logEvent("PaytmInitpage_Event_InitPayFailure_New", bundle);

                            GlobalCalls.events("PaytmInitpage_Event_InitPayFailure_New",CheckSum.this);

                        }
                    });
        }
    }

    private void saveDetails() {

        String url = AppUrls.BASE_URL + AppUrls.PAY_SAVE;

        JSONObject jsonObj = new JSONObject();

        try {
            jsonObj.put("orderId", ORDER_ID);

        } catch (Exception e) {

        }

        Log.d("JsonObj", jsonObj.toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("PaymentStatus", response.toString());

                try {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");
                    String code = jsonObject.getString("code");

                    Log.d("Status", status + "\n" + message + "\n" + code);

                    if (status.equalsIgnoreCase("2000")) {

                        editor.clear();
                        editor.commit();
                        sstEditor.clear();
                        sstEditor.commit();
                        serviceEditor.clear();
                        serviceEditor.commit();
                        inspectionEditor.clear();
                        inspectionEditor.commit();

                        GlobalCalls.showToast(message, CheckSum.this);
                        Intent intent = new Intent(CheckSum.this, MainActivity.class);
                        intent.putExtra("activity", "CheckSum");
                        startActivity(intent);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                    Log.d("ResError", e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(CheckSum.this);
        requestQueue.add(request);
    }
}
