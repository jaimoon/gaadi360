package in.g360.owner.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import in.g360.owner.MainActivity;
import in.g360.owner.R;
import in.g360.owner.activities.SplashScreenActivity;

public class MyFirebaseInstanceIDService extends FirebaseMessagingService {

    PendingIntent pendingIntent;
    NotificationCompat.Builder builder;
    int NOTIFICATION_ID = 2020;
    NotificationManager notificationManager;
    long pattern[] = {100, 200, 300, 400, 500, 400, 300, 200, 400};

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.d("NEW_TOKEN", s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        if (remoteMessage.getData().size()>0){

            String title,message,img_url;

            title = remoteMessage.getData().get("title");
            message = remoteMessage.getData().get("message");
            img_url = remoteMessage.getData().get("img_url");

            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_ONE_SHOT);
            Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
            builder.setContentTitle(title);
            builder.setContentText(message);
            builder.setContentIntent(pendingIntent);
            builder.setSound(soundUri);
            builder.setSmallIcon(R.drawable.app_icon);

            ImageRequest imageRequest = new ImageRequest(img_url, new Response.Listener<Bitmap>() {
                @Override
                public void onResponse(Bitmap response) {

                    builder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(response));
                    NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    notificationManager.notify(0,builder.build());

                }
            }, 0, 0, null, Bitmap.Config.RGB_565,  new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });

            MySingleton.getmInstance(this).addToRequestQueue(imageRequest);
        }

        /*String message = remoteMessage.getData().get("message");
        String imageUri = remoteMessage.getData().get("image");
        Log.d("NotCheck", "Remote"+"\n"+message + "\n" + imageUri);

        Log.d("Notification", remoteMessage.getNotification().getTitle() + "\n" + remoteMessage.getNotification().getBody());

        notificationManager = (NotificationManager) MyFirebaseInstanceIDService.this.getSystemService(Context.NOTIFICATION_SERVICE);


        Intent intent = new Intent(MyFirebaseInstanceIDService.this, SplashScreenActivity.class);
        pendingIntent = PendingIntent.getActivity(MyFirebaseInstanceIDService.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Bitmap icon = BitmapFactory.decodeResource(MyFirebaseInstanceIDService.this.getResources(), R.drawable.app_icon);

        builder = new NotificationCompat.Builder(MyFirebaseInstanceIDService.this, "default")
                .setSmallIcon(R.drawable.bikeservice)
                .setContentTitle(remoteMessage.getNotification().getTitle())
                .setContentText(remoteMessage.getNotification().getBody())
                //.setColor(getResources().getColor(R.color.colorPrimary))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setLargeIcon(icon)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE)
                .setContentIntent(pendingIntent);

        if (remoteMessage.getNotification() != null) {

            Log.d("Notification", remoteMessage.getNotification().getBody());

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                NotificationChannel channel = new NotificationChannel("in.g360.owner", "NOTIFICATION_CHANNEL_NAME", NotificationManager.IMPORTANCE_HIGH);

                channel.enableLights(true);
                channel.setLightColor(Color.RED);
                channel.enableVibration(true);
                channel.setVibrationPattern(pattern);

                builder.setChannelId("in.g360.owner");

                if (notificationManager != null) {
                    notificationManager.createNotificationChannel(channel);
                }
            }

            assert notificationManager != null;
            notificationManager.notify(NOTIFICATION_ID, builder.build());

        }*/

    }
}
